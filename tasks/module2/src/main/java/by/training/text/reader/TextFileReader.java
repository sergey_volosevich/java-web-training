package by.training.text.reader;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class TextFileReader {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CAN_NOT_READ_FILE = "Can not read file";

    public String readFile(String pathFile) {
        StringBuilder builder = new StringBuilder();
        List<String> list = new ArrayList<>();
        try {
            list = Files.readAllLines(Paths.get(pathFile), StandardCharsets.UTF_8);
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, CAN_NOT_READ_FILE, e);
        }
        for (String s : list) {
            builder.append(s).append("\n");
        }
        return builder.toString();
    }
}
