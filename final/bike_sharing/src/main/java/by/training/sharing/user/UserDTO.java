package by.training.sharing.user;

import by.training.sharing.validation.Validation;
import by.training.sharing.validation.ValidationPattern;

import java.io.Serializable;
import java.util.Objects;

import static by.training.sharing.user.UserFieldNameConstants.*;

@Validation
public class UserDTO implements Serializable {

    private static final long serialVersionUID = -6211029249865093377L;

    private Long id;

    @ValidationPattern(regexp = "^[a-zA-Z0-9-_\\.]{5,30}$", fieldName = USER_LOGIN,
            message = "user.login.validation.msg")
    private String userName;

    @ValidationPattern(regexp = "(?=^.{8,128}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z]*$",
            fieldName = USER_PASSWORD, message = "user.password.validation.msg")
    private String password;

    @ValidationPattern(regexp = "^[а-яА-ЯёЁa-zA-Z-?]{1,45}$", fieldName = USER_NAME, message = "user.name.validation.msg")
    private String name;

    @ValidationPattern(regexp = "^[а-яА-ЯёЁa-zA-Z-?]{1,45}$", fieldName = USER_LAST_NAME,
            message = "user.lastName.validation.msg")
    private String lastName;

    @ValidationPattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|" +
            "(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",
            fieldName = USER_EMAIL, message = "user.email.validation.msg")
    private String email;

    @ValidationPattern(regexp = "^[+]\\d{3}((?:29)|(?:33)|(?:44)|(?:25))\\d{7}$", fieldName = USER_PHONE,
            message = "user.phone.validation.msg")
    private String phone;

    private UserStatus status;

    public UserDTO() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public UserStatus getStatus() {
        return status;
    }

    public void setStatus(UserStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserDTO)) return false;
        UserDTO userDTO = (UserDTO) o;
        return Objects.equals(getId(), userDTO.getId()) &&
                Objects.equals(getUserName(), userDTO.getUserName()) &&
                Objects.equals(getPassword(), userDTO.getPassword()) &&
                Objects.equals(getName(), userDTO.getName()) &&
                Objects.equals(getLastName(), userDTO.getLastName()) &&
                Objects.equals(getEmail(), userDTO.getEmail()) &&
                Objects.equals(getPhone(), userDTO.getPhone()) &&
                getStatus() == userDTO.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getUserName(), getPassword(), getName(), getLastName(), getEmail(), getPhone(), getStatus());
    }

    @Override
    public String toString() {
        return "UserDTO{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                ", password='" + password + '\'' +
                ", name='" + name + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", phone='" + phone + '\'' +
                ", status=" + status +
                '}';
    }
}
