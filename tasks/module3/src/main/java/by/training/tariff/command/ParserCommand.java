package by.training.tariff.command;

import by.training.tariff.parser.Parser;
import by.training.tariff.parser.ParserException;

import java.util.List;

public abstract class ParserCommand<T> implements Command<T> {

    private Parser<T> parser;

    public ParserCommand(Parser<T> parser) {
        this.parser = parser;
    }

    @Override
    public List<T> execute(String path) throws CommandException {
        List<T> listTariffs;
        try {
            listTariffs = parser.parse(path);
        } catch (ParserException e) {
            throw new CommandException(e);
        }
        return listTariffs;
    }
}
