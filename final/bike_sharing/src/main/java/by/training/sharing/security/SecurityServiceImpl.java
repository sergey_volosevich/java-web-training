package by.training.sharing.security;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.role.RoleDAO;
import by.training.sharing.role.RoleDTO;
import by.training.sharing.user.UserDAO;
import by.training.sharing.user.UserDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Stream;

import static by.training.sharing.core.BeanNameConstants.*;

/**
 * This class contains methods for determining for determining the access level of the user and method to login
 * and logout the user from the  application
 */

@Bean(name = SECURITY_SERVICE)
public class SecurityServiceImpl implements SecurityService {

    private static final Logger LOGGER = LogManager.getLogger(SecurityServiceImpl.class);
    private static final String USER_WITH_ID_WAS_LOGOUT = "User with id {} was logout";
    private static final String LOGIN_ERROR_MSG = "User with id {} is already logged in";
    private static final String CREDENTIAL_NOT_FOUND = "Credential for session with id - {0} not found";
    private static final String USER_IS_ALREADY_IN_SYSTEM = "User is already in system";
    private static final String ALL_USERS = "ALL";
    private static final String SESSION_ATTR_ROLES = "roles";
    private static final String SESSION_ATTR_USER_NAME = "userName";

    private Map<HttpSession, UserCredential> authenticatedUsers = new ConcurrentHashMap<>();
    private UserDAO userDAO;
    private PasswordEncoder passwordEncoder;
    private RoleDAO roleDAO;

    public SecurityServiceImpl(@BeanQualifier(USER_DAO) UserDAO userDAO,
                               @BeanQualifier(PASSWORD_ENCODER) PasswordEncoder passwordEncoder,
                               @BeanQualifier(ROLE_DAO) RoleDAO roleDAO) {
        this.userDAO = userDAO;
        this.passwordEncoder = passwordEncoder;
        this.roleDAO = roleDAO;
    }

    @Override
    public boolean loginUser(HttpSession session, UserDTO userDTO) throws SecurityException {
        try {
            Optional<UserDTO> optionalUser = userDAO.findByLogin(userDTO.getUserName());
            boolean isLogin = optionalUser.filter(u ->
            {
                String token = u.getPassword();
                String password = userDTO.getPassword();
                return passwordEncoder.authenticate(password.toCharArray(), token);
            }).isPresent();
            if (isLogin) {
                UserDTO user = optionalUser.get();
                UserCredential userCredential = new UserCredential();
                userCredential.setUserId(user.getId());
                List<RoleDTO> userRoles = roleDAO.getUserRoles(user.getId());
                List<String> roles = new ArrayList<>();
                userRoles.forEach(role -> roles.add(role.getRoleName()));
                userCredential.setRoles(roles);
                if (authenticatedUsers.containsValue(userCredential)) {
                    Set<Map.Entry<HttpSession, UserCredential>> entries = authenticatedUsers.entrySet();
                    for (Map.Entry<HttpSession, UserCredential> entry : entries) {
                        if (entry.getValue().equals(userCredential)) {
                            HttpSession authSession = entry.getKey();
                            authenticatedUsers.remove(authSession);
                            authSession.invalidate();
                            LOGGER.debug(LOGIN_ERROR_MSG, user.getId());
                            throw new LoginException(USER_IS_ALREADY_IN_SYSTEM);
                        }
                    }
                }
                authenticatedUsers.put(session, userCredential);
                session.setAttribute(SESSION_ATTR_ROLES, roles);
                session.setAttribute(SESSION_ATTR_USER_NAME, user.getName());
            }
            return isLogin;
        } catch (DAOException e) {
            throw new SecurityException(e);
        }
    }

    @Override
    public void logoutUser(HttpSession session) {
        if (session != null && authenticatedUsers.containsKey(session)) {
            UserCredential remove = authenticatedUsers.remove(session);
            session.invalidate();
            LOGGER.debug(USER_WITH_ID_WAS_LOGOUT, remove.getUserId());
        }
    }

    @Override
    public UserCredential getCurrentUser(HttpSession session) throws CredentialNotFoundException {
        if (authenticatedUsers.containsKey(session)) {
            return authenticatedUsers.get(session);
        } else {
            throw new CredentialNotFoundException(MessageFormat.format(CREDENTIAL_NOT_FOUND, session.getId()));
        }
    }

    @Override
    public int canExecute(String commandName, HttpSession session) {
        if (commandName == null) {
            return HttpServletResponse.SC_OK;
        }
        SecurityContext securityContext = SecurityContext.getInstance();
        String accessRestriction = securityContext.getAccessRestriction(commandName);
        if (accessRestriction == null) {
            return HttpServletResponse.SC_BAD_REQUEST;
        }
        if (accessRestriction.equalsIgnoreCase(ALL_USERS)) {
            return HttpServletResponse.SC_OK;
        }
        UserCredential userCredential = authenticatedUsers.get(session);
        if (userCredential != null) {
            List<String> roles = userCredential.getRoles();
            String[] restrictions = accessRestriction.split(",");
            return Stream.of(restrictions).filter(restriction -> roles.stream()
                    .anyMatch(role -> role.equalsIgnoreCase(restriction)))
                    .findFirst().map(str -> HttpServletResponse.SC_OK).orElse(HttpServletResponse.SC_FORBIDDEN);
        } else {
            return HttpServletResponse.SC_UNAUTHORIZED;
        }
    }
}

