package by.training.port.model;

public enum TaskType {
    LOAD,
    UNLOAD,
    UNLOAD_AND_LOAD
}
