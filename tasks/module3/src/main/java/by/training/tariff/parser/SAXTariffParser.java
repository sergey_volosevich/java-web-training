package by.training.tariff.parser;

import by.training.tariff.entity.LightTariff;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.io.IOException;
import java.util.List;

public class SAXTariffParser implements Parser<LightTariff> {

    @Override
    public List<LightTariff> parse(String pathXML) throws ParserException {
        SAXParserFactory factory = SAXParserFactory.newInstance();
        SAXParser parser;
        try {
            factory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            parser = factory.newSAXParser();
        } catch (ParserConfigurationException | SAXException e) {
            throw new ParserException(e);
        }
        SAXTariffHandler handler = new SAXTariffHandler();
        try {
            parser.parse(pathXML, handler);
        } catch (SAXException | IOException e) {
            throw new ParserException(e);
        }
        return handler.getTariffs();
    }
}
