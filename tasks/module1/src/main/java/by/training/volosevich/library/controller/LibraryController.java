package by.training.volosevich.library.controller;

import by.training.volosevich.library.builder.BuilderFactory;
import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.EditionType;
import by.training.volosevich.library.entity.GenreType;
import by.training.volosevich.library.parser.LineParser;
import by.training.volosevich.library.reader.DataFileReader;
import by.training.volosevich.library.service.LibraryService;
import by.training.volosevich.library.validation.DataValidator;
import by.training.volosevich.library.validation.FileValidator;
import by.training.volosevich.library.validation.ValidationResult;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.List;
import java.util.Map;

public class LibraryController {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TYPE_OF_OBJECT = "type";

    private LibraryService service;

    public LibraryController(LibraryService service) {
        this.service = service;
    }

    public int sumOfPagesByGenre(GenreType genreType) {
        return service.sumOfPagesByGenre(genreType);
    }

    public List<Edition> sortIncreaseByReleaseYear() {
        return service.sortIncreaseByReleaseYear();
    }

    public List<Edition> sortDecreaseByReleaseYear() {
        return service.sortDecreaseByReleaseYear();
    }

    public List<Edition> sortByReleaseYearByNumberOfPages() {
        return service.sortByReleaseYearByNumberOfPages();
    }

    public List<Edition> findByReleaseYearRange(int lowerRange, int upperRange) {
        return service.findByReleaseYearRange(lowerRange, upperRange);
    }

    public List<Edition> findByGenreTypeRangeYear(GenreType type, int lowerRange, int upperRange) {
        return service.findByGenreTypeRangeYear(type, lowerRange, upperRange);
    }

    public int saveEntityFromFile(String pathFile) {
        int numberOfReadLines = 0;
        ValidationResult validationResult = new FileValidator().validateFile(pathFile);
        if (!validationResult.isValid()) {
            return numberOfReadLines;
        }
        List<String> listLines;
        try {
            listLines = new DataFileReader().readData(pathFile);
        } catch (IOException e) {
            LOGGER.log(Level.ERROR, "Cannot read file", e);
            return numberOfReadLines;
        }
        Map<String, String> mapFields;
        for (String line : listLines) {
            mapFields = new LineParser().parseLine(line);
            validationResult = new DataValidator().validateData(mapFields, listLines.indexOf(line));
            if (validationResult.isValid()) {
                BuilderFactory factory = new BuilderFactory(mapFields);
                Edition edition = factory.getByType(EditionType.valueOf(mapFields.get(TYPE_OF_OBJECT).toUpperCase()))
                        .build();
                service.addNewEdition(edition);
                numberOfReadLines++;
                LOGGER.log(Level.DEBUG, "Entity successfully saved");
            } else {
                LOGGER.log(Level.ERROR, () -> "Can not save entity");
            }
        }
        return numberOfReadLines;
    }

}
