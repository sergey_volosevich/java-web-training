package by.training.sharing.bike;

import java.util.Optional;
import java.util.stream.Stream;

/**
 * This enum used to indicate state of the bike
 */
public enum BikeStatus {
    UNUSED, RIDE;

    public static Optional<BikeStatus> getStatus(int ordinal) {
        return Stream.of(BikeStatus.values()).filter(bs -> bs.ordinal() == ordinal).findFirst();
    }
}
