package by.training.sharing.validation;

import java.math.BigDecimal;

import static by.training.sharing.bike.BikeFiledNameConstants.LATITUDE;
import static by.training.sharing.bike.BikeFiledNameConstants.LONGITUDE;

@Validation
public class TestEntityValidDecimal {

    @DecimalConstraint(fieldName = LATITUDE, message = "bike.latitude.max.validation.msg", value = "53.977702",
            constraint = DecimalConstraintType.LESS_THAN_OR_EQUALS)
    @DecimalConstraint(fieldName = LATITUDE, message = "bike.latitude.min.validation.msg", value = "53.822202",
            constraint = DecimalConstraintType.MORE_THAN_OR_EQUALS)
    private BigDecimal latitude;

    @DecimalConstraint(fieldName = LONGITUDE, message = "bike.longitude.max.validation.msg", value = "27.700280",
            constraint = DecimalConstraintType.LESS_THAN_OR_EQUALS)
    @DecimalConstraint(fieldName = LONGITUDE, message = "bike.longitude.min.validation.msg", value = "27.388028",
            constraint = DecimalConstraintType.MORE_THAN_OR_EQUALS)
    private BigDecimal longitude;
}
