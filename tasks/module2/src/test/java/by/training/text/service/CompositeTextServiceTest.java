package by.training.text.service;

import by.training.text.entity.Paragraph;
import by.training.text.entity.Sentence;
import by.training.text.model.TextComposite;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CompositeTextServiceTest {

    @Mock
    private SentenceService sentenceService;
    @Mock
    private ParagraphService paragraphService;

    private CompositeTextService compositeTextService;
    private List<Paragraph> paragraphs;
    private List<Sentence> sentencesFirstParagraph;
    private List<Sentence> sentencesSecondParagraph;
    private List<Sentence> sentencesThirdParagraph;


    @Before
    public void setUp() throws Exception {
        paragraphs = new ArrayList<>();
        paragraphs.add(new Paragraph(1, 1));
        paragraphs.add(new Paragraph(2, 1));
        paragraphs.add(new Paragraph(3, 1));

        sentencesFirstParagraph = new ArrayList<>();
        sentencesFirstParagraph.add(new Sentence(1, 1));
        sentencesFirstParagraph.add(new Sentence(2, 1));
        sentencesFirstParagraph.add(new Sentence(3, 1));
        sentencesFirstParagraph.add(new Sentence(4, 1));

        sentencesSecondParagraph = new ArrayList<>();
        sentencesSecondParagraph.add(new Sentence(5, 2));
        sentencesSecondParagraph.add(new Sentence(6, 2));
        sentencesSecondParagraph.add(new Sentence(7, 2));

        sentencesThirdParagraph = new ArrayList<>();
        sentencesThirdParagraph.add(new Sentence(8, 3));
        sentencesThirdParagraph.add(new Sentence(9, 3));
        compositeTextService = new CompositeTextService(paragraphService, sentenceService);
    }

    @Test
    public void sortParagraphsIncreasingOrder() {
        when(paragraphService.findParagraphByTextId(1L)).thenReturn(paragraphs);
        for (int i = 0; i < paragraphs.size(); i++) {
            if (i == 0) {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesFirstParagraph);
            } else if (i == 1) {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesSecondParagraph);
            } else {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesThirdParagraph);
            }
        }
        List<TextComposite> textComposites = compositeTextService.sortParagraphsIncreasingOrder(1L);
        assertEquals(2, textComposites.get(0).getComponents().size());
    }

    @Test
    public void sortParagraphsDecreasingOrder() {
        when(paragraphService.findParagraphByTextId(1L)).thenReturn(paragraphs);
        for (int i = 0; i < paragraphs.size(); i++) {
            if (i == 0) {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesFirstParagraph);
            } else if (i == 1) {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesSecondParagraph);
            } else {
                when(sentenceService.findSentenceByParagraphId(paragraphs.get(i).getId()))
                        .thenReturn(sentencesThirdParagraph);
            }
        }
        List<TextComposite> textComposites = compositeTextService.sortParagraphsDecreasingOrder(1L);
        assertEquals(4, textComposites.get(0).getComponents().size());
    }
}