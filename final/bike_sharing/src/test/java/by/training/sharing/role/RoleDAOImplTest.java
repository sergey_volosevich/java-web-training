package by.training.sharing.role;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class RoleDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(RoleDAOImplTest.class);

    private RoleDAO roleDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace)) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
        LOGGER.info("Schema is successfully created.");
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        TransactionManagerImpl transactionManager = new TransactionManagerImpl();
        ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        roleDAO = new RoleDAOImpl(jdbcTemplate);
    }

    @Test
    public void assignDefaultRoles() throws DAOException, SQLException {
        JDBCTemplateImpl jdbcTemplate = Mockito.mock(JDBCTemplateImpl.class);
        RoleDAO roleDAO = new RoleDAOImpl(jdbcTemplate);
        List<Long> roleId = new ArrayList<>();
        roleId.add(1L);
        Mockito.when(jdbcTemplate.query(Mockito.anyString(), Mockito.any(RowMapper.class))).thenReturn(roleId);
        roleDAO.assignDefaultRoles(3L);
        Mockito.verify(jdbcTemplate, Mockito.times(1)).query(Mockito.anyString(), Mockito.any(RowMapper.class));
        Mockito.verify(jdbcTemplate, Mockito.times(1)).executeUpdate("insert into" +
                " role_relation (user_id, role_id) values (?,?);", new Object[]{3L, roleId.get(0)});
    }

    @Test
    public void assignRole() throws DAOException, SQLException {
        JDBCTemplateImpl jdbcTemplate = Mockito.mock(JDBCTemplateImpl.class);
        RoleDAOImpl roleDAO = new RoleDAOImpl(jdbcTemplate);
        roleDAO.assignRole(1, 2);
        Mockito.verify(jdbcTemplate, Mockito.times(1)).executeUpdate("insert into" +
                " role_relation (user_id, role_id) values (?,?);", new Object[]{2L, 1L});
    }

    @Test
    public void getUserRolesByExistId() throws DAOException {
        List<RoleDTO> userRoles = roleDAO.getUserRoles(3);
        assertEquals(1, userRoles.size());
        assertEquals("USER", userRoles.get(0).getRoleName());
    }

    @Test(expected = RoleNotFoundException.class)
    public void getUserRolesByNonExistId() throws DAOException {
        List<RoleDTO> userRoles = roleDAO.getUserRoles(20);
    }

    @Test
    public void save() {
    }

    @Test
    public void update() {
    }

    @Test
    public void delete() {
    }

    @Test
    public void getById() {
    }

    @Test
    public void findAll() {
    }
}