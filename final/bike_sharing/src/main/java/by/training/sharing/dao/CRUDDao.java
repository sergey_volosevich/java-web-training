package by.training.sharing.dao;

import java.util.List;

public interface CRUDDao<T> {

    long save(T t) throws DAOException;

    boolean update(T t);

    boolean delete(long id);

    T getById(long id) throws DAOException;

    List<T> findAll() throws DAOException;
}
