package by.training.sharing.rental;


import by.training.sharing.bike.BikeService;
import by.training.sharing.core.*;
import by.training.sharing.security.CredentialNotFoundException;
import by.training.sharing.security.SecurityService;
import by.training.sharing.security.UserCredential;
import by.training.sharing.service.ServiceException;
import by.training.sharing.util.RequestUtils;
import by.training.sharing.wallet.WalletDTO;
import by.training.sharing.wallet.WalletService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.Optional;

import static by.training.sharing.ApplicationConstants.*;
import static by.training.sharing.bike.BikeFiledNameConstants.BIKE_ID;
import static by.training.sharing.core.BeanNameConstants.*;

@Command
@Bean(name = RENTAL_CONTROLLER)
public class RentalController {

    private static final Logger LOGGER = LogManager.getLogger(RentalController.class);

    private static final String FAILED_TO_FIND_BIKE = "Failed to find bike by id. Cause:";
    private static final String FAILED_TO_FIND_USER = "Failed to find user. Cause:";
    private static final String VALUE_OF_BIKE_ID = "Value of bike id - {}";
    private static final String WALLET_REQ_ATTRIBUTE = "wallet";

    private BikeService bikeService;
    private WalletService walletService;

    public RentalController(@BeanQualifier(BIKE_SERVICE) BikeService bikeService,
                            @BeanQualifier(WALLET_SERVICE) WalletService walletService) {
        this.bikeService = bikeService;
        this.walletService = walletService;
    }

    @CommandName(name = GET_ORDER_BIKE_VIEW)
    public void toOrderBikeView(HttpServletRequest request, HttpServletResponse response) {
        String bikeId = request.getParameter(BIKE_ID);
        HttpSession session = request.getSession();
        session.setAttribute(BIKE_ID, bikeId);
        RequestUtils.sendRedirectToCommand(request, response, GET_ORDER_BIKE);
    }


    @CommandName(name = GET_ORDER_BIKE)
    public void orderBike(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_ORDER_BIKE);
        HttpSession session = request.getSession();
        Object bikeId = session.getAttribute(BIKE_ID);
        if (bikeId == null) {
            RequestUtils.sendRedirectToCommand(request, response, GET_CHOOSE_BIKE);
            return;
        }
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        SecurityService securityService = applicationContext.getBean(SECURITY_SERVICE);
        try {
            UserCredential currentUser = securityService.getCurrentUser(session);
            Optional<WalletDTO> walletByUserId = walletService.findByUserId(currentUser.getUserId());
            walletByUserId.ifPresent(wallet -> request.setAttribute(WALLET_REQ_ATTRIBUTE, wallet));
            bikeService.getById(Long.parseLong((String) bikeId));
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (CredentialNotFoundException e) {
            LOGGER.error(FAILED_TO_FIND_USER, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_FIND_BIKE, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }
}
