package by.training.sharing.wallet;

import by.training.sharing.dao.CRUDDao;
import by.training.sharing.dao.DAOException;

import java.util.Set;

public interface CreditCardDAO extends CRUDDao<CreditCardDTO> {

    Set<CreditCardDTO> findByWalletId(long walletId) throws DAOException;
}
