package by.training.tariff.validator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.nio.file.Paths;

public class XMLValidator {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CAN_NOT_VALIDATE_FILE = "Can not validate file";
    private static final String XML_VALIDATION_ERROR = "XML validation error";
    private static final String CAN_NOT_FIND_XSD = "XSD not found";
    private static final String ERROR_CODE_XSD = "XSD";
    private static final String XSD_IS_NULL = "XSD is null";
    private static final String XSD_NAME_NOT_NULL = "XSD file must not be null";
    private static final String ERROR_PATH_CODE = "path";
    private static final String FILE_NOT_FOUND = "File not found";

    private String nameXSD;

    public XMLValidator(String nameXSD) {
        this.nameXSD = nameXSD;
    }

    public ValidationResult validate(String pathXML) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        ValidationResult validationResult = new ValidationResult();
        if (nameXSD == null) {
            validationResult.setValidationInfo(ERROR_CODE_XSD, XSD_NAME_NOT_NULL);
            LOGGER.error(XSD_IS_NULL);
            return validationResult;
        }
        String pathXSD;
        try {
            URL url = getClass().getClassLoader().getResource(nameXSD);
            if (url != null) {
                pathXSD = Paths.get(url.toURI()).toString();
            } else {
                validationResult.setValidationInfo(ERROR_CODE_XSD, CAN_NOT_FIND_XSD);
                LOGGER.error(CAN_NOT_FIND_XSD);
                return validationResult;
            }
        } catch (URISyntaxException e) {
            validationResult.setValidationInfo(ERROR_CODE_XSD, CAN_NOT_FIND_XSD);
            LOGGER.error(CAN_NOT_FIND_XSD, e);
            return validationResult;
        }
        XMLErrorHandler errorHandler = new XMLErrorHandler();
        try {
            Schema schema = schemaFactory.newSchema(new File(pathXSD));
            Validator validator = schema.newValidator();
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_DTD, "");
            validator.setProperty(XMLConstants.ACCESS_EXTERNAL_SCHEMA, "");
            validator.setErrorHandler(errorHandler);
            validator.validate(new StreamSource(new File(pathXML)));
            validationResult = errorHandler.getValidationResult();
        } catch (SAXException e) {
            validationResult = errorHandler.getValidationResult();
            validationResult.setValidationInfo(XML_VALIDATION_ERROR, CAN_NOT_VALIDATE_FILE);
            LOGGER.error(CAN_NOT_VALIDATE_FILE, e);
        } catch (IOException e) {
            validationResult.setValidationInfo(ERROR_PATH_CODE, FILE_NOT_FOUND);
            LOGGER.error(FILE_NOT_FOUND, e);
        }
        return validationResult;
    }
}
