package by.training.volosevich.library.repository;

import java.util.List;

public interface Repository<T> {

    void add(T entity);

    void remove(T entity);

    List<T> getAll();

    List<T> find(Specification<T> specification);
}
