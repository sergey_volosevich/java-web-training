package by.training.sharing.core;

import by.training.sharing.command.ServletCommand;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class BeanRegistryImplTest {

    private BeanRegistryImpl provider;
    private TestDAO testDAO;

    @Before
    public void setUp() throws Exception {
        testDAO = new TestDAO();
        provider = new BeanRegistryImpl();
        provider.registerBean(TestService.class);
        provider.registerBean(TestController.class);
        provider.registerBean(TestDAO.class);
    }

    @Test
    public void registerBeanByBeanClass() {
        TestController testController = provider.getBean("TestCommand");
        assertNotNull(testController);
        assertTrue(testController.checkMethod());
        assertSame(TestController.class, testController.getClass());
    }

    @Test
    public void getCommand() {
        ServletCommand execute = provider.getCommand("execute");
        ServletCommand noSuchCommand = provider.getCommand("NoSuchCommand");
        ServletCommand command = provider.getCommand(null);
        assertNull(command);
        assertNull(noSuchCommand);
        assertNotNull(execute);
    }

    @Test
    public void getBean() {
        TestController testController = provider.getBean("TestCommand");
        Object bean = provider.getBean(null);
        Object noSuchBean = provider.getBean("NoSuchBean");
        assertNull(bean);
        assertNull(noSuchBean);
        assertSame(TestController.class, testController.getClass());
    }

    @Test
    public void removeBean() {
        provider.removeBean(testDAO);
        TestDAO testDAO = provider.getBean("TestDAO");
        assertNull(testDAO);
    }

    @Test
    public void destroy() {
        provider.destroy();
        TestController testCommand = provider.getBean("TestCommand");
        assertNull(testCommand);
    }
}
