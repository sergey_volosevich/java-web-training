package by.training.text.controller;

import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.parser.ParserChain;
import by.training.text.reader.TextFileReader;
import by.training.text.service.CompositeTextService;
import by.training.text.validator.FileValidator;
import by.training.text.validator.ValidationResult;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

public class TextController {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CAN_NOT_SAVE_TEXT_REASON = "Can not save text. Reason: {}";

    private FileValidator validator;
    private TextFileReader reader;
    private ParserChain<TextLeaf> parserChain;
    private CompositeTextService service;

    public TextController(FileValidator validator, TextFileReader reader, ParserChain<TextLeaf> parserChain, CompositeTextService service) {
        this.validator = validator;
        this.reader = reader;
        this.parserChain = parserChain;
        this.service = service;
    }

    public void saveTextFromFile(String path) {
        ValidationResult validationResult = validator.validateFile(path);
        if (validationResult.isValid()) {
            String text = reader.readFile(path);
            TextLeaf compositeText = parserChain.parse(text);
            compositeText.save();
        } else {
            LOGGER.log(Level.ERROR, CAN_NOT_SAVE_TEXT_REASON, validationResult);
        }
    }

    public List<TextComposite> sortParagraphsIncreasingOrder(Long id) {
        return service.sortParagraphsIncreasingOrder(id);
    }

    public List<TextComposite> sortParagraphsDecreasingOrder(Long id) {
        return service.sortParagraphsDecreasingOrder(id);
    }

}

