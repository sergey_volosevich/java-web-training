<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@ tag import="by.training.sharing.ApplicationConstants" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<c:choose>
    <c:when test="${not empty sessionScope.get('userName')}">
        <p><c:out value="${sessionScope.get('userName')}"/></p>
        <a class="mdl-navigation__link"
           href="<bsh:command value="${ApplicationConstants.GET_USER_LOGOUT}"/>">
            <fmt:message key="links.logout"/> </a>
    </c:when>
    <c:otherwise>
        <a class="mdl-navigation__link"
           href="<bsh:command value="${ApplicationConstants.GET_USER_REGISTRATION}"/>">
            <fmt:message key="links.registration"/> </a>
        <a class="mdl-navigation__link"
           href="<bsh:command value="${ApplicationConstants.GET_USER_LOGIN}"/>">
            <fmt:message key="links.sing.in"/></a>
    </c:otherwise>
</c:choose>
