package by.training.port.controller;

import by.training.port.controller.LineParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class LineParserTest {
    private LineParser lineParser;
    private Map<String, String> lines;

    @Before
    public void setUp() {
        lineParser = new LineParser();
        lines = new HashMap<>();
        lines.put("validLine", "id:1, taskType:unload, maxCapacity:150, currentNumContainer:140");
        lines.put("invalidLine", "id.1, taskType!unload, maxCapacity<150, currentNumContainer'140");
        lines.put("partiallyCorrectLine", "id:1, taskType>unload, maxCapacity!150, currentNumContainer:140");
    }

    @Test
    public void parseLineNull() {
        assertTrue(lineParser.parseLine(null).isEmpty());
    }

    @Test
    public void parseLineByValidLine() {
        assertEquals(4, lineParser.parseLine(lines.get("validLine")).size());
    }

    @Test
    public void parseLineByInvalidLine() {
        assertEquals(0, lineParser.parseLine(lines.get("invalidLine")).size());
    }

    @Test
    public void parseLineByPartiallyValidLine() {
        assertEquals(2, lineParser.parseLine(lines.get("partiallyCorrectLine")).size());
    }
}