package by.training.sharing.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * This exception indicates that an exception occurred while calling a method
 * {@link by.training.sharing.command.ServletCommand#execute(HttpServletRequest, HttpServletResponse)}
 */

public class CommandException extends Exception {

    private static final long serialVersionUID = 4517725248370012108L;

    public CommandException(Throwable cause) {
        super(cause);
    }
}
