package by.training.text.validator;

import java.util.HashMap;
import java.util.Map;

public class ValidationResult {

    private Map<String, String> validationInfo = new HashMap<>();

    @Override
    public String toString() {
        return "ValidationResult{" +
                "validationInfo=" + validationInfo +
                '}';
    }

    public Map<String, String> getValidationInfo() {
        return validationInfo;
    }

    public void setValidationInfo(String code, String message) {
        validationInfo.put(code, message);
    }

    public boolean isValid() {
        return validationInfo.isEmpty();
    }
}
