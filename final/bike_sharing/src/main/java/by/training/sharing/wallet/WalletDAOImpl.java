package by.training.sharing.wallet;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.EntityNotFoundException;
import by.training.sharing.dao.JDBCTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Optional;

import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;
import static by.training.sharing.core.BeanNameConstants.WALLET_DAO;

@Bean(name = WALLET_DAO)
public class WalletDAOImpl implements WalletDAO {

    private static final Logger LOGGER = LogManager.getLogger(WalletDAOImpl.class);

    private static final String FAILED_TO_UPDATE_WALLET = "Failed to update wallet. Cause:";

    private static final String SELECT_WALLET_BY_USER_ID = "select wallet_id, current_amount, user_id" +
            " from wallet where user_id=?;";

    private static final String INSERT_QUERY_WALLET = "INSERT INTO wallet (current_amount, user_id)" +
            " VALUES (?, ?);";
    private static final String UPDATE_WALLET = "UPDATE wallet SET current_amount = ? WHERE wallet_id = ?;";

    private static final String SELECT_WALLET_BY_ID = "SELECT wallet_id, current_amount, user_id FROM wallet" +
            " where wallet_id = ?;";
    private static final String NOT_FOUND_WALLET_ENTITY = "Not found wallet entity by id - {0}";
    private static final String OPERATION_NOT_SUPPORTED = "This operation is not supported.";

    private JDBCTemplate jdbcTemplate;

    public WalletDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long save(WalletDTO walletDTO) throws DAOException {
        return 0;
    }

    @Override
    public boolean update(WalletDTO walletDTO) {// TODO: 04.01.2020 refactor method
        try {
            jdbcTemplate.executeUpdate(UPDATE_WALLET, new Object[]{walletDTO.getCurrentAmount(), walletDTO.getId()});
            return true;
        } catch (SQLException e) {
            LOGGER.error(FAILED_TO_UPDATE_WALLET, e);
            return false;
        }
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public WalletDTO getById(long id) throws DAOException {
        try {
            WalletDTO walletDTO = jdbcTemplate.queryForObject(SELECT_WALLET_BY_ID, new Object[]{id},
                    new WalletRowMapper());
            if (walletDTO != null) {
                return walletDTO;
            }
            throw new EntityNotFoundException(MessageFormat.format(NOT_FOUND_WALLET_ENTITY, id));
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<WalletDTO> findAll() throws DAOException {
        LOGGER.error(OPERATION_NOT_SUPPORTED);
        throw new UnsupportedOperationException(OPERATION_NOT_SUPPORTED);
    }

    @Override
    public Optional<WalletDTO> findByUserId(long id) throws DAOException {
        try {
            WalletDTO walletDTO = jdbcTemplate.queryForObject(SELECT_WALLET_BY_USER_ID, new Object[]{id},
                    new WalletRowMapper());
            return Optional.ofNullable(walletDTO);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public long save(long userId) throws DAOException {
        try {
            return jdbcTemplate.executeAndReturnKey(INSERT_QUERY_WALLET, new Object[]{BigDecimal.valueOf(0.00),
                    userId}).longValue();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
