package by.training.volosevich.library.repository;

import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.GenreType;

public class GenreTypeSpecification implements Specification<Edition> {

    private GenreType type;

    public GenreTypeSpecification(GenreType type) {
        this.type = type;
    }

    @Override
    public boolean match(Edition entity) {
        return entity.getGenreType().equals(type);
    }
}
