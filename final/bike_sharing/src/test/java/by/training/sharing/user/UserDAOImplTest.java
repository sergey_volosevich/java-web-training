package by.training.sharing.user;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class UserDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(UserDAOImplTest.class);

    private UserDTO userDTO;
    private UserDAO userDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace)) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        TransactionManager transactionManager = new TransactionManagerImpl();
        userDTO = new UserDTO();
        userDTO.setUserName("Sergey123");
        userDTO.setPassword("123ABC");
        userDTO.setEmail("sfdxgd@fdgfd");
        userDTO.setName("Сергей");
        userDTO.setLastName("Волосевич");
        userDTO.setPhone("+375293894233");
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplate jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        userDAO = new UserDAOImpl(jdbcTemplate);
    }

    @Test
    public void findDuplicateExistEmail() throws DAOException {
        final boolean duplicateEmail = userDAO.findDuplicateEmail("ivan.ivanou92@gmail.com");
        assertTrue(duplicateEmail);
    }

    @Test
    public void findDuplicateNonExistEmail() throws DAOException {
        final boolean duplicateEmail = userDAO.findDuplicateEmail("test12@gmail.com");
        assertFalse(duplicateEmail);
    }

    @Test
    public void findDuplicateExistLogin() throws DAOException {
        final boolean duplicateLogin = userDAO.findDuplicateLogin("user1234");
        assertTrue(duplicateLogin);
    }

    @Test
    public void findDuplicateNonExistLogin() throws DAOException {
        final boolean duplicateLogin = userDAO.findDuplicateLogin("user");
        assertFalse(duplicateLogin);
    }

    @Test
    public void findByLoginExist() throws DAOException {
        Optional<UserDTO> optionalUserDTO = userDAO.findByLogin("user1234");
        assertTrue(optionalUserDTO.isPresent());
    }

    @Test
    public void findByLoginNotExist() throws DAOException {
        Optional<UserDTO> optionalUserDTO = userDAO.findByLogin("fdafsdgf");
        assertFalse(optionalUserDTO.isPresent());
    }

    @Test
    public void save() throws DAOException {
        final long save = userDAO.save(userDTO);
        assertEquals(14, save);
    }

    @Test
    public void findAll() throws DAOException {
        List<UserDTO> users = userDAO.findAll();
        UserDTO userDTO = users.get(2);
        assertEquals(13, users.size());
        assertEquals(3, userDTO.getId().longValue());
        assertEquals("user1234", userDTO.getUserName());
        assertEquals("Sergey", userDTO.getName());
        assertEquals("Volosevich", userDTO.getLastName());
        assertEquals("trueman230593@gmail.com", userDTO.getEmail());
        assertEquals("+375293894233", userDTO.getPhone());
        assertEquals(UserStatus.ACTIVE, userDTO.getStatus());
    }

    @Test
    public void findPartOfUsers() throws DAOException {
        List<UserDTO> users = userDAO.findPartOfUsers(3, 3);
        UserDTO userDTO = users.get(0);
        assertEquals(4, userDTO.getId().longValue());
        assertEquals("user12345", userDTO.getUserName());
    }

    @Test
    public void countOfRows() throws DAOException {
        int count = userDAO.countOfRows();
        assertEquals(13, count);
    }
}