package by.training.volosevich.library.validation;

import by.training.volosevich.library.entity.EditionType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

public class DataValidator {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String OBJECT_IDENTIFIER_FIELD_NAME = "type";
    private static final String NUMBER_OF_LINE = "Number of line - ";

    public ValidationResult validateData(Map<String, String> fields, int numberOfLine) {
        ValidationResult validationResult = new ValidationResult();
        if (fields.isEmpty()) {
            validationResult.setValidationInfo(NUMBER_OF_LINE + numberOfLine, ValidationMessage.NO_DATA);
            LOGGER.log(Level.ERROR, () -> NUMBER_OF_LINE + numberOfLine + " - " + ValidationMessage.NO_DATA);
            return validationResult;
        }
        if (fields.containsKey(OBJECT_IDENTIFIER_FIELD_NAME)) {
            ValidatorFactory factory = new ValidatorFactory();
            Validator validator;
            if (EditionType.fromString(fields.get(OBJECT_IDENTIFIER_FIELD_NAME)).isPresent()) {
                validator = factory.getByType(EditionType.valueOf(fields.get(OBJECT_IDENTIFIER_FIELD_NAME)
                        .toUpperCase()));
                List<String> list = validator.validate(fields);
                if (!list.isEmpty()) {
                    validationResult.setValidationInfo(NUMBER_OF_LINE + numberOfLine, list);
                }
            } else {
                validationResult.setValidationInfo(NUMBER_OF_LINE + numberOfLine, ValidationMessage.NO_SUCH_TYPE);
                LOGGER.log(Level.WARN, () -> NUMBER_OF_LINE + numberOfLine + " - " + ValidationMessage.NO_SUCH_TYPE);
            }
        } else {
            validationResult.setValidationInfo(NUMBER_OF_LINE + numberOfLine, ValidationMessage.NO_FIELD_TYPE);
            LOGGER.log(Level.ERROR, () -> NUMBER_OF_LINE + numberOfLine + " - " + ValidationMessage.NO_FIELD_TYPE);
        }
        return validationResult;
    }
}
