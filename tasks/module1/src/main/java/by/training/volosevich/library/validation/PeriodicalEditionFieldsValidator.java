package by.training.volosevich.library.validation;

import by.training.volosevich.library.entity.PeriodicityType;
import by.training.volosevich.library.validation.util.ValueChecker;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;

abstract class PeriodicalEditionFieldsValidator extends EditionFieldsValidator {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final int MIN_VALUE_RELEASE_NUMBER = 1;

    private List<String> fields = new ArrayList<>();

    PeriodicalEditionFieldsValidator() {
        fields.add("yearReleaseNumber");
        fields.add("releaseNumber");
        fields.add("periodicity");
    }

    void validatePeriodicalEditionFields(Map<String, String> values) {
        validateEditionFields(values);
        for (String field : fields) {
            if (values.containsKey(field)) {
                String value = values.get(field);
                switch (field) {
                    case "yearReleaseNumber":
                        validateYearReleaseNumberField(field, value);
                        break;
                    case "releaseNumber":
                        validateReleaseNumberField(field, value);
                        break;
                    case "periodicity":
                        validatePeriodicityFiled(field, value);
                        break;
                }
            } else {
                errorMessages.add(ValidationMessage.formatMessage(field, ValidationMessage.NOT_ENOUGH_FIELD));
                LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(field,
                        ValidationMessage.NOT_ENOUGH_FIELD));
            }
        }
    }

    private void validateYearReleaseNumberField(String nameField, String value) {
        if (ValueChecker.isInteger(value)) {
            if (Integer.parseInt(value) < MIN_VALUE_RELEASE_NUMBER) {
                errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_RELEASE_NUMBER));
                LOGGER.log(Level.WARN, ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_RELEASE_NUMBER));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }

    private void validateReleaseNumberField(String nameField, String value) {
        if (ValueChecker.isInteger(value)) {
            if (Integer.parseInt(value) < MIN_VALUE_RELEASE_NUMBER) {
                errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_RELEASE_NUMBER));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_RELEASE_NUMBER));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }

    private void validatePeriodicityFiled(String nameField, String value) {
        Optional<PeriodicityType> genreValue = PeriodicityType.fromString(value);
        if (!genreValue.isPresent()) {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.NO_SUCH_PERIODICITY_TYPE));
            LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                    ValidationMessage.NO_SUCH_PERIODICITY_TYPE));
        }
    }
}
