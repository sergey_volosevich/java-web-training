<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="by.training.sharing.ApplicationConstants" %>
<%@ page import="by.training.sharing.wallet.CreditCardFieldNameConstants" %>
<%@page import="java.time.Year" %>
<%@taglib prefix="vr" uri="bikeSharing" %>
<%@taglib prefix="bsh" tagdir="/WEB-INF/tags" %>

<c:set var="cardNumber" value="${CreditCardFieldNameConstants.CARD_NUMBER}"/>
<c:set var="holderName" value="${CreditCardFieldNameConstants.HOLDER_NAME}"/>
<c:set var="cvv" value="${CreditCardFieldNameConstants.CVV}"/>
<c:set var="vrMessages" value="${ApplicationConstants.VALIDATION_MESSAGES}"/>
<c:set var="error" value="${ApplicationConstants.FORM_ERROR_CODE}"/>
<div class="container">
    <div class="row">
        <div class="creditCardForm">
            <div class="heading">
                <h1><fmt:message key="card.form.header"/></h1>
            </div>
            <div class="payment">
                <form id="creditCard" action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
                    <input type="hidden" name="commandName" value="addCard"/>
                    <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>">
                    <div class="form-group owner">
                        <label for="owner"><fmt:message key="card.cardHolder.form.label"/></label>
                        <input type="text" class="form-control" id="owner" name="<c:out value="${holderName}"/>"
                               value="<c:out value="${param.get(holderName)}"/>">
                        <p class="form-text alert-danger text-center">
                            <vr:validationMessage fieldName="${holderName}" validationMessages="${vrMessages}"/>
                        </p>
                    </div>
                    <div class="form-group CVV">
                        <label for="cvv">CVV</label>
                        <input type="text" class="form-control" id="cvv" name="<c:out value="${cvv}"/>"
                               value="<c:out value="${param.get(cvv)}"/>">
                        <p class="form-text alert-danger text-center">
                            <vr:validationMessage fieldName="${cvv}" validationMessages="${vrMessages}"/>
                        </p>
                    </div>
                    <div class="form-group" id="card-number-field">
                        <label for="cardNumber"><fmt:message key="card.cardNumber.form.label"/></label>
                        <input type="text" class="form-control" id="cardNumber" name="<c:out value="${cardNumber}"/>"
                               value="<c:out value="${param.get(cardNumber)}"/>">
                        <p class="form-text alert-danger text-center">
                            <vr:validationMessage fieldName="${cardNumber}" validationMessages="${vrMessages}"/>
                        </p>
                    </div>
                    <div class="form-group" id="expiration-date">
                        <label><fmt:message key="card.expirationDate.form.label"/></label>
                        <select name="expireMonth">
                            <option value="1">01</option>
                            <option value="2">02</option>
                            <option value="3">03</option>
                            <option value="4">04</option>
                            <option value="5">05</option>
                            <option value="6">06</option>
                            <option value="7">07</option>
                            <option value="8">08</option>
                            <option value="9">09</option>
                            <option value="10">10</option>
                            <option value="11">11</option>
                            <option value="12">12</option>
                        </select>
                        <select name="expireYear">
                            <c:set var="startValue" value="${Year.now()}"/>
                            <c:forEach begin="0" end="4">
                                <option value="<c:out value="${startValue}"/>">
                                    <c:out value="${startValue}"/>
                                    <c:set var="startValue" value="${startValue.plusYears(1)}"/>
                                </option>
                            </c:forEach>
                        </select>
                    </div>
                    <div class="form-group" id="credit_cards">
                        <img src="static/style/img/mastercard.jpg" id="mastercard">
                        <img src="static/style/img/visa.jpg" id="visa">
                    </div>
                    <div class="form-group" id="pay-now">
                        <c:if test="${not empty requestScope.get('success')}">
                            <div class="alert alert-success text-center" role="alert">
                                <fmt:message key="card.add.form.success"/>
                            </div>
                        </c:if>
                        <p class="form-text alert-danger text-center">
                            <vr:validationMessage fieldName="${error}" validationMessages="${vrMessages}"/>
                        </p>
                        <button type="submit" class="btn btn-outline-dark" id="add-card">
                            <fmt:message key="form.add.button"/>
                        </button>
                        <a class="btn btn-outline-dark" href="<bsh:command value="walletView"/>" role="button">
                            <fmt:message key="form.back.button"/>
                        </a>
                    </div>
                </form>
            </div>
        </div>

    </div>
</div>
<script src="static/style/js/jquery.payform.min.js" charset="utf-8"></script>
<script src="static/style/js/script.js"></script>
