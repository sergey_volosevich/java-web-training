package by.training.sharing.wallet;

import by.training.sharing.dao.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;

public class WalletRowMapper implements RowMapper<WalletDTO> {
    @Override
    public WalletDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        WalletDTO walletDTO = new WalletDTO();
        long id = rs.getLong("wallet_id");
        walletDTO.setId(id);
        BigDecimal currentAmount = rs.getBigDecimal("current_amount");
        walletDTO.setCurrentAmount(currentAmount);
        rs.getInt("user_id");
        return walletDTO;
    }
}
