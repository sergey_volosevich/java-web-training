package by.training.volosevich.library.builder;

import by.training.volosevich.library.entity.Book;
import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.EditionType;
import by.training.volosevich.library.entity.GenreType;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class BookBuilder implements Builder<Edition> {

    private static final Logger LOGGER = LogManager.getLogger();

    private Map<String, String> fields;

    public BookBuilder(Map<String, String> fields) {
        this.fields = fields;
    }

    @Override
    public Edition build() {
        String name = fields.get("name");
        GenreType genreType = GenreType.valueOf(fields.get("genreType").toUpperCase());
        int numberOfPages = Integer.parseInt(fields.get("numberOfPages"));
        String publishingHouse = fields.get("publishingHouse");
        int releaseYear = Integer.parseInt(fields.get("releaseYear"));
        EditionType type = EditionType.valueOf(fields.get("type").toUpperCase());
        String author = fields.get("author");
        LOGGER.log(Level.DEBUG, () -> type + " - entity successfully build");
        return new Book(name, genreType, numberOfPages, publishingHouse, releaseYear, type, author);
    }
}
