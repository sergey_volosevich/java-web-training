package by.training.volosevich.library.validation;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {

    private static final Logger LOGGER = LogManager.getLogger();

    public ValidationResult validateFile(String pathFile) {
        ValidationResult validationResult = new ValidationResult();
        if (pathFile == null) {
            validationResult.setValidationInfo(null, ValidationMessage.FILE_PATH_NULL);
            LOGGER.log(Level.ERROR, validationResult.getValidationInfo());
            return validationResult;
        }
        Path path = Paths.get(pathFile);
        try {
            if (!Files.isReadable(path) || path.toFile().isDirectory()) {
                validationResult.setValidationInfo(pathFile, ValidationMessage.FILE_DOES_NOT_EXIST);
                LOGGER.log(Level.ERROR, validationResult.getValidationInfo());
                return validationResult;
            } else if (Files.size(Paths.get(pathFile)) == 0) {
                validationResult.setValidationInfo(pathFile, ValidationMessage.FILE_IS_EMPTY);
                LOGGER.log(Level.WARN, validationResult.getValidationInfo());
            }
        } catch (IOException e) {
            validationResult.setValidationInfo(pathFile, ValidationMessage.FILE_UNREACHABLE);
            LOGGER.log(Level.ERROR, validationResult.getValidationInfo(), e);
        }
        return validationResult;
    }
}
