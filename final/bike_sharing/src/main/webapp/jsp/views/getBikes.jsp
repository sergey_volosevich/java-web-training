<%@ page contentType="text/xml;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<bikes>
    <c:forEach items="${requestScope.get('bikes')}" var="bike">
        <bike id="<c:out value="${bike.id}"/>" serialNumber="<c:out value="${bike.serialNumber}"/>"
              latitude="<c:out value="${bike.latitude}"/>" longitude="<c:out value="${bike.longitude}"/>"
        csrf="<c:out value="${sessionScope.get('csrf')}"/>"/>
    </c:forEach>
</bikes>