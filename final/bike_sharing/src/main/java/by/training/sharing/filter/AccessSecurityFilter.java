package by.training.sharing.filter;

import by.training.sharing.ApplicationConstants;
import by.training.sharing.core.ApplicationContext;
import by.training.sharing.security.SecurityService;
import by.training.sharing.util.RequestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Set;

import static by.training.sharing.ApplicationConstants.CMD_REQ_PARAMETER;
import static by.training.sharing.ApplicationConstants.GET_USER_LOGIN;
import static by.training.sharing.core.BeanNameConstants.SECURITY_SERVICE;

/**
 * This Web Filter provides functionality to limit user access to unauthorized resources and commands.
 * Prevents the processing of requests by GET method, intended for processing by POST method.
 * It also checks if processing an incoming request from the client is possible.
 */

@WebFilter(filterName = "AccessSecurityFilter", servletNames = "sharing")
public class AccessSecurityFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(AccessSecurityFilter.class);
    private static final String FILTER_INIT = "Access security filter successful initialized";
    private static final String FILTER_DESTROYED = "Access security filter successful destroyed";
    private static final String GET_METHOD = "GET";
    private static final String ENCODING = "UTF-8";

    public void destroy() {
        LOGGER.info(FILTER_DESTROYED);
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = ((HttpServletRequest) req);
        HttpServletResponse httpServletResponse = ((HttpServletResponse) resp);
        httpServletRequest.setCharacterEncoding(ENCODING);
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        SecurityService securityService = applicationContext.getBean(SECURITY_SERVICE);
        String commandName = httpServletRequest.getParameter(CMD_REQ_PARAMETER);
        Set<String> postCommands = ApplicationConstants.POST_COMMANDS;
        boolean isPostCommand = postCommands.stream().anyMatch(postCommand -> postCommand.equalsIgnoreCase(commandName));
        String method = httpServletRequest.getMethod();
        if (isPostCommand && GET_METHOD.equalsIgnoreCase(method)) {
            httpServletResponse.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }
        HttpSession session = httpServletRequest.getSession();
        int httpCode = securityService.canExecute(commandName, session);
        switch (httpCode) {
            case HttpServletResponse.SC_FORBIDDEN:
                httpServletResponse.sendError(HttpServletResponse.SC_FORBIDDEN);
                break;
            case HttpServletResponse.SC_UNAUTHORIZED:
                RequestUtils.sendRedirectToCommand(httpServletRequest, httpServletResponse, GET_USER_LOGIN);
                break;
            case HttpServletResponse.SC_BAD_REQUEST:
                httpServletResponse.sendError(HttpServletResponse.SC_BAD_REQUEST);
                break;
            default:
                chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) {
        LOGGER.info(FILTER_INIT);
    }
}
