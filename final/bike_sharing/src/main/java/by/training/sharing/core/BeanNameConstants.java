package by.training.sharing.core;

/**
 * Name constants for beans of the whole context.
 */

public class BeanNameConstants {
    public static final String CONNECTION_MANAGER = "ConnectionManager";
    public static final String TRANSACTION_MANAGER = "TransactionManager";
    public static final String USER_DAO = "UserDAO";
    public static final String VALIDATOR = "Validator";
    public static final String USER_CONTROLLER = "UserController";
    public static final String PASSWORD_ENCODER = "PasswordEncoder";
    public static final String USER_SERVICE = "UserService";
    public static final String ROLE_DAO = "RoleDAO";
    public static final String BIKE_DAO = "BikeDAO";
    public static final String BIKE_SERVICE = "BikeService";
    public static final String BIKE_CONTROLLER = "BikeController";
    public static final String RENTAL_CONTROLLER = "RentalController";
    public static final String SECURITY_SERVICE = "SecurityService";
    public static final String PRICE_DAO = "PriceDAO";
    public static final String PRICE_SERVICE = "PriceService";
    public static final String WALLET_SERVICE = "WalletService";
    public static final String WALLET_DAO = "WalletDAO";
    public static final String CREDIT_CARD_DAO = "CreditCardDAO";
    public static final String WALLET_CONTROLLER = "WalletController";
    public static final String TRANSACTION_INTERCEPTOR = "TransactionInterceptor";
    public static final String JDBC_TEMPLATE = "JDBCTemplate";

    private BeanNameConstants() {
    }
}
