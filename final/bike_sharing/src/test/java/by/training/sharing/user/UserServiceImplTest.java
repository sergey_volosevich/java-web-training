package by.training.sharing.user;

import by.training.sharing.dao.DAOException;
import by.training.sharing.role.RoleDAO;
import by.training.sharing.security.PasswordEncoder;
import by.training.sharing.service.ServiceException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class UserServiceImplTest {

    @InjectMocks
    private UserServiceImpl userService;
    @Mock
    private UserDAO userDAO;
    @Mock
    private RoleDAO roleDAO;
    @Mock
    private PasswordEncoder passwordEncoder;

    @Before
    public void setUp() throws Exception {
    }

    @Test
    public void registerUser() {
    }

    @Test
    public void getAllUsers() throws DAOException, ServiceException {
        UserDTO userDTO1 = new UserDTO();
        userDTO1.setUserName("admin");
        UserDTO userDTO2 = new UserDTO();
        userDTO2.setUserName("test01");
        List<UserDTO> users = new ArrayList<>();
        users.add(userDTO1);
        users.add(userDTO2);
        Mockito.when(userDAO.findAll()).thenReturn(users);
        List<UserDTO> allUsers = userService.getAllUsers();
        assertEquals(1, allUsers.size());
        assertEquals("test01", allUsers.get(0).getUserName());
    }

    @Test
    public void findDuplicateFields() {
    }
}