package by.training.tariff.entity;

public enum BillingType {
    MINUTE, SECOND
}
