package by.training.volosevich.library.entity;

public abstract class Edition {

    private String name;
    private GenreType genreType;
    private int numberOfPages;
    private String publishingHouse;
    private int releaseYear;
    private EditionType type;

    public Edition(String name, GenreType genreType, int numberOfPages, String publishingHouse, int releaseYear, EditionType type) {
        this.name = name;
        this.genreType = genreType;
        this.numberOfPages = numberOfPages;
        this.publishingHouse = publishingHouse;
        this.releaseYear = releaseYear;
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public GenreType getGenreType() {
        return genreType;
    }

    public void setGenreType(GenreType genreType) {
        this.genreType = genreType;
    }

    public int getNumberOfPages() {
        return numberOfPages;
    }

    public void setNumberOfPages(int numberOfPages) {
        this.numberOfPages = numberOfPages;
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public void setPublishingHouse(String publishingHouse) {
        this.publishingHouse = publishingHouse;
    }

    public int getReleaseYear() {
        return releaseYear;
    }

    public void setReleaseYear(int releaseYear) {
        this.releaseYear = releaseYear;
    }

    public EditionType getType() {
        return type;
    }

    public void setType(EditionType type) {
        this.type = type;
    }
}
