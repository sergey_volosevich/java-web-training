package by.training.sharing.core;

import by.training.sharing.command.CommandException;
import by.training.sharing.command.ServletCommand;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.annotation.Annotation;
import java.lang.reflect.*;
import java.text.MessageFormat;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

/**
 * Implementation of {@link BeanRegistry}.
 */

class BeanRegistryImpl implements BeanRegistry {

    private static final String MORE_THAN_1_CONSTRUCTOR_IS_PRESENT_FOR_CLASS = "More than 1 constructor is present for class - {0}";
    private static final String BEAN_ANNOTATION_NOT_EXISTS = "{0} doesn't have @Bean annotation";
    private static final String BEAN_DOES_NOT_REGISTERED = "Could not autowire. Bean with name - {0} - does not registered.";
    private static final String BEAN_ALREADY_REGISTERED = "Bean with name - {0} - already registered";
    private static final String FAILED_TO_INSTANTIATE_BEAN = "Failed to instantiate bean";
    private static final String NO_BEAN_QUALIFIER_ANNOTATION = "Could not autowire. No @BeanQualifier annotation. No beans of - {0} - type found.";
    /**
     * This java.util.concurrent.ConcurrentHashMap contains name of commands, which is used to process client request,
     * as keys, and java.lang.reflect.Method objects as values.
     */
    private final Map<String, Method> commands = new ConcurrentHashMap<>();

    /**
     * Object, that contains java.util.concurrent.ConcurrentHashMap with registration information about class as keys,
     * and bean objects as values.
     */
    private final FactoryBean factoryBean = new FactoryBean();

    /**
     * java.util.Set that contains registration information about all beans in application context.
     */
    private final Set<RegistryInfo> beanRegistry = new HashSet<>();


    /**
     * This method is used to registered object of bean. Calculates needed information about class of bean object and
     * put it in java.util.Set.
     *
     * @param bean The bean object to register.
     */

    @Override
    public <T> void registerBean(T bean) {

        RegistryInfo info = calculateRegistryInfo(bean.getClass());
        info.setConcreteBean(bean);
        addRegistryInfo(info);
    }

    /**
     * This method is used to registered bean by its class. Calculates needed information about class of bean object and
     * put it in java.util.Set.
     *
     * @param beanClass The bean class to register.
     */

    @Override
    public <T> void registerBean(Class<T> beanClass) {

        RegistryInfo info = calculateRegistryInfo(beanClass);
        final Supplier<Object> factory = createFactory(info);
        info.setFactory(factory);
        addRegistryInfo(info);
    }

    /**
     * Adds registration information about class.
     *
     * @param info registration information about the class to be registered
     * @throws NotUniqueBeanException throws if class already registered.
     */

    private void addRegistryInfo(RegistryInfo info) {
        beanRegistry.stream()
                .filter(registryInfo -> registryInfo.getName().equals(info.getName()))
                .findFirst()
                .ifPresent(registryInfo -> {
                    throw new NotUniqueBeanException(MessageFormat.format(BEAN_ALREADY_REGISTERED, registryInfo.getName()));
                });
        beanRegistry.add(info);
    }

    /**
     * Creates a factory for further object creation.
     *
     * @param info object with registration information.
     * @return java.util.function.Supplier.
     */

    private Supplier<Object> createFactory(RegistryInfo info) {

        Class<?> clazz = info.getClazz();
        Constructor<?>[] constructors = clazz.getDeclaredConstructors();
        if (constructors.length > 1) {
            throw new BeanInstantiationException(MessageFormat.format(MORE_THAN_1_CONSTRUCTOR_IS_PRESENT_FOR_CLASS,
                    clazz.getSimpleName()));
        }

        return () -> {
            Constructor<?> constructor = constructors[0];
            if (constructor.getParameterCount() > 0) {
                Object[] args = getConstructorParameters(constructor);
                try {
                    return constructor.newInstance(args);
                } catch (InstantiationException | InvocationTargetException | IllegalAccessException e) {
                    throw new BeanInstantiationException(FAILED_TO_INSTANTIATE_BEAN, e);
                }
            } else {
                try {
                    return clazz.newInstance();
                } catch (InstantiationException | IllegalAccessException e) {
                    throw new BeanInstantiationException(FAILED_TO_INSTANTIATE_BEAN, e);
                }
            }
        };
    }

    /**
     * Gets constructor parameters
     *
     * @param constructor object of constructor class.
     * @return array of objects for beans constructor.
     * @throws BeanRegistrationException Throws if parameter of constructor has not annotation {@link BeanQualifier} or
     *                                   bean with name {@link BeanQualifier#value()} not found.
     */

    private Object[] getConstructorParameters(Constructor<?> constructor) {
        Parameter[] parameters = constructor.getParameters();
        Object[] args = new Object[parameters.length];
        for (int i = 0; i < parameters.length; i++) {
            Class<?> type = parameters[i].getType();
            BeanQualifier beanQualifier = parameters[i].getAnnotation(BeanQualifier.class);
            if (beanQualifier != null) {
                Predicate<RegistryInfo> searchBean = searchInfo -> searchInfo.getName().equals(beanQualifier.value());
                Object bean = getBean(searchBean);
                if (bean != null) {
                    args[i] = bean;
                } else {
                    throw new BeanRegistrationException(MessageFormat.format(BEAN_DOES_NOT_REGISTERED, beanQualifier.value()));
                }
            } else {
                throw new MissedAnnotationException(MessageFormat.format(NO_BEAN_QUALIFIER_ANNOTATION, type.getName()));
            }
        }
        return args;
    }

    /**
     * Collects information about the registered bean.
     *
     * @param beanClass class of class about which information is collected
     * @return Object with information about class.
     * @throws MissedAnnotationException throws if class has not {@link Bean} annotation.
     */

    private RegistryInfo calculateRegistryInfo(Class<?> beanClass) {
        Bean bean = beanClass.getAnnotation(Bean.class);
        if (bean == null) {
            throw new MissedAnnotationException(MessageFormat.format(BEAN_ANNOTATION_NOT_EXISTS, beanClass.getName()));
        }
        RegistryInfo info = new RegistryInfo();
        info.setClazz(beanClass);

        Class<?>[] interfaces = beanClass.getInterfaces();
        info.setInterfaces(Arrays.stream(interfaces).collect(Collectors.toSet()));

        Annotation[] annotations = beanClass.getAnnotations();
        info.setAnnotations(Arrays.stream(annotations).collect(Collectors.toSet()));
        Command command = beanClass.getAnnotation(Command.class);
        if (command != null) {
            Method[] methods = beanClass.getMethods();
            for (Method method : methods) {
                CommandName annotation = method.getAnnotation(CommandName.class);
                if (annotation != null) {
                    commands.put(annotation.name(), method);
                }
            }
        }
        Interceptor interceptor = beanClass.getAnnotation(Interceptor.class);
        if (interceptor != null) {
            info.setInterceptor(interceptor);
        }

        String beanName = bean.name();
        if (beanName.trim().length() > 0) {
            info.setName(beanName);
        } else {
            info.setName(beanClass.getSimpleName());
        }
        return info;
    }

    /**
     * Returns a link to the functional interface {@link by.training.sharing.command.ServletCommand} with
     * realization method {@link by.training.sharing.command.ServletCommand#execute(HttpServletRequest, HttpServletResponse)}.
     * found by the name, specified in the {@link CommandName#name()} annotation, in Beans, marked with annotation
     * {@link by.training.sharing.core.Command}
     *
     * @param commandName Name of the method specified in the {@link CommandName#name()} annotation
     * @return Returns a link to the functional interface <code>ServletCommand</code>, or <code>null</code>
     * if command with {@param commandName}
     * does not exist in Beans, marked with annotation {@link by.training.sharing.core.Command}
     * in the application context.
     */

    @Override
    public ServletCommand getCommand(String commandName) {
        if (commandName != null) {
            Method method = commands.get(commandName);
            if (method != null) {
                Class<?> declaringClass = method.getDeclaringClass();
                Predicate<RegistryInfo> searchBean = info -> info.getClazz().equals(declaringClass);
                Object bean = getBean(searchBean);
                return (req, res) -> {
                    try {
                        method.invoke(bean, req, res);
                    } catch (IllegalAccessException | InvocationTargetException e) {
                        throw new CommandException(e);
                    }
                };
            }
        }
        return null;
    }


    /**
     * Get bean by registered name indicated in the annotation {@link Bean#name()}
     *
     * @param beanName Name of the bean
     * @param <T>  Generalized type that can be cast to the required bean
     * @return Bean found by name, or <code>null</code> if bean with such name does not exist.
     */

    @Override
    public <T> T getBean(String beanName) {
        Predicate<RegistryInfo> searchBean = info -> info.getName().equals(beanName);
        return getBean(searchBean);
    }

    @SuppressWarnings("unchecked")
    private <T> T getBean(Predicate<RegistryInfo> searchBean) {
        List<RegistryInfo> registryInfoList = beanRegistry
                .stream()
                .filter(searchBean)
                .collect(Collectors.toList());
        return (T) registryInfoList
                .stream()
                .map(this::mapToBean)
                .findFirst().orElse(null);
    }

    /**
     * Removes bean from context
     * @param bean bean object
     * @return <code>true</code> if removes from context, otherwise - <code>false</code>
     */

    @Override
    public <T> boolean removeBean(T bean) {
        RegistryInfo registryInfo = calculateRegistryInfo(bean.getClass());
        return beanRegistry.remove(registryInfo);
    }

    /**
     * Removes all beans from context.
     */

    @Override
    public void destroy() {
        commands.clear();
        factoryBean.destroy();
        beanRegistry.clear();
    }

    @SuppressWarnings("unchecked")
    private <T> T mapToBean(RegistryInfo registryInfo) {
        T service = (T) factoryBean.getBean(registryInfo);
        Set<RegistryInfo> availableInterceptors = beanRegistry.stream()
                .filter(RegistryInfo::isInterceptor)
                .filter(interceptorInfo -> registryInfo.getAnnotations()
                        .stream()
                        .anyMatch(a -> a.annotationType().equals(interceptorInfo.getInterceptor().clazz())))
                .collect(Collectors.toSet());

        if (availableInterceptors.isEmpty()) {
            return service;
        } else {
            List<BeanInterceptor> interceptors = availableInterceptors.stream()
                    .map(interceptorInfo -> (BeanInterceptor) factoryBean.getBean(interceptorInfo))
                    .collect(Collectors.toList());
            return getServiceProxy(service, registryInfo, interceptors);
        }
    }

    @SuppressWarnings("unchecked")
    private <T> T getServiceProxy(T service, RegistryInfo info, List<BeanInterceptor> interceptors) {
        Class<?>[] toProxy = new Class[info.getInterfaces().size()];
        Class<?>[] interfaces = info.getInterfaces().toArray(toProxy);
        return (T) Proxy.newProxyInstance(this.getClass().getClassLoader(), interfaces,
                (proxy, method, args) -> {
                    try {
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.before(proxy, service, method, args);
                        }
                        Object invoked = method.invoke(service, args);
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.success(proxy, service, method, args);
                        }
                        return invoked;
                    } catch (Exception e) {
                        for (BeanInterceptor interceptor : interceptors) {
                            interceptor.fail(proxy, service, method, args);
                        }
                        throw e.getCause();
                    }
                });
    }

    private static class RegistryInfo {

        private String name;
        private Class<?> clazz;
        private Set<Class<?>> interfaces;
        private Set<Annotation> annotations;
        private Interceptor interceptor;
        private Supplier<Object> factory;
        private Object concreteBean;

        RegistryInfo() {
        }

        boolean isInterceptor() {
            return this.interceptor != null;
        }

        String getName() {
            return name;
        }

        void setName(String name) {
            this.name = name;
        }

        Class<?> getClazz() {
            return clazz;
        }

        void setClazz(Class<?> clazz) {
            this.clazz = clazz;
        }

        Set<Class<?>> getInterfaces() {
            return interfaces;
        }

        void setInterfaces(Set<Class<?>> interfaces) {
            this.interfaces = interfaces;
        }

        Set<Annotation> getAnnotations() {
            return annotations;
        }

        void setAnnotations(Set<Annotation> annotations) {
            this.annotations = annotations;
        }

        Interceptor getInterceptor() {
            return interceptor;
        }

        void setInterceptor(Interceptor interceptor) {
            this.interceptor = interceptor;
        }

        Supplier<Object> getFactory() {
            return factory;
        }

        void setFactory(Supplier<Object> factory) {
            this.factory = factory;
        }

        Object getConcreteBean() {
            return concreteBean;
        }

        void setConcreteBean(Object concreteBean) {
            this.concreteBean = concreteBean;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (!(o instanceof RegistryInfo)) return false;
            RegistryInfo that = (RegistryInfo) o;
            return Objects.equals(getName(), that.getName()) &&
                    Objects.equals(getClazz(), that.getClazz());
        }

        @Override
        public int hashCode() {
            return Objects.hash(getName(), getClazz());
        }
    }

    private static class FactoryBean {

        private Map<RegistryInfo, Object> beans = new ConcurrentHashMap<>();

        Object getBean(RegistryInfo info) {
            Object concreteBean = info.getConcreteBean();
            if (concreteBean != null) {
                beans.put(info, concreteBean);
            } else if (!beans.containsKey(info)) {
                final Object bean = info.getFactory().get();
                beans.put(info, bean);
            }
            return beans.get(info);
        }

        void destroy() {
            beans.clear();
        }
    }
}
