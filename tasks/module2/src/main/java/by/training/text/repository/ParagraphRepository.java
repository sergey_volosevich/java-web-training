package by.training.text.repository;

import by.training.text.entity.Paragraph;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class ParagraphRepository implements Repository<Paragraph> {

    private AtomicLong idGenerator = new AtomicLong(1);

    private Map<Long, Paragraph> mapParagraphs = new HashMap<>();

    @Override
    public long create(Paragraph entity) {
        long id = idGenerator.getAndIncrement();
        entity.setId(id);
        mapParagraphs.put(id, entity);
        return id;
    }

    @Override
    public boolean update(Paragraph entity) {
        long id = entity.getId();
        if (mapParagraphs.containsKey(id)) {
            mapParagraphs.put(id, entity);
            return true;
        }
        return false;
    }

    @Override
    public Paragraph read(long id) {
        return mapParagraphs.get(id);
    }

    @Override
    public boolean delete(long id) {
        Paragraph paragraph = mapParagraphs.get(id);
        return mapParagraphs.remove(id, paragraph);
    }

    @Override
    public List<Paragraph> find(Specification<Paragraph> specification) {
        List<Paragraph> paragraphs = new ArrayList<>(mapParagraphs.values());
        List<Paragraph> foundParagraphs = new ArrayList<>();
        for (Paragraph paragraph : paragraphs) {
            if (specification.match(paragraph)) {
                foundParagraphs.add(paragraph);
            }
        }
        return foundParagraphs;
    }

    @Override
    public List<Paragraph> getAll() {
        return new ArrayList<>(mapParagraphs.values());
    }
}
