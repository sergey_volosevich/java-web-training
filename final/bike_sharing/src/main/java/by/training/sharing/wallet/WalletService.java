package by.training.sharing.wallet;

import by.training.sharing.service.ServiceException;

import java.util.Optional;
import java.util.Set;

public interface WalletService {

    Optional<WalletDTO> findByUserId(long id) throws ServiceException;

    boolean createWallet(long userId);

    long addCreditCard(CreditCardDTO creditCardDTO) throws ServiceException;

    boolean updateBalance(WalletDTO walletDTO);

    CreditCardDTO getCreditCardById(long creditCardId) throws ServiceException;

    boolean deleteCard(long creditCard);

    Set<CreditCardDTO> findCardsByWalletId(long walletId) throws ServiceException;
}
