package by.training.sharing.core;

@Bean(name = "TestService")
public class TestService {

    private TestDAO testDAO;

    public TestService(@BeanQualifier("TestDAO") TestDAO testDAO) {
        this.testDAO = testDAO;
    }

    public boolean checkMethod() {
        return testDAO.checkMethod();
    }
}
