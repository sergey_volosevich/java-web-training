package by.training.text.service;

import by.training.text.entity.Paragraph;
import by.training.text.entity.Sentence;
import by.training.text.model.ParagraphComposite;
import by.training.text.model.SentenceComposite;
import by.training.text.model.TextComposite;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class CompositeTextService {

    private ParagraphService paragraphService;
    private SentenceService sentenceService;

    public CompositeTextService(ParagraphService paragraphService, SentenceService sentenceService) {
        this.paragraphService = paragraphService;
        this.sentenceService = sentenceService;
    }

    public List<TextComposite> sortParagraphsIncreasingOrder(Long textId) {
        List<Paragraph> listParagraphs = paragraphService.findParagraphByTextId(textId);
        List<TextComposite> listParagraphComposite = new ArrayList<>();
        for (Paragraph paragraph : listParagraphs) {
            ParagraphComposite paragraphComposite = new ParagraphComposite(paragraph, paragraphService);
            for (Sentence sentence : sentenceService.findSentenceByParagraphId(paragraph.getId())) {
                SentenceComposite sentenceComposite = new SentenceComposite(sentence, sentenceService);
                paragraphComposite.addText(sentenceComposite);
            }
            listParagraphComposite.add(paragraphComposite);
        }
        listParagraphComposite.sort(Comparator.comparingInt(o -> o.getComponents().size()));
        return listParagraphComposite;
    }

    public List<TextComposite> sortParagraphsDecreasingOrder(Long textId) {
        List<Paragraph> listParagraphs = paragraphService.findParagraphByTextId(textId);
        List<TextComposite> listParagraphComposite = new ArrayList<>();
        for (Paragraph paragraph : listParagraphs) {
            ParagraphComposite paragraphComposite = new ParagraphComposite(paragraph, paragraphService);
            for (Sentence sentence : sentenceService.findSentenceByParagraphId(paragraph.getId())) {
                SentenceComposite sentenceComposite = new SentenceComposite(sentence, sentenceService);
                paragraphComposite.addText(sentenceComposite);
            }
            listParagraphComposite.add(paragraphComposite);
        }
        listParagraphComposite.sort(Comparator.comparingInt(o -> o.getComponents().size()));
        Collections.reverse(listParagraphComposite);
        return listParagraphComposite;
    }
}



