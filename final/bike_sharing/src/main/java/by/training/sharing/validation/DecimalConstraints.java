package by.training.sharing.validation;

import java.lang.annotation.*;

/**
 * This annotation allows the use of several type {@link by.training.sharing.validation.DecimalConstraint} annotations
 * on one class field.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface DecimalConstraints {
    DecimalConstraint[] value();
}
