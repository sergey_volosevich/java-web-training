package by.training.tariff.parser;

import java.util.List;

public interface Parser<T> {
    List<T> parse(String pathXML) throws ParserException;
}
