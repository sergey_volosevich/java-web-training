package by.training.sharing.validation;

import org.apache.taglibs.standard.tag.common.fmt.BundleSupport;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.jstl.fmt.LocalizationContext;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

/**
 * This is a tag handler of <code></code>validationMessage.tag</code>. This tag is used to display validation messages
 * on jsp pages. Validation messages retrieved from the request.
 */

public class ValidationMessageTag extends TagSupport {

    private static final long serialVersionUID = 7522155879872787935L;

    private String fieldName = null;
    private String validationMessages = null;

    public void setFieldName(String fieldName) {
        this.fieldName = fieldName;
    }

    public void setValidationMessages(String validationMessages) {
        this.validationMessages = validationMessages;
    }

    @Override
    public int doStartTag() throws JspException {
        LocalizationContext localizationContext = BundleSupport.getLocalizationContext(pageContext);
        Object validationResult = pageContext.getRequest().getAttribute(validationMessages);
        if (validationResult != null && validationResult.getClass() == ValidationResult.class) {
            Map<String, List<String>> validationInfo = ((ValidationResult) validationResult).getValidationInfo();
            List<String> stringList = validationInfo.get(fieldName);
            ResourceBundle resourceBundle = localizationContext.getResourceBundle();
            if (stringList != null) {
                List<String> messages = stringList.stream().map(msg->{
                    if (resourceBundle.containsKey(msg)) {
                        return resourceBundle.getString(msg);
                    } else {
                        return msg;
                    }}).collect(Collectors.toList());
                JspWriter out = pageContext.getOut();
                for (String s : messages) {
                    try {
                        out.write(s);
                    } catch (IOException e) {
                        throw new JspException(e);
                    }
                }
            }
        }
        return SKIP_BODY;
    }

    @Override
    public int doEndTag() throws JspException {
        return EVAL_PAGE;
    }

    @Override
    public void release() {
        super.release();
        validationMessages = null;
        fieldName = null;
    }
}
