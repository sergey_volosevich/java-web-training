package by.training.text.parser;

import by.training.text.model.TextLeaf;
import by.training.text.service.ParagraphService;
import by.training.text.service.SentenceService;
import by.training.text.service.TextService;
import by.training.text.service.WordService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(MockitoJUnitRunner.class)
public class ParserChainTest {
    private ParserChain<TextLeaf> parserChain;
    private String validText;
    private String textWithoutParagraphs;
    private String textWithOnlyWords;
    private String textWithoutCharacters;

    @Mock
    private TextService textService;

    @Mock
    private ParagraphService paragraphService;

    @Mock
    private SentenceService sentenceService;

    @Mock
    private WordService wordService;

    @Before
    public void setUp() {
        ParagraphParser paragraphParser = new ParagraphParser(paragraphService);
        SentenceParser sentenceParser = new SentenceParser(sentenceService);
        WordParser wordParser = new WordParser(wordService);
        parserChain = new TextParser(textService);
        parserChain.linkWith(paragraphParser).linkWith(sentenceParser).linkWith(wordParser);


        validText = "\tIt has survived not only five centuries, but also the leap into electronic\n" +
                "typesetting, remaining essentially unchanged. It was popularised in the with the\n" +
                "release of Letraset sheets containing Lorem Ipsum passages, and more recently with\n" +
                "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "\tIt is a long established fact that a reader will be distracted by the readable\n" +
                "content of a page when looking at its layout. The point of using Ipsum is that it has a\n" +
                "more-or-less normal distribution of letters, as opposed to using 'Content here, content\n" +
                "here', making it look like readable English.\n" +
                "\tIt is a established fact that a reader will be of a page when looking at its\n" +
                "layout...\n" +
                "\tBye.\n";

        textWithoutParagraphs = "It has survived not only five centuries, but also the leap into electronic\n" +
                "typesetting, remaining essentially unchanged. It was popularised in the with the\n" +
                "release of Letraset sheets containing Lorem Ipsum passages, and more recently with\n" +
                "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.\n" +
                "It is a long established fact that a reader will be distracted by the readable\n" +
                "content of a page when looking at its layout. The point of using Ipsum is that it has a\n" +
                "more-or-less normal distribution of letters, as opposed to using 'Content here, content\n" +
                "here', making it look like readable English.\n" +
                "It is a established fact that a reader will be of a page when looking at its\n" +
                "layout...\n" +
                "Bye.\n";

        textWithOnlyWords = "It has survived not only five centuries, but also the leap into electronic\n" +
                "typesetting, remaining essentially unchanged It was popularised in the with the\n" +
                "release of Letraset sheets containing Lorem Ipsum passages, and more recently with\n" +
                "desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum\n" +
                "It is a long established fact that a reader will be distracted by the readable\n" +
                "content of a page when looking at its layout The point of using Ipsum is that it has a\n" +
                "more-or-less normal distribution of letters, as opposed to using 'Content here, content\n" +
                "here', making it look like readable English\n";
        textWithoutCharacters = " ";
    }

    @Test
    public void parseValidText() {
        TextLeaf leaf = parserChain.parse(validText);
        String parsedText = leaf.getString();
        assertEquals(validText, parsedText);
    }

    @Test
    public void parseTextWithoutParagraphs() {
        TextLeaf leaf = parserChain.parse(textWithoutParagraphs);
        String parsedText = leaf.getString();
        assertEquals(textWithoutParagraphs, parsedText);
    }

    @Test
    public void parseTextWithOnlyWords() {
        TextLeaf leaf = parserChain.parse(textWithOnlyWords);
        String parsedText = leaf.getString();
        assertEquals(textWithOnlyWords, parsedText);
    }

    @Test
    public void parseTextWithoutCharacters() {
        TextLeaf leaf = parserChain.parse(textWithoutCharacters);
        String parsedText = leaf.getString();
        assertNotEquals(textWithoutCharacters, parsedText);
    }
}