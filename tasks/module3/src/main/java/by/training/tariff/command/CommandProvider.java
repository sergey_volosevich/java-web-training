package by.training.tariff.command;

public interface CommandProvider<T> {

    Command<T> getCommand(String type);

    void addCommand(CommandType type, Command<T> command);

}
