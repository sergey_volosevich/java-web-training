package by.training.volosevich.library.service;

import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.GenreType;
import by.training.volosevich.library.repository.GenreTypeSpecification;
import by.training.volosevich.library.repository.ReleaseYearRangeSpecification;
import by.training.volosevich.library.repository.Repository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Comparator;
import java.util.List;

public class LibraryService {

    private static final Logger LOGGER = LogManager.getLogger();

    private Repository<Edition> libraryRepository;

    public LibraryService(Repository<Edition> libraryRepository) {
        this.libraryRepository = libraryRepository;
    }

    public void addNewEdition(Edition edition) {
        libraryRepository.add(edition);
    }

    public int sumOfPagesByGenre(GenreType genreType) {
        LOGGER.log(Level.DEBUG, () -> "summ of pages by genre - " + genreType.name());
        if (genreType == null) {
            return 0;
        }
        int sumOfPages = 0;
        for (Edition edition : libraryRepository.getAll()) {
            if (edition.getGenreType().equals(genreType)) {
                sumOfPages += edition.getNumberOfPages();
            }
        }
        LOGGER.log(Level.DEBUG, String.format("summ of pages = %s", sumOfPages));
        return sumOfPages;
    }

    public List<Edition> sortIncreaseByReleaseYear() {
        List<Edition> sortedEditions = libraryRepository.getAll();
        sortedEditions.sort(Comparator.comparingInt(Edition::getReleaseYear));
        return sortedEditions;
    }

    public List<Edition> sortDecreaseByReleaseYear() {
        List<Edition> sortedEditions = libraryRepository.getAll();
        sortedEditions.sort(Comparator.comparingInt(Edition::getReleaseYear).reversed());
        return sortedEditions;
    }

    public List<Edition> sortByReleaseYearByNumberOfPages() {
        List<Edition> sortedEditions = libraryRepository.getAll();
        sortedEditions.sort(Comparator.comparingInt(Edition::getReleaseYear).thenComparing(Edition::getNumberOfPages));
        return sortedEditions;
    }

    public List<Edition> findByReleaseYearRange(int lowerRange, int upperRange) {
        return libraryRepository.find(new ReleaseYearRangeSpecification(lowerRange, upperRange));
    }

    public List<Edition> findByGenreTypeRangeYear(GenreType type, int lowerRange, int upperRange) {
        return libraryRepository.find(new GenreTypeSpecification(type)
                .and(new ReleaseYearRangeSpecification(lowerRange, upperRange)));
    }
}
