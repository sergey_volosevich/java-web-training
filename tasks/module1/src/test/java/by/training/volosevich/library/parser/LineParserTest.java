package by.training.volosevich.library.parser;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class LineParserTest {

    private LineParser lineParser;
    private Map<String, String> lines;

    @Before
    public void setUp() {
        lineParser = new LineParser();
        lines = new HashMap<>();
        lines.put("validLine", "editionType:magazine, name: Magazine1, releaseYear:2007, genreType:cars," +
                " numberOfPages:35, publishingHouse:\"Minsk Publishing\", releaseMonth:September");
        lines.put("invalidLine", "editionType>magazine, name! Magazine1, releaseYear!2007, genreType.cars," +
                " numberOfPages;35, publishingHouse{\"Minsk Publishing\", releaseMonth>September");
        lines.put("partiallyCorrectLine", "editionType:magazine, name: Magazine1, releaseYear!2007, genreType.cars," +
                " numberOfPages;35, publishingHouse:\"Minsk Publishing\", releaseMonth>September");
    }

    @Test
    public void parseLineNull() {
        assertTrue(lineParser.parseLine(null).isEmpty());
    }

    @Test
    public void parseLineByValidLine() {
        assertEquals(7, lineParser.parseLine(lines.get("validLine")).size());
    }

    @Test
    public void parseLineByInvalidLine() {
        assertEquals(0, lineParser.parseLine(lines.get("invalidLine")).size());
    }

    @Test
    public void parseLineByPartiallyValidLine() {
        assertEquals(3, lineParser.parseLine(lines.get("partiallyCorrectLine")).size());
    }
}