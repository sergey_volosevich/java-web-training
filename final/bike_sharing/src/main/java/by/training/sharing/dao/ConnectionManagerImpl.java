package by.training.sharing.dao;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Connection;
import java.sql.SQLException;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static by.training.sharing.core.BeanNameConstants.TRANSACTION_MANAGER;


@Bean(name = CONNECTION_MANAGER)
public class ConnectionManagerImpl implements ConnectionManager {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionManagerImpl.class);
    private static final String CONNECTION_FROM_TRANSACTION_MANAGER = "Connection from transaction manager";
    private static final String CONNECTION_FROM_CONNECTION_POOL = "Connection from connection pool";

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();
    private TransactionManager transactionManager;

    public ConnectionManagerImpl(@BeanQualifier(TRANSACTION_MANAGER) TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection managerConnection = transactionManager.getConnection();
        if (managerConnection != null) {
            LOGGER.debug(CONNECTION_FROM_TRANSACTION_MANAGER);
            return managerConnection;
        } else {
            Connection connection = connectionPool.getConnection();
            LOGGER.debug(CONNECTION_FROM_CONNECTION_POOL);
            return connection;
        }
    }

    @Override
    public void destroyConnectionPool() {
        connectionPool.destroyPool();
    }
}
