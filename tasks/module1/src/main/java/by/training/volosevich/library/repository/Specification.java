package by.training.volosevich.library.repository;

public interface Specification<T> {
    boolean match(T entity);

    default Specification<T> and(Specification<T> other) {
        return entity -> match(entity) && other.match(entity);
    }

    default Specification<T> or(Specification<T> other) {
        return entity -> match(entity) || other.match(entity);
    }
}
