package by.training.sharing.user;


import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.TransactionSupport;
import by.training.sharing.dao.Transactional;
import by.training.sharing.role.RoleDAO;
import by.training.sharing.security.PasswordEncoder;
import by.training.sharing.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import static by.training.sharing.core.BeanNameConstants.*;
import static by.training.sharing.user.UserFieldNameConstants.USER_EMAIL;
import static by.training.sharing.user.UserFieldNameConstants.USER_LOGIN;

@Bean(name = USER_SERVICE)
@TransactionSupport
public class UserServiceImpl implements UserService {

    private static final Logger LOGGER = LogManager.getLogger(UserServiceImpl.class);

    private static final String FAILED_TO_REGISTER_USER = "Failed to register user. Cause:";
    private static final String FAILED_TO_FIND_DUPLICATES = "Failed to find duplicates of email and login. Cause: ";

    private static final String LOGIN_DUPLICATE_MSG = "user.login.validation.duplicate.msg";
    private static final String EMAIL_DUPLICATE_MSG = "user.email.validation.duplicate.msg";
    private static final String ADMIN = "admin";

    private UserDAO userDAO;
    private RoleDAO roleDAO;
    private PasswordEncoder passwordEncoder;

    public UserServiceImpl(@BeanQualifier(USER_DAO) UserDAO userDAO, @BeanQualifier(ROLE_DAO) RoleDAO roleDAO,
                           @BeanQualifier(PASSWORD_ENCODER) PasswordEncoder passwordEncoder) {
        this.userDAO = userDAO;
        this.roleDAO = roleDAO;
        this.passwordEncoder = passwordEncoder;
    }


    @Override
    public List<UserDTO> getPartOfUsers(int currentPage, int recordsPerPage) throws ServiceException {
        int start = currentPage * recordsPerPage - recordsPerPage;
        try {
            List<UserDTO> users = userDAO.findPartOfUsers(start, recordsPerPage);
            return users.stream().filter((userDTO -> !userDTO.getUserName().equalsIgnoreCase(ADMIN)))
                    .collect(Collectors.toList());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    @Transactional
    public boolean registerUser(UserDTO userDTO) {
        try {
            String password = userDTO.getPassword();
            String hashPassword = passwordEncoder.hash(password.toCharArray());
            userDTO.setPassword(hashPassword);
            long save = userDAO.save(userDTO);
            roleDAO.assignDefaultRoles(save);
            return true;
        } catch (DAOException e) {
            LOGGER.error(FAILED_TO_REGISTER_USER, e);
            return false;
        }
    }

    @Override
    public List<UserDTO> getAllUsers() throws ServiceException {
        try {
            List<UserDTO> users = userDAO.findAll();
            return users.stream().filter((userDTO -> !userDTO.getUserName().equalsIgnoreCase(ADMIN)))
                    .collect(Collectors.toList());
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public Map<String, String> findDuplicateFields(String login, String email) {
        Map<String, String> duplicates = new HashMap<>();
        try {
            if (userDAO.findDuplicateLogin(login)) {
                duplicates.put(USER_LOGIN, LOGIN_DUPLICATE_MSG);
            }
            if (userDAO.findDuplicateEmail(email)) {
                duplicates.put(USER_EMAIL, EMAIL_DUPLICATE_MSG);
            }
            return duplicates;
        } catch (DAOException e) {
            LOGGER.error(FAILED_TO_FIND_DUPLICATES, e);
            return duplicates;
        }
    }

    @Override
    public int countOfRows() throws ServiceException {
        try {
            return userDAO.countOfRows();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}

