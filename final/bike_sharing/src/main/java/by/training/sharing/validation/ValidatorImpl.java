package by.training.sharing.validation;

import by.training.sharing.core.Bean;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.text.MessageFormat;
import java.util.Collections;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import static by.training.sharing.core.BeanNameConstants.VALIDATOR;

/**
 * This class implements a {@link by.training.sharing.validation.Validator} interface.
 * Allows validation of field values marked with annotations from package {@link by.training.sharing.validation}.
 */

@Bean(name = VALIDATOR)
public class ValidatorImpl implements Validator {

    private static final String MISSING_ANNOTATION = "Missing \'Validation\' annotation on class - {0}.";
    private static final String NO_CONSTRAINTS_ON_FILED_OF_CLASS = "No constraints on filed of class  - {0}.";
    private static final ValidationPatternHandler PATTERN_HANDLER = new ValidationPatternHandler();
    private static final DecimalConstraintHandler DECIMAL_MAX_VALUE_HANDLER = new DecimalConstraintHandler();
    private static final Set<Class<? extends Annotation>> CONSTRAINTS;

    static {
        Set<Class<? extends Annotation>> constraintsSet = new HashSet<>();
        constraintsSet.add(ValidationPattern.class);
        constraintsSet.add(DecimalConstraints.class);
        constraintsSet.add(ValidationPatterns.class);
        CONSTRAINTS = Collections.unmodifiableSet(constraintsSet);
    }

    @Override
    public ValidationResult validate(Map<String, String> map, Class<?> clazz) {
        ValidationResult result = new ValidationResult();
        Validation validation = clazz.getAnnotation(Validation.class);
        if (validation == null) {
            throw new MissingAnnotationException(MessageFormat.format(MISSING_ANNOTATION, clazz));
        }
        boolean hasConstraint = false;
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field field : declaredFields) {
            Annotation[] annotations = field.getAnnotations();
            for (Annotation annotation : annotations) {
                if (isConstraintAnnotation(annotation)) {
                    constraintValidator(annotation, map, result);
                    hasConstraint = true;
                }
            }
        }
        if (!hasConstraint) {
            throw new MissingAnnotationException(MessageFormat.format(NO_CONSTRAINTS_ON_FILED_OF_CLASS, clazz));
        }
        return result;
    }

    /**
     * This method invoke method of needed annotation handler class.
     *
     * @param annotation Annotation of the field
     * @param map        java.util.Map with pairs - key is field name, value - filed value.
     * @param result     by.training.sharing.validation.ValidationResult - to store validation messages.
     */

    private void constraintValidator(Annotation annotation, Map<String, String> map, ValidationResult result) {
        if (ValidationPattern.class == annotation.annotationType()) {
            PATTERN_HANDLER.handleAnnotation(annotation, map, result);
        } else if (ValidationPatterns.class == annotation.annotationType()) {
            ValidationPattern[] validationPatterns = ((ValidationPatterns) annotation).value();
            for (ValidationPattern validationPattern : validationPatterns) {
                PATTERN_HANDLER.handleAnnotation(validationPattern, map, result);
            }
        } else if (DecimalConstraints.class == annotation.annotationType()) {
            DecimalConstraint[] decimalConstraints = ((DecimalConstraints) annotation).value();
            for (DecimalConstraint decimalConstraint : decimalConstraints) {
                DECIMAL_MAX_VALUE_HANDLER.handleAnnotation(decimalConstraint, map, result);
            }
        }
    }

    /**
     * This method checks if filed has annotation from package {@link by.training.sharing.validation}.
     *
     * @param annotation Annotation of the field
     * @return <code>true</code> if field contains annotation from package {@link by.training.sharing.validation},
     * otherwise return <code>false</code>.
     */

    private boolean isConstraintAnnotation(Annotation annotation) {
        Set<Class<? extends Annotation>> set = CONSTRAINTS.stream()
                .filter(aClass -> annotation.annotationType() == aClass).collect(Collectors.toSet());
        return !set.isEmpty();
    }
}
