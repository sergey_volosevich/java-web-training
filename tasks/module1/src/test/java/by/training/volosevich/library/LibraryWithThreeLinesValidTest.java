package by.training.volosevich.library;

import by.training.volosevich.library.controller.LibraryController;
import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.GenreType;
import by.training.volosevich.library.repository.LibraryRepository;
import by.training.volosevich.library.service.LibraryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LibraryWithThreeLinesValidTest {
    private static final String FILE_NAME = "threeLinesValid.txt";

    private LibraryRepository repository;
    private LibraryService service;
    private LibraryController controller;

    @Before
    public void setUp() throws URISyntaxException {
        repository = new LibraryRepository();
        service = new LibraryService(repository);
        controller = new LibraryController(service);
        controller.saveEntityFromFile(Paths.get(getClass().getClassLoader().getResource(FILE_NAME).toURI()).toString());
    }

    @Test
    public void sumOfPagesByGenre() {
        assertEquals(110, controller.sumOfPagesByGenre(GenreType.CARS));
    }

    @Test
    public void sortIncreaseByReleaseYear() {
        List<Edition> list = controller.sortIncreaseByReleaseYear();
        assertEquals(2007, list.get(0).getReleaseYear());
        assertEquals(2019, list.get(2).getReleaseYear());
    }


    @Test
    public void sortDecreaseByReleaseYear() {
        List<Edition> list = controller.sortDecreaseByReleaseYear();
        assertEquals(2019, list.get(0).getReleaseYear());
        assertEquals(2007, list.get(2).getReleaseYear());
    }

    @Test
    public void sortByReleaseYearByNumberOfPages() {
        List<Edition> list = controller.sortByReleaseYearByNumberOfPages();
        assertEquals(35, list.get(0).getNumberOfPages());
        assertEquals(40, list.get(1).getNumberOfPages());
    }

    @Test
    public void findByReleaseYearRange() {
        List<Edition> list = controller.findByReleaseYearRange(2007, 2018);
        assertEquals(2, list.size());
    }

    @Test
    public void findByGenreTypeRangeYear() {
        List<Edition> list = controller.findByGenreTypeRangeYear(GenreType.CARS, 2007, 2010);
        assertEquals(2, list.size());
    }
}
