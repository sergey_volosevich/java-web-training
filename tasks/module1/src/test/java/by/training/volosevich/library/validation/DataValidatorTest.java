package by.training.volosevich.library.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class DataValidatorTest {

    private DataValidator dataValidator;

    @Before
    public void setUp() {
        dataValidator = new DataValidator();
    }

    @Test
    public void validateDataByEmptyMap() {
        Map<String, String> emptyMap = new HashMap<>();
        ValidationResult vr = dataValidator.validateData(emptyMap, 0);
        List<List<String>> list = new ArrayList<>(vr.getValidationInfo().values());
        assertEquals(1, list.size());
        List<String> listString = list.get(0);
        assertTrue(listString.contains(ValidationMessage.NO_DATA));
    }

    @Test
    public void validateDataByNoTypeKey() {
        Map<String, String> noTypeKey = new HashMap<>();
        noTypeKey.put("typeS", "Book");
        ValidationResult vr = dataValidator.validateData(noTypeKey, 0);
        List<List<String>> list = new ArrayList<>(vr.getValidationInfo().values());
        assertEquals(1, list.size());
        List<String> listString = list.get(0);
        assertTrue(listString.contains(ValidationMessage.NO_FIELD_TYPE));
    }

    @Test
    public void validateDataByNoTypeValue() {
        Map<String, String> noTypeValue = new HashMap<>();
        noTypeValue.put("type", "Book1");
        noTypeValue.put("nuberOfpages", "130");
        ValidationResult vr = dataValidator.validateData(noTypeValue, 0);
        List<List<String>> list = new ArrayList<>(vr.getValidationInfo().values());
        assertEquals(1, list.size());
        List<String> listString = list.get(0);
        assertTrue(listString.contains(ValidationMessage.NO_SUCH_TYPE));
    }
}