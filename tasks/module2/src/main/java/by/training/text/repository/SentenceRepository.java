package by.training.text.repository;

import by.training.text.entity.Sentence;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class SentenceRepository implements Repository<Sentence> {

    private AtomicLong idGenerator = new AtomicLong(1);

    private Map<Long, Sentence> mapSentences = new HashMap<>();

    @Override
    public long create(Sentence entity) {
        long id = idGenerator.getAndIncrement();
        entity.setId(id);
        mapSentences.put(id, entity);
        return id;
    }

    @Override
    public boolean update(Sentence entity) {
        long id = entity.getId();
        if (mapSentences.containsKey(id)) {
            mapSentences.put(id, entity);
            return true;
        }
        return false;
    }

    @Override
    public Sentence read(long id) {
        return mapSentences.get(id);
    }

    @Override
    public boolean delete(long id) {
        Sentence sentence = mapSentences.get(id);
        return mapSentences.remove(id, sentence);
    }

    @Override
    public List<Sentence> find(Specification<Sentence> specification) {
        List<Sentence> sentences = new ArrayList<>(mapSentences.values());
        List<Sentence> foundSentences = new ArrayList<>();
        for (Sentence sentence : sentences) {
            if (specification.match(sentence)) {
                foundSentences.add(sentence);
            }
        }
        return foundSentences;
    }

    @Override
    public List<Sentence> getAll() {
        return new ArrayList<>(mapSentences.values());
    }
}
