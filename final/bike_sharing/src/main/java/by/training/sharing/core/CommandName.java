package by.training.sharing.core;


import java.lang.annotation.*;

/**
 * This annotation used to mark method, which processes the client request. The method must belong to the class
 * marked with the annotation {@link Command}.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface CommandName {

    /**
     * Sets the name of command
     *
     * @return name
     */
    String name();
}
