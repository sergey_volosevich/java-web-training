package by.training.sharing.dao;

public class ConnectionPoolInitializeException extends RuntimeException {
    private static final long serialVersionUID = 3122913969640988346L;

    public ConnectionPoolInitializeException(String message) {
        super(message);
    }
}
