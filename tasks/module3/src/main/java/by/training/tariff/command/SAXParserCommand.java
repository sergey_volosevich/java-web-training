package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.Parser;

public class SAXParserCommand extends ParserCommand<LightTariff> {
    public SAXParserCommand(Parser<LightTariff> parser) {
        super(parser);
    }
}
