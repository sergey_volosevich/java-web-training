package by.training.sharing.role;

import by.training.sharing.dao.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class RoleRowMapper implements RowMapper<RoleDTO> {
    @Override
    public RoleDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        RoleDTO roleDTO = new RoleDTO();
        long id = rs.getLong("role_id");
        roleDTO.setId(id);
        String userRole = rs.getString("user_role");
        roleDTO.setRoleName(userRole);
        return roleDTO;
    }
}
