package by.training.port.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayDeque;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.ReentrantLock;

public class Port {
    private static final Integer INITIAL_PORT_SIZE = 4;
    private static Port instance;
    private static AtomicBoolean isCreated = new AtomicBoolean();
    private static ReentrantLock lock = new ReentrantLock();
    private static Condition condition = lock.newCondition();
    private static ArrayDeque<Dock> availableDocks = new ArrayDeque<>();
    private static final Logger LOGGER = LogManager.getLogger();

    private Port() {
        Warehouse warehouse = new Warehouse(1000, 1500);
        for (int i = 0; i < INITIAL_PORT_SIZE; i++) {
            availableDocks.add(new Dock((i + 1), warehouse));
        }

    }

    public static Port getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new Port();
                    isCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    public Dock getDock() {
        lock.lock();
        try {
            while (availableDocks.isEmpty()) {
                try {
                    condition.await();
                } catch (InterruptedException e) {
                    LOGGER.error("Interrupted!", e);
                    Thread.currentThread().interrupt();
                }
            }
            return availableDocks.poll();
        } finally {
            lock.unlock();
        }
    }

    public void releaseDock(Dock dock) {
        lock.lock();
        try {
            availableDocks.push(dock);
            condition.signalAll();
        } finally {
            lock.unlock();
        }
    }
}
