package by.training.port.controller;

import by.training.port.model.Ship;
import by.training.port.validation.FileValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class ShipControllerTest {
    private ShipController controller;
    private static final String VALID_FILE = "valid.txt";
    private String pathToValidFile;

    @Before
    public void setUp() throws Exception {
        pathToValidFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_FILE))
                .toURI()).toString();
        DataFileReader dataFileReader = new DataFileReader();
        LineParser parser = new LineParser();
        FileValidator validator = new FileValidator();
        Builder<Ship> builder = new ShipBuilder();
        controller = new ShipController(dataFileReader, parser, validator, builder);
    }

    @Test
    public void loadFromFile() {
        List<Ship> shipList = controller.loadFromFile(pathToValidFile);
        for (Ship ship : shipList) {
            System.out.println(ship.getTaskType());
        }
        assertEquals(6, shipList.size());
    }
}