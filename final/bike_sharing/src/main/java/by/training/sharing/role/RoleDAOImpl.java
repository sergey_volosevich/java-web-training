package by.training.sharing.role;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.JDBCTemplate;

import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;
import static by.training.sharing.core.BeanNameConstants.ROLE_DAO;

@Bean(name = ROLE_DAO)
public class RoleDAOImpl implements RoleDAO {

    private static final String SELECT_DEFAULT_ROLES = "select role_id from user_roles where user_role =" +
            " default(user_role);";
    private static final String ASSIGN_ROLE = "insert into role_relation (user_id, role_id) values (?,?);";
    private static final String SELECT_ROLES_BY_USER_ID = "select ur.role_id, ur.user_role " +
            "from user_roles ur inner join role_relation rr on rr.user_id =? and ur.role_id = rr.role_id;";

    private static final String FAILED_TO_ASSIGN_DEFAULT_ROLE = "Failed to assign default role";
    private static final String ROLES_FOR_USER_NOT_FOUND = "Roles for user with id - \'{0} =\' not found";

    private JDBCTemplate jdbcTemplate;

    public RoleDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public void assignDefaultRoles(long userId) throws DAOException {
        try {
            List<Long> roleId = jdbcTemplate.query(SELECT_DEFAULT_ROLES, (rs, rowNum) -> rs.getLong(1));
            jdbcTemplate.executeUpdate(ASSIGN_ROLE, new Object[]{userId, roleId.get(0)});
        } catch (SQLException e) {
            throw new DAOException(FAILED_TO_ASSIGN_DEFAULT_ROLE, e);
        }
    }

    @Override
    public void assignRole(long roleId, long userId) throws DAOException {
        try {
            jdbcTemplate.executeUpdate(ASSIGN_ROLE, new Object[]{userId, roleId});
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<RoleDTO> getUserRoles(long userId) throws DAOException {
        try {
            List<RoleDTO> roles = jdbcTemplate.queryForList(SELECT_ROLES_BY_USER_ID, new Object[]{userId},
                    new RoleRowMapper());
            if (roles.isEmpty()) {
                throw new RoleNotFoundException(MessageFormat.format(ROLES_FOR_USER_NOT_FOUND, userId));
            }
            return roles;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public long save(RoleDTO roleDTO) throws DAOException {
        return 0;
    }

    @Override
    public boolean update(RoleDTO roleDTO) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public RoleDTO getById(long id) throws DAOException {
        return null;
    }

    @Override
    public List<RoleDTO> findAll() throws DAOException {
        return null;
    }
}
