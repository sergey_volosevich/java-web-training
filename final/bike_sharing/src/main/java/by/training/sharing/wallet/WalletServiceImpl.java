package by.training.sharing.wallet;


import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Optional;
import java.util.Set;

import static by.training.sharing.core.BeanNameConstants.*;

@Bean(name = WALLET_SERVICE)
public class WalletServiceImpl implements WalletService {

    private static final Logger LOGGER = LogManager.getLogger(WalletServiceImpl.class);
    private static final String FAILED_TO_CREATE_WALLET = "Failed to create wallet. Cause: ";

    private WalletDAO walletDAO;
    private CreditCardDAO creditCardDAO;

    public WalletServiceImpl(@BeanQualifier(WALLET_DAO) WalletDAO walletDAO,
                             @BeanQualifier(CREDIT_CARD_DAO) CreditCardDAO creditCardDAO) {
        this.walletDAO = walletDAO;
        this.creditCardDAO = creditCardDAO;
    }

    @Override
    public Optional<WalletDTO> findByUserId(long id) throws ServiceException {
        try {
            Optional<WalletDTO> wallet = walletDAO.findByUserId(id);
            if (!wallet.isPresent()) {
                return wallet;
            }
            WalletDTO walletDTO = wallet.get();
            long walletId = walletDTO.getId();
            Set<CreditCardDTO> creditCards = creditCardDAO.findByWalletId(walletId);
            creditCards.forEach(creditCardDTO -> {
                String cardNumber = creditCardDTO.getCardNumber();
                String[] partsCardNumber = cardNumber.split(" ");
                partsCardNumber[2] = "****";
                cardNumber = String.join("", partsCardNumber);
                creditCardDTO.setCardNumber(cardNumber);
            });
            walletDTO.setCreditCards(creditCards);
            return Optional.of(walletDTO);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean createWallet(long userId) {
        try {
            walletDAO.save(userId);
            return true;
        } catch (DAOException e) {
            LOGGER.error(FAILED_TO_CREATE_WALLET, e);
            return false;
        }
    }

    @Override
    public long addCreditCard(CreditCardDTO creditCardDTO) throws ServiceException {
        try {
            long walletId = creditCardDTO.getWalletId();
            Set<CreditCardDTO> creditCards = creditCardDAO.findByWalletId(walletId);
            boolean isDuplicate = creditCards.stream()
                    .anyMatch((creditCard -> creditCard.getCardNumber().equals(creditCardDTO.getCardNumber())));
            if (isDuplicate) {
                return -1;
            } else {
                return creditCardDAO.save(creditCardDTO);
            }
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean updateBalance(WalletDTO walletDTO) {
        return walletDAO.update(walletDTO);
    }

    @Override
    public CreditCardDTO getCreditCardById(long creditCardId) throws ServiceException {
        try {
            return creditCardDAO.getById(creditCardId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Override
    public boolean deleteCard(long creditCard) {
        return creditCardDAO.delete(creditCard);
    }

    @Override
    public Set<CreditCardDTO> findCardsByWalletId(long walletId) throws ServiceException {
        try {
            return creditCardDAO.findByWalletId(walletId);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
