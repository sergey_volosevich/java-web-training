package by.training.sharing.validation;

import java.lang.annotation.*;

/**
 * This annotation allows the use of several type {@link by.training.sharing.validation.ValidationPattern} annotations
 * on one class field.
 */

@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidationPatterns {
    ValidationPattern[] value();
}
