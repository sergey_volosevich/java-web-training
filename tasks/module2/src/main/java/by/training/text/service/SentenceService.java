package by.training.text.service;

import by.training.text.entity.Sentence;
import by.training.text.repository.Repository;
import by.training.text.repository.SentenceByParagIdSpecification;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class SentenceService {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String SENTENCE_IS_NULL = "Sentence is null";
    private static final String NOT_VALID_ID = "id less or equals zero";

    private Repository<Sentence> repository;

    public SentenceService(Repository<Sentence> repository) {
        this.repository = repository;
    }

    public long create(Sentence entity) {
        if (entity != null) {
            return repository.create(entity);
        } else {
            LOGGER.log(Level.ERROR, SENTENCE_IS_NULL);
            return -1;
        }
    }

    public List<Sentence> findSentenceByParagraphId(Long id) {
        return repository.find(new SentenceByParagIdSpecification(id));
    }


    public boolean update(Sentence sentence) {
        if (sentence != null) {
            return repository.update(sentence);
        }
        LOGGER.log(Level.ERROR, SENTENCE_IS_NULL);
        return false;
    }

    public Optional<Sentence> read(long id) {
        if (id > 0) {
            return Optional.ofNullable(repository.read(id));
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return Optional.empty();
    }

    public boolean delete(long id) {
        if (id > 0) {
            return repository.delete(id);
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return false;
    }

    public List<Sentence> getAll() {
        return repository.getAll();
    }
}
