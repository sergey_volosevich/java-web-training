package by.training.sharing.bike;


import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.TransactionSupport;
import by.training.sharing.dao.Transactional;
import by.training.sharing.service.ServiceException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;

import static by.training.sharing.core.BeanNameConstants.BIKE_DAO;
import static by.training.sharing.core.BeanNameConstants.BIKE_SERVICE;

@Bean(name = BIKE_SERVICE)
@TransactionSupport
public class BikeServiceImpl implements BikeService {

    private static final Logger LOGGER = LogManager.getLogger(BikeServiceImpl.class);
    private static final String FAILED_TO_SAVE_BIKE_CAUSE = "Failed to save bike. Cause:";

    private BikeDAO bikeDAO;

    public BikeServiceImpl(@BeanQualifier(BIKE_DAO) BikeDAO bikeDAO) {
        this.bikeDAO = bikeDAO;
    }

    @Override
    public List<BikeDTO> findAllBikesByState(BikeStatus status) throws ServiceException {
        try {
            return bikeDAO.findAllWithLocationByState(BikeStatus.UNUSED);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }

    @Transactional
    @Override
    public boolean save(BikeDTO bikeDTO) {
        try {
            bikeDAO.save(bikeDTO);
            return true;
        } catch (DAOException e) {
            LOGGER.error(FAILED_TO_SAVE_BIKE_CAUSE, e);
            return false;
        }
    }

    @Override
    public BikeDTO getById(long id) throws ServiceException {
        try {
            return bikeDAO.getById(id);
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
