package by.training.sharing.core;

/**
 * This Exception is used to indicate, that object not registered in application context, and if no annotation
 * {@link BeanQualifier} on parameter in bean's constructor with parameters.
 */

public class BeanRegistrationException extends RuntimeException {

    private static final long serialVersionUID = 5328957878025695966L;

    public BeanRegistrationException(String message) {
        super(message);
    }

    public BeanRegistrationException(String message, Exception e) {
        super(message, e);
    }
}
