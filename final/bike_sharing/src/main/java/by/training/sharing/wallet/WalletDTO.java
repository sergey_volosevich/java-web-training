package by.training.sharing.wallet;

import by.training.sharing.validation.Validation;
import by.training.sharing.validation.ValidationPattern;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;
import java.util.Set;

@Validation
public class WalletDTO implements Serializable {
    private static final long serialVersionUID = -4568998135816516925L;

    private long id;
    @ValidationPattern(fieldName = "amount", message = "", regexp = "^\\d{1,6}\\.\\d{2}$")
    private BigDecimal currentAmount;
    private long userId;
    private Set<CreditCardDTO> creditCards;

    public WalletDTO() {
    }

    public WalletDTO(long id, BigDecimal currentAmount, long userId, Set<CreditCardDTO> creditCards) {
        this.id = id;
        this.currentAmount = currentAmount;
        this.userId = userId;
        this.creditCards = creditCards;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public BigDecimal getCurrentAmount() {
        return currentAmount;
    }

    public void setCurrentAmount(BigDecimal currentAmount) {
        this.currentAmount = currentAmount;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public Set<CreditCardDTO> getCreditCards() {
        return creditCards;
    }

    public void setCreditCards(Set<CreditCardDTO> creditCards) {
        this.creditCards = creditCards;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof WalletDTO)) return false;
        WalletDTO walletDTO = (WalletDTO) o;
        return getId() == walletDTO.getId() &&
                getUserId() == walletDTO.getUserId() &&
                Objects.equals(getCurrentAmount(), walletDTO.getCurrentAmount()) &&
                Objects.equals(getCreditCards(), walletDTO.getCreditCards());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getCurrentAmount(), getUserId(), getCreditCards());
    }
}
