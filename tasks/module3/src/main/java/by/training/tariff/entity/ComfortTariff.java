package by.training.tariff.entity;

import java.util.Objects;

public class ComfortTariff extends LightTariff {

    private int freeMinutesOutNetwork;

    public ComfortTariff() {
    }

    public ComfortTariff(long id, String tariffName, String dateCreation, OperatorName name, double payroll,
                         AdditionalInfo parameters, CallPrice price, int freeMinutesInNetwork, int internetTraffic,
                         int freeMinutesOutNetwork) {
        super(id, tariffName, dateCreation, name, payroll, parameters, price, freeMinutesInNetwork, internetTraffic);
        this.freeMinutesOutNetwork = freeMinutesOutNetwork;
    }

    public int getFreeMinutesOutNetwork() {
        return freeMinutesOutNetwork;
    }

    public void setFreeMinutesOutNetwork(int freeMinutesOutNetwork) {
        this.freeMinutesOutNetwork = freeMinutesOutNetwork;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ComfortTariff)) return false;
        if (!super.equals(o)) return false;
        ComfortTariff tariff = (ComfortTariff) o;
        return getFreeMinutesOutNetwork() == tariff.getFreeMinutesOutNetwork();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFreeMinutesOutNetwork());
    }

    @Override
    public String toString() {
        return "ComfortTariff{" +
                "freeMinutesOutNetwork=" + freeMinutesOutNetwork +
                "} " + super.toString();
    }
}
