package by.training.text.model;

public interface TextLeaf {
    String getString();

    void save(Long id);

    void save();
}
