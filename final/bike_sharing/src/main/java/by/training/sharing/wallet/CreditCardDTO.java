package by.training.sharing.wallet;

import by.training.sharing.validation.Validation;
import by.training.sharing.validation.ValidationPattern;

import java.time.LocalDate;
import java.util.Objects;

import static by.training.sharing.wallet.CreditCardFieldNameConstants.*;

@Validation
public class CreditCardDTO {

    @ValidationPattern(fieldName = CARD_ID, message = "creditCard.cardId.validation.msg", regexp = "^\\d+$")
    private long cardId;
    @ValidationPattern(fieldName = CARD_NUMBER, message = "creditCard.cardNumber.validation.msg",
            regexp = "^[0-9]{4}\\s[0-9]{4}\\s[0-9]{4}\\s[0-9]{4}$")
    private String cardNumber;
    @ValidationPattern(fieldName = HOLDER_NAME, message = "creditCard.holderName.validation.msg",
            regexp = "^((?:[A-Za-z]+ ?){1,3})$")
    private String holderName;

    @ValidationPattern(fieldName = EXPIRE_MONTH, message = "creditCard.expireMonth.validation.msg",
            regexp = "^([1-9]|1[012])$")
    @ValidationPattern(fieldName = EXPIRE_YEAR, message = "creditCard.expireYear.validation.msg", regexp = "^\\d{4}$")
    private LocalDate expireDate;
    @ValidationPattern(fieldName = CVV, regexp = "^[0-9]{3,4}$", message = "creditCard.cvc.validation.msg")
    private int cvv;

    private long walletId;

    public CreditCardDTO() {
    }

    public CreditCardDTO(long cardId, String cardNumber, String holderName, LocalDate expireDate, int cvv, long walletId) {
        this.cardId = cardId;
        this.cardNumber = cardNumber;
        this.holderName = holderName;
        this.expireDate = expireDate;
        this.cvv = cvv;
        this.walletId = walletId;
    }

    public long getCardId() {
        return cardId;
    }

    public void setCardId(long cardId) {
        this.cardId = cardId;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }

    public String getHolderName() {
        return holderName;
    }

    public void setHolderName(String holderName) {
        this.holderName = holderName;
    }

    public LocalDate getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(LocalDate expireDate) {
        this.expireDate = expireDate;
    }

    public int getCvv() {
        return cvv;
    }

    public void setCvv(int cvv) {
        this.cvv = cvv;
    }

    public long getWalletId() {
        return walletId;
    }

    public void setWalletId(long walletId) {
        this.walletId = walletId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CreditCardDTO)) return false;
        CreditCardDTO that = (CreditCardDTO) o;
        return getCardId() == that.getCardId() &&
                getCvv() == that.getCvv() &&
                getWalletId() == that.getWalletId() &&
                Objects.equals(getCardNumber(), that.getCardNumber()) &&
                Objects.equals(getHolderName(), that.getHolderName()) &&
                Objects.equals(getExpireDate(), that.getExpireDate());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCardId(), getCardNumber(), getHolderName(), getExpireDate(), getCvv(), getWalletId());
    }
}
