package by.training.volosevich.library.validation;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FileValidatorTest {

    private FileValidator validator;

    @Before
    public void setUp() {
        validator = new FileValidator();
    }

    @Test
    public void validateFileByNullPath() {

        assertTrue(validator.validateFile(null).getValidationInfo().get(null)
                .contains(ValidationMessage.FILE_PATH_NULL));
    }

    @Test
    public void validateFileByNoFile() {
        String directoryPath = "scr";
        assertTrue(validator.validateFile(directoryPath).getValidationInfo().get(directoryPath)
                .contains(ValidationMessage.FILE_DOES_NOT_EXIST));
    }

    @Test
    public void validateFileByEmptyFile() throws URISyntaxException {
        String pathToEmptyFile = Paths.get(getClass().getClassLoader().getResource("empty.txt").toURI()).toString();
        assertTrue(validator.validateFile(pathToEmptyFile).getValidationInfo().get(pathToEmptyFile)
                .contains(ValidationMessage.FILE_IS_EMPTY));
    }

    @Test
    public void validateFileByValidFile() throws URISyntaxException {
        String pathToValidFile = Paths.get(getClass().getClassLoader().getResource("valid.txt").toURI()).toString();
        assertTrue(validator.validateFile(pathToValidFile).isValid());
    }
}