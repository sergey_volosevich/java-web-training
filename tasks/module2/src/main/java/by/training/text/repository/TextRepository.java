package by.training.text.repository;

import by.training.text.entity.Text;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class TextRepository implements Repository<Text> {

    private AtomicLong idGenerator = new AtomicLong(1);

    private Map<Long, Text> mapTexts = new HashMap<>();

    @Override
    public long create(Text entity) {
        long id = idGenerator.getAndIncrement();
        entity.setId(id);
        mapTexts.put(id, entity);
        return id;
    }

    @Override
    public boolean update(Text entity) {
        long id = entity.getId();
        if (mapTexts.containsKey(id)) {
            mapTexts.put(id, entity);
            return true;
        }
        return false;
    }

    @Override
    public Text read(long id) {
        return mapTexts.get(id);
    }

    @Override
    public boolean delete(long id) {
        Text text = mapTexts.get(id);
        return mapTexts.remove(id, text);
    }

    @Override
    public List<Text> find(Specification<Text> specification) {
        List<Text> texts = new ArrayList<>(mapTexts.values());
        List<Text> foundTexts = new ArrayList<>();
        for (Text text : texts) {
            if (specification.match(text)) {
                foundTexts.add(text);
            }
        }
        return foundTexts;
    }

    @Override
    public List<Text> getAll() {
        return new ArrayList<>(mapTexts.values());
    }
}
