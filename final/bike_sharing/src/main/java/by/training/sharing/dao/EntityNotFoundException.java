package by.training.sharing.dao;

public class EntityNotFoundException extends DAOException {
    private static final long serialVersionUID = 111247849077392082L;

    public EntityNotFoundException(String message) {
        super(message);
    }
}
