package by.training.text.parser;

import by.training.text.entity.Text;
import by.training.text.model.EntireTextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.service.TextService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TextParser extends AbstractTextParser {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PARAGRAPH_REGEX = "(?:\\t[A-Z])(?:.\\n*)+?(?:[.?!]+\\n)";
    private static final String NO_PARAGRAPHS_IN_TEXT = "No paragraphs in text";

    private TextService service;

    public TextParser(TextService service) {
        this.service = service;
    }

    @Override
    public TextLeaf parse(String line) {
        Text text = new Text();
        EntireTextComposite textComposite = new EntireTextComposite(text, service);
        Pattern pattern = Pattern.compile(PARAGRAPH_REGEX);
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            TextLeaf paragraph = nextParse(matcher.group());
            textComposite.addText(paragraph);
        }
        if (textComposite.getComponents().isEmpty()) {
            LOGGER.log(Level.WARN, NO_PARAGRAPHS_IN_TEXT);
            return nextParse(line);
        }
        return textComposite;
    }
}
