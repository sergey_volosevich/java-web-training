<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ attribute name="value" required="true" rtexprvalue="true" %>
<%@ tag import="by.training.sharing.ApplicationConstants" %>

<c:url value="" var="command">
    <c:param name="${ApplicationConstants.CMD_REQ_PARAMETER}" value="${value}"/>
</c:url>
<c:out value="${command}"/>