package by.training.tariff.validator;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class FileValidatorTest {
    private static final String VALID_FILE = "valid.xml";
    private static final String EMPTY_FILE = "empty.xml";
    private static final String DIRECTORY_PATH = "src\\";

    @Test
    public void validateValidFile() throws URISyntaxException {
        String pathFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_FILE))
                .toURI()).toString();
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(pathFile);
        assertTrue(result.isValid());

    }

    @Test
    public void validateEmptyFile() throws URISyntaxException {
        String pathFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(EMPTY_FILE))
                .toURI()).toString();
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(pathFile);
        assertFalse(result.isValid());
    }

    @Test
    public void validateDirectoryPath() {
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(Paths.get(DIRECTORY_PATH).toFile().getAbsolutePath());
        assertFalse(result.isValid());
    }
}