package by.training.sharing.bike;

/**
 * This class contains constants that are used for the common namespace on the display forms
 * Example <input type="text" name="SERIAL_NUMBER" value=""> and for retrieving
 * request parameters and validating the input parameters used by{@link by.training.sharing.validation.ValidatorImpl}.
 */

public class BikeFiledNameConstants {

    public static final String BIKE_ID = "bikeId";
    public static final String SERIAL_NUMBER = "serialNumber";
    public static final String BIKE_PASSWORD = "bikePassword";
    public static final String LATITUDE = "latitude";
    public static final String LONGITUDE = "longitude";
    public static final String PRICE_ID = "priceId";

    private BikeFiledNameConstants() {
    }
}
