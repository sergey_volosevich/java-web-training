package by.training.sharing.wallet;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Optional;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.assertEquals;

public class WalletDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(WalletDAOImpl.class);
    private WalletDAO walletDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace);) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        TransactionManagerImpl transactionManager = new TransactionManagerImpl();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        walletDAO = new WalletDAOImpl(jdbcTemplate);
    }

    @Test
    public void save() throws DAOException {
        long save = walletDAO.save(4);
        assertEquals(3, save);
    }

    @Test
    public void update() throws DAOException {
        WalletDTO walletDTO = new WalletDTO(1, BigDecimal.valueOf(10.00), 2, null);
        walletDAO.update(walletDTO);
        final WalletDTO wallet = walletDAO.getById(1);
        assertEquals(0, BigDecimal.valueOf(10.00).compareTo(wallet.getCurrentAmount()));
    }

    @Test
    public void getById() throws DAOException {
        final WalletDTO walletDTO = walletDAO.getById(1);
        assertEquals(1, walletDTO.getId());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByInValidId() throws DAOException {
        walletDAO.getById(10);
    }

    @Test
    public void findByUserId() throws DAOException {
        Optional<WalletDTO> optionalWalletDTO = walletDAO.findByUserId(2);
        WalletDTO walletDTO = optionalWalletDTO.get();
        assertEquals(10.00, walletDTO.getCurrentAmount().doubleValue(), 0.00);
    }
}