package by.training.volosevich.library.entity;

import java.time.Month;
import java.util.Objects;

public class Magazine extends PeriodicalEdition {

    private Month releaseMonth;

    public Magazine(String name, GenreType genreType, int numberOfPages, String publishingHouse, int releaseYear,
                    EditionType type, PeriodicityType periodicity, int releaseNumber,
                    int yearReleaseNumber, Month releaseMonth) {
        super(name, genreType, numberOfPages, publishingHouse, releaseYear, type, periodicity, releaseNumber,
                yearReleaseNumber);
        this.releaseMonth = releaseMonth;
    }

    public Month getReleaseMonth() {
        return releaseMonth;
    }

    public void setReleaseMonth(Month releaseMonth) {
        this.releaseMonth = releaseMonth;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Magazine)) return false;
        if (!super.equals(o)) return false;
        Magazine magazine = (Magazine) o;
        return getReleaseMonth() == magazine.getReleaseMonth();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getReleaseMonth());
    }

    @Override
    public String toString() {
        return "Magazine{" +
                "releaseMonth=" + releaseMonth +
                "} " + super.toString();
    }
}
