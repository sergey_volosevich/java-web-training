package by.training.sharing.core;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Bean(name = "TestCommand")
@Command
public class TestController {

    private TestService testService;

    public TestController(@BeanQualifier("TestService") TestService testService) {
        this.testService = testService;
    }

    @CommandName(name = "execute")
    public void execute(HttpServletRequest request, HttpServletResponse response) {

    }

    public boolean checkMethod() {
        return testService.checkMethod();
    }
}
