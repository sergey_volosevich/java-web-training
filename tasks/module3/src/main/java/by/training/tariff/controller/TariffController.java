package by.training.tariff.controller;

import by.training.tariff.command.Command;
import by.training.tariff.command.CommandException;
import by.training.tariff.command.CommandProvider;
import by.training.tariff.entity.LightTariff;
import by.training.tariff.service.TariffService;
import by.training.tariff.validator.FileValidator;
import by.training.tariff.validator.ValidationResult;
import by.training.tariff.validator.XMLValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.NavigableSet;

public class TariffController {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String FILE_INVALID = "Can not upload file. File invalid. Reason: {}";
    private static final String CANNOT_PARSE_FILE = "Cannot parse file";
    private static final String INVALID_FILE_REASON = "Invalid file. Reason: {}";

    private TariffService service;
    private FileValidator fileValidator;
    private XMLValidator xmlValidator;
    private CommandProvider<LightTariff> commandProvider;


    public TariffController(TariffService service, FileValidator fileValidator, XMLValidator xmlValidator,
                            CommandProvider<LightTariff> commandProvider) {
        this.service = service;
        this.fileValidator = fileValidator;
        this.xmlValidator = xmlValidator;
        this.commandProvider = commandProvider;
    }

    public boolean uploadFile(String pathFile, String command) {
        ValidationResult validationResult = fileValidator.validateFile(pathFile);
        if (!validationResult.isValid()) {
            LOGGER.error(FILE_INVALID, validationResult);
            return false;
        }
        validationResult = xmlValidator.validate(pathFile);
        if (!validationResult.isValid()) {
            LOGGER.error(INVALID_FILE_REASON, validationResult);
            return false;
        }
        Command<LightTariff> tariffCommand = commandProvider.getCommand(command);
        List<LightTariff> tariffs;
        try {
            tariffs = tariffCommand.execute(pathFile);
        } catch (CommandException e) {
            LOGGER.error(CANNOT_PARSE_FILE, e);
            return false;
        }
        tariffs.forEach(a -> service.save(a));
        return true;
    }
}
