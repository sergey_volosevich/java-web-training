package by.training.sharing.security;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

/**
 * Objects of this class represent a unique user identifier. Used in
 * {@link by.training.sharing.security.SecurityServiceImpl} to store users who are logged in, and to determine
 * user privileges.
 */

public class UserCredential implements Serializable {
    private static final long serialVersionUID = -8352718823810662740L;

    private long userId;
    private List<String> roles;

    public UserCredential() {
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof UserCredential)) return false;
        UserCredential that = (UserCredential) o;
        return getUserId() == that.getUserId() &&
                Objects.equals(getRoles(), that.getRoles());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getUserId(), getRoles());
    }
}
