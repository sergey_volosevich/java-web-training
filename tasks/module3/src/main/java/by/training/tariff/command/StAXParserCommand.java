package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.Parser;

public class StAXParserCommand extends ParserCommand<LightTariff> {
    public StAXParserCommand(Parser<LightTariff> parser) {
        super(parser);
    }
}
