package by.training.sharing.security;

import by.training.sharing.util.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Object of this class represents a security context in which all application commands with access
 * rights to them are stored.
 */

public class SecurityContext {

    private static final Logger LOGGER = LogManager.getLogger(SecurityContext.class);

    private static final String SECURITY_PROPERTIES = "security.properties";
    private static final String SECURITY_CONTEXT_SUCCESSFUL_INITIALIZED = "Security context successful initialized";
    private static final String SECURITY_CONTEXT_SUCCESSFUL_DESTROYED = "Security context successful destroyed";
    private static SecurityContext instance;
    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean isCreated = new AtomicBoolean();
    private final Properties accessRestrictions;

    private SecurityContext() {
        try {
            accessRestrictions = PropertiesReader.readProperties(SECURITY_PROPERTIES);
            if (accessRestrictions == null) {
                throw new SecurityContextInitializeException("Failed to initialize security context");
            }
        } catch (IOException e) {
            throw new SecurityContextInitializeException(e);
        }
        LOGGER.info(SECURITY_CONTEXT_SUCCESSFUL_INITIALIZED);
    }

    public static SecurityContext getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new SecurityContext();
                    isCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    String getAccessRestriction(String commandName) {
        return accessRestrictions.getProperty(commandName);
    }

    public void destroy() {
        accessRestrictions.clear();
        LOGGER.info(SECURITY_CONTEXT_SUCCESSFUL_DESTROYED);
    }
}
