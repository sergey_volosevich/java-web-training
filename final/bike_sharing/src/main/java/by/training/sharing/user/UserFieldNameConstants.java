package by.training.sharing.user;

/**
 * This class contains constants that are used for the common namespace on the display forms
 * Example <input type="text" name="USER_LOGIN" value=""> and for retrieving
 * request parameters and validating the input parameters used by{@link by.training.sharing.validation.ValidatorImpl}.
 */

public class UserFieldNameConstants {
    public static final String USER_LOGIN = "userName";
    public static final String USER_PASSWORD = "password";
    public static final String USER_NAME = "name";
    public static final String USER_LAST_NAME = "lastName";
    public static final String USER_EMAIL = "email";
    public static final String USER_PHONE = "phone";

    private UserFieldNameConstants() {
    }
}
