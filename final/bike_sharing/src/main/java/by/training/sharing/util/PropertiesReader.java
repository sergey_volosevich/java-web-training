package by.training.sharing.util;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URL;
import java.util.Properties;

/**
 * This util class is used for reading properties from file.
 */

public class PropertiesReader {

    private PropertiesReader() {
    }

    /**
     * Read properties from file.
     *
     * @param fileName Name of property file
     * @return {@link java.util.Properties} with information from file, otherwise return <code>null</code>
     * @throws IOException if an error occurred when reading from the input stream.
     */

    public static Properties readProperties(String fileName) throws IOException {
        Properties properties = new Properties();
        URL resource = PropertiesReader.class.getClassLoader().getResource(fileName);
        if (resource != null) {
            File file = new File(resource.getFile());
            try (FileReader reader = new FileReader(file)) {
                properties.load(reader);
            }
            return properties;
        }
        return null;
    }
}
