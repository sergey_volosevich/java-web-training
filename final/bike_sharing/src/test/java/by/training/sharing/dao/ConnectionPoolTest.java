package by.training.sharing.dao;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.ConcurrentSkipListSet;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

@RunWith(JUnit4.class)
public class ConnectionPoolTest {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private static final int N_THREADS = 15;
    private static final String GET_CONNECTION = "Try to get connection";
    private static final String WORKING_WITH_CONNECTION = "working with connection...";
    private static final String RELEASE_CONNECTION = "release connection: {}";

    @Test
    public void getInstance() {
        ExecutorService executorService = Executors.newFixedThreadPool(N_THREADS);
        Set<Integer> hashCodes = new ConcurrentSkipListSet<>();
        IntStream.range(0, N_THREADS).forEach(i -> executorService.submit(() -> {
            sleep();
            LOGGER.info(GET_CONNECTION);
            try (Connection connection = ConnectionPool.getInstance().getConnection()) {
                LOGGER.info(WORKING_WITH_CONNECTION);
                sleep();
                int hashCode = connection.hashCode();
                hashCodes.add(hashCode);
                LOGGER.info(RELEASE_CONNECTION, hashCode);
            } catch (SQLException | IllegalStateException e) {
                LOGGER.error(e);
            }
        }));

        try {
            executorService.awaitTermination(6, TimeUnit.SECONDS);
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
        Assert.assertEquals(5, hashCodes.size());
    }

    private void sleep() {
        try {
            TimeUnit.MILLISECONDS.sleep(Math.abs(new Random().nextInt(1000)));
        } catch (InterruptedException e) {
            LOGGER.error(e);
        }
    }
}