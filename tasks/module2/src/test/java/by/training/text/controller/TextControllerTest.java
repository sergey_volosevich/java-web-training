package by.training.text.controller;

import by.training.text.model.TextComposite;
import by.training.text.model.TextLeaf;
import by.training.text.parser.*;
import by.training.text.reader.TextFileReader;
import by.training.text.repository.ParagraphRepository;
import by.training.text.repository.SentenceRepository;
import by.training.text.repository.TextRepository;
import by.training.text.repository.WordRepository;
import by.training.text.service.*;
import by.training.text.validator.FileValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class TextControllerTest {

    private static final String FILE_NAME = "validFile.txt";
    private TextController controller;
    private String pathFile;
    private TextRepository textRepository;
    private ParagraphRepository paragraphRepository;
    private SentenceRepository sentenceRepository;
    private WordRepository wordRepository;

    @Before
    public void setUp() throws Exception {
        textRepository = new TextRepository();
        paragraphRepository = new ParagraphRepository();
        sentenceRepository = new SentenceRepository();
        wordRepository = new WordRepository();
        TextService textService = new TextService(textRepository);
        ParagraphService paragraphService = new ParagraphService(paragraphRepository);
        SentenceService sentenceService = new SentenceService(sentenceRepository);
        WordService wordService = new WordService(wordRepository);
        CompositeTextService service = new CompositeTextService(paragraphService, sentenceService);
        FileValidator validator = new FileValidator();
        TextFileReader reader = new TextFileReader();
        WordParser wordParser = new WordParser(wordService);
        SentenceParser sentenceParser = new SentenceParser(sentenceService);
        ParagraphParser paragraphParser = new ParagraphParser(paragraphService);
        ParserChain<TextLeaf> parserChain = new TextParser(textService);
        parserChain.linkWith(paragraphParser).linkWith(sentenceParser).linkWith(wordParser);
        controller = new TextController(validator, reader, parserChain, service);
        pathFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(FILE_NAME))
                .toURI()).toString();
    }

    @Test
    public void saveTextFromFile() {
        controller.saveTextFromFile(pathFile);
        assertEquals(1, textRepository.getAll().size());
        assertEquals(4, paragraphRepository.getAll().size());
        assertEquals(6, sentenceRepository.getAll().size());
        assertEquals(119, wordRepository.getAll().size());
    }

    @Test
    public void sortParagraphsIncreasingOrder() {
        controller.saveTextFromFile(pathFile);
        List<TextComposite> textComposites = controller.sortParagraphsIncreasingOrder(1L);
        assertEquals(2, textComposites.get(2).getComponents().size());
    }

    @Test
    public void sortParagraphsDecreasingOrder() {
        controller.saveTextFromFile(pathFile);
        List<TextComposite> textComposites = controller.sortParagraphsDecreasingOrder(1L);
        assertEquals(1, textComposites.get(2).getComponents().size());
    }
}