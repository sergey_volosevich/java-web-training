package by.training.port.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Random;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Dock {
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String WAREHOUSE_HAS_MAX_LOAD = "Warehouse has max load. Current capacity = {}";
    private static final String LOAD_CONTAINERS_TO_SHIP = "Load containers to ship-{}";
    private static final String CONTAINERS_LOADED = "All containers are load to ship-{}";
    private static final String UNLOAD_CONTAINERS_FROM_SHIP = "Unload containers from ship-{} to warehouse.";
    private static final String CONTAINERS_UNLOADED = "All containers are unload to ship-{}";

    private long id;
    private final Warehouse warehouse;
    private Random random = new Random();

    public Dock(long id, Warehouse warehouse) {
        this.id = id;
        this.warehouse = warehouse;
    }

    public void doTask(Ship ship) {
        int canUnload = ship.getCurrentNumContainer() + warehouse.getStore().get();
        TaskType taskType = ship.getTaskType();
        if (taskType.equals(TaskType.LOAD)) {
            loadContainers(ship);
        } else if (taskType.equals(TaskType.UNLOAD) || taskType.equals(TaskType.UNLOAD_AND_LOAD)) {
            if (canUnload < warehouse.getMaxCapacity()) {
                if (taskType.equals(TaskType.UNLOAD)) {
                    unloadContainers(ship);
                } else {
                    unloadContainers(ship);
                    loadContainers(ship);
                }
            } else {
                LOGGER.info(WAREHOUSE_HAS_MAX_LOAD, warehouse.getStore().get());
            }
        }
    }

    public long getId() {
        return id;
    }

    private void loadContainers(Ship ship) {
        int numContainer = ship.getCurrentNumContainer();
        int maxCapacity = ship.getMaxCapacity();
        int loadedContainers = random.nextInt(maxCapacity - numContainer + 1) + numContainer;
        LOGGER.info(LOAD_CONTAINERS_TO_SHIP, ship.getId());
        while (ship.getCurrentNumContainer() != loadedContainers) {
            AtomicInteger store = warehouse.getStore();
            store.getAndDecrement();
            ship.setCurrentNumContainer(++numContainer);
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.error("Interrupted!", e);
                Thread.currentThread().interrupt();
            }
        }
        LOGGER.info(CONTAINERS_LOADED, ship.getId());
    }

    private void unloadContainers(Ship ship) {
        int numContainer = ship.getCurrentNumContainer();
        LOGGER.info(UNLOAD_CONTAINERS_FROM_SHIP, ship.getId());
        while (numContainer != 0) {
            AtomicInteger store = warehouse.getStore();
            store.getAndIncrement();
            ship.setCurrentNumContainer(--numContainer);
            try {
                TimeUnit.MILLISECONDS.sleep(100);
            } catch (InterruptedException e) {
                LOGGER.error("Interrupted!", e);
                Thread.currentThread().interrupt();
            }
        }
        LOGGER.info(CONTAINERS_UNLOADED, ship.getId());
    }
}
