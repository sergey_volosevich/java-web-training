package by.training.tariff.validator;

import org.xml.sax.ErrorHandler;
import org.xml.sax.SAXParseException;

public class XMLErrorHandler implements ErrorHandler {

    private static final String ERROR_CODE = "line %s: column %s";

    private ValidationResult validationResult = new ValidationResult();

    @Override
    public void warning(SAXParseException exception) {
        validationResult.setValidationInfo(String.format(ERROR_CODE, exception.getLineNumber(),
                exception.getColumnNumber()), exception.getMessage());
    }

    @Override
    public void error(SAXParseException exception) {
        validationResult.setValidationInfo(String.format(ERROR_CODE, exception.getLineNumber(),
                exception.getColumnNumber()), exception.getMessage());
    }

    @Override
    public void fatalError(SAXParseException exception) {
        validationResult.setValidationInfo(String.format(ERROR_CODE, exception.getLineNumber(),
                exception.getColumnNumber()), exception.getMessage());
    }

    public ValidationResult getValidationResult() {
        return validationResult;
    }
}
