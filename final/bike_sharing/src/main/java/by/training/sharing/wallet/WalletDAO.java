package by.training.sharing.wallet;

import by.training.sharing.dao.CRUDDao;
import by.training.sharing.dao.DAOException;

import java.util.Optional;

public interface WalletDAO extends CRUDDao<WalletDTO> {

    Optional<WalletDTO> findByUserId(long id) throws DAOException;

    long save(long userId) throws DAOException;
}
