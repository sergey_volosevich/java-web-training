package by.training.tariff.repository;

import by.training.tariff.entity.LightTariff;

import java.util.ArrayList;
import java.util.List;

public class TariffRepository implements Repository<LightTariff> {


    private List<LightTariff> tariffs = new ArrayList<>();

    @Override
    public void create(LightTariff entity) {
        tariffs.add(entity);
    }

    @Override
    public boolean update(LightTariff entity) {
        return false;
    }

    @Override
    public LightTariff read(long id) {
        return null;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public List<LightTariff> find(Specification<LightTariff> specification) {
        return null;
    }

    @Override
    public List<LightTariff> getAll() {
        return new ArrayList<>(tariffs);
    }
}
