package by.training.sharing.service;

public class ServiceException extends Exception {
    private static final long serialVersionUID = -5152941814274098864L;

    public ServiceException(Throwable cause) {
        super(cause);
    }
}
