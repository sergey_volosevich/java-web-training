package by.training.port.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.HashMap;
import java.util.Map;

public class LineParser {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final String FIRST_SEPARATOR = ",";
    private static final String SECOND_SEPARATOR = ":";

    public Map<String, String> parseLine(String line) {
        LOGGER.log(Level.DEBUG, () -> "start parse line - " + line);
        Map<String, String> parseResult = new HashMap<>();
        if (line == null) {
            LOGGER.log(Level.ERROR, "line value - null");
            return parseResult;
        }
        String[] arrayKeyValue = line.split(FIRST_SEPARATOR);
        String[] keyValue;
        for (String s : arrayKeyValue) {
            keyValue = s.split(SECOND_SEPARATOR);
            if (keyValue.length == 2) {
                parseResult.put(keyValue[0].trim(), keyValue[keyValue.length - 1].trim());
                LOGGER.log(Level.DEBUG, () -> "successfully saved key and value pair");
            } else {
                LOGGER.log(Level.WARN, () -> "cannot match pair \"key:value\"");
            }
        }
        LOGGER.log(Level.DEBUG, () -> "end parse line with values - " + parseResult);
        return parseResult;
    }
}
