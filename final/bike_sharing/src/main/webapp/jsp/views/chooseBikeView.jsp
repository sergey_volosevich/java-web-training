<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--12-col mdl-cell--middle">
        <div class="text-center text-light bg-dark">
            <h1><fmt:message key="choose.bike.header"/></h1>
            <div id="map"></div>
            <div class="pt-2 d-flex justify-content-between">
                <a class="btn btn-outline-light ref-full-width" role="button"
                   href="<bsh:command value="welcomeView"/>">
                    <fmt:message key="form.back.button"/></a>
            </div>
        </div>
        <script async defer src="static/style/js/sharing-maps.js"></script>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxrOiL-gcZdZtrLdzpwyEzXoXZJVwVLa0&callback=initMap">
        </script>
    </div>
</div>
