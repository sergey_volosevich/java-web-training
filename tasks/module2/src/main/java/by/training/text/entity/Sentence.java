package by.training.text.entity;

public class Sentence {

    private long id;
    private long paragraphId;

    public Sentence() {
    }

    public Sentence(long id, long paragraphId) {
        this.id = id;
        this.paragraphId = paragraphId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getParagraphId() {
        return paragraphId;
    }

    public void setParagraphId(long paragraphId) {
        this.paragraphId = paragraphId;
    }
}
