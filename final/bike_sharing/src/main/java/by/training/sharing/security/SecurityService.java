package by.training.sharing.security;

import by.training.sharing.user.UserDTO;

import javax.servlet.http.HttpSession;

public interface SecurityService {

    boolean loginUser(HttpSession session, UserDTO userDTO) throws SecurityException;

    void logoutUser(HttpSession session);

    UserCredential getCurrentUser(HttpSession session) throws CredentialNotFoundException;

    int canExecute(String commandName, HttpSession session);
}
