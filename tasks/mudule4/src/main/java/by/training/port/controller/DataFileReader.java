package by.training.port.controller;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class DataFileReader {

    public List<String> readData(String pathFile) throws IOException {
        return Files.readAllLines(Paths.get(pathFile), StandardCharsets.UTF_8);
    }
}
