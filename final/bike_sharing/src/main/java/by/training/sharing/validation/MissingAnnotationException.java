package by.training.sharing.validation;

/**
 * This exception is used to indicate, that the passed class does not have annotation {@link Validation} or none of its
 * fields contains annotations from {@link by.training.sharing.validation}.
 */

class MissingAnnotationException extends RuntimeException {

    private static final long serialVersionUID = -1989191468388654999L;

    public MissingAnnotationException(String message) {
        super(message);
    }

}
