package by.training.sharing.core;


public class NotUniqueBeanException extends BeanRegistrationException {

    private static final long serialVersionUID = 1290352266444787563L;

    public NotUniqueBeanException(String message) {
        super(message);
    }


}
