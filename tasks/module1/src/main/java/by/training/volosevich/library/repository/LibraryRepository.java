package by.training.volosevich.library.repository;

import by.training.volosevich.library.entity.Edition;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;

public class LibraryRepository implements Repository<Edition> {

    private static final Logger LOGGER = LogManager.getLogger();

    private List<Edition> library = new ArrayList<>();

    @Override
    public void add(Edition entity) {
        library.add(entity);
    }

    @Override
    public void remove(Edition entity) {
        library.remove(entity);
    }

    @Override
    public List<Edition> getAll() {
        return new ArrayList<>(this.library);
    }

    @Override
    public List<Edition> find(Specification<Edition> specification) {
        LOGGER.log(Level.DEBUG, "Start find by specification");
        List<Edition> foundEditions = new ArrayList<>();
        for (Edition edition : library) {
            if (specification.match(edition)) {
                foundEditions.add(edition);
            }
        }
        LOGGER.log(Level.DEBUG, () -> "Find values " + foundEditions);
        return foundEditions;
    }
}
