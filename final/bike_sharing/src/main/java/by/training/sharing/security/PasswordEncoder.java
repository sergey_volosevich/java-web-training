package by.training.sharing.security;

import by.training.sharing.core.Bean;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.KeySpec;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Base64;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static by.training.sharing.core.BeanNameConstants.PASSWORD_ENCODER;


@Bean(name = PASSWORD_ENCODER)
public class PasswordEncoder {

    /**
     * Each token produced by this class uses this identifier as a prefix.
     */
    private static final String ID = "$93$";

    /**
     * The minimum recommended cost, used by default
     */
    private static final int DEFAULT_COST = 16;

    private static final String ALGORITHM = "PBKDF2WithHmacSHA1";

    private static final int SIZE = 128;

    private static final Pattern layout = Pattern.compile("\\$93\\$(\\d\\d?)\\$(.{43})");
    private static final String MISSING_ALGORITHM = "Missing algorithm: {0}";
    private static final String INVALID_SECRET_KEY_FACTORY = "Invalid SecretKeyFactory";
    private static final String INVALID_TOKEN_FORMAT = "Invalid token format";

    private final SecureRandom random;
    private final int cost;

    public PasswordEncoder() {
        this.cost = DEFAULT_COST;
        this.random = new SecureRandom();
    }

    private int iterations(int cost) {
        if ((cost < 0) || (cost > 30))
            throw new IllegalArgumentException("cost: " + cost);
        return 1 << cost;
    }

    private byte[] pbkdf2(char[] password, byte[] salt, int iterations) {
        KeySpec spec = new PBEKeySpec(password, salt, iterations, SIZE);
        try {
            SecretKeyFactory f = SecretKeyFactory.getInstance(ALGORITHM);
            return f.generateSecret(spec).getEncoded();
        } catch (NoSuchAlgorithmException ex) {
            throw new IllegalStateException(MessageFormat.format(MISSING_ALGORITHM, ALGORITHM), ex);
        } catch (InvalidKeySpecException ex) {
            throw new IllegalStateException(INVALID_SECRET_KEY_FACTORY, ex);
        }
    }

    /**
     * Hash a password for storage.
     *
     * @return a secure authentication token to be stored for later authentication
     */
    public String hash(char[] password) {
        byte[] salt = new byte[SIZE / 8];
        random.nextBytes(salt);
        byte[] dk = pbkdf2(password, salt, 1 << cost);
        byte[] hash = new byte[salt.length + dk.length];
        System.arraycopy(salt, 0, hash, 0, salt.length);
        System.arraycopy(dk, 0, hash, salt.length, dk.length);
        Base64.Encoder enc = Base64.getUrlEncoder().withoutPadding();
        return ID + cost + '$' + enc.encodeToString(hash);
    }

    /**
     * Authenticate with a password and a stored password token.
     *
     * @return true if the password and token match
     */
    public boolean authenticate(char[] password, String token) {
        Matcher m = layout.matcher(token);
        if (!m.matches()) {
            throw new IllegalArgumentException(INVALID_TOKEN_FORMAT);
        }
        int iterations = iterations(Integer.parseInt(m.group(1)));
        byte[] hash = Base64.getUrlDecoder().decode(m.group(2));
        byte[] salt = Arrays.copyOfRange(hash, 0, SIZE / 8);
        byte[] check = pbkdf2(password, salt, iterations);
        int zero = 0;
        for (int idx = 0; idx < check.length; ++idx) {
            zero |= hash[salt.length + idx] ^ check[idx];
        }
        return zero == 0;
    }
}
