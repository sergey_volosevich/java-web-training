package by.training.tariff.validator;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class FileValidator {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String ERROR_CODE_FILE = "file";
    private static final String ERROR_PATH_CODE = "path";

    public ValidationResult validateFile(String pathFile) {
        ValidationResult validationResult = new ValidationResult();
        if (pathFile == null) {
            validationResult.setValidationInfo(ERROR_PATH_CODE, "Path is null");
            LOGGER.log(Level.ERROR, "Path is null");
            return validationResult;
        }
        Path path = Paths.get(pathFile);
        try {
            if (path.toFile().isDirectory()) {
                validationResult.setValidationInfo(ERROR_PATH_CODE, "File not found");
                LOGGER.log(Level.ERROR, "File not found");
                return validationResult;
            }
            if (!Files.isReadable(path)) {
                validationResult.setValidationInfo(ERROR_CODE_FILE, "File is not readable");
                LOGGER.log(Level.ERROR, "Can not read the file");
                return validationResult;
            }
            if (Files.size(Paths.get(pathFile)) == 0) {
                validationResult.setValidationInfo(ERROR_CODE_FILE, "File is empty");
                LOGGER.log(Level.ERROR, "File is empty");
            }
        } catch (IOException e) {
            validationResult.setValidationInfo(ERROR_CODE_FILE, "File unreachable");
            LOGGER.log(Level.ERROR, "File unreachable", e);
        }
        return validationResult;
    }
}
