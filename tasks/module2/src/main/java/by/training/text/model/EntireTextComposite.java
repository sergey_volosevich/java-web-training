package by.training.text.model;

import by.training.text.entity.Text;
import by.training.text.service.TextService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class EntireTextComposite implements TextComposite {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String STARTING_PARAGRAPH = "\t";
    private static final String MESSAGE = "method save(Long id) not supported";

    private List<TextLeaf> paragraphs = new LinkedList<>();
    private Text text;
    private TextService service;

    public EntireTextComposite(Text text, TextService service) {
        this.text = text;
        this.service = service;
    }

    @Override
    public void addText(TextLeaf textLeaf) {
        paragraphs.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getComponents() {
        return paragraphs;
    }

    @Override
    public String getString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (TextLeaf paragraph : paragraphs) {
            stringBuilder.append(STARTING_PARAGRAPH).append(paragraph.getString());
        }
        return stringBuilder.toString();
    }

    @Override
    public void save(Long id) {
        LOGGER.log(Level.ERROR, MESSAGE);
    }

    @Override
    public void save() {
        long textId = service.create(text);
        for (TextLeaf paragraph : paragraphs) {
            paragraph.save(textId);
        }
    }
}

