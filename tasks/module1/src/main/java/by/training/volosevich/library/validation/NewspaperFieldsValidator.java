package by.training.volosevich.library.validation;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.DayOfWeek;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class NewspaperFieldsValidator extends PeriodicalEditionFieldsValidator {

    private static final Logger LOGGER = LogManager.getLogger();

    private List<String> fields = new ArrayList<>();

    public NewspaperFieldsValidator() {
        fields.add("releaseDay");
    }

    public List<String> validate(Map<String, String> values) {
        validatePeriodicalEditionFields(values);
        for (String field : fields) {
            if (values.containsKey(field)) {
                String value = values.getOrDefault(field, null);
                if ("releaseDay".equals(field)) {
                    validateReleaseDay(field, value);
                }
            } else {
                errorMessages.add(ValidationMessage.formatMessage(field, ValidationMessage.NOT_ENOUGH_FIELD));
                LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(field,
                        ValidationMessage.NOT_ENOUGH_FIELD));
            }
        }
        return errorMessages;
    }

    private void validateReleaseDay(String nameField, String value) {
        DayOfWeek[] allTypes = DayOfWeek.values();
        int count = 0;
        for (DayOfWeek type : allTypes) {
            if (type.name().equalsIgnoreCase(value)) {
                count++;
            }
        }
        if (count == 0) {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_DAY));
            LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_DAY));
        }
    }
}
