package by.training.text.validator;

import org.junit.Test;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class FileValidatorTest {

    private static final String VALID_FILE = "validFile.txt";
    private static final String EMPTY_FILE = "emptyFile.txt";
    private static final String DIRECTORY_PATH = "src\\";

    @Test
    public void validateValidFile() throws URISyntaxException {
        String pathFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_FILE))
                .toURI()).toString();
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(pathFile);
        assertTrue(result.isValid());

    }

    @Test
    public void validateEmptyFile() throws URISyntaxException {
        String pathFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(EMPTY_FILE))
                .toURI()).toString();
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(pathFile);
        assertFalse(result.isValid());
    }

    @Test
    public void validateDirectoryPath() {
        FileValidator validator = new FileValidator();
        ValidationResult result = validator.validateFile(Paths.get(DIRECTORY_PATH).toFile().getAbsolutePath());
        assertFalse(result.isValid());
    }
}