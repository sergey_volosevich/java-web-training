package by.training.volosevich.library.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum EditionType {
    BOOK,
    MAGAZINE,
    NEWSPAPER;

    public static Optional<EditionType> fromString(String type) {

        return Stream.of(EditionType.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
