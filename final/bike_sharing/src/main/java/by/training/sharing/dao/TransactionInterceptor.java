package by.training.sharing.dao;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanInterceptor;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.core.Interceptor;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Method;
import java.sql.SQLException;
import java.util.Arrays;

import static by.training.sharing.core.BeanNameConstants.TRANSACTION_INTERCEPTOR;

@Bean(name = TRANSACTION_INTERCEPTOR)
@Interceptor(clazz = TransactionSupport.class)
public class TransactionInterceptor implements BeanInterceptor {

    private static final Logger LOGGER = LogManager.getLogger(TransactionInterceptor.class);
    private static final String TRANSACTION_BEGINS = "Transaction begins";
    private static final String TRANSACTION_SUCCESS = "Transaction success";
    private static final String TRANSACTION_FAILED = "Transaction failed";
    private static final String FAILED_TO_ROLLBACK_TRANSACTION = "Failed to rollback transaction";
    private static final String FAILED_TO_COMMIT_TRANSACTION = "Failed to commit transaction";
    private static final String FAILED_TO_BEGIN_TRANSACTION = "Failed to begin transaction";
    private TransactionManager transactionManager;

    public TransactionInterceptor(@BeanQualifier("TransactionManager") TransactionManager transactionManager) {
        this.transactionManager = transactionManager;
    }

    @Override
    public void before(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.beginTransaction();
                LOGGER.debug(TRANSACTION_BEGINS);
            } catch (SQLException e) {
                throw new IllegalStateException(FAILED_TO_BEGIN_TRANSACTION, e);
            }
        }
    }

    @Override
    public void success(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.commitTransaction();
                LOGGER.debug(TRANSACTION_SUCCESS);
            } catch (SQLException e) {
                throw new IllegalStateException(FAILED_TO_COMMIT_TRANSACTION, e);
            }
        }
    }

    @Override
    public void fail(Object proxy, Object service, Method method, Object[] args) {
        if (isMethodHasTransaction(service, method)) {
            try {
                transactionManager.rollbackTransaction();
                LOGGER.debug(TRANSACTION_FAILED);
            } catch (SQLException e) {
                throw new IllegalStateException(FAILED_TO_ROLLBACK_TRANSACTION, e);
            }
        }
    }

    private boolean isMethodHasTransaction(Object service, Method method) {
        return Arrays.stream(service.getClass().getDeclaredMethods())
                .filter(serviceMethod -> serviceMethod.getName().equals(method.getName()))
                .anyMatch(serviceMethod -> serviceMethod.getAnnotation(Transactional.class) != null);
    }
}
