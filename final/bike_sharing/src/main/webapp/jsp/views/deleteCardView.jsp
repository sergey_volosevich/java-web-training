<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="vr" uri="bikeSharing" %>
<%@ page import="by.training.sharing.wallet.CreditCardFieldNameConstants" %>


<c:set var="cardIdField" value="${CreditCardFieldNameConstants.CARD_ID}"/>

<div class="container text-dark">
    <div class="row mt-1 mb-3">
        <div class="col-md-6 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><fmt:message key="card.delete.form.header"/></h3>
                </div>
                <div class="panel-body">
                    <form action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
                        <input type="hidden" name="commandName" value="deleteCard"/>
                        <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                        <div class="form-group">
                            <c:choose>
                            <c:when test="${not empty wallet.creditCards}">
                            <label for="card"><fmt:message key="wallet.card.form.label"/></label>
                            <select id="card" class="form-control" name="<c:out value="${cardIdField}"/>">
                                <c:forEach items="${wallet.creditCards}" var="card">
                                    <option value="<c:out value="${card.cardId}"/>">
                                        <c:out value="${card.cardNumber}"/>
                                    </option>
                                </c:forEach>
                            </select>
                        </div>
                        <c:if test="${not empty requestScope.get('success')}">
                            <div class="alert alert-success" role="alert">
                                <fmt:message key="card.form.success.delete"/>
                            </div>
                        </c:if>
                        <p class="form-text alert-danger text-center">
                            <vr:validationMessage fieldName="${error}" validationMessages="${vrMessages}"/>
                        </p>
                        <div class="pt-2 d-flex justify-content-between">
                            <a class="btn btn-outline-dark" role="button"
                               href="<bsh:command value="walletView"/>">
                                <fmt:message key="form.back.button"/></a>
                            <button class="btn btn-outline-dark">
                                <fmt:message key="form.delete.button"/></button>
                        </div>
                        </c:when>
                        <c:otherwise>
                            <div class="text-center">
                                <fmt:message key="card.delete.form.helper"/>
                                <a href="<bsh:command value="addCardView"/>" class="mdl-badge mdl-badge--overlap">
                                    <fmt:message key="links.click.here"/>
                                </a>
                                <c:if test="${not empty requestScope.get('success')}">
                                    <div class="alert alert-success" role="alert">
                                        <fmt:message key="card.form.success.delete"/>
                                    </div>
                                </c:if>
                                <div class="pt-2 d-flex justify-content-between">
                                    <a class="btn btn-outline-dark" style="width: 100%" role="button"
                                       href="<bsh:command value="walletView"/>">
                                        <fmt:message key="form.back.button"/></a>
                                </div>
                            </div>
                        </c:otherwise>
                        </c:choose>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>