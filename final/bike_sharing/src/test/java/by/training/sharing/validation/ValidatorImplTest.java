package by.training.sharing.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static by.training.sharing.bike.BikeFiledNameConstants.LATITUDE;
import static by.training.sharing.bike.BikeFiledNameConstants.LONGITUDE;
import static by.training.sharing.user.UserFieldNameConstants.*;
import static org.junit.Assert.*;

@RunWith(JUnit4.class)
public class ValidatorImplTest {

    private static final Logger LOGGER = LogManager.getLogger();

    private Map<String, String> validValuesPatternAnnotation;
    private Map<String, String> invalidValuesPatternAnnotation;
    private Map<String, String> validValuesDecimalAnnotation;
    private Map<String, String> invalidValuesDecimalAnnotation;
    private Validator validator;

    @Before
    public void setUp() throws Exception {

        invalidValuesDecimalAnnotation = new HashMap<>();
        invalidValuesDecimalAnnotation.put(LATITUDE, "40.0000");
        invalidValuesDecimalAnnotation.put(LONGITUDE, "26.38");

        validValuesDecimalAnnotation = new HashMap<>();
        validValuesDecimalAnnotation.put(LATITUDE, "53.89000");
        validValuesDecimalAnnotation.put(LONGITUDE, "27.51000");

        validValuesPatternAnnotation = new HashMap<>();
        validValuesPatternAnnotation.put(USER_LOGIN, "aBc123");
        validValuesPatternAnnotation.put(USER_PASSWORD, "12345AbcQ");
        validValuesPatternAnnotation.put(USER_NAME, "Sergey");
        validValuesPatternAnnotation.put(USER_LAST_NAME, "Volosevich");
        validValuesPatternAnnotation.put(USER_EMAIL, "sergey.volosevich93@gmail.com");
        validValuesPatternAnnotation.put(USER_PHONE, "+375293456789");
        invalidValuesPatternAnnotation = new HashMap<>();
        invalidValuesPatternAnnotation.put(USER_LOGIN, "123");
        invalidValuesPatternAnnotation.put(USER_PASSWORD, "123");
        invalidValuesPatternAnnotation.put(USER_NAME, "Sergey133");
        invalidValuesPatternAnnotation.put(USER_LAST_NAME, "Volosevich34124");
        invalidValuesPatternAnnotation.put(USER_EMAIL, "sergey.volosevich93@gmail");
        invalidValuesPatternAnnotation.put(USER_PHONE, "+375293456789df");

        validator = new ValidatorImpl();
    }

    @Test
    public void validateValidFieldsByPatternAnnotation() {
        ValidationResult result = validator.validate(validValuesPatternAnnotation, TestUserDTO.class);
        assertTrue(result.isValid());
    }

    @Test(expected = MissingAnnotationException.class)
    public void validateWithoutValidationAnnotation() {
        ValidationResult result = validator.validate(validValuesPatternAnnotation, Integer.class);
    }

    @Test(expected = MissingAnnotationException.class)
    public void validateWithoutConstraintAnnotation() {
        ValidationResult result = validator.validate(validValuesPatternAnnotation, TestBikeDTO.class);
    }

    @Test
    public void validateInvalidFieldsByPatternAnnotation() {
        ValidationResult validationResult = validator.validate(invalidValuesPatternAnnotation, TestUserDTO.class);
        Map<String, List<String>> validationInfo = validationResult.getValidationInfo();
        assertFalse(validationResult.isValid());
        assertEquals("user.login.validation.msg", validationInfo.get(USER_LOGIN).get(0));
        assertEquals("user.password.validation.msg", validationInfo.get(USER_PASSWORD).get(0));
        assertEquals("user.email.validation.msg", validationInfo.get(USER_EMAIL).get(0));
        assertEquals("user.phone.validation.msg", validationInfo.get(USER_PHONE).get(0));
        assertEquals("user.name.validation.msg", validationInfo.get(USER_NAME).get(0));
        assertEquals("user.lastName.validation.msg", validationInfo.get(USER_LAST_NAME).get(0));
    }

    @Test
    public void validateFieldsByDecimalAnnotation() {
        ValidationResult result = validator.validate(validValuesDecimalAnnotation, TestEntityValidDecimal.class);
        assertTrue(result.isValid());
    }


    @Test
    public void validateInvalidFieldsByDecimalAnnotation() {
        ValidationResult result = validator.validate(invalidValuesDecimalAnnotation, TestEntityValidDecimal.class);
        Map<String, List<String>> validationInfo = result.getValidationInfo();
        assertFalse(result.isValid());
        assertEquals("bike.latitude.min.validation.msg", validationInfo.get(LATITUDE).get(0));
        assertEquals("bike.longitude.min.validation.msg", validationInfo.get(LONGITUDE).get(0));
    }

    @Test
    public void isNumberByValidValue() {
        boolean isnumber = validator.isNumber("12");
        assertTrue(isnumber);
    }

    @Test
    public void isNumberByInvalidValue() {
        boolean isnumber = validator.isNumber("not Number");
        assertFalse(isnumber);
    }

    @Test
    public void isNumberByNull() {
        boolean isnumber = validator.isNumber(null);
        assertFalse(isnumber);
    }

    @Test
    public void isNumberByNegativeValue() {
        boolean isnumber = validator.isNumber("-12");
        assertTrue(isnumber);
    }


}