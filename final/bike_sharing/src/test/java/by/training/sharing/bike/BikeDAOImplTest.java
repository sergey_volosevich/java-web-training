package by.training.sharing.bike;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.assertEquals;

public class BikeDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(BikeDAOImplTest.class);

    private BikeDTO bikeDTO;
    private BikeDAO bikeDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace);) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        bikeDTO = new BikeDTO();
        bikeDTO.setSerialNumber("TestBike11");
        bikeDTO.setBikePassword("123");
        bikeDTO.setLatitude(new BigDecimal("53.833202"));
        bikeDTO.setLongitude(new BigDecimal("27.450280"));
        bikeDTO.setPriceId(1);
        TransactionManagerImpl transactionManager = new TransactionManagerImpl();
        ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        bikeDAO = new BikeDAOImpl(jdbcTemplate);
    }

    @Test
    public void save() throws DAOException {
        final long bikeId = bikeDAO.save(bikeDTO);
        assertEquals(11, bikeId);
    }

    @Test
    public void getById() throws DAOException {
        BikeDTO bike = bikeDAO.getById(1);
        assertEquals(1, bike.getId());
        assertEquals(53.886971, bike.getLatitude().doubleValue(),0.0);
        assertEquals(27.420341, bike.getLongitude().doubleValue(), 0.0);
        assertEquals("Bike01", bike.getSerialNumber());
        assertEquals("123", bike.getBikePassword());
        assertEquals(0, bike.getStatus().ordinal());
    }

    @Test(expected = EntityNotFoundException.class)
    public void getByInvalidId() throws DAOException {
        bikeDAO.getById(25);
    }

    @Test
    public void findAllWithLocationByState() throws DAOException {
        List<BikeDTO> unusedBikes = bikeDAO.findAllWithLocationByState(BikeStatus.UNUSED);
        assertEquals(1, unusedBikes.get(0).getId());
        assertEquals(8, unusedBikes.get(4).getId());
    }
}