package by.training.volosevich.library.validation;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Month;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class MagazineFieldsValidator extends PeriodicalEditionFieldsValidator {

    private static final Logger LOGGER = LogManager.getLogger();

    private List<String> fields = new ArrayList<>();

    public MagazineFieldsValidator() {
        fields.add("releaseMonth");
    }

    public List<String> validate(Map<String, String> values) {
        validatePeriodicalEditionFields(values);
        for (String field : fields) {
            if (values.containsKey(field)) {
                String value = values.get(field);
                switch (field) {
                    case "releaseMonth":
                        validateMonth(field, value);
                        break;
                }
            } else {
                errorMessages.add(ValidationMessage.formatMessage(field, ValidationMessage.NOT_ENOUGH_FIELD));
                LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(field,
                        ValidationMessage.NOT_ENOUGH_FIELD));
            }
        }
        return errorMessages;
    }

    private void validateMonth(String nameField, String value) {
        Month[] allTypes = Month.values();
        int count = 0;
        for (Month type : allTypes) {
            if (type.name().equalsIgnoreCase(value)) {
                count++;
            }
        }
        if (count == 0) {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_MONTH));
            LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_MONTH));
        }
    }
}
