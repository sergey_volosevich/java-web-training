<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<c:choose>
    <c:when test="${not empty requestScope.get('lang')}">
        <fmt:setLocale value="${requestScope.get('lang')}"/>
    </c:when>
    <c:otherwise>
        <fmt:setLocale value="${cookie['lang'].value}"/>
    </c:otherwise>
</c:choose>
<fmt:setBundle basename="/i18n/ApplicationMessages" scope="application"/>
<!DOCTYPE html>
<html>
<head>
    <head>
        <title><fmt:message key="app.name"/></title>
        <link rel="stylesheet" href="static/style/css/material.css">
        <link rel="stylesheet" href="static/style/css/styles.css">
        <link rel="stylesheet" href="static/style/css/bootstrap.min.css">
        <link rel="stylesheet" href="static/style/css/bundle-login.css">
        <link rel="stylesheet" href="static/style/css/icons.css">
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
        <script defer src="static/style/js/bootstrap.min.js"></script>
        <script defer src="static/style/js/material.js"></script>
        <script defer src="static/style/js/bundle-login.js"></script>
        <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js"></script>
        <link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Roboto:300,400,500,700" type="text/css">
    </head>
</head>
<body style="position: relative;
padding-bottom: 100px;">
<style>
    .demo-layout-transparent {
    <c:url var="layoutImg" value="static/style/img/layout2.jpg"/> background: url(<c:out value="${layoutImg}"/>) center / cover;
    }
</style>
<div class="mdl-layout mdl-js-layout demo-layout-transparent">
    <header class="mdl-layout__header mdl-layout__header--transparent ">
        <div class="mdl-layout__header-row">
            <span class="mdl-layout-title"><fmt:message key="app.name"/> </span>
            <div class="mdl-layout-spacer"></div>
            <nav class="mdl-navigation">
                <bsh:lang/>
                <bsh:navigation/>
            </nav>
        </div>
    </header>
    <bsh:nav-links/>
