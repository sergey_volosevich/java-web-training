package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.EnumMap;
import java.util.Optional;

public class CommandProviderImpl implements CommandProvider<LightTariff> {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CANNOT_FIND_SUCH_COMMAND = "Can not find such command. Command name - {}";
    private static final String MESSAGE = "Can not find command";

    private EnumMap<CommandType, Command<LightTariff>> commands =
            new EnumMap<>(CommandType.class);

    @Override
    public Command<LightTariff> getCommand(String type){
        Optional<CommandType> commandType = CommandType.fromString(type);
        if (commandType.isPresent()) {
            Command<LightTariff> command = commands.get(commandType.get());
            if (command != null) {
                return command;
            } else {
                LOGGER.error(CANNOT_FIND_SUCH_COMMAND, type);
                throw new CommandNotFoundException(MESSAGE);
            }
        } else {
            LOGGER.error(CANNOT_FIND_SUCH_COMMAND, type);
            throw new CommandNotFoundException(MESSAGE);
        }
    }

    @Override
    public void addCommand(CommandType type, Command<LightTariff> command) {
        commands.putIfAbsent(type, command);
    }
}
