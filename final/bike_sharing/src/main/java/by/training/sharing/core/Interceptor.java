package by.training.sharing.core;

import java.lang.annotation.*;

/**
 * This annotation is used for mark class, which is a class interceptor ad implements {@link BeanInterceptor}.
 */

@Documented
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Interceptor {
    /**
     * Sets annotation class whose object will be intercepted.
     * @return annotation class.
     */
    Class<? extends Annotation> clazz();
}
