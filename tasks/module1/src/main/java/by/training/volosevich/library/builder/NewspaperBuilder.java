package by.training.volosevich.library.builder;

import by.training.volosevich.library.entity.*;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.DayOfWeek;
import java.util.Map;

public class NewspaperBuilder implements Builder<Edition> {

    private static final Logger LOGGER = LogManager.getLogger();

    private Map<String, String> fields;

    public NewspaperBuilder(Map<String, String> fields) {
        this.fields = fields;
    }

    @Override
    public Edition build() {
        String name = fields.get("name");
        GenreType genreType = GenreType.valueOf(fields.get("genreType").toUpperCase());
        int numberOfPages = Integer.parseInt(fields.get("numberOfPages"));
        String publishingHouse = fields.get("publishingHouse");
        int releaseYear = Integer.parseInt(fields.get("releaseYear"));
        EditionType type = EditionType.valueOf(fields.get("type").toUpperCase());
        PeriodicityType periodicity = PeriodicityType.valueOf(fields.get("periodicity").
                toUpperCase().replace(" ", "_"));
        int releaseNumber = Integer.parseInt(fields.get("releaseNumber"));
        int yearReleaseNumber = Integer.parseInt(fields.get("yearReleaseNumber"));
        DayOfWeek releaseDay = DayOfWeek.valueOf(fields.get("releaseDay").toUpperCase());
        LOGGER.log(Level.DEBUG, () -> type + " - entity successfully build");
        return new Newspaper(name, genreType, numberOfPages, publishingHouse, releaseYear, type, periodicity,
                releaseNumber, yearReleaseNumber, releaseDay);
    }
}
