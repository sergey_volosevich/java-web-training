package by.training.volosevich.library.validation;

import by.training.volosevich.library.entity.EditionType;

public class ValidatorFactory {

    public Validator getByType(EditionType type) {
        switch (type) {
            case BOOK:
                return new BookFieldsValidator();
            case MAGAZINE:
                return new MagazineFieldsValidator();
            case NEWSPAPER:
                return new NewspaperFieldsValidator();
            default:
                return null;
        }
    }
}
