package by.training.sharing.security;

/**
 * This exception indicates, that logged user not found in security context.
 */

public class CredentialNotFoundException extends SecurityException {
    private static final long serialVersionUID = 7626074302147457498L;

    public CredentialNotFoundException(String message) {
        super(message);
    }
}
