package by.training.tariff.parser;

public class TariffFields {
    public static final String LIGHT_TARIFF = "light_tariff";
    public static final String COMFORT_TARIFF = "comfort_tariff";
    public static final String BUSINESS_TARIFF = "business_tariff";
    public static final String ID = "id";
    public static final String FREE_INTERNET_TRAFFIC = "free_internet_traffic";
    public static final String TARIFF_NAME = "tariff_name";
    public static final String DATE_CREATION = "date_creation";
    public static final String OPERATOR_NAME = "operator_name";
    public static final String PAYROLL = "payroll";
    public static final String FREE_MINUTES_IN_NETWORK = "free_minutes_in_network";
    public static final String CALL_SMS_PRICES = "call_sms_prices";
    public static final String ADDITIONAL_INFO = "additional_info";
    public static final String FREE_MINUTES_OUT_NETWORK = "free_minutes_out_network";
    public static final String FREE_MINUTES_INTERNATIONAL_CALL = "free_minutes_international_call";
    public static final String IN_NETWORK_CALL = "in_network_call";
    public static final String OUT_NETWORK_CALL = "out_network_call";
    public static final String INTERNATIONAL_CALL = "international_call";
    public static final String SMS_PRICE = "sms_price";
    public static final String BILLING_TYPE = "billing_type";
    public static final String FAVORITE_NUMBER = "favorite_number";
    public static final String CONNECTION_PRICE = "connection_price";
    public static final String TARIFFS = "tariffs";
}
