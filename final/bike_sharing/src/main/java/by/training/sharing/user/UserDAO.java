package by.training.sharing.user;

import by.training.sharing.dao.CRUDDao;
import by.training.sharing.dao.DAOException;

import java.util.List;
import java.util.Optional;

public interface UserDAO extends CRUDDao<UserDTO> {

    boolean findDuplicateEmail(String email) throws DAOException;

    boolean findDuplicateLogin(String login) throws DAOException;

    Optional<UserDTO> findByLogin(String login) throws DAOException;

    List<UserDTO> findPartOfUsers(int limit, int offset) throws DAOException;

    int countOfRows() throws DAOException;
}
