package by.training.sharing.wallet;

import by.training.sharing.dao.RowMapper;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;

public class CreditCardRowMapper implements RowMapper<CreditCardDTO> {
    @Override
    public CreditCardDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        CreditCardDTO creditCardDTO = new CreditCardDTO();
        long id = rs.getLong("wallet_card_id");
        creditCardDTO.setCardId(id);
        String cardNumber = rs.getString("card_number");
        creditCardDTO.setCardNumber(cardNumber);
        String holderName = rs.getString("holder_name");
        creditCardDTO.setHolderName(holderName);
        Date expireDate = rs.getDate("expire_date");
        creditCardDTO.setExpireDate(expireDate.toLocalDate());
        int cvv = rs.getInt("cvv");
        creditCardDTO.setCvv(cvv);
        long walletId = rs.getLong("wallet_id");
        creditCardDTO.setWalletId(walletId);
        return creditCardDTO;
    }
}
