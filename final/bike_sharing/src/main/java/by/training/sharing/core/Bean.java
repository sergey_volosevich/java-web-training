package by.training.sharing.core;

import java.lang.annotation.*;

/**
 * This annotation is used to mark classes, which used in {@link by.training.sharing.core.BeanRegistry}
 * for the subsequent construction of objects.
 */

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Bean {
    /**
     * Sets bean name.
     *
     * @return name.
     */
    String name();
}
