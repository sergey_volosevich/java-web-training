package by.training.sharing.wallet;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.stream.Stream;

public enum DepositAmountType {
    AMOUNT_FIVE(BigDecimal.valueOf(5.00)), AMOUNT_TEN(BigDecimal.valueOf(10.00)),
    AMOUNT_FIFTEEN(BigDecimal.valueOf(15.00)),
    AMOUNT_TWENTY_FIVE(BigDecimal.valueOf(25.00)), AMOUNT_FIFTY(BigDecimal.valueOf(50.00));

    private BigDecimal amount;

    DepositAmountType(BigDecimal amount) {
        this.amount = amount;
    }

    public static Optional<DepositAmountType> getAmountType(BigDecimal amount) {
        return Stream.of(DepositAmountType.values()).filter(depositType ->
                depositType.getAmount().compareTo(amount) == 0).findFirst();
    }

    public BigDecimal getAmount() {
        return amount;
    }

}
