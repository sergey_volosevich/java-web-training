package by.training.sharing.validation;

import java.io.Serializable;
import java.util.*;

/**
 * This class is the result of field validation. Validation result containing filed names as keys and validation
 * messages of this field values as map values. The keys are of type String,
 * values are type of <code>List<String></code>.
 */

public class ValidationResult implements Serializable {

    private static final long serialVersionUID = 22707623352314657L;
    private Map<String, List<String>> validationInfo = new HashMap<>();

    public Map<String, List<String>> getValidationInfo() {
        return new HashMap<>(validationInfo);
    }

    public void setValidationInfo(String code, String message) {
        if (validationInfo.containsKey(code)) {
            List<String> list = validationInfo.get(code);
            list.add(message);
            validationInfo.put(code, list);
        } else {
            List<String> list = new ArrayList<>();
            list.add(message);
            validationInfo.put(code, list);
        }
    }

    public boolean isValid() {
        return validationInfo.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        Set<Map.Entry<String, List<String>>> entries = validationInfo.entrySet();
        for (Map.Entry<String, List<String>> entry : entries) {
            String key = entry.getKey();
            List<String> value = entry.getValue();
            for (String message : value) {
                stringBuilder.append(key).append(" - ").append(message).append("\n");
            }
        }
        return stringBuilder.toString();
    }
}
