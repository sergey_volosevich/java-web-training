package by.training.sharing.dao;

import by.training.sharing.bike.BikeDTO;
import by.training.sharing.bike.BikeStatus;
import by.training.sharing.core.ApplicationContext;
import by.training.sharing.user.UserDTO;
import by.training.sharing.wallet.CreditCardDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.*;

public class JDBCTemplateImplTest {

    private static final Logger LOGGER = LogManager.getLogger(JDBCTemplateImplTest.class);

    private static final String INSERT_INTO_BIKE = "INSERT INTO bike (serial_number, password, rental_price_id) VALUES (?, ?, ?);";
    private static final String SELECT_CREDIT_CARD_BY_ID = "SELECT wallet_card_id, card_number, holder_name," +
            " expire_date, cvv FROM wallet_card where wallet_card_id = ?;";
    private static final String SELECT_ALL_USERS = "select user_id, user_name, password from user_account;";
    private static final String SELECT_BIKES_BY_RENTAL_PRICE = "select bike_id, serial_number, password, bike_state," +
            " rental_price_id from bike where rental_price_id = ?;";

    private static final String INSERT_QUERY_USER_ACCOUNT = "INSERT INTO user_account (user_name, password) " +
            "VALUES (?, ?);";

    private JDBCTemplate jdbcTemplate;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace)) {
                statement.execute();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
        LOGGER.info("Schema is successfully created.");
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() {
        TransactionManagerImpl transactionManager = new TransactionManagerImpl();
        ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(transactionManager);
        jdbcTemplate = new JDBCTemplateImpl(connectionManager);
    }

    @Test
    public void executeUpdate() throws SQLException {
        TransactionManagerImpl transactionManager = new TransactionManagerImpl();
        ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        BikeDTO bikeDTO = new BikeDTO();
        bikeDTO.setSerialNumber("123Bike");
        bikeDTO.setBikePassword("123test");
        bikeDTO.setPriceId(1);
        final int i = jdbcTemplate.executeUpdate(INSERT_INTO_BIKE, new Object[]{bikeDTO.getSerialNumber(),
                bikeDTO.getBikePassword(), bikeDTO.getPriceId()});
        assertTrue(i > 0);
        assertNotEquals(-1, 1);
    }


    @Test
    public void queryForObjectByExistId() throws SQLException {
        final CreditCardDTO creditCard = jdbcTemplate.queryForObject(SELECT_CREDIT_CARD_BY_ID, new Object[]{1},
                (rs, rowNum) -> {
                    CreditCardDTO creditCardDTO = new CreditCardDTO();
                    final long walletCardId = rs.getLong("wallet_card_id");
                    creditCardDTO.setCardId(walletCardId);
                    final String cardNumber = rs.getString("card_number");
                    creditCardDTO.setCardNumber(cardNumber);
                    final String holderName = rs.getString("holder_name");
                    creditCardDTO.setHolderName(holderName);
                    final Date expireDate = rs.getDate("expire_date");
                    creditCardDTO.setExpireDate(expireDate.toLocalDate());
                    final int cvv = rs.getInt("cvv");
                    creditCardDTO.setCvv(cvv);
                    return creditCardDTO;
                });
        assertNotNull(creditCard);
        assertEquals(1, creditCard.getCardId());
        assertEquals("5105 1051 0510 5100", creditCard.getCardNumber());
        assertEquals("sergey volosevich", creditCard.getHolderName());
        assertEquals(LocalDate.of(2019, 12, 1), creditCard.getExpireDate());
        assertEquals(123, creditCard.getCvv());
    }

    @Test
    public void queryForObjectByNonExistentId() throws SQLException {
        final CreditCardDTO creditCard = jdbcTemplate.queryForObject(SELECT_CREDIT_CARD_BY_ID, new Object[]{5},
                (rs, rowNum) -> {
                    CreditCardDTO creditCardDTO = new CreditCardDTO();
                    final long walletCardId = rs.getLong("wallet_card_id");
                    creditCardDTO.setCardId(walletCardId);
                    final String cardNumber = rs.getString("card_number");
                    creditCardDTO.setCardNumber(cardNumber);
                    final String holderName = rs.getString("holder_name");
                    creditCardDTO.setHolderName(holderName);
                    final Date expireDate = rs.getDate("expire_date");
                    creditCardDTO.setExpireDate(expireDate.toLocalDate());
                    final int cvv = rs.getInt("cvv");
                    creditCardDTO.setCvv(cvv);
                    return creditCardDTO;
                });
        assertNull(creditCard);
    }

    @Test
    public void query() throws SQLException {
        List<UserDTO> users = jdbcTemplate.query(SELECT_ALL_USERS, (rs, rowNum) -> {
            UserDTO userDTO = new UserDTO();
            long id = rs.getLong("user_id");
            userDTO.setId(id);
            String userName = rs.getString("user_name");
            userDTO.setUserName(userName);
            String password = rs.getString("password");
            userDTO.setPassword(password);
            return userDTO;
        });
        assertEquals(13, users.size());
        assertEquals("admin", users.get(0).getUserName());
    }

    @Test
    public void queryForList() throws SQLException {
        List<BikeDTO> bikes = jdbcTemplate.queryForList(SELECT_BIKES_BY_RENTAL_PRICE, new Object[]{2}, (rs, rowNum) -> {
            BikeDTO bikeDTO = new BikeDTO();
            long bikeId = rs.getLong("bike_id");
            bikeDTO.setId(bikeId);
            String serialNumber = rs.getString("serial_number");
            bikeDTO.setSerialNumber(serialNumber);
            String password = rs.getString("password");
            bikeDTO.setBikePassword(password);
            int bikeState = rs.getInt("bike_state");
            Optional<BikeStatus> status = BikeStatus.getStatus(bikeState);
            bikeDTO.setStatus(status.get());
            long id = rs.getLong("rental_price_id");
            bikeDTO.setPriceId(id);
            return bikeDTO;
        });
        assertEquals(3, bikes.size());
        assertEquals(4, bikes.get(0).getId());
    }

    @Test
    public void executeAndReturnKey() throws SQLException {
        Number number = jdbcTemplate.executeAndReturnKey(INSERT_QUERY_USER_ACCOUNT, new Object[]{"test17", "123THG"});
        assertEquals(14, number.longValue());
    }
}