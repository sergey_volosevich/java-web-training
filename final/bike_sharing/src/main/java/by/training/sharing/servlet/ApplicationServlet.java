package by.training.sharing.servlet;

import by.training.sharing.command.CommandException;
import by.training.sharing.command.ServletCommand;
import by.training.sharing.core.ApplicationContext;
import by.training.sharing.util.RequestUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import static by.training.sharing.ApplicationConstants.CMD_REQ_PARAMETER;

@WebServlet(urlPatterns = "/", loadOnStartup = 1, name = "sharing")
public class ApplicationServlet extends HttpServlet {

    private static final long serialVersionUID = -3817173818990240811L;

    private static final Logger LOGGER = LogManager.getLogger(ApplicationServlet.class);

    private static final String FAILED_TO_EXECUTE_COMMAND_CAUSE = "Failed to execute command - {}. Cause:";
    private static final String MAIN_LAYOUT = "/jsp/layout.jsp";
    private static final String FIND_COMMAND = "FIND COMMAND with commandName - \'{}\'";

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) {
        String commandName = req.getParameter(CMD_REQ_PARAMETER);
        ServletCommand command = ApplicationContext.getInstance().getCommand(commandName);
        if (command != null) {
            try {
                LOGGER.debug(FIND_COMMAND, commandName);
                command.execute(req, resp);
            } catch (CommandException e) {
                LOGGER.error(FAILED_TO_EXECUTE_COMMAND_CAUSE, commandName, e);
                RequestUtils.sendError(resp, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
            }
        } else {
            RequestUtils.forward(req, resp, MAIN_LAYOUT);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) {
        doGet(req, resp);
    }
}
