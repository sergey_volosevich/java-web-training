package by.training.sharing.util;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

/**
 * This util class helps with client requests and client responses.
 */

public class RequestUtils {

    private static final Logger LOGGER = LogManager.getLogger(RequestUtils.class);
    private static final String FORWARD_EXCEPTION_CAUSE = "Forward failed. Cause:";
    private static final String REDIRECT_EXCEPTION_CAUSE = "Redirect failed. Cause:";
    private static final String REDIRECT_TO_ERROR_PAGE_FAILED_CAUSE = "Redirect to error page failed. Cause:";
    private static final String CSRF_TOKEN_ADDED = "CSRF token added to session. CSRF token - {}";
    private static final String CSRF_TOKEN_REMOVED = "CSRF token removed from session.";
    private static final String CSRF = "csrf";

    private RequestUtils() {
    }

    /**
     * This method forwards a request from a servlet to another resource (servlet, JSP file, or HTML file) on the server.
     * Throws RequestUtilException if the target resource throws this exception.
     *
     * @param request     a {@link ServletRequest} object that represents the
     *                    request the client makes of the servlet
     * @param response    a {@link ServletResponse} object that represents
     *                    the response the servlet returns to the client
     * @param forwardPage Name of page, where forward request.
     */

    public static void forward(HttpServletRequest request, HttpServletResponse response, String forwardPage) {
        try {
            request.getRequestDispatcher(forwardPage).forward(request, response);
        } catch (ServletException | IOException e) {
            LOGGER.error(FORWARD_EXCEPTION_CAUSE, e);
            throw new RequestUtilException(e);
        }
    }

    /**
     * Sends a temporary redirect response to the client using the specified redirect location URL.
     * Throws RequestUtilException if an input or output exception occurs.
     *
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     * @param location the redirect location URL
     */

    public static void sendRedirect(HttpServletResponse response, String location) {
        try {
            response.sendRedirect(location);
        } catch (IOException e) {
            LOGGER.error(REDIRECT_EXCEPTION_CAUSE, e);
            throw new RequestUtilException(e);
        }
    }

    /**
     * Sends an error response to the client using the specified status code.
     * Throws RequestUtilException if an input or output exception occurs
     *
     * @param response  a {@link ServletResponse} object that represents
     *                  the response the servlet returns to the client
     * @param errorCode the error status code
     */

    public static void sendError(HttpServletResponse response, int errorCode) {
        try {
            response.sendError(errorCode);
        } catch (IOException e) {
            LOGGER.error(REDIRECT_TO_ERROR_PAGE_FAILED_CAUSE, e);
            throw new RequestUtilException(e);
        }
    }

    /**
     * This method retrieves parameters from a client request.
     *
     * @param request    a {@link ServletRequest} object that represents the
     *                   request the client makes of the servlet
     * @param filedNames names of parameters to be extracted from the request from the client.
     * @return java.util.Map containing parameter names as keys and parameter values as map values. The keys and values
     * in the parameter map are of type String.
     */

    public static Map<String, String> getParametersFromRequest(HttpServletRequest request, String... filedNames) {
        Map<String, String> parameters = new HashMap<>();
        for (String s : filedNames) {
            parameters.put(s, request.getParameter(s));
        }
        return parameters;
    }

    /**
     * This method redirects to a command in this application, that displays needed view.
     *
     * @param request     a {@link ServletRequest} object that represents the
     *                    request the client makes of the servlet
     * @param response    a {@link ServletResponse} object that represents
     *                    the response the servlet returns to the client
     * @param commandName name of the command to be redirected to.
     */

    public static void sendRedirectToCommand(HttpServletRequest request, HttpServletResponse response, String commandName) {
        try {
            response.sendRedirect(request.getContextPath() + "?commandName=" + commandName);
        } catch (IOException e) {
            LOGGER.error(REDIRECT_EXCEPTION_CAUSE, e);
            throw new RequestUtilException(e);
        }
    }

    /**
     * This method adds flush attribute.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param attrName name of flush attribute.
     * @param object   object that is added as an attribute
     */

    public static <T> void setFlushAttribute(HttpServletRequest request, String attrName, T object) {
        HttpSession session = request.getSession();
        session.setAttribute(attrName, object);
    }

    /**
     * This method gets and removes flush attribute.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param attrName name of flush attribute.
     */

    public static void getFlushAttribute(HttpServletRequest request, String attrName) {
        HttpSession session = request.getSession();
        Object attribute = session.getAttribute(attrName);
        if (attribute != null) {
            request.setAttribute(attrName, attribute);
            session.removeAttribute(attrName);
        }
    }

    /**
     * This method generates and adds csrf token to request and session to prevent csrf-attacks.
     *
     * @param request a {@link ServletRequest} object that represents the
     *                request the client makes of the servlet
     */

    public static void addCsrfToken(HttpServletRequest request) {
        HttpSession session = request.getSession();
        Object sessionAttribute = session.getAttribute(CSRF);
        if (sessionAttribute != null) {
            String csrf = (String) sessionAttribute;
            request.setAttribute(CSRF, csrf);
        } else {
            UUID csrfToken = UUID.randomUUID();
            request.setAttribute(CSRF, csrfToken.toString());
            session.setAttribute(CSRF, csrfToken.toString());
            LOGGER.debug(CSRF_TOKEN_ADDED, csrfToken);
        }
    }

    /**
     * This method removes csrf token from session.
     *
     * @param request a {@link ServletRequest} object that represents the
     *                request the client makes of the servlet
     */

    public static void removeCsrfToken(HttpServletRequest request) {
        HttpSession session = request.getSession();
        session.removeAttribute(CSRF);
        LOGGER.debug(CSRF_TOKEN_REMOVED);
    }
}
