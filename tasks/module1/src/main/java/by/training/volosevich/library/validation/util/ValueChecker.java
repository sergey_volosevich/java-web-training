package by.training.volosevich.library.validation.util;

public class ValueChecker {

    private ValueChecker() {
    }

    public static boolean isInteger(String value) {
        try {
            Integer.valueOf(value);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }

    public static boolean isNotNull(String value) {
        return value != null;
    }
}
