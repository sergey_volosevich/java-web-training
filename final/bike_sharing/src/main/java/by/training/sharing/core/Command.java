package by.training.sharing.core;


import java.lang.annotation.*;

/**
 * This annotation is used to mark class, which represents container of commands
 */

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Command {
}
