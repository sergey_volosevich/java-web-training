package by.training.sharing.user;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.core.Command;
import by.training.sharing.core.CommandName;
import by.training.sharing.security.LoginException;
import by.training.sharing.security.SecurityException;
import by.training.sharing.security.SecurityService;
import by.training.sharing.service.RecordsPerPage;
import by.training.sharing.service.ServiceException;
import by.training.sharing.util.RequestUtils;
import by.training.sharing.validation.ValidationResult;
import by.training.sharing.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static by.training.sharing.ApplicationConstants.FORM_ERROR_CODE;
import static by.training.sharing.ApplicationConstants.FORM_ERROR_MESSAGE;
import static by.training.sharing.ApplicationConstants.GET_USERS;
import static by.training.sharing.ApplicationConstants.GET_USER_LOGIN;
import static by.training.sharing.ApplicationConstants.GET_USER_LOGOUT;
import static by.training.sharing.ApplicationConstants.GET_USER_REGISTRATION;
import static by.training.sharing.ApplicationConstants.GET_WELCOME_VIEW;
import static by.training.sharing.ApplicationConstants.MAIN_LAYOUT;
import static by.training.sharing.ApplicationConstants.POST_USER_LOGIN;
import static by.training.sharing.ApplicationConstants.POST_USER_REGISTRATION;
import static by.training.sharing.ApplicationConstants.VALIDATION_MESSAGES;
import static by.training.sharing.ApplicationConstants.VIEW_NAME_REQ_ATTRIBUTE;
import static by.training.sharing.core.BeanNameConstants.SECURITY_SERVICE;
import static by.training.sharing.core.BeanNameConstants.USER_CONTROLLER;
import static by.training.sharing.core.BeanNameConstants.USER_SERVICE;
import static by.training.sharing.core.BeanNameConstants.VALIDATOR;
import static by.training.sharing.user.UserFieldNameConstants.USER_EMAIL;
import static by.training.sharing.user.UserFieldNameConstants.USER_LAST_NAME;
import static by.training.sharing.user.UserFieldNameConstants.USER_LOGIN;
import static by.training.sharing.user.UserFieldNameConstants.USER_NAME;
import static by.training.sharing.user.UserFieldNameConstants.USER_PASSWORD;
import static by.training.sharing.user.UserFieldNameConstants.USER_PHONE;

/**
 * This class contains commands for working with {@link by.training.sharing.user.UserDTO}.
 * Each method is one command executed per 1 request from the client.
 */


@Bean(name = USER_CONTROLLER)
@Command
public class UserController {

    private static final Logger LOGGER = LogManager.getLogger(UserController.class);
    private static final String RECORDS_PER_PAGE = "recordsPerPage";
    private static final String FAILED_TO_AUTHENTICATE_USER_CAUSE = "Failed to authenticate user. Cause:";
    private static final String USER_LOGIN_FORM_ERROR = "user.login.form.error";
    private static final String USER_ALREADY_LOGIN_ERROR = "user.already.login.error";
    private static final String LOGOUT_PARAMETER = "?logout=ok";
    private static final String FAILED_TO_GET_LIST_OF_USERS = "Failed to get list of users";
    private UserService userService;
    private Validator validator;
    private SecurityService securityService;

    public UserController(@BeanQualifier(USER_SERVICE) UserService userService,
                          @BeanQualifier(VALIDATOR) Validator validator,
                          @BeanQualifier(SECURITY_SERVICE) SecurityService securityService) {
        this.userService = userService;
        this.validator = validator;
        this.securityService = securityService;
    }

    /**
     * This method gets and displays view with user registration form.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_USER_REGISTRATION)
    public void getUserRegisterForm(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.addCsrfToken(request);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_USER_REGISTRATION);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    /**
     * This method processes the user data from the request and registers it in the application.
     * Checks duplicates email and login of user. If user successful registered, redirect to view with login form,
     * otherwise forward to view with registration form with validation messages.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_USER_REGISTRATION)
    public void register(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> filedValues = RequestUtils.getParametersFromRequest(request, USER_LOGIN, USER_PASSWORD,
                USER_EMAIL, USER_PHONE, USER_LAST_NAME, USER_NAME);
        ValidationResult validationResult = validator.validate(filedValues, UserDTO.class);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_USER_REGISTRATION);
        request.setAttribute(VALIDATION_MESSAGES, validationResult);
        if (!validationResult.isValid()) {
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        UserDTO userDTO = buildUserDTO(filedValues);
        Map<String, String> duplicateFields = userService.findDuplicateFields(userDTO.getUserName(), userDTO.getEmail());
        if (!duplicateFields.isEmpty()) {
            for (Map.Entry<String, String> entry : duplicateFields.entrySet()) {
                validationResult.setValidationInfo(entry.getKey(), entry.getValue());
            }
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        boolean save = userService.registerUser(userDTO);
        if (save) {
            RequestUtils.removeCsrfToken(request);
            RequestUtils.sendRedirectToCommand(request, response, GET_WELCOME_VIEW);
        } else {
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    /**
     * This method gets and displays view with user login form.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_USER_LOGIN)
    public void getLoginForm(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.addCsrfToken(request);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_USER_LOGIN);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    /**
     * This method processes the user data from the request and try to login user in the application. If user successful
     * logged-in, redirect to view with welcome message, otherwise forward to view with login form with error message.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_USER_LOGIN)
    public void loginUser(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> fieldValues = RequestUtils.getParametersFromRequest(request, USER_LOGIN, USER_PASSWORD);
        ValidationResult validationResult = validator.validate(fieldValues, UserDTO.class);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_USER_LOGIN);
        request.setAttribute(VALIDATION_MESSAGES, validationResult);
        try {
            if (!validationResult.isValid()) {
                validationResult.setValidationInfo(FORM_ERROR_CODE, USER_LOGIN_FORM_ERROR);
                RequestUtils.forward(request, response, MAIN_LAYOUT);
                return;
            }
            UserDTO userDTO = buildUserDTO(fieldValues);
            boolean isLogin = securityService.loginUser(request.getSession(), userDTO);
            if (isLogin) {
                RequestUtils.removeCsrfToken(request);
                RequestUtils.sendRedirectToCommand(request, response, GET_WELCOME_VIEW);
            } else {
                validationResult.setValidationInfo(FORM_ERROR_CODE, USER_LOGIN_FORM_ERROR);
                RequestUtils.forward(request, response, MAIN_LAYOUT);
            }
        } catch (LoginException e) {
            validationResult.setValidationInfo(FORM_ERROR_CODE, USER_ALREADY_LOGIN_ERROR);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (SecurityException e) {
            LOGGER.error(FAILED_TO_AUTHENTICATE_USER_CAUSE, e);
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    /**
     * This method is used to exit the user from the application. Redirects to main view.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_USER_LOGOUT)
    public void logoutUser(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession(false);
        securityService.logoutUser(session);
        RequestUtils.sendRedirect(response, request.getContextPath() + LOGOUT_PARAMETER);
    }

    /**
     * This method gets and displays view with welcome message.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_WELCOME_VIEW)
    public void getWelcomeView(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.removeCsrfToken(request);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_WELCOME_VIEW);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    @CommandName(name = GET_USERS)
    public void getAllUsersView(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_USERS);
        String requestRecordsPerPage = request.getParameter(RECORDS_PER_PAGE);
        HttpSession session = request.getSession();
        Integer sessionRecordsPerPage = (Integer) session.getAttribute(RECORDS_PER_PAGE);
        if (sessionRecordsPerPage == null) {
            session.setAttribute(RECORDS_PER_PAGE, RecordsPerPage.TEN_ROWS.getNumberOfRecords());
        }
        if (requestRecordsPerPage != null) {
            if (!validator.isNumber(requestRecordsPerPage)) {
                RequestUtils.sendError(response, HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            Optional<RecordsPerPage> numberOfRows = RecordsPerPage.getNumberOfRows(Integer.parseInt(requestRecordsPerPage));
            if (numberOfRows.isPresent()) {
                session.setAttribute(RECORDS_PER_PAGE, numberOfRows.get().getNumberOfRecords());
            } else {
                RequestUtils.sendError(response, HttpServletResponse.SC_NOT_FOUND);
                return;
            }
        }
        try {
            String currentPage = request.getParameter("currentPage");
            int currentPageValue = 1;
            if (validator.isNumber(currentPage)) {
                currentPageValue = Integer.parseInt(currentPage);
            } else if (!validator.isNumber(currentPage) && currentPage != null) {
                RequestUtils.sendError(response, HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            sessionRecordsPerPage = (Integer) session.getAttribute(RECORDS_PER_PAGE);
            int numberOfRows = userService.countOfRows();
            int numberOfPages = numberOfRows / sessionRecordsPerPage;
            if (numberOfPages % sessionRecordsPerPage >= 0) {
                numberOfPages++;
            }
            if (currentPageValue <= 0 || currentPageValue > numberOfPages) {
                RequestUtils.sendError(response, HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            List<UserDTO> users = userService.getPartOfUsers(currentPageValue, sessionRecordsPerPage);
            request.setAttribute("users", users);
            request.setAttribute("numberOfPages", numberOfPages);
            request.setAttribute("currentPage", currentPageValue);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_GET_LIST_OF_USERS, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }


    private UserDTO buildUserDTO(Map<String, String> parameterMap) {
        UserDTO userDTO = new UserDTO();
        String userName = getParameter(USER_LOGIN, parameterMap);
        userDTO.setUserName(userName);
        String password = getParameter(USER_PASSWORD, parameterMap);
        userDTO.setPassword(password);
        String name = getParameter(USER_NAME, parameterMap);
        userDTO.setName(name);
        String lastName = getParameter(USER_LAST_NAME, parameterMap);
        userDTO.setLastName(lastName);
        String email = getParameter(USER_EMAIL, parameterMap);
        userDTO.setEmail(email);
        String phone = getParameter(USER_PHONE, parameterMap);
        userDTO.setPhone(phone);
        return userDTO;
    }

    private String getParameter(String parameterName, Map<String, String> map) {
        return map.get(parameterName);
    }
}
