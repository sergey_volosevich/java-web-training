package by.training.sharing.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles annotation type {@link by.training.sharing.validation.ValidationPattern}.
 */

class ValidationPatternHandler implements AnnotationHandler {

    private static final Logger LOGGER = LogManager.getLogger(ValidationPatternHandler.class);
    private static final String FAILED_TO_VALIDATE_FIELD = "Can not validate field - \'{}\' with value 'null'";

    @Override
    public void handleAnnotation(Annotation annotation, Map<String, String> map, ValidationResult result) {
        String fieldName = ((ValidationPattern) annotation).fieldName();
        if (map.containsKey(fieldName)) {
            String fieldValue = map.get(fieldName);
            String regexp = ((ValidationPattern) annotation).regexp();
            Pattern pattern = Pattern.compile(regexp);
            if (Objects.nonNull(fieldValue)) {
                Matcher matcher = pattern.matcher(fieldValue);
                if (!matcher.matches()) {
                    result.setValidationInfo(fieldName, ((ValidationPattern) annotation).message());
                }
            } else {
                LOGGER.debug(FAILED_TO_VALIDATE_FIELD, fieldName);
                result.setValidationInfo(fieldName, ((ValidationPattern) annotation).message());
            }
        }
    }
}
