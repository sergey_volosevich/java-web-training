package by.training.port.controller;

import by.training.port.model.Ship;
import by.training.port.model.TaskType;

import java.util.Map;

public class ShipBuilder implements Builder<Ship> {

    @Override
    public Ship build(Map<String, String> values) {
        String idValue = values.get("id");
        long id = Long.parseLong(idValue);
        String taskType = values.get("taskType").toUpperCase().replace(" ", "_");
        TaskType type = TaskType.valueOf(taskType);
        String maxCapacity = values.get("maxCapacity");
        int maxCapacityValue = Integer.parseInt(maxCapacity);
        String currentNumContainer = values.get("currentNumContainer");
        int currentCapacity = Integer.parseInt(currentNumContainer);
        return new Ship(id, type, maxCapacityValue, currentCapacity);
    }
}
