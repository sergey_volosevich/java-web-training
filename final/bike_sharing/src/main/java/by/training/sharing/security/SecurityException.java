package by.training.sharing.security;

public class SecurityException extends Exception {

    private static final long serialVersionUID = 8944826371497846760L;

    public SecurityException(String message) {
        super(message);
    }

    public SecurityException(Throwable cause) {
        super(cause);
    }
}
