package by.training.sharing.core;

import by.training.sharing.bike.BikeController;
import by.training.sharing.bike.BikeDAOImpl;
import by.training.sharing.bike.BikeServiceImpl;
import by.training.sharing.command.ServletCommand;
import by.training.sharing.dao.*;
import by.training.sharing.price.PriceDAOImpl;
import by.training.sharing.price.PriceService;
import by.training.sharing.rental.RentalController;
import by.training.sharing.role.RoleDAOImpl;
import by.training.sharing.security.PasswordEncoder;
import by.training.sharing.security.SecurityServiceImpl;
import by.training.sharing.user.UserController;
import by.training.sharing.user.UserDAOImpl;
import by.training.sharing.user.UserServiceImpl;
import by.training.sharing.validation.ValidatorImpl;
import by.training.sharing.wallet.CreditCardDAOImpl;
import by.training.sharing.wallet.WalletController;
import by.training.sharing.wallet.WalletDAOImpl;
import by.training.sharing.wallet.WalletServiceImpl;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * This class represents the application context where beans are stored and registered.
 * Used to get the right bean, as well as to get the right command, needed to process a user request.
 */

public class ApplicationContext {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContext.class);

    private static final String APPLICATION_CONTEXT_SUCCESSFUL_DESTROYED = "Application context successful destroyed";
    private static final String APPLICATION_CONTEXT_SUCCESSFUL_INITIALIZED = "Application context successful initialized";

    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean isCreated = new AtomicBoolean();
    private static ApplicationContext instance;

    private BeanRegistry beanRegistry = new BeanRegistryImpl();

    private ApplicationContext() {
    }

    public static ApplicationContext getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ApplicationContext();
                    instance.init();
                    isCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    /**
     * Destroy application context, frees up resources, such as a database connection pool.
     * Invoked before application destroyed.
     */

    public void destroy() {
        ConnectionManager connectionManager = beanRegistry.getBean(BeanNameConstants.CONNECTION_MANAGER);
        connectionManager.destroyConnectionPool();
        beanRegistry.destroy();
        LOGGER.info(APPLICATION_CONTEXT_SUCCESSFUL_DESTROYED);
    }

    /**
     * Initial method, invoked after instance application context. The method registers all the necessary beans.
     */

    private void init() {
        registerDataSource();
        registerDAO();
        registerService();
        registerController();
        LOGGER.info(APPLICATION_CONTEXT_SUCCESSFUL_INITIALIZED);
    }

    private void registerDataSource() {
        beanRegistry.registerBean(ConnectionManagerImpl.class);
        beanRegistry.registerBean(TransactionManagerImpl.class);
        beanRegistry.registerBean(TransactionInterceptor.class);
        beanRegistry.registerBean(JDBCTemplateImpl.class);
    }

    private void registerDAO() {
        beanRegistry.registerBean(RoleDAOImpl.class);
        beanRegistry.registerBean(UserDAOImpl.class);
        beanRegistry.registerBean(BikeDAOImpl.class);
        beanRegistry.registerBean(PriceDAOImpl.class);
        beanRegistry.registerBean(WalletDAOImpl.class);
        beanRegistry.registerBean(CreditCardDAOImpl.class);
    }

    private void registerService() {
        beanRegistry.registerBean(UserServiceImpl.class);
        beanRegistry.registerBean(BikeServiceImpl.class);
        beanRegistry.registerBean(SecurityServiceImpl.class);
        beanRegistry.registerBean(PriceService.class);
        beanRegistry.registerBean(WalletServiceImpl.class);
        beanRegistry.registerBean(PasswordEncoder.class);
    }

    private void registerController() {
        beanRegistry.registerBean(ValidatorImpl.class);
        beanRegistry.registerBean(UserController.class);
        beanRegistry.registerBean(BikeController.class);
        beanRegistry.registerBean(RentalController.class);
        beanRegistry.registerBean(WalletController.class);
    }

    /**
     * Get bean from the application context by registered name indicated in the annotation {@link Bean#name()}
     *
     * @param name Name of the bean
     * @param <T>  Generalized type that can be cast to the required bean
     * @return Bean found by name, or <code>null</code> if bean with such name does not exist in the application context
     */

    public <T> T getBean(String name) {
        return this.beanRegistry.getBean(name);
    }

    /**
     * Returns a link to the functional interface {@link by.training.sharing.command.ServletCommand} with
     * realization method {@link by.training.sharing.command.ServletCommand#execute(HttpServletRequest, HttpServletResponse)}.
     * found by the name, specified in the {@link CommandName#name()} annotation, in Beans, marked with annotation
     * {@link by.training.sharing.core.Command}
     *
     * @param commandName Name of the method specified in the {@link CommandName#name()} annotation
     * @return Returns a link to the functional interface <code>ServletCommand</code>, or <code>null</code>
     * if command with {@param commandName}
     * does not exist in Beans, marked with annotation {@link by.training.sharing.core.Command}
     * in the application context.
     */
    public ServletCommand getCommand(String commandName) {
        return beanRegistry.getCommand(commandName);
    }
}
