package by.training.sharing.wallet;

/**
 * This class contains constants that are used for the common namespace on the display forms
 * Example <input type="text" name="CARD_NUMBER" value=""> and for retrieving
 * request parameters and validating the input parameters used by{@link by.training.sharing.validation.ValidatorImpl}.
 */

public class CreditCardFieldNameConstants {
    public static final String CARD_ID = "cardId";
    public static final String CARD_NUMBER = "cardNumber";
    public static final String HOLDER_NAME = "holderName";
    public static final String EXPIRE_YEAR = "expireYear";
    public static final String EXPIRE_MONTH = "expireMonth";
    public static final String CVV = "cvv";

    private CreditCardFieldNameConstants() {
    }
}
