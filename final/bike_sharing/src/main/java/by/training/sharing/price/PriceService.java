package by.training.sharing.price;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.service.ServiceException;

import java.util.List;

import static by.training.sharing.core.BeanNameConstants.PRICE_DAO;
import static by.training.sharing.core.BeanNameConstants.PRICE_SERVICE;

@Bean(name = PRICE_SERVICE)
public class PriceService {

    private PriceDAO priceDAO;

    public PriceService(@BeanQualifier(PRICE_DAO) PriceDAO priceDAO) {
        this.priceDAO = priceDAO;
    }

    public List<PriceDTO> findAll() throws ServiceException {
        try {
            return priceDAO.findAll();
        } catch (DAOException e) {
            throw new ServiceException(e);
        }
    }
}
