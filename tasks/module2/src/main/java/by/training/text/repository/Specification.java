package by.training.text.repository;

public interface Specification<T> {
    boolean match(T entity);
}
