package by.training.tariff.entity;

import java.util.Objects;

public class CallPrice {

    private double inNetworkCall;
    private double outNetworkCall;
    private double internationalCall;
    private double smsPrice;

    public CallPrice() {
    }

    public CallPrice(double inNetworkCall, double outNetworkCall, double internationalCall, double smsPrice) {
        this.inNetworkCall = inNetworkCall;
        this.outNetworkCall = outNetworkCall;
        this.internationalCall = internationalCall;
        this.smsPrice = smsPrice;
    }

    public double getInNetworkCall() {
        return inNetworkCall;
    }

    public void setInNetworkCall(double inNetworkCall) {
        this.inNetworkCall = inNetworkCall;
    }

    public double getOutNetworkCall() {
        return outNetworkCall;
    }

    public void setOutNetworkCall(double outNetworkCall) {
        this.outNetworkCall = outNetworkCall;
    }

    public double getInternationalCall() {
        return internationalCall;
    }

    public void setInternationalCall(double internationalCall) {
        this.internationalCall = internationalCall;
    }

    public double getSmsPrice() {
        return smsPrice;
    }

    public void setSmsPrice(double smsPrice) {
        this.smsPrice = smsPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CallPrice)) return false;
        CallPrice callPrice = (CallPrice) o;
        return Double.compare(callPrice.getInNetworkCall(), getInNetworkCall()) == 0 &&
                Double.compare(callPrice.getOutNetworkCall(), getOutNetworkCall()) == 0 &&
                Double.compare(callPrice.getInternationalCall(), getInternationalCall()) == 0 &&
                Double.compare(callPrice.getSmsPrice(), getSmsPrice()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getInNetworkCall(), getOutNetworkCall(), getInternationalCall(), getSmsPrice());
    }

    @Override
    public String toString() {
        return "CallPrice{" +
                "inNetworkCall=" + inNetworkCall +
                ", outNetworkCall=" + outNetworkCall +
                ", internationalCall=" + internationalCall +
                ", smsPrice=" + smsPrice +
                '}';
    }
}
