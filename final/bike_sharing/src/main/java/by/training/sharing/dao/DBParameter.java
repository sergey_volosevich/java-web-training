package by.training.sharing.dao;

/**
 * This class contains constants that are used for initialize {@link by.training.sharing.dao.ConnectionPool}
 */

class DBParameter {

    static final String DB_DRIVER = "db.driver";
    static final String DB_POOL_CAPACITY = "db.pool.capacity";
    static final String DB_URL = "db.url";
    static final String DB_USER = "db.user";
    static final String DB_PASSWORD = "db.password";

    private DBParameter() {
    }
}
