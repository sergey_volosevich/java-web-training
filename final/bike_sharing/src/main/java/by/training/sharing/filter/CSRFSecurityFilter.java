package by.training.sharing.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * This Web Filter is designed to protect against CSRF-attacks (Cross-Site Request Forgery).
 * Used 'Synchronizer Tokens' protection method.
 */

@WebFilter(filterName = "CSRFSecurityFilter", servletNames = "sharing")
public class CSRFSecurityFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(CSRFSecurityFilter.class);
    private static final String FILTER_INIT = "CSRF security filter successful initialized";
    private static final String CSRF_SECURITY_FILTER_SUCCESSFUL_DESTROYED = "CSRF security filter successful destroyed";
    private static final String CSRF_TOKEN_NOT_VALID = "CSRF token not valid.";
    private static final String CSRF = "csrf";
    private static final String ENCODING = "UTF-8";
    private static final String METHOD_POST = "POST";

    public void destroy() {
        LOGGER.info(CSRF_SECURITY_FILTER_SUCCESSFUL_DESTROYED);
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        httpServletRequest.setCharacterEncoding(ENCODING);
        String method = httpServletRequest.getMethod();
        if (!method.equals(METHOD_POST)) {
            chain.doFilter(req, resp);
            return;
        }
        HttpSession session = httpServletRequest.getSession();
        String sessionToken = (String) session.getAttribute(CSRF);
        String token = httpServletRequest.getParameter(CSRF);
        if (token == null || !token.equals(sessionToken)) {
            LOGGER.error(CSRF_TOKEN_NOT_VALID);
            ((HttpServletResponse) resp).sendError(HttpServletResponse.SC_FORBIDDEN);
        } else {
            chain.doFilter(req, resp);
        }
    }

    public void init(FilterConfig config) throws ServletException {
        LOGGER.info(FILTER_INIT);
    }
}
