package by.training.volosevich.library.entity;

import java.util.Objects;

public class Book extends Edition {

    private String author;

    public Book(String name, GenreType genreType, int numberOfPages, String publishingHouse, int releaseYear,
                EditionType type, String author) {
        super(name, genreType, numberOfPages, publishingHouse, releaseYear, type);
        this.author = author;
    }

    public String getAuthors() {
        return author;
    }

    public void setAuthors(String authors) {
        this.author = authors;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Book)) return false;
        if (!super.equals(o)) return false;
        Book book = (Book) o;
        return getAuthors().equals(book.getAuthors());
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getAuthors());
    }

    @Override
    public String toString() {
        return "Book{" +
                "authors='" + author + '\'' +
                "} " + super.toString();
    }
}
