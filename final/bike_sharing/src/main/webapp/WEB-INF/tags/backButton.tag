<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@ attribute name="value" required="true" rtexprvalue="true" %>

<div class="pt-2 d-flex justify-content-between">
    <a class="btn btn-outline-dark btn-block" role="button"
       href="<bsh:command value="${value}"/>">
        <fmt:message key="form.back.button"/></a>
</div>
