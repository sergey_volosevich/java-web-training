package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.entity.OperatorName;
import by.training.tariff.parser.StAXTariffParser;
import org.junit.Before;
import org.junit.Test;

import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

public class StAXParserCommandTest {

    private static final String VALID_XML = "valid.xml";
    private static final String NOT_XML = "notXML.txt";

    private String pathValidXML;
    private LightTariff tariff;
    private String pathNotXMLFile;

    @Before
    public void setUp() throws Exception {

        pathValidXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_XML))
                .toURI()).toString();
        pathNotXMLFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_XML)).toURI()).toString();
        tariff = new LightTariff();
        tariff.setPayroll(5.59);
        tariff.setDateCreation("2017-05-11");
        tariff.setTariffName("Light 1");
        tariff.setOperatorName(OperatorName.A1);
    }

    @Test
    public void executeValidFile() throws CommandException {
        StAXTariffParser stAXTariffParser = new StAXTariffParser();
        StAXParserCommand command = new StAXParserCommand(stAXTariffParser);
        List<LightTariff> list = command.execute(pathValidXML);
        assertEquals(18, list.size());
        assertEquals(tariff.getPayroll(), list.get(0).getPayroll(), 0.0);
        assertEquals(tariff.getDateCreation(), list.get(0).getDateCreation());
        assertEquals(tariff.getTariffName(), list.get(0).getTariffName());
        assertEquals(tariff.getOperatorName(), list.get(0).getOperatorName());
    }

    @Test(expected = CommandException.class)
    public void executeInvalidFile() throws CommandException {
        StAXTariffParser stAXTariffParser = new StAXTariffParser();
        StAXParserCommand command = new StAXParserCommand(stAXTariffParser);
        command.execute(pathNotXMLFile);
    }
}