package by.training.volosevich.library.repository;

import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.EditionType;

public class EditionTypeSpecification implements Specification<Edition> {

    private EditionType editionType;

    public EditionTypeSpecification(EditionType editionType) {
        this.editionType = editionType;
    }

    @Override
    public boolean match(Edition entity) {
        return entity.getType().equals(editionType);
    }
}
