<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="cmd" tagdir="/WEB-INF/tags" %>
<%@ tag import="by.training.sharing.ApplicationConstants" %>

<c:set var="admin" value="ADMIN"/>
<c:set var="user" value="USER"/>
<c:forEach items="${sessionScope.get('roles')}" var="role">
    <div class="mdl-layout__drawer">
        <span class="mdl-layout-title"><fmt:message key="user.hello.msg"/> <c:out value=" - ${userName}"/> </span>
        <nav class="mdl-navigation">
            <c:choose>
                <c:when test="${role eq user}">
                    <c:forEach items="${ApplicationConstants.USER_COMMANDS}" var="command">
                        <a class="mdl-navigation__link"
                           href="<cmd:command value="${command}"/>"><fmt:message
                                key="links.user.command.${command}"/> </a>
                    </c:forEach>
                </c:when>
                <c:when test="${role eq admin}">
                    <c:forEach items="${ApplicationConstants.ADMIN_COMMANDS}" var="command">
                        <a class="mdl-navigation__link"
                           href="<cmd:command value="${command}"/>"><fmt:message
                                key="links.admin.command.${command}"/> </a>
                    </c:forEach>
                </c:when>
            </c:choose>
        </nav>
    </div>
</c:forEach>
