package by.training.text.model;

import java.util.List;

public interface TextComposite extends TextLeaf {
    void addText(TextLeaf textLeaf);

    List<TextLeaf> getComponents();
}
