<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.sharing.user.UserFieldNameConstants" %>
<%@taglib prefix="bsh" uri="bikeSharing" %>

<fmt:message key="user.login.form.label" var="login"/>
<fmt:message key="user.password.form.label" var="password"/>
<fmt:message key="form.back.button" var="back"/>
<fmt:message key="form.submit.button" var="submit"/>

<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--12-col mdl-cell--middle">
        <form class="login-form" action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
            <input type="hidden" name="commandName" value="loginUser">
            <div class="mdc-text-field username">
                <input type="text" class="mdc-text-field__input" id="username-input"
                       name="<c:out value="${UserFieldNameConstants.USER_LOGIN}"/>"
                       value="<c:out value="${param.get(UserFieldNameConstants.USER_LOGIN)}"/>" required>
                <label class="mdc-floating-label" for="username-input">
                    <c:out value="${login}"/>
                </label>
                <div class="mdc-line-ripple"></div>
            </div>
            <div class="mdc-text-field-helper-line helper">
                <div class="mdc-text-field-helper-text"></div>
            </div>
            <div class="mdc-text-field password">
                <input type="password" class="mdc-text-field__input" id="password" required
                       name="<c:out value="${UserFieldNameConstants.USER_PASSWORD}"/>"
                       value="<c:out value="${param.get(UserFieldNameConstants.USER_PASSWORD)}"/>">
                <label class="mdc-floating-label" for="password">
                    <c:out value="${password}"/>
                </label>
                <div class="mdc-line-ripple"></div>
            </div>
            <div class="mdc-text-field-helper-line helper">
                <div class="mdc-text-field-helper-text"></div>
                <bsh:validationMessage fieldName="error" validationMessages="vrMessages"/>
            </div>
            <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>"/>
            <div class="button-container">
                <a role="button" class="mdc-button mdc-button--raised cancel"
                   href="<c:out value="${pageContext.request.contextPath}"/>">
                    <c:out value="${back}"/>
                </a>
                <button class="mdc-button mdc-button--raised submit">
                    <span class="mdc-button__label">
                        <c:out value="${submit}"/>
                    </span>
                </button>
            </div>
        </form>
    </div>
</div>