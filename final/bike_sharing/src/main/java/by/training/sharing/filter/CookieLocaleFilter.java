package by.training.sharing.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Optional;
import java.util.stream.Stream;

/**
 * This filter is responsible for changing the language in the application.
 */
@WebFilter(servletNames = "sharing", filterName = "CookieLocaleFilter", initParams = {
        @WebInitParam(name = "locale", value = "ru")})
public class CookieLocaleFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(CookieLocaleFilter.class);
    private static final String FILTER_INIT = "CookieLocaleFilter was successful initialized. Default locale is \'{}\'";
    private static final String FILTER_DESTROYED = "CookieLocaleFilter was successful destroyed.";
    private static final String COOKIE_NAME = "lang";
    private static final String DEFAULT_LOCALE = "en";

    private String locale;

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        locale = filterConfig.getInitParameter("locale");
        if (locale == null) {
            locale = DEFAULT_LOCALE;
        }
        LOGGER.info(FILTER_INIT, locale);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            String lang = request.getParameter(COOKIE_NAME);
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            if (("en".equalsIgnoreCase(lang) || "ru".equalsIgnoreCase(lang))) {
                Cookie langCookie = new Cookie(COOKIE_NAME, lang);
                langCookie.setPath(httpRequest.getContextPath());
                httpRequest.setAttribute(COOKIE_NAME, lang);
                ((HttpServletResponse) response).addCookie(langCookie);
            } else {
                Optional<Cookie[]> cookies = Optional.ofNullable(httpRequest.getCookies());
                Optional<Cookie> optionalCookie = cookies.map(Stream::of).orElse(Stream.empty())
                        .filter(cookie -> cookie.getName().equalsIgnoreCase(COOKIE_NAME)).findFirst();
                if (!optionalCookie.isPresent()) {
                    Cookie langCookie = new Cookie(COOKIE_NAME, locale);
                    httpRequest.setAttribute(COOKIE_NAME, locale);
                    langCookie.setPath(httpRequest.getContextPath());
                    ((HttpServletResponse) response).addCookie(langCookie);
                }
            }
        }
        chain.doFilter(request, response);
    }

    @Override
    public void destroy() {
        LOGGER.info(FILTER_DESTROYED);
    }
}
