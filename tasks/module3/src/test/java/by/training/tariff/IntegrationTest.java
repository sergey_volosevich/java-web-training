package by.training.tariff;


import by.training.tariff.command.*;
import by.training.tariff.controller.TariffController;
import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.DOMTariffParser;
import by.training.tariff.parser.SAXTariffParser;
import by.training.tariff.parser.StAXTariffParser;
import by.training.tariff.repository.TariffRepository;
import by.training.tariff.service.TariffService;
import by.training.tariff.validator.FileValidator;
import by.training.tariff.validator.XMLValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

@RunWith(JUnit4.class)
public class IntegrationTest {

    private TariffRepository repository;
    private TariffController controller;
    private static final String VALID_XML = "valid.xml";
    private String pathValidXML;
    private static final String NOT_WELL_FORMED = "notWellFormed.xml";
    private static final String NO_VALID_DATA = "invalidData.xml";
    private static final String NO_VALID_VALUES = "invalidValues.xml";
    private static final String NOT_XML = "notXML.txt";
    private String pathNotWellFormedXML;
    private String pathNoValidDataXML;
    private String pathNoValidValuesXML;
    private String pathNotXMLFile;

    @Before
    public void setUp() throws Exception {
        FileValidator fileValidator = new FileValidator();
        XMLValidator xmlValidator = new XMLValidator("tariff.xsd");
        CommandProvider<LightTariff> commandProvider = new CommandProviderImpl();
        DOMTariffParser domTariffParser = new DOMTariffParser();
        DOMParserCommand domParserCommand = new DOMParserCommand(domTariffParser);
        commandProvider.addCommand(CommandType.DOM, domParserCommand);
        SAXTariffParser saxTariffParser = new SAXTariffParser();
        SAXParserCommand saxParserCommand = new SAXParserCommand(saxTariffParser);
        commandProvider.addCommand(CommandType.SAX, saxParserCommand);
        StAXTariffParser stAXTariffParser = new StAXTariffParser();
        StAXParserCommand stAXParserCommand = new StAXParserCommand(stAXTariffParser);
        commandProvider.addCommand(CommandType.STAX, stAXParserCommand);
        repository = new TariffRepository();
        TariffService service = new TariffService(repository);
        controller = new TariffController(service, fileValidator, xmlValidator, commandProvider);
        pathValidXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_XML))
                .toURI()).toString();
        pathNotWellFormedXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_WELL_FORMED)).toURI()).toString();
        pathNoValidDataXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NO_VALID_DATA)).toURI()).toString();
        pathNoValidValuesXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NO_VALID_VALUES)).toURI()).toString();
        pathNotXMLFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_XML)).toURI()).toString();
    }

    @Test
    public void uploadFileUsingDOMCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "dom"));
        assertEquals(18, repository.getAll().size());
    }

    @Test
    public void uploadFileUsingSAXCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "sax"));
        assertEquals(18, repository.getAll().size());
    }

    @Test
    public void uploadFileUsingStAXCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "stax"));
        assertEquals(18, repository.getAll().size());
    }

    @Test(expected = CommandNotFoundException.class)
    public void uploadFileUsingInvalidCommand() {
        controller.uploadFile(pathValidXML, "jaxb");
    }

    @Test
    public void uploadFileNotXMLUsingSAXCommand() {
        assertFalse(controller.uploadFile(pathNotXMLFile, "sax"));
        assertEquals(0, repository.getAll().size());
    }

    @Test
    public void uploadFileInvalidValuesUsingSAXCommand() {
        assertFalse(controller.uploadFile(pathNoValidValuesXML, "sax"));
        assertEquals(0, repository.getAll().size());
    }

    @Test
    public void uploadFileInvalidDataUsingDOMCommand() {
        assertFalse(controller.uploadFile(pathNoValidDataXML, "dom"));
        assertEquals(0, repository.getAll().size());
    }

    @Test
    public void uploadFileInvalidDataUsingStAXCommand() {
        assertFalse(controller.uploadFile(pathNotWellFormedXML, "stax"));
        assertEquals(0, repository.getAll().size());
    }
}