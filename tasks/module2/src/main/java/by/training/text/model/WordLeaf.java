package by.training.text.model;

import by.training.text.entity.Word;
import by.training.text.service.WordService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WordLeaf implements TextLeaf {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MESSAGE = "method save() not supported";

    private Word word;
    private WordService service;

    public WordLeaf(Word word, WordService service) {
        this.word = word;
        this.service = service;
    }

    @Override
    public String getString() {
        return word.getValue() + word.getEnding();
    }

    @Override
    public void save(Long id) {
        word.setSentenceId(id);
        service.create(word);
    }

    @Override
    public void save() {
        LOGGER.log(Level.ERROR, MESSAGE);
    }
}
