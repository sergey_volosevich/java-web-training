package by.training.sharing.command;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Common interface for calling a method from classes marked with annotation
 * {@link by.training.sharing.core.Command}. Method must be marked by {@link by.training.sharing.core.CommandName}
 * to invoke it.
 */

@FunctionalInterface
public interface ServletCommand {

    void execute(HttpServletRequest req, HttpServletResponse resp) throws CommandException;
}
