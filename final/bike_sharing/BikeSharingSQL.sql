CREATE DATABASE  IF NOT EXISTS `bike_rental` /*!40100 DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci */ /*!80016 DEFAULT ENCRYPTION='N' */;
USE `bike_rental`;
-- MySQL dump 10.13  Distrib 8.0.16, for Win64 (x86_64)
--
-- Host: localhost    Database: bike_rental
-- ------------------------------------------------------
-- Server version	8.0.16

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bike`
--

DROP TABLE IF EXISTS `bike`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bike` (
  `bike_id` int(11) NOT NULL AUTO_INCREMENT,
  `serial_number` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `bike_state` int(1) NOT NULL DEFAULT '0',
  `rental_price_id` int(11) NOT NULL,
  PRIMARY KEY (`bike_id`),
  UNIQUE KEY `serial_number_unique` (`serial_number`),
  KEY `rental_price_id_idx` (`rental_price_id`),
  CONSTRAINT `fk_bike_rental_price_id` FOREIGN KEY (`rental_price_id`) REFERENCES `rental_price` (`rental_price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bike`
--

LOCK TABLES `bike` WRITE;
/*!40000 ALTER TABLE `bike` DISABLE KEYS */;
INSERT INTO `bike` VALUES (1,'Bike01','123',0,1),(2,'Bike02','123',0,1),(3,'Bike03','123',0,1),(4,'TestBike04','test04',0,2),(8,'TestBike05','test05',0,2),(9,'TesBike06','test06',0,2),(10,'TestBike06','test06',0,1),(11,'Test07','test07',0,2),(12,'TestBike07','test07',0,2),(13,'TestBike08','test08',0,2),(14,'TestBike09','test09',0,2),(15,'TestBike10','test10',0,2),(16,'TestBike12','test12',0,2),(17,'TestBike13','test13',0,2);
/*!40000 ALTER TABLE `bike` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bike_location`
--

DROP TABLE IF EXISTS `bike_location`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `bike_location` (
  `bike_location_id` int(11) NOT NULL AUTO_INCREMENT,
  `latitude` decimal(8,6) NOT NULL,
  `longitude` decimal(8,6) NOT NULL,
  `bike_id` int(11) NOT NULL,
  PRIMARY KEY (`bike_location_id`),
  UNIQUE KEY `bike_id_unique` (`bike_id`) /*!80000 INVISIBLE */,
  UNIQUE KEY `longitude_UNIQUE` (`longitude`),
  UNIQUE KEY `latitude_UNIQUE` (`latitude`),
  KEY `bike_id_idx` (`bike_id`),
  CONSTRAINT `fk_bike_location_bike_id` FOREIGN KEY (`bike_id`) REFERENCES `bike` (`bike_id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bike_location`
--

LOCK TABLES `bike_location` WRITE;
/*!40000 ALTER TABLE `bike_location` DISABLE KEYS */;
INSERT INTO `bike_location` VALUES (1,53.886971,27.420341,1),(2,53.906576,27.461981,2),(3,53.893785,27.447653,3),(4,53.885186,27.504743,4),(5,53.940266,27.596013,8),(6,53.920000,27.630000,9),(7,53.956727,27.693465,10),(8,53.899823,27.686545,11),(9,53.859865,27.468795,12),(10,53.868965,27.568965,13),(11,53.878933,27.616781,14),(12,53.911111,27.523412,15),(13,53.930962,27.509921,16),(14,53.928954,27.625678,17);
/*!40000 ALTER TABLE `bike_location` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental_information`
--

DROP TABLE IF EXISTS `rental_information`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rental_information` (
  `rental_id` int(11) NOT NULL AUTO_INCREMENT,
  `rental_date` timestamp NOT NULL,
  `distance` decimal(4,2) NOT NULL,
  `start_time` timestamp NOT NULL,
  `end_time` timestamp NOT NULL,
  `calories` int(11) NOT NULL,
  `average_speed` decimal(4,2) NOT NULL,
  `rental_price` decimal(4,2) NOT NULL,
  `bike_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`rental_id`),
  KEY `bike_id_idx` (`bike_id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `fk_rental_information_bike_id` FOREIGN KEY (`bike_id`) REFERENCES `bike` (`bike_id`),
  CONSTRAINT `fk_rental_information_user_account_id` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental_information`
--

LOCK TABLES `rental_information` WRITE;
/*!40000 ALTER TABLE `rental_information` DISABLE KEYS */;
INSERT INTO `rental_information` VALUES (1,'2019-11-15 23:00:00',12.30,'2019-11-16 15:25:30','2019-11-16 16:50:50',276,7.50,1.00,1,2);
/*!40000 ALTER TABLE `rental_information` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rental_price`
--

DROP TABLE IF EXISTS `rental_price`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `rental_price` (
  `rental_price_id` int(11) NOT NULL AUTO_INCREMENT,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `current_price` decimal(4,2) NOT NULL,
  PRIMARY KEY (`rental_price_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rental_price`
--

LOCK TABLES `rental_price` WRITE;
/*!40000 ALTER TABLE `rental_price` DISABLE KEYS */;
INSERT INTO `rental_price` VALUES (1,'2019-11-27 14:38:35',4.20),(2,'2019-12-02 23:00:00',1.50);
/*!40000 ALTER TABLE `rental_price` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role_relation`
--

DROP TABLE IF EXISTS `role_relation`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `role_relation` (
  `user_id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`role_id`),
  KEY `user_id_idx` (`user_id`) /*!80000 INVISIBLE */,
  KEY `role_id_idx` (`role_id`),
  CONSTRAINT `fk_user_has_role_role_id` FOREIGN KEY (`role_id`) REFERENCES `user_roles` (`role_id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_user_has_role_user_id` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role_relation`
--

LOCK TABLES `role_relation` WRITE;
/*!40000 ALTER TABLE `role_relation` DISABLE KEYS */;
INSERT INTO `role_relation` VALUES (1,1),(2,2),(3,2),(4,2),(5,2),(6,2),(7,2),(8,2),(9,2),(10,2),(11,2),(12,2),(13,2),(14,2),(16,2),(17,2);
/*!40000 ALTER TABLE `role_relation` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_account`
--

DROP TABLE IF EXISTS `user_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_account` (
  `user_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(30) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(128) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `enabled` int(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`user_id`),
  UNIQUE KEY `user_name_unique` (`user_name`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_account`
--

LOCK TABLES `user_account` WRITE;
/*!40000 ALTER TABLE `user_account` DISABLE KEYS */;
INSERT INTO `user_account` VALUES (1,'admin','$93$16$JhiYMaWCYwX-Yd5w_RcmnhVsEFbNjbv04B72JICnTd0',1),(2,'user123','$93$16$JhiYMaWCYwX-Yd5w_RcmnhVsEFbNjbv04B72JICnTd0',1),(3,'user1234','$93$16$6y1nEYSlY4fbHrEk_3mcIM0gPa6S1-9LqCjJy_phShI',1),(4,'user12345','$93$16$hDz3vUHG2DMeqHCYx1frn9rOgVq-lnS8QB16J5zka6g',1),(5,'user123q','$93$16$2nIc95m3CB66JFntUwfmcf-HnDmYiRRMcNSeo1DQ6o8',1),(6,'test01','$93$16$sl-YqSK5xTRqf5Ik40EP6EvMTMYESO86uki4g5BS9kc',1),(7,'test02','$93$16$K4ANZdfZ72z7rZymOCdxBk5FA739wPvFbKsH6JpuKSE',1),(8,'test04','$93$16$fhC8t4jCjoEfgDTUOtIAi0_-B8mMRBIcFDB-IMR4zGo',1),(9,'test06','$93$16$V9E-iutqi3F0Bvs4jTy1Utiwe7MT1BS_Ux9DkivsSpg',1),(10,'test07','$93$16$4On7zEdYHwt_dAgUzNPBTrTDqxLpR19QqYt5wS9ygTA',1),(11,'test08','$93$16$BX7aIdx4bs80y9DYKMMa-t_aHV_poA-vDArLQ6sZhSs',1),(12,'test09','$93$16$EyThjfsQt_40wW9V8YwuvFUhnYTI01Kry7RDOQ7bZBI',1),(13,'test10','$93$16$z8qum3y7dSp7ECEG7JRyLH2MYRFpZfjePmI5j8SfkEE',1),(14,'test11','$93$16$CYrsHBTxFZMLHdfvMwBvQFSl83Yel--3_dWkZaFWXHk',1),(16,'test12','$93$16$oY5jw2bRiSvlN1iLPtu0YKG6rgANdlj47-nXv48c2B8',1),(17,'test13','$93$16$UHOoUpMX17lRZOQTj1eE-v0CxKb7TD8pgCohXtkLVwY',1);
/*!40000 ALTER TABLE `user_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_info`
--

DROP TABLE IF EXISTS `user_info`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_info` (
  `user_info_id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_name` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(129) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` varchar(14) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`user_info_id`),
  UNIQUE KEY `email_unique` (`email`),
  UNIQUE KEY `user_id_unique` (`user_id`),
  KEY `user_id_idx` (`user_id`) /*!80000 INVISIBLE */,
  CONSTRAINT `fk_user_info_user_account_id` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_info`
--

LOCK TABLES `user_info` WRITE;
/*!40000 ALTER TABLE `user_info` DISABLE KEYS */;
INSERT INTO `user_info` VALUES (1,'Сергей','Волосевич','sergey.volosevich93@gmail.com','+375293894233',1),(2,'Иван','Иванов','ivan.ivanou92@gmail.com','+375291234567',2),(3,'Sergey','Volosevich','trueman230593@gmail.com','+375293894233',3),(4,'Сергей','Волосевич','sergey1.volosevich93@gmail.com','+375293894233',4),(5,'Владимир','Волосевич','vladimir.volosevich68@gmail.com','+375293894233',5),(6,'Сергей','Волосевич','test01@gmail.com','+375293894233',6),(7,'Владимир','Волосевич','test02@gmail.com','+375291234567',7),(8,'Николай','Иванов','test04@gmail.com','+375291234789',8),(9,'Сергей','Иванов','test06@gmail.com','+375291234567',9),(10,'Иван','Петров','test07@gmail.com','+375293334567',10),(11,'Сергей','Волосевич','test08@gmail.com','+375441234567',11),(12,'Иван','Иванов','test09@gmail.com','+375334567812',12),(13,'Сергей','Волосевич','test10@gmail.com','+375295671234',13),(14,'Сергей','Волосевич','test11@gmail.com','+375295672341',14),(16,'Sergey','Volosevich','test12@gmail.com','+375334546789',16),(17,'Сергей','Волосевич','test13@gmail.com','+375291235678',17);
/*!40000 ALTER TABLE `user_info` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_roles`
--

DROP TABLE IF EXISTS `user_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `user_roles` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `user_role` varchar(45) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'USER',
  PRIMARY KEY (`role_id`),
  UNIQUE KEY `user_role_UNIQUE` (`user_role`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_roles`
--

LOCK TABLES `user_roles` WRITE;
/*!40000 ALTER TABLE `user_roles` DISABLE KEYS */;
INSERT INTO `user_roles` VALUES (1,'ADMIN'),(2,'USER');
/*!40000 ALTER TABLE `user_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallet`
--

DROP TABLE IF EXISTS `wallet`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wallet` (
  `wallet_id` int(11) NOT NULL AUTO_INCREMENT,
  `current_amount` decimal(6,2) NOT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`wallet_id`),
  UNIQUE KEY `user_id_unique` (`user_id`),
  KEY `user_id_idx` (`user_id`),
  CONSTRAINT `fk_wallet_user_account_id` FOREIGN KEY (`user_id`) REFERENCES `user_account` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallet`
--

LOCK TABLES `wallet` WRITE;
/*!40000 ALTER TABLE `wallet` DISABLE KEYS */;
INSERT INTO `wallet` VALUES (1,50.50,2),(2,166.00,6),(4,0.00,7),(5,0.00,8),(7,0.00,13),(9,15.00,14),(10,80.00,17),(11,0.00,10);
/*!40000 ALTER TABLE `wallet` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `wallet_card`
--

DROP TABLE IF EXISTS `wallet_card`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `wallet_card` (
  `wallet_card_id` int(11) NOT NULL AUTO_INCREMENT,
  `card_number` varchar(19) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `holder_name` varchar(60) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `expire_date` date NOT NULL,
  `cvv` int(11) NOT NULL,
  `wallet_id` int(11) NOT NULL,
  PRIMARY KEY (`wallet_card_id`),
  KEY `fk_wallet_card_wallet_id_idx` (`wallet_id`),
  CONSTRAINT `fk_wallet_card_wallet_id` FOREIGN KEY (`wallet_id`) REFERENCES `wallet` (`wallet_id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `wallet_card`
--

LOCK TABLES `wallet_card` WRITE;
/*!40000 ALTER TABLE `wallet_card` DISABLE KEYS */;
INSERT INTO `wallet_card` VALUES (1,'5105 1051 0510 5100','sergei volosevich','2020-12-01',123,1),(2,'4012 8888 8888 1881','ivan ivanou','2021-11-01',1234,1);
/*!40000 ALTER TABLE `wallet_card` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'bike_rental'
--

--
-- Dumping routines for database 'bike_rental'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-12-18 16:34:07
