package by.training.sharing.bike;

import by.training.sharing.dao.RowMapper;

import java.math.BigDecimal;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;
import java.util.stream.Stream;

public class BikeRowMapper implements RowMapper<BikeDTO> {
    @Override
    public BikeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        BikeDTO bikeDTO = new BikeDTO();
        long bikeId = rs.getLong("bike_id");
        bikeDTO.setId(bikeId);
        String serialNumber = rs.getString("serial_number");
        bikeDTO.setSerialNumber(serialNumber);
        String password = rs.getString("password");
        bikeDTO.setBikePassword(password);
        int bikeState = rs.getInt("bike_state");
        BikeStatus[] states = BikeStatus.values();
        Optional<BikeStatus> status = Stream.of(states).filter(state -> state.ordinal() == bikeState).findFirst();
        BikeStatus bikeStatus = status.get();
        bikeDTO.setStatus(bikeStatus);
        BigDecimal latitude = rs.getBigDecimal("latitude");
        bikeDTO.setLatitude(latitude);
        BigDecimal longitude = rs.getBigDecimal("longitude");
        bikeDTO.setLongitude(longitude);
        long priceId = rs.getLong("rental_price_id");
        bikeDTO.setId(priceId);
        return bikeDTO;
    }
}
