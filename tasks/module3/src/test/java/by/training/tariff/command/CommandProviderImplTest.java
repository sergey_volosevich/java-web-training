package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.DOMTariffParser;
import by.training.tariff.parser.SAXTariffParser;
import by.training.tariff.parser.StAXTariffParser;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class CommandProviderImplTest {

    private CommandProviderImpl commandProvider;
    private DOMParserCommand domParserCommand;
    private SAXParserCommand saxParserCommand;
    private StAXParserCommand stAXParserCommand;

    @Before
    public void setUp() throws Exception {
        commandProvider = new CommandProviderImpl();
        DOMTariffParser domTariffParser = new DOMTariffParser();
        domParserCommand = new DOMParserCommand(domTariffParser);
        commandProvider.addCommand(CommandType.DOM, domParserCommand);
        SAXTariffParser saxTariffParser = new SAXTariffParser();
        saxParserCommand = new SAXParserCommand(saxTariffParser);
        commandProvider.addCommand(CommandType.SAX, saxParserCommand);
        StAXTariffParser stAXTariffParser = new StAXTariffParser();
        stAXParserCommand = new StAXParserCommand(stAXTariffParser);
        commandProvider.addCommand(CommandType.STAX, stAXParserCommand);
    }

    @Test
    public void getCommandValidCommands() {
        Command<LightTariff> domCommand = commandProvider.getCommand("dom");
        Command<LightTariff> saxCommand = commandProvider.getCommand("sax");
        Command<LightTariff> staxCommand = commandProvider.getCommand("stax");
        assertEquals(domParserCommand.getClass(), domCommand.getClass());
        assertEquals(saxParserCommand.getClass(), saxCommand.getClass());
        assertEquals(stAXParserCommand.getClass(), staxCommand.getClass());
    }

    @Test(expected = CommandNotFoundException.class)
    public void getCommandInvalidCommand() {
        commandProvider.getCommand("jaxb");
    }
}