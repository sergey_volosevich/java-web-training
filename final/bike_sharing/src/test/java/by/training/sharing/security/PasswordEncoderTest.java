package by.training.sharing.security;


import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class PasswordEncoderTest {
    private PasswordEncoder passwordEncoder;
    private String password;
    private String passwordHash;

    @Before
    public void setUp() throws Exception {
        password = "3894233qQ";
        passwordHash = "$93$16$JhiYMaWCYwX-Yd5w_RcmnhVsEFbNjbv04B72JICnTd0";
        passwordEncoder = new PasswordEncoder();
    }

    @Test
    public void hash() {
        String actual = passwordEncoder.hash(password.toCharArray());
        assertNotSame(password, actual);
    }

    @Test
    public void authenticate() {
        boolean actual = passwordEncoder.authenticate(password.toCharArray(), passwordHash);
        assertTrue(actual);
    }
}