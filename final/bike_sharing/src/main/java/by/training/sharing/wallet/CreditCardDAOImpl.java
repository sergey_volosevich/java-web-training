package by.training.sharing.wallet;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.EntityNotFoundException;
import by.training.sharing.dao.JDBCTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.Date;
import java.sql.SQLException;
import java.text.MessageFormat;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static by.training.sharing.core.BeanNameConstants.CREDIT_CARD_DAO;
import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;

@Bean(name = CREDIT_CARD_DAO)
public class CreditCardDAOImpl implements CreditCardDAO {

    private static final Logger LOGGER = LogManager.getLogger(CreditCardDAOImpl.class);

    private static final String NOT_FOUND_CARD_ENTITY = "Not found card entity by id - {0}";
    private static final String FAILED_TO_DELETE_CREDIT_CARD = "Failed to delete credit card. Cause:";

    private static final String INSERT_QUERY_WALLET_CARD = "INSERT INTO wallet_card (card_number, holder_name," +
            " expire_date, cvv, wallet_id) VALUES (?, ?, ?, ?, ?);";

    private static final String SELECT_CARD_BY_WALLET_ID = "select wallet_card_id, card_number, holder_name," +
            " expire_date, cvv, wallet_id from wallet_card where wallet_id = ?;";

    private static final String DELETE_CREDIT_CARD = "DELETE FROM wallet_card WHERE wallet_card_id = ?;";
    private static final String SELECT_CREDIT_CARD_BY_ID = "SELECT wallet_card_id, holder_name, card_number," +
            " expire_date, cvv, wallet_id FROM wallet_card where wallet_card_id = ?;";

    private JDBCTemplate jdbcTemplate;

    public CreditCardDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long save(CreditCardDTO creditCardDTO) throws DAOException {
        try {
            return jdbcTemplate.executeAndReturnKey(INSERT_QUERY_WALLET_CARD, new Object[]{creditCardDTO.getCardNumber(),
                    creditCardDTO.getHolderName(), Date.valueOf(creditCardDTO.getExpireDate()), creditCardDTO.getCvv(),
                    creditCardDTO.getWalletId()}).longValue();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(CreditCardDTO creditCardDTO) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        try {
            jdbcTemplate.executeUpdate(DELETE_CREDIT_CARD, new Object[]{id});
            return true;
        } catch (SQLException e) {
            LOGGER.error(FAILED_TO_DELETE_CREDIT_CARD, e);
            return false;
        }
    }

    @Override
    public CreditCardDTO getById(long id) throws DAOException {
        try {
            CreditCardDTO creditCard = jdbcTemplate.queryForObject(SELECT_CREDIT_CARD_BY_ID, new Object[]{id},
                    new CreditCardRowMapper());
            if (creditCard != null) {
                return creditCard;
            }
            throw new EntityNotFoundException(MessageFormat.format(NOT_FOUND_CARD_ENTITY, id));
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<CreditCardDTO> findAll() throws DAOException {
        return null;
    }

    @Override
    public Set<CreditCardDTO> findByWalletId(long id) throws DAOException {
        try {
            List<CreditCardDTO> creditCards = jdbcTemplate.queryForList(SELECT_CARD_BY_WALLET_ID, new Object[]{id},
                    new CreditCardRowMapper());
            return new HashSet<>(creditCards);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
