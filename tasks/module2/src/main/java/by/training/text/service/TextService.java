package by.training.text.service;

import by.training.text.entity.Text;
import by.training.text.repository.Repository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class TextService {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String TEXT_IS_NULL = "Text is null";
    private static final String NOT_VALID_ID = "id less or equals zero";

    private Repository<Text> repository;

    public TextService(Repository<Text> repository) {
        this.repository = repository;
    }

    public long create(Text entity) {
        if (entity != null) {
            return repository.create(entity);
        } else {
            LOGGER.log(Level.ERROR, TEXT_IS_NULL);
            return -1;
        }
    }

    public boolean update(Text text) {
        if (text != null) {
            return repository.update(text);
        }
        LOGGER.log(Level.ERROR, TEXT_IS_NULL);
        return false;
    }

    public Optional<Text> read(long id) {
        if (id > 0) {
            return Optional.ofNullable(repository.read(id));
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return Optional.empty();
    }

    public boolean delete(long id) {
        if (id > 0) {
            return repository.delete(id);
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return false;
    }

    public List<Text> getAll() {
        return repository.getAll();
    }
}
