package by.training.sharing.core;

import by.training.sharing.command.ServletCommand;

/**
 * Common interface of registry beans.
 */
public interface BeanRegistry {

    <T> void registerBean(T bean);

    <T> void registerBean(Class<T> beanClass);

    <T> T getBean(String name);

    <T> boolean removeBean(T bean);

    ServletCommand getCommand(String commandName);

    void destroy();
}