package by.training.sharing.wallet;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Set;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.assertEquals;

public class CreditCardDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(CreditCardDAOImpl.class);

    private CreditCardDAO creditCardDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace);) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        TransactionManager transactionManager = new TransactionManagerImpl();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        creditCardDAO = new CreditCardDAOImpl(jdbcTemplate);
    }

    @Test
    public void save() {
        CreditCardDTO creditCardDTO = new CreditCardDTO();
        creditCardDTO.setCardNumber("4012 8888 8888 1881");
        creditCardDTO.setHolderName("sergey volosevich");
        creditCardDTO.setExpireDate(LocalDate.of(2019, 12, 1));
        creditCardDTO.setCvv(123);
        creditCardDTO.setWalletId(2);
        long saveId = 0;
        try {
            saveId = creditCardDAO.save(creditCardDTO);
        } catch (DAOException e) {
            LOGGER.error(e);
        }
        assertEquals(4, saveId);
    }


    @Test(expected = EntityNotFoundException.class)
    public void delete() throws DAOException {
        CreditCardDTO creditCardDTO = new CreditCardDTO();
        creditCardDTO.setCardNumber("4012 8888 8888 1881");
        creditCardDTO.setHolderName("sergey volosevich");
        creditCardDTO.setExpireDate(LocalDate.of(2019, 12, 1));
        creditCardDTO.setCvv(123);
        creditCardDTO.setWalletId(1);
        long save = creditCardDAO.save(creditCardDTO);
        boolean isDelete = creditCardDAO.delete(save);
        creditCardDAO.getById(save);
    }

    @Test
    public void getById() throws DAOException {
        CreditCardDTO creditCardById = creditCardDAO.getById(1);
        assertEquals(1, creditCardById.getCardId());
    }

    @Test
    public void findByWalletId() throws DAOException {
        Set<CreditCardDTO> creditCards = creditCardDAO.findByWalletId(1);
        int size = creditCards.size();
        assertEquals(2, size);
    }
}