package by.training.sharing.security;

/**
 * This exception indicates that a user who is already logged in is trying to log in.
 */

public class LoginException extends SecurityException {
    private static final long serialVersionUID = -6578698351260775118L;

    public LoginException(String message) {
        super(message);
    }
}
