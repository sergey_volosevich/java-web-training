package by.training.text.parser;

public interface ParserChain<T> {

    T parse(String line);

    ParserChain<T> linkWith(ParserChain<T> next);
}
