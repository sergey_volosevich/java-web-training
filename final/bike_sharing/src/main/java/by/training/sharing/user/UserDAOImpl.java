package by.training.sharing.user;


import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.JDBCTemplate;

import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;
import static by.training.sharing.core.BeanNameConstants.USER_DAO;

@Bean(name = USER_DAO)
public class UserDAOImpl implements UserDAO {

    private static final String EMAIL = "email";
    private static final String LOGIN = "user_name";
    private static final String INSERT_QUERY_USER_ACCOUNT = "INSERT INTO user_account (user_name, password) " +
            "VALUES (?, ?);";
    private static final String INSERT_QUERY_USER_INFO = "insert into user_info (name, last_name, email, phone," +
            " user_id) values (?, ?, ?, ?, ?);";
    private static final String SELECT_EMAIL = "select ui.email from user_info ui where email = ?;";
    private static final String SELECT_LOGIN = "select ua.user_name from user_account ua where user_name = ?;";
    private static final String SELECT_BY_USER_LOGIN = "select ua.user_id, ua.user_name, ua.password," +
            " ui.name from user_account ua, user_info ui where ua.user_name = ? and ua.user_id = ui.user_id;";

    private static final String SELECT_ALL_USERS = "select ua.user_id, ua.user_name, ua.enabled, ui.name, ui.last_name, ui.email," +
            "ui.phone from user_account ua, user_info ui where ui.user_id = ua.user_id;";

    private static final String SELECT_ALL_USERS_BY_LIMIT_ROWS = "select ua.user_id, ua.user_name, ua.enabled, ui.name," +
            " ui.last_name, ui.email, ui.phone from user_account ua, user_info ui where ui.user_id = ua.user_id limit ?, ?;";

    private static final String SELECT_ROW_COUNT = "select count(user_id) as count from user_account;";

    private JDBCTemplate jdbcTemplate;

    public UserDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long save(UserDTO userDTO) throws DAOException {
        try {
            Number userId = jdbcTemplate.executeAndReturnKey(INSERT_QUERY_USER_ACCOUNT,
                    new Object[]{userDTO.getUserName(), userDTO.getPassword()});
            jdbcTemplate.executeUpdate(INSERT_QUERY_USER_INFO, new Object[]{userDTO.getName(), userDTO.getLastName(),
                    userDTO.getEmail(), userDTO.getPhone(), userId.longValue()});
            return userId.longValue();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(UserDTO userDTO) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public UserDTO getById(long id) {
        return null;
    }

    @Override
    public List<UserDTO> findAll() throws DAOException {
        try {
            return jdbcTemplate.query(SELECT_ALL_USERS, new UserRowMapper());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean findDuplicateEmail(String email) throws DAOException {
        try {
            String duplicateEmail = jdbcTemplate.queryForObject(SELECT_EMAIL, new Object[]{email},
                    (rs, rowNum) -> rs.getString(EMAIL));
            return duplicateEmail != null;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean findDuplicateLogin(String login) throws DAOException {
        try {
            String duplicateLogin = jdbcTemplate.queryForObject(SELECT_LOGIN, new Object[]{login}, (rs, rowNum) -> rs.getString(LOGIN));
            return duplicateLogin != null;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public Optional<UserDTO> findByLogin(String login) throws DAOException {
        try {
            UserDTO user = jdbcTemplate.queryForObject(SELECT_BY_USER_LOGIN, new Object[]{login},
                    (rs, rowNum) -> {
                        String userName = rs.getString(LOGIN);
                        String password = rs.getString("password");
                        String name = rs.getString("name");
                        long id = rs.getLong("user_id");
                        UserDTO userDTO = new UserDTO();
                        userDTO.setId(id);
                        userDTO.setUserName(userName);
                        userDTO.setPassword(password);
                        userDTO.setName(name);
                        return userDTO;
                    });
            return Optional.ofNullable(user);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<UserDTO> findPartOfUsers(int limit, int offset) throws DAOException {
        try {
            return jdbcTemplate.queryForList(SELECT_ALL_USERS_BY_LIMIT_ROWS, new Object[]{limit, offset}, new UserRowMapper());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public int countOfRows() throws DAOException {
        try {
            return jdbcTemplate.queryForObject(SELECT_ROW_COUNT, new Object[0], (rs, rowNum) -> rs.getInt("count"));
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
