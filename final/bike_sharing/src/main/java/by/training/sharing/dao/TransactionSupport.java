package by.training.sharing.dao;

import java.lang.annotation.*;

/**
 * This annotation is used to mark a class that at least one of its methods must be executed transitionally.
 */

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface TransactionSupport {
}
