<%@ tag language="java" trimDirectiveWhitespaces="true"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@tag import="by.training.sharing.service.RecordsPerPage" %>
<%@taglib prefix="cmd" tagdir="/WEB-INF/tags" %>
<%@attribute name="commandName" required="true" rtexprvalue="true" %>

<div class="text-right">
    <fmt:message key="pagination.records.page"/>
    <div class="btn-group">
        <c:set var="recordsNumber" value="${RecordsPerPage.values()}"/>
        <c:forEach var="recordNumber" items="${recordsNumber}">
            <c:set var="number" value="${recordNumber.numberOfRecords}"/>
            <c:choose>
                <c:when test="${recordsPerPage == number}">
                    <a href="<cmd:command value="${commandName}"/>&recordsPerPage=<c:out value="${number}"/>"
                       role="button" class="btn btn-outline-light disabled" tabindex="-1" aria-disabled="true">
                        <c:out value="${number}"/>
                    </a>
                </c:when>
                <c:otherwise>
                    <a href="<cmd:command value="${commandName}"/>&recordsPerPage=<c:out value="${number}"/>"
                       role="button" class="btn btn-outline-light">
                        <c:out value="${number}"/>
                    </a>
                </c:otherwise>
            </c:choose>
        </c:forEach>
    </div>
</div>

