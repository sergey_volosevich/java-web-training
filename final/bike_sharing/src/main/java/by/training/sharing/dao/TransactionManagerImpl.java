package by.training.sharing.dao;

import by.training.sharing.core.Bean;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.SQLException;

import static by.training.sharing.core.BeanNameConstants.TRANSACTION_MANAGER;

@Bean(name = TRANSACTION_MANAGER)
public class TransactionManagerImpl implements TransactionManager {

    private static final Logger LOGGER = LogManager.getLogger(TransactionManagerImpl.class);
    private static final String CONNECTION_SETS_IN_THREAD_LOCAL_STORAGE = "Connection sets in thread local storage";
    private static final String TRANSACTION_ALREADY_STARTED = "Transaction already started";
    private static final String CONNECTION_TO_REMOVE = "Connection to remove - {}";
    private static final String CONNECTION_REMOVED_FROM_THREAD_LOCAL = "Connection removed from thread local.";
    private static final String TRANSACTION_COMMITTED = "Transaction committed";
    private static final String ROLLBACK_TRANSACTION = "Rollback transaction";
    private static final String METHOD_CLOSE = "close";

    private final ConnectionPool connectionPool = ConnectionPool.getInstance();
    private ThreadLocal<Connection> localConnection = new ThreadLocal<>();

    @Override
    public void beginTransaction() throws SQLException {
        if (localConnection.get() == null) {
            Connection connection = connectionPool.getConnection();
            connection.setAutoCommit(false);
            localConnection.set(connection);
            LOGGER.debug(CONNECTION_SETS_IN_THREAD_LOCAL_STORAGE);
        } else {
            LOGGER.debug(TRANSACTION_ALREADY_STARTED);
        }
    }

    @Override
    public void commitTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.commit();
            connection.setAutoCommit(true);
            connection.close();
        }
        LOGGER.debug(CONNECTION_TO_REMOVE, localConnection.get());
        localConnection.remove();
        LOGGER.debug(CONNECTION_REMOVED_FROM_THREAD_LOCAL);
        LOGGER.debug(TRANSACTION_COMMITTED);
    }

    @Override
    public void rollbackTransaction() throws SQLException {
        Connection connection = localConnection.get();
        if (connection != null) {
            connection.rollback();
            connection.setAutoCommit(true);
            connection.close();
        }
        localConnection.remove();
        LOGGER.debug(CONNECTION_REMOVED_FROM_THREAD_LOCAL);
        LOGGER.debug(ROLLBACK_TRANSACTION);
    }

    @Override
    public Connection getConnection() {
        if (localConnection.get() != null) {
            return (Connection) Proxy.newProxyInstance(getClass().getClassLoader(), new Class[]{Connection.class},
                    (proxy, method, args) -> {
                        if (method.getName().equals(METHOD_CLOSE)) {
                            return null;
                        } else {
                            Connection realConnection = localConnection.get();
                            return method.invoke(realConnection, args);
                        }
                    });
        }
        return null;
    }
}
