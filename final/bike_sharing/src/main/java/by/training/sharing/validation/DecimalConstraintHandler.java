package by.training.sharing.validation;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.lang.annotation.Annotation;
import java.math.BigDecimal;
import java.util.Map;
import java.util.Objects;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * This class handles annotation type {@link by.training.sharing.validation.DecimalConstraint}.
 */

class DecimalConstraintHandler implements AnnotationHandler {

    private static final Logger LOGGER = LogManager.getLogger(DecimalConstraintHandler.class);
    private static final String FAILED_TO_VALIDATE_FIELD = "Can not validate field - \'{}\' with value 'null'";
    private static final String REGEX_TO_DECIMAL = "^\\d{1,2}\\.\\d{1,6}$";
    private static final String NOT_DECIMAL_NUMBER = "validation.msg.decimal.constraint";

    @Override
    public void handleAnnotation(Annotation annotation, Map<String, String> map, ValidationResult result) {
        String fieldName = ((DecimalConstraint) annotation).fieldName();
        if (map.containsKey(fieldName)) {
            String fieldValue = map.get(fieldName);
            if (Objects.nonNull(fieldValue)) {
                Pattern pattern = Pattern.compile(REGEX_TO_DECIMAL);
                Matcher matcher = pattern.matcher(fieldValue);
                if (matcher.matches()) {
                    String constraint = ((DecimalConstraint) annotation).value();
                    DecimalConstraintType constraintType = ((DecimalConstraint) annotation).constraint();
                    if (!checkValue(constraintType, new BigDecimal(fieldValue), new BigDecimal(constraint))) {
                        result.setValidationInfo(fieldName, ((DecimalConstraint) annotation).message());
                    }
                } else {
                    result.setValidationInfo(fieldName, NOT_DECIMAL_NUMBER);
                }
            } else {
                LOGGER.debug(FAILED_TO_VALIDATE_FIELD, fieldName);
                result.setValidationInfo(fieldName, ((DecimalConstraint) annotation).message());
            }
        }
    }

    private boolean checkValue(DecimalConstraintType type, BigDecimal checkedValue, BigDecimal constraint) {
        int i = checkedValue.compareTo(constraint);
        switch (type) {
            case GREATER_THAN:
                return i > 0;
            case LESS_THAN:
                return i < 0;
            case LESS_THAN_OR_EQUALS:
                return i <= 0;
            default:
                return i >= 0;
        }
    }
}
