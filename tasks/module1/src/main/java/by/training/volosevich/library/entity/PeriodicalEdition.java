package by.training.volosevich.library.entity;

public abstract class PeriodicalEdition extends Edition {
    private PeriodicityType periodicity;
    private int releaseNumber;    //release number from the start of publication
    private int yearReleaseNumber;     //release number from the beginning of the year

    public PeriodicalEdition(String name, GenreType genreType, int numberOfPages, String publishingHouse,
                             int releaseYear, EditionType type, PeriodicityType periodicity, int releaseNumber,
                             int yearReleaseNumber) {
        super(name, genreType, numberOfPages, publishingHouse, releaseYear, type);
        this.periodicity = periodicity;
        this.releaseNumber = releaseNumber;
        this.yearReleaseNumber = yearReleaseNumber;
    }

    public PeriodicityType getPeriodicity() {
        return periodicity;
    }

    public void setPeriodicity(PeriodicityType periodicity) {
        this.periodicity = periodicity;
    }

    public int getReleaseNumber() {
        return releaseNumber;
    }

    public void setReleaseNumber(int releaseNumber) {
        this.releaseNumber = releaseNumber;
    }

    public int getYearReleaseNumber() {
        return yearReleaseNumber;
    }

    public void setYearReleaseNumber(int yearReleaseNumber) {
        this.yearReleaseNumber = yearReleaseNumber;
    }
}
