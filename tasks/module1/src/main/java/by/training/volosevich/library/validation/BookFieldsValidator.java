package by.training.volosevich.library.validation;

import by.training.volosevich.library.validation.util.ValueChecker;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class BookFieldsValidator extends EditionFieldsValidator {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final int MIN_SIZE_OF_AUTHOR_FIELD = 10;

    private List<String> fields = new ArrayList<>();

    public BookFieldsValidator() {
        fields.add("author");
    }

    public List<String> validate(Map<String, String> values) {
        validateEditionFields(values);
        for (String field : fields) {
            if (values.containsKey(field)) {
                String value = values.get(field);
                if (field.equals("author")) {
                    validateAuthorField(field, value);
                }
            } else {
                errorMessages.add(ValidationMessage.formatMessage(field, ValidationMessage.NOT_ENOUGH_FIELD));
                LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(field,
                        ValidationMessage.NOT_ENOUGH_FIELD));
            }
        }
        return errorMessages;
    }

    private void validateAuthorField(String nameField, String value) {
        if (ValueChecker.isNotNull(value)) {
            if (value.length() < MIN_SIZE_OF_AUTHOR_FIELD) {
                errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_AUTHOR));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_AUTHOR));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }
}
