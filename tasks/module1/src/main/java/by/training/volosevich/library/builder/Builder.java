package by.training.volosevich.library.builder;

public interface Builder<T> {
    T build();
}
