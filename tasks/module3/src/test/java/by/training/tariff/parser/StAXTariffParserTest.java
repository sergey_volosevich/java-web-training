package by.training.tariff.parser;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.entity.OperatorName;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;
import java.util.List;
import java.util.Objects;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class StAXTariffParserTest {

    private static final String VALID_XML = "valid.xml";
    private static final String NOT_XML = "notXML.txt";
    private String pathValidXML;
    private LightTariff tariff;
    private String pathNotXMLFile;

    @Before
    public void setUp() throws Exception {
        pathValidXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_XML))
                .toURI()).toString();
        pathNotXMLFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_XML)).toURI()).toString();
        tariff = new LightTariff();
        tariff.setPayroll(5.59);
        tariff.setDateCreation("2017-05-11");
        tariff.setTariffName("Light 1");
        tariff.setOperatorName(OperatorName.A1);
    }

    @Test
    public void parseValidFile() throws ParserException {
        StAXTariffParser parser = new StAXTariffParser();
        List<LightTariff> list = parser.parse(pathValidXML);
        assertEquals(18, list.size());
        assertEquals(tariff.getPayroll(), list.get(0).getPayroll(), 0.0);
        assertEquals(tariff.getDateCreation(), list.get(0).getDateCreation());
        assertEquals(tariff.getTariffName(), list.get(0).getTariffName());
        assertEquals(tariff.getOperatorName(), list.get(0).getOperatorName());
    }

    @Test(expected = ParserException.class)
    public void parseInvalidFile() throws ParserException {
        StAXTariffParser parser = new StAXTariffParser();
        parser.parse(pathNotXMLFile);
    }
}