package by.training.sharing.user;

import by.training.sharing.dao.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Optional;

public class UserRowMapper implements RowMapper<UserDTO> {
    @Override
    public UserDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        UserDTO userDTO = new UserDTO();
        long userId = rs.getLong("user_id");
        userDTO.setId(userId);
        String userName = rs.getString("user_name");
        userDTO.setUserName(userName);
        int enabled = rs.getInt("enabled");
        Optional<UserStatus> status = UserStatus.getStatus(enabled);
        userDTO.setStatus(status.get());
        String name = rs.getString("name");
        userDTO.setName(name);
        String lastName = rs.getString("last_name");
        userDTO.setLastName(lastName);
        String email = rs.getString("email");
        userDTO.setEmail(email);
        String phone = rs.getString("phone");
        userDTO.setPhone(phone);
        return userDTO;
    }
}
