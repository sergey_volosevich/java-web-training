package by.training.volosevich.library.validation;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResult {

    private List<String> messages;
    private Map<String, List<String>> validationInfo = new HashMap<>();

    public Map<String, List<String>> getValidationInfo() {
        return validationInfo;
    }

    public void setValidationInfo(String code, String message) {
        messages = new ArrayList<>();
        messages.add(message);
        validationInfo.put(code, messages);
    }

    public void setValidationInfo(String code, List<String> messages) {
        validationInfo.put(code, messages);
    }

    public boolean isValid() {
        return validationInfo.isEmpty();
    }
}
