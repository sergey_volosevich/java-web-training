package by.training.sharing.validation;


import java.lang.annotation.*;

/**
 * This annotation is used to declare restrictions for fields of type {@link java.math.BigDecimal}.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Repeatable(DecimalConstraints.class)
public @interface DecimalConstraint {
    /**
     * Sets limits on the value
     *
     * @return limit value
     */
    String value();

    /**
     * Sets the validation message.
     *
     * @return validation message.
     */
    String message();

    /**
     * Sets field name to check.
     *
     * @return filed name.
     */
    String fieldName();

    /**
     * Sets the type of constraint
     *
     * @return constraint type
     */
    DecimalConstraintType constraint();
}

