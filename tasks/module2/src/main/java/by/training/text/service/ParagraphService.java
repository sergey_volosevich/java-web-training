package by.training.text.service;

import by.training.text.entity.Paragraph;
import by.training.text.repository.ParagraphByTextIdSpecification;
import by.training.text.repository.Repository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class ParagraphService {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String PARAGRAPH_IS_NULL = "Paragraph is null";
    private static final String NOT_VALID_ID = "id less or equals zero";

    private Repository<Paragraph> repository;

    public ParagraphService(Repository<Paragraph> repository) {
        this.repository = repository;
    }

    public long create(Paragraph entity) {
        if (entity != null) {
            return repository.create(entity);
        } else {
            LOGGER.log(Level.ERROR, PARAGRAPH_IS_NULL);
            return -1;
        }
    }

    public List<Paragraph> findParagraphByTextId(Long id) {
        return repository.find(new ParagraphByTextIdSpecification(id));
    }

    public boolean update(Paragraph paragraph) {
        if (paragraph != null) {
            return repository.update(paragraph);
        }
        LOGGER.log(Level.ERROR, PARAGRAPH_IS_NULL);
        return false;
    }

    public Optional<Paragraph> read(long id) {
        if (id > 0) {
            return Optional.ofNullable(repository.read(id));
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return Optional.empty();
    }

    public boolean delete(long id) {
        if (id > 0) {
            return repository.delete(id);
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return false;
    }

    public List<Paragraph> getAll() {
        return repository.getAll();
    }
}
