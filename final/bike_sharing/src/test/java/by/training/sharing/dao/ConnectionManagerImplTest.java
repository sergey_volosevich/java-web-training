package by.training.sharing.dao;

import by.training.sharing.core.ApplicationContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.*;

public class ConnectionManagerImplTest {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionManagerImplTest.class);

    private static final String SELECT_WALLET_BY_ID = "SELECT wallet_id, current_amount, user_id FROM wallet" +
            " where wallet_id = ?;";

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace);) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Test
    public void getConnection() throws SQLException {
        ConnectionManagerImpl connectionManager = new ConnectionManagerImpl(Mockito.mock(TransactionManagerImpl.class));
        long id = 0;
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(SELECT_WALLET_BY_ID)) {
            statement.setLong(1, 1);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
               id = resultSet.getLong("wallet_id");
            }
        }
        assertEquals(1, id);
    }
}