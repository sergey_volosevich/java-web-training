package by.training.sharing.core;

public class MissedAnnotationException extends BeanRegistrationException {

    private static final long serialVersionUID = -7711194550418126576L;

    public MissedAnnotationException(String message) {
        super(message);
    }

}
