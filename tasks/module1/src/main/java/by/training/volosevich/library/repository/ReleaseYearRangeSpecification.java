package by.training.volosevich.library.repository;

import by.training.volosevich.library.entity.Edition;

public class ReleaseYearRangeSpecification implements Specification<Edition> {

    private int lowerRange;
    private int upperRange;

    public ReleaseYearRangeSpecification(int lowerRange, int upperRange) {
        this.lowerRange = lowerRange;
        this.upperRange = upperRange;
    }

    @Override
    public boolean match(Edition entity) {
        return entity.getReleaseYear() >= lowerRange && entity.getReleaseYear() <= upperRange;
    }
}
