package by.training.tariff.parser;

import by.training.tariff.entity.*;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamConstants;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class StAXTariffParser implements Parser<LightTariff> {

    private static final String NO_SUCH_FILED_IN_DOCUMENT = "No such filed in document - {1}";
    private ArrayList<LightTariff> tariffs;
    private String content;
    private CallPrice price;
    private AdditionalInfo info;
    private LightTariff tariff;

    @Override
    public List<LightTariff> parse(String pathXML) throws ParserException {
        XMLInputFactory factory = XMLInputFactory.newInstance();
        factory.setProperty(XMLInputFactory.IS_SUPPORTING_EXTERNAL_ENTITIES, false);
        try (FileInputStream fileInputStream = new FileInputStream(pathXML)) {
            XMLStreamReader reader = factory.createXMLStreamReader(fileInputStream);
            tariffs = new ArrayList<>();
            while (reader.hasNext()) {
                int event = reader.next();
                switch (event) {
                    case XMLStreamConstants.START_ELEMENT:
                        parseStartElement(reader);
                        break;
                    case XMLStreamConstants.END_ELEMENT:
                        parseEndElement(reader);
                        break;
                    case XMLStreamConstants.CHARACTERS:
                        content = reader.getText();
                        break;
                    default:
                }
            }
        } catch (XMLStreamException | IOException e) {
            throw new ParserException(e);
        }
        return tariffs;
    }

    private void parseEndElement(XMLStreamReader reader) throws ParserException {
        final String tagName = reader.getLocalName();
        switch (tagName) {
            case TariffFields.TARIFF_NAME:
                tariff.setTariffName(content);
                break;
            case TariffFields.DATE_CREATION:
                tariff.setDateCreation(content);
                break;
            case TariffFields.OPERATOR_NAME:
                OperatorName operatorName = OperatorName.valueOf(content.toUpperCase());
                tariff.setOperatorName(operatorName);
                break;
            case TariffFields.PAYROLL:
                tariff.setPayroll(Double.parseDouble(content));
                break;
            case TariffFields.FREE_MINUTES_IN_NETWORK:
                tariff.setFreeMinutesInNetwork(Integer.parseInt(content));
                break;
            case TariffFields.IN_NETWORK_CALL:
                price.setInNetworkCall(Double.parseDouble(content));
                break;
            case TariffFields.OUT_NETWORK_CALL:
                price.setOutNetworkCall(Double.parseDouble(content));
                break;
            case TariffFields.INTERNATIONAL_CALL:
                price.setInternationalCall(Double.parseDouble(content));
                break;
            case TariffFields.SMS_PRICE:
                price.setSmsPrice(Double.parseDouble(content));
                break;
            case TariffFields.CALL_SMS_PRICES:
                tariff.setPrice(price);
                break;
            case TariffFields.BILLING_TYPE:
                BillingType billingType = BillingType.valueOf(content.toUpperCase());
                info.setType(billingType);
                break;
            case TariffFields.FAVORITE_NUMBER:
                info.setFavoriteNumbers(Integer.parseInt(content));
                break;
            case TariffFields.CONNECTION_PRICE:
                info.setConnectionPrice(Double.parseDouble(content));
                break;
            case TariffFields.ADDITIONAL_INFO:
                tariff.setAdditionalInfo(info);
                break;
            case TariffFields.FREE_MINUTES_OUT_NETWORK:
                ((ComfortTariff) tariff).setFreeMinutesOutNetwork(Integer.parseInt(content));
                break;
            case TariffFields.FREE_MINUTES_INTERNATIONAL_CALL:
                ((BusinessTariff) tariff).setFreeInternationalCall(Integer.parseInt(content));
                break;
            case TariffFields.TARIFFS:
                break;
            case TariffFields.LIGHT_TARIFF:
            case TariffFields.COMFORT_TARIFF:
            case TariffFields.BUSINESS_TARIFF:
                tariffs.add(tariff);
                break;
            default:
                throw new ParserException(MessageFormat.format(NO_SUCH_FILED_IN_DOCUMENT, tagName));
        }
    }

    private void parseStartElement(XMLStreamReader reader) {
        switch (reader.getLocalName()) {
            case TariffFields.LIGHT_TARIFF:
                tariff = new LightTariff();
                setAttributes(reader);
                break;
            case TariffFields.COMFORT_TARIFF:
                tariff = new ComfortTariff();
                setAttributes(reader);
                break;
            case TariffFields.BUSINESS_TARIFF:
                tariff = new BusinessTariff();
                setAttributes(reader);
                break;
            case TariffFields.CALL_SMS_PRICES:
                price = new CallPrice();
                break;
            case TariffFields.ADDITIONAL_INFO:
                info = new AdditionalInfo();
                break;
            default:
        }
    }

    private void setAttributes(XMLStreamReader reader) {
        for (int i = 0; i < reader.getAttributeCount(); i++) {
            String attributeLocalName = reader.getAttributeLocalName(i);
            if (attributeLocalName.equals(TariffFields.ID)) {
                long id = Long.parseLong(reader.getAttributeValue(i).substring(1));
                tariff.setId(id);
            } else if (attributeLocalName.equals(TariffFields.FREE_INTERNET_TRAFFIC)) {
                int internetTraffic = Integer.parseInt(reader.getAttributeValue(i));
                tariff.setInternetTraffic(internetTraffic);
            }
        }
    }
}
