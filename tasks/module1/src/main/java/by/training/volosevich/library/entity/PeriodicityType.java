package by.training.volosevich.library.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum PeriodicityType {
    EVERYDAY,
    EVERY_WEEK,
    EVERY_MONTH,
    EVERY_QUARTER;

    public static Optional<PeriodicityType> fromString(String type) {
        return Stream.of(PeriodicityType.values())
                .filter(t -> t.name().equalsIgnoreCase(type.replace(" ", "_")))
                .findFirst();
    }
}
