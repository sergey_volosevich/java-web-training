<%@ tag pageEncoding="UTF-8" language="java" trimDirectiveWhitespaces="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ attribute name="commandName" required="true" rtexprvalue="true" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<nav>
    <ul class="pagination justify-content-center">
        <c:if test="${currentPage != 1}">
            <li class="page-item">
                <a class="btn btn-outline-light"
                   href="<bsh:command value="${commandName}"/>&currentPage=<c:out value="${currentPage-1}"/>">
                <fmt:message key="pagination.previous"/>
            </a>
            </li>
        </c:if>
        <c:forEach begin="1" end="${numberOfPages}" var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <li class="page-item active" aria-current="page"><a
                            href="<bsh:command value="${commandName}"/>&currentPage=<c:out value="${i}"/>"
                            class="page-link btn btn-outline-light">
                        <span class="sr-only">(current)</span><c:out value="${i}"/></a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item"><a class="btn btn-outline-light"
                                             href="<bsh:command value="${commandName}"/>&currentPage=<c:out value="${i}"/>">
                        <c:out value="${i}"/></a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:if test="${currentPage lt numberOfPages}">
            <li class="page-item">
                <a class="btn btn-outline-light"
                   href="<bsh:command value="${commandName}"/>&currentPage=<c:out value="${currentPage+1}"/>">
                    <fmt:message key="pagination.next"/></a>
            </li>
        </c:if>
    </ul>
</nav>