package by.training.sharing.wallet;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.core.Command;
import by.training.sharing.core.CommandName;
import by.training.sharing.security.CredentialNotFoundException;
import by.training.sharing.security.SecurityService;
import by.training.sharing.security.UserCredential;
import by.training.sharing.service.ServiceException;
import by.training.sharing.util.RequestUtils;
import by.training.sharing.validation.ValidationResult;
import by.training.sharing.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDate;
import java.time.Month;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static by.training.sharing.ApplicationConstants.*;
import static by.training.sharing.core.BeanNameConstants.*;
import static by.training.sharing.wallet.CreditCardFieldNameConstants.*;

/**
 * This class contains commands for working with {@link by.training.sharing.wallet.WalletDTO} and
 * {@link by.training.sharing.wallet.CreditCardDTO}. Each method is one command executed per 1 request from the client.
 */

@Command
@Bean(name = WALLET_CONTROLLER)
public class WalletController {

    private static final Logger LOGGER = LogManager.getLogger(WalletController.class);
    private static final String CARD_NUMBER_DUPLICATE_MSG = "creditCard.cardNumber.validation.duplicate.msg";
    private static final String FAILED_TO_FIND_CREDIT_CARD = "Failed to find credit card. Cause:";
    private static final String FAILED_TO_ADD_CREDIT_CARD = "Failed to add credit card. Cause:";
    private static final String FAILED_TO_FIND_USER = "Failed to find user. Cause:";
    private static final String FAILED_TO_FIND_WALLET = "Failed to find wallet. Cause: ";
    private static final String WALLET_ATTRIBUTE = "wallet";
    private static final String SUCCESS_MSG = "ok";
    private static final String CARD_ERROR_MSG = "card.error";
    private static final String WALLET_AMOUNT = "amount";

    private Validator validator;
    private SecurityService securityService;
    private WalletService walletService;

    public WalletController(@BeanQualifier(VALIDATOR) Validator validator,
                            @BeanQualifier(SECURITY_SERVICE) SecurityService securityService,
                            @BeanQualifier(WALLET_SERVICE) WalletService walletService) {
        this.validator = validator;
        this.securityService = securityService;
        this.walletService = walletService;
    }

    /**
     * This method gets and displays view with wallet and wallet actions.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */
    @CommandName(name = GET_WALLET_VIEW)
    public void getWalletView(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.getFlushAttribute(request, SUCCESS_ADDING);
        RequestUtils.addCsrfToken(request);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_WALLET_VIEW);
        HttpSession session = request.getSession();
        try {
            UserCredential currentUser = securityService.getCurrentUser(session);
            Optional<WalletDTO> userWalletOptional = walletService.findByUserId(currentUser.getUserId());
            userWalletOptional.ifPresent(wallet -> session.setAttribute(WALLET_ATTRIBUTE, wallet));
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (CredentialNotFoundException e) {
            LOGGER.error(FAILED_TO_FIND_USER, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_UNAUTHORIZED);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_FIND_WALLET, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method gets and displays view with form to add credit card to wallet.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_ADD_CARD_VIEW)
    public void getCreateWallet(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.addCsrfToken(request);
        RequestUtils.getFlushAttribute(request, SUCCESS_ADDING);
        HttpSession session = request.getSession();
        WalletDTO wallet = (WalletDTO) session.getAttribute(WALLET_ATTRIBUTE);
        if (wallet == null) {
            RequestUtils.sendRedirectToCommand(request, response, GET_WALLET_VIEW);
            return;
        }
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_ADD_CARD_VIEW);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    /**
     * This method creates wallet for user. If creation success, redirects to view, that displays current amount
     * of wallet with success message. If wallet does not created, sends error.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_CREATE_WALLET)
    public void createWallet(HttpServletRequest request, HttpServletResponse response) {
        HttpSession session = request.getSession();
        try {
            UserCredential currentUser = securityService.getCurrentUser(session);
            boolean isCreated = walletService.createWallet(currentUser.getUserId());
            if (!isCreated) {
                RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
                return;
            }
            RequestUtils.removeCsrfToken(request);
            RequestUtils.sendRedirectToCommand(request, response, GET_WALLET_VIEW);
        } catch (CredentialNotFoundException e) {
            LOGGER.error(FAILED_TO_FIND_USER);
            RequestUtils.sendError(response, HttpServletResponse.SC_UNAUTHORIZED);
        }
    }

    /**
     * This method add credit card to wallet. If adding is success - redirects to view with card form adding with success
     * message, otherwise - forwards to view with card adding form with displayed validation messages.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_ADD_CARD)
    public void addCreditCard(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_ADD_CARD_VIEW);
        Map<String, String> fieldValues = RequestUtils.getParametersFromRequest(request, HOLDER_NAME, CARD_NUMBER,
                CVV, EXPIRE_MONTH, EXPIRE_YEAR);
        ValidationResult validationResult = validator.validate(fieldValues, CreditCardDTO.class);
        request.setAttribute(VALIDATION_MESSAGES, validationResult);
        if (!validationResult.isValid()) {
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        CreditCardDTO creditCardDTO = buildCreditCardDTO(fieldValues);
        HttpSession session = request.getSession();
        WalletDTO wallet = (WalletDTO) session.getAttribute(WALLET_ATTRIBUTE);
        long walletId = wallet.getId();
        creditCardDTO.setWalletId(walletId);
        try {
            long creditCardId = walletService.addCreditCard(creditCardDTO);
            if (creditCardId < 0) {
                validationResult.setValidationInfo(CARD_NUMBER, CARD_NUMBER_DUPLICATE_MSG);
                RequestUtils.forward(request, response, MAIN_LAYOUT);
                return;
            }
            CreditCardDTO card = walletService.getCreditCardById(creditCardId);
            wallet.getCreditCards().add(card);
            RequestUtils.setFlushAttribute(request, SUCCESS_ADDING, SUCCESS_MSG);
            RequestUtils.removeCsrfToken(request);
            RequestUtils.sendRedirectToCommand(request, response, GET_ADD_CARD_VIEW);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_ADD_CREDIT_CARD, e);
            RequestUtils.removeCsrfToken(request);
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    /**
     * This method gets and displays view with fill balance form.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_FILL_BALANCE_VIEW)
    public void getFillBalanceView(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.getFlushAttribute(request, SUCCESS_ADDING);
        RequestUtils.addCsrfToken(request);
        HttpSession session = request.getSession();
        Object sessionAttribute = session.getAttribute(WALLET_ATTRIBUTE);
        if (sessionAttribute == null) {
            RequestUtils.sendRedirectToCommand(request, response, GET_WALLET_VIEW);
            return;
        }
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_FILL_BALANCE_VIEW);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    /**
     * This method gets and displays view with list of cards.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_DELETE_CARD_VIEW)
    public void getDeleteCardView(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.addCsrfToken(request);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_DELETE_CARD_VIEW);
        RequestUtils.getFlushAttribute(request, SUCCESS_ADDING);
        HttpSession session = request.getSession();
        Object sessionAttribute = session.getAttribute(WALLET_ATTRIBUTE);
        if (sessionAttribute == null) {
            RequestUtils.sendRedirectToCommand(request, response, GET_WALLET_VIEW);
            return;
        }
        WalletDTO wallet = ((WalletDTO) sessionAttribute);
        long id = wallet.getId();
        try {
            Set<CreditCardDTO> cardsByWalletId = walletService.findCardsByWalletId(id);
            wallet.setCreditCards(cardsByWalletId);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_FIND_CREDIT_CARD, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method deletes credit card from users wallet. If delete success, return to view with list of cards with
     * displayed success message.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */
    @CommandName(name = POST_DELETE_CARD)
    public void deleteCard(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_DELETE_CARD_VIEW);
        Map<String, String> parametersFromRequest = RequestUtils.getParametersFromRequest(request, CARD_ID);
        ValidationResult validationResult = validator.validate(parametersFromRequest, CreditCardDTO.class);
        request.setAttribute(VALIDATION_MESSAGES, validationResult);
        if (!validationResult.isValid()) {
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        String cardId = request.getParameter(CARD_ID);
        HttpSession session = request.getSession();
        WalletDTO wallet = (WalletDTO) session.getAttribute(WALLET_ATTRIBUTE);
        long deletedCardId = Long.parseLong(cardId);
        boolean hasSuchCard = wallet.getCreditCards().stream().anyMatch(creditCardDTO -> {
            long id = creditCardDTO.getCardId();
            return id == deletedCardId;
        });
        if (!hasSuchCard) {
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            validationResult.setValidationInfo(FORM_ERROR_CODE, CARD_ERROR_MSG);
            return;
        }
        boolean isDeleted = walletService.deleteCard(deletedCardId);
        if (isDeleted) {
            RequestUtils.removeCsrfToken(request);
            RequestUtils.setFlushAttribute(request, SUCCESS_ADDING, SUCCESS_MSG);
            RequestUtils.sendRedirectToCommand(request, response, GET_DELETE_CARD_VIEW);
        } else {
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    /**
     * This method is used for filling balance of user's wallet. Redirects to view with filling balance form and
     * displayed success message.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_FILL_BALANCE)
    public void fillBalance(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> fieldValues = RequestUtils.getParametersFromRequest(request, WALLET_AMOUNT);
        ValidationResult validationResult = validator.validate(fieldValues, WalletDTO.class);
        if (!validationResult.isValid()) {
            request.setAttribute(VALIDATION_MESSAGES, validationResult);
            request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_FILL_BALANCE_VIEW);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        String amount = request.getParameter(WALLET_AMOUNT);
        BigDecimal filledAmount = new BigDecimal(amount);
        boolean isPresent = DepositAmountType.getAmountType(filledAmount).isPresent();
        if (!isPresent) {
            request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_FILL_BALANCE_VIEW);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        HttpSession session = request.getSession();
        Object sessionAttribute = session.getAttribute(WALLET_ATTRIBUTE);
        WalletDTO wallet = (WalletDTO) sessionAttribute;
        BigDecimal currentAmount = wallet.getCurrentAmount();
        BigDecimal newBalance = currentAmount.add(new BigDecimal(amount), MathContext.DECIMAL32);
        wallet.setCurrentAmount(newBalance);
        boolean isUpdate = walletService.updateBalance(wallet);
        if (isUpdate) {
            RequestUtils.setFlushAttribute(request, SUCCESS_ADDING, amount);
            RequestUtils.removeCsrfToken(request);
            RequestUtils.sendRedirectToCommand(request, response, GET_FILL_BALANCE_VIEW);
        } else {
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            request.setAttribute(VALIDATION_MESSAGES, validationResult);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    private CreditCardDTO buildCreditCardDTO(Map<String, String> parameterMap) {
        CreditCardDTO creditCardDTO = new CreditCardDTO();
        String cardNumber = getParameter(CARD_NUMBER, parameterMap);
        creditCardDTO.setCardNumber(cardNumber);
        String holderName = getParameter(HOLDER_NAME, parameterMap);
        creditCardDTO.setHolderName(holderName);
        String cvvParameter = getParameter(CVV, parameterMap);
        int cvv = Integer.parseInt(cvvParameter);
        creditCardDTO.setCvv(cvv);
        String parameterYear = getParameter(EXPIRE_YEAR, parameterMap);
        int year = Integer.parseInt(parameterYear);
        String parameterMonth = getParameter(EXPIRE_MONTH, parameterMap);
        Month month = Month.of(Integer.parseInt(parameterMonth));
        LocalDate expireDate = LocalDate.of(year, month.getValue(), 1);
        creditCardDTO.setExpireDate(expireDate);
        return creditCardDTO;
    }

    private String getParameter(String parameterName, Map<String, String> map) {
        return map.get(parameterName);
    }
}
