function initMap() {
    var map = new google.maps.Map(document.getElementById('map'), {
        center: new google.maps.LatLng(53.899866, 27.559728),
        zoom: 11
    });
    var image = {
        url: 'http://localhost:9080/sharing/static/style/img/bike_icon.png',
        size: new google.maps.Size(80, 80),
        origin: new google.maps.Point(0, 0),
        anchor: new google.maps.Point(5, 10),
        scaledSize: new google.maps.Size(45, 45)
    };

    var infoWindow = new google.maps.InfoWindow;

    downloadUrl('http://localhost:9080/sharing/?commandName=getBikes', function (data) {
        var xml = data.responseXML;
        var bikes = xml.documentElement.getElementsByTagName('bike');
        Array.prototype.forEach.call(bikes, function (bikeElem) {
            var id = bikeElem.getAttribute('id');
            var serialNumber = bikeElem.getAttribute('serialNumber');
            var point = new google.maps.LatLng(
                parseFloat(bikeElem.getAttribute('latitude')),
                parseFloat(bikeElem.getAttribute('longitude')));
            var token = bikeElem.getAttribute('csrf');

            var information = document.createElement('div');
            information.setAttribute('class', 'text-dark text-center');
            var text = document.createElement('p');
            text.textContent = 'Bike - ' + serialNumber;
            var form = document.createElement('form');
            form.setAttribute('action', 'http://localhost:9080/sharing/?commandName=toOrderBikeView');
            form.setAttribute('method', 'POST')
            var input = document.createElement('input');
            var csrf = document.createElement("input");
            csrf.setAttribute('type', 'hidden');
            csrf.setAttribute('name', 'csrf');
            csrf.setAttribute('value', token);
            input.setAttribute('type', 'hidden');
            input.setAttribute('name', 'bikeId');
            input.setAttribute('value', id);
            var button = document.createElement('button');
            button.setAttribute('class', 'mdl-button mdl-button--colored mdl-js-button mdl-button--raised mdl-js-ripple-effect');
            button.textContent = 'Order';
            form.appendChild(input);
            form.appendChild(csrf);
            form.appendChild(button);
            information.appendChild(text);
            information.appendChild(form);

            var bike = new google.maps.Marker({
                map: map,
                position: point,
                animation: google.maps.Animation.DROP,
                icon: image
            });

            bike.addListener('click', function () {
                infoWindow.setContent(information);
                infoWindow.open(map, bike);
            });

            bike.addListener('click', function () {
                map.setZoom(15);
                map.setCenter(bike.getPosition());
            });
        });
    });
}

function downloadUrl(url, callback) {
    var request = window.ActiveXObject ?
        new ActiveXObject('Microsoft.XMLHTTP') :
        new XMLHttpRequest;

    request.onreadystatechange = function () {
        if (request.readyState === 4) {
            request.onreadystatechange = doNothing;
            callback(request, request.status);
        }
    };
    request.open('GET', url, true);
    request.send(null);
}

function doNothing() {
}