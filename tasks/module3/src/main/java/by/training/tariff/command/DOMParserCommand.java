package by.training.tariff.command;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.Parser;

public class DOMParserCommand extends ParserCommand<LightTariff> {
    public DOMParserCommand(Parser<LightTariff> parser) {
        super(parser);
    }
}
