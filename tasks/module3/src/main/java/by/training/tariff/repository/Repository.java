package by.training.tariff.repository;

import java.util.List;

public interface Repository<T> {
    void create(T entity);

    boolean update(T entity);

    T read(long id);

    boolean delete(long id);

    List<T> find(Specification<T> specification);

    List<T> getAll();
}
