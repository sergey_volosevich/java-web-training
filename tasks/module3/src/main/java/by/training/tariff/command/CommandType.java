package by.training.tariff.command;

import java.util.Optional;
import java.util.stream.Stream;

public enum CommandType {
    DOM, SAX, STAX;

    public static Optional<CommandType> fromString(String value) {
        return Stream.of(CommandType.values()).filter(a -> a.name().equalsIgnoreCase(value)).findFirst();
    }
}
