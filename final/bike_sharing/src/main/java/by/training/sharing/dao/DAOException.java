package by.training.sharing.dao;

public class DAOException extends Exception {
    private static final long serialVersionUID = -1670292528982688579L;

    public DAOException() {
    }

    public DAOException(String message) {
        super(message);
    }

    public DAOException(String message, Throwable cause) {
        super(message, cause);
    }

    public DAOException(Throwable cause) {
        super(cause);
    }
}
