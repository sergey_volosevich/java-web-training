package by.training.sharing.validation;

import java.lang.annotation.Annotation;
import java.util.Map;

/**
 * Common interface to handle validation annotations, used in {@link by.training.sharing.validation.ValidatorImpl}
 */

public interface AnnotationHandler {
    void handleAnnotation(Annotation annotation, Map<String, String> map, ValidationResult result);
}
