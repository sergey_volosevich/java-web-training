package by.training.sharing.util;

public class RequestUtilException extends RuntimeException {

    private static final long serialVersionUID = -6330931592706101684L;

    public RequestUtilException() {
    }

    public RequestUtilException(String message) {
        super(message);
    }

    public RequestUtilException(String message, Throwable cause) {
        super(message, cause);
    }

    public RequestUtilException(Throwable cause) {
        super(cause);
    }
}
