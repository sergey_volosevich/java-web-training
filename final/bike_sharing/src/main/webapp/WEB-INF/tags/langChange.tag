<%@ tag pageEncoding="UTF-8" trimDirectiveWhitespaces="true" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ attribute name="name" required="true" type="java.lang.String" %>
<%@ attribute name="value" required="true" type="java.lang.String" %>


<c:url value="" var="langUrl">
    <c:set var="parameters" value="${pageContext.request.parameterMap}"/>
    <c:choose>
        <c:when test="${not empty parameters}">
            <c:choose>
                <c:when test="${parameters.containsKey(name)}">
                    <c:forEach var="parameter" items="${parameters}">
                        <c:if test="${!parameter.key.equals(name)}">
                            <c:param name="${parameter.key}" value="${parameter.value[0]}"/>
                        </c:if>
                    </c:forEach>
                    <c:param name="${name}" value="${value}"/>
                </c:when>
                <c:otherwise>
                    <c:forEach var="parameter" items="${parameters}">
                        <c:param name="${parameter.key}" value="${parameter.value[0]}"/>
                    </c:forEach>
                    <c:param name="${name}" value="${value}"/>
                </c:otherwise>
            </c:choose>
        </c:when>
        <c:otherwise>
            <c:param name="${name}" value="${value}"/>
        </c:otherwise>
    </c:choose>
</c:url>
<c:out value="${langUrl}"/>
