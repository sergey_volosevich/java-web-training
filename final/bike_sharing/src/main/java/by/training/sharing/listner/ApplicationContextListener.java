package by.training.sharing.listner;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.security.SecurityContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

@WebListener
public class ApplicationContextListener implements ServletContextListener {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationContextListener.class);
    private static final String CONTEXT_WAS_SUCCESSFULLY_DESTROYED = "Servlet Context successful destroyed";
    private static final String CONTEXT_WAS_SUCCESSFULLY_INITIALIZED = "Servlet Context successful initialized";

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        ApplicationContext.getInstance();
        SecurityContext.getInstance();
        LOGGER.info(CONTEXT_WAS_SUCCESSFULLY_INITIALIZED);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        SecurityContext securityContext = SecurityContext.getInstance();
        securityContext.destroy();
        applicationContext.destroy();
        LOGGER.info(CONTEXT_WAS_SUCCESSFULLY_DESTROYED);
    }
}
