package by.training.text.entity;

public class Paragraph {

    private long id;
    private long textId;

    public Paragraph() {
    }

    public Paragraph(long id, long textId) {
        this.id = id;
        this.textId = textId;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTextId() {
        return textId;
    }

    public void setTextId(long textId) {
        this.textId = textId;
    }
}
