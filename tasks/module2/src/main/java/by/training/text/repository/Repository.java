package by.training.text.repository;

import java.util.List;

public interface Repository<T> {
    long create(T entity);

    boolean update(T entity);

    T read(long id);

    boolean delete(long id);

    List<T> find(Specification<T> specification);

    List<T> getAll();
}
