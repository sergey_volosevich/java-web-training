package by.training.port.controller;

import java.util.Map;

public interface Builder<T> {

    T build(Map<String, String> values);
}
