<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<div class="mdl-grid">
    <div class="mdl-cell mdl-cell--12-col mdl-cell--middle">
        <div class="text-center text-light bg-dark">
            <h3 class="">
                <fmt:message key="all.users.view.header"/>
            </h3>
            <div class="container">
                <div class="row align-items-center justify-content-md-center">
                    <div class="col justify-content-center">
                        <bsh:recordsPerPage commandName="allUsersView"/>
                        <br>
                        <table class="table table-bordered text-light">
                            <thead>
                            <tr>
                                <th scope="row"><fmt:message key="all.users.view.table.numberRow"/></th>
                                <th><fmt:message key="all.users.view.table.userNameRow"/></th>
                                <th><fmt:message key="all.users.view.table.userState"/></th>
                                <th><fmt:message key="all.users.view.table.name"/></th>
                                <th><fmt:message key="all.users.view.table.lastName"/></th>
                                <th><fmt:message key="all.users.view.table.email"/></th>
                                <th><fmt:message key="all.users.view.table.phone"/></th>

                            </tr>
                            </thead>
                            <tbody>
                            <c:set var="index" value="0"/>
                            <c:forEach items="${users}" var="user">
                                <tr>
                                    <c:set var="index" value="${index + 1}"/>
                                    <th scope="row"><c:out value="${index}"/></th>
                                    <td><c:out value="${user.userName}"/></td>
                                    <td><c:out value="${user.status}"/></td>
                                    <td><c:out value="${user.name}"/></td>
                                    <td><c:out value="${user.lastName}"/></td>
                                    <td><c:out value="${user.email}"/></td>
                                    <td><c:out value="${user.phone}"/></td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row align-items-center justify-content-md-center">
                    <div class="col justify-content-center">
                        <bsh:pagination commandName="allUsersView"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
