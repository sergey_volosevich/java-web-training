package by.training.text.model;

import by.training.text.entity.Sentence;
import by.training.text.service.SentenceService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class SentenceComposite implements TextComposite {


    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MESSAGE = "method save() not supported";

    private List<TextLeaf> words = new LinkedList<>();
    private Sentence sentence;
    private SentenceService service;

    public SentenceComposite(Sentence sentence, SentenceService service) {
        this.sentence = sentence;
        this.service = service;
    }

    @Override
    public void addText(TextLeaf textLeaf) {
        words.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getComponents() {
        return words;
    }

    @Override
    public String getString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (TextLeaf word : words) {
            stringBuilder.append(word.getString());
        }
        return stringBuilder.toString();
    }

    @Override
    public void save(Long id) {
        sentence.setParagraphId(id);
        long sentenceId = service.create(sentence);
        for (TextLeaf word : words) {
            word.save(sentenceId);
        }
    }

    @Override
    public void save() {
        LOGGER.log(Level.ERROR, MESSAGE);
    }
}
