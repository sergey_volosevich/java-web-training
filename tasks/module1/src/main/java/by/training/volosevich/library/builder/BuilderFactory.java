package by.training.volosevich.library.builder;

import by.training.volosevich.library.entity.Edition;
import by.training.volosevich.library.entity.EditionType;

import java.util.Map;

public class BuilderFactory {

    private Map<String, String> fields;

    public BuilderFactory(Map<String, String> fields) {
        this.fields = fields;
    }

    public Builder<Edition> getByType(EditionType type) {
        switch (type) {
            case NEWSPAPER:
                return new NewspaperBuilder(fields);
            case MAGAZINE:
                return new MagazineBuilder(fields);
            case BOOK:
                return new BookBuilder(fields);
            default:
                return null;
        }
    }
}
