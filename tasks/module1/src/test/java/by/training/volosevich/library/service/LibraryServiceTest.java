package by.training.volosevich.library.service;

import by.training.volosevich.library.entity.*;
import by.training.volosevich.library.repository.LibraryRepository;
import by.training.volosevich.library.repository.Repository;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.time.DayOfWeek;
import java.time.Month;
import java.util.List;

import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class LibraryServiceTest {

    private LibraryService service;
    private Repository<Edition> repository;

    @Before
    public void setUp() {
        repository = new LibraryRepository();
        service = new LibraryService(repository);
        service.addNewEdition(new Book("Book", GenreType.BUILDING, 254,
                "Publishing House \"Belarus\"", 2007, EditionType.BOOK, "SDFDS; gsdhfd"));
        service.addNewEdition(new Magazine("Building", GenreType.BUILDING, 57,
                "Publishing House \"Belarus\"", 2007, EditionType.MAGAZINE,
                PeriodicityType.EVERY_MONTH, 8, 127, Month.AUGUST));
        service.addNewEdition(new Newspaper("New York Times", GenreType.CARS, 35,
                "Publishing House \"Belarus\"", 2019, EditionType.NEWSPAPER,
                PeriodicityType.EVERY_WEEK, 37, 145, DayOfWeek.WEDNESDAY));
        service.addNewEdition(new Book("Book2", GenreType.ADUCATIONAL, 254,
                "Publishing House \"Belarus\"", 2015, EditionType.BOOK, "Author1; Author2"));
    }

    @Test
    public void sumOfPagesByGenre() {
        assertEquals(311, service.sumOfPagesByGenre(GenreType.BUILDING));
    }

    @Test
    public void sumOfPagesByGenreByNull() {
        assertEquals(0, service.sumOfPagesByGenre(null));
    }

    @Test
    public void sortIncreaseByReleaseYear() {
        List<Edition> list = service.sortIncreaseByReleaseYear();
        assertEquals(2007, list.get(0).getReleaseYear());
        assertEquals(2019, list.get(3).getReleaseYear());
    }

    @Test
    public void sortDecreaseByReleaseYear() {
        List<Edition> list = service.sortDecreaseByReleaseYear();
        assertEquals(2019, list.get(0).getReleaseYear());
        assertEquals(2007, list.get(3).getReleaseYear());
    }

    @Test
    public void sortByReleaseYearByNumberOfPages() {
        List<Edition> list = service.sortByReleaseYearByNumberOfPages();
        assertEquals(57, list.get(0).getNumberOfPages());
        assertEquals(254, list.get(1).getNumberOfPages());
    }

    @Test
    public void findByReleaseYearRange() {
        List<Edition> list = service.findByReleaseYearRange(2007, 2015);
        assertEquals(3, list.size());
    }

    @Test
    public void findByGenreTypeRangeYear() {
        List<Edition> list = service.findByGenreTypeRangeYear(GenreType.BUILDING, 2007, 2010);
        assertEquals(2, list.size());
    }
}