package by.training.volosevich.library.entity;

import java.time.DayOfWeek;
import java.util.Objects;

public class Newspaper extends PeriodicalEdition {
    private DayOfWeek releaseDay;

    public Newspaper(String name, GenreType genreType, int numberOfPages, String publishingHouse, int releaseYear,
                     EditionType type, PeriodicityType periodicity, int releaseNumber,
                     int yearReleaseNumber, DayOfWeek releaseDay) {
        super(name, genreType, numberOfPages, publishingHouse, releaseYear, type, periodicity, releaseNumber,
                yearReleaseNumber);
        this.releaseDay = releaseDay;
    }

    public DayOfWeek getReleaseDay() {
        return releaseDay;
    }

    public void setReleaseDay(DayOfWeek releaseDay) {
        this.releaseDay = releaseDay;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Newspaper)) return false;
        if (!super.equals(o)) return false;
        Newspaper newspaper = (Newspaper) o;
        return getReleaseDay() == newspaper.getReleaseDay();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getReleaseDay());
    }

    @Override
    public String toString() {
        return "Newspaper{" +
                "releaseDay=" + releaseDay +
                "} " + super.toString();
    }
}
