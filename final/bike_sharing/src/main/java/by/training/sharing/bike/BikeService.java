package by.training.sharing.bike;

import by.training.sharing.service.ServiceException;

import java.util.List;

public interface BikeService {

    List<BikeDTO> findAllBikesByState(BikeStatus status) throws ServiceException;

    boolean save(BikeDTO bikeDTO);

    BikeDTO getById(long id) throws ServiceException;

}
