package by.training.port.controller;

import by.training.port.model.Ship;
import by.training.port.validation.FileValidator;
import by.training.port.validation.ValidationResult;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ShipController {

    private static final String FILE_INVALID = "Can not load file. File invalid. Reason: {}";
    private static final String CAN_NOT_READ_THE_FILE = "Can not read the file.";
    private DataFileReader fileReader;
    private LineParser parser;
    private FileValidator validator;
    private Builder<Ship> builder;

    public ShipController(DataFileReader fileReader, LineParser parser, FileValidator validator, Builder<Ship> builder) {
        this.fileReader = fileReader;
        this.parser = parser;
        this.validator = validator;
        this.builder = builder;
    }

    public List<Ship> loadFromFile(String pathFile) {
        ValidationResult validationResult = validator.validateFile(pathFile);
        if (!validationResult.isValid()) {
            throw new ControllerException(MessageFormat.format(FILE_INVALID, validationResult.toString()));
        }
        List<String> readData;
        try {
            readData = fileReader.readData(pathFile);
        } catch (IOException e) {
            throw new ControllerException(CAN_NOT_READ_THE_FILE, e);
        }
        Map<String, String> values;
        List<Ship> shipList = new ArrayList<>();
        for (String string : readData) {
            values = parser.parseLine(string);
            Ship ship = builder.build(values);
            shipList.add(ship);
        }
        return shipList;
    }
}
