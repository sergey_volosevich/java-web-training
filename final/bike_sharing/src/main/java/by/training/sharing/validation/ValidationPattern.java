package by.training.sharing.validation;

import java.lang.annotation.*;

/**
 * This annotation is used to validate fields by pattern.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
@Repeatable(ValidationPatterns.class)
public @interface ValidationPattern {
    /**
     * Sets regex string
     * @return regex string
     */

    String regexp();

    /**
     * Sets field name to check.
     *
     * @return filed name.
     */

    String fieldName();

    /**
     * Sets the validation message.
     *
     * @return validation message.
     */
    String message();

}
