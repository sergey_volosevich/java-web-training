package by.training.text.repository;

import by.training.text.entity.Word;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.atomic.AtomicLong;

public class WordRepository implements Repository<Word> {

    private AtomicLong idGenerator = new AtomicLong(1);

    private Map<Long, Word> mapWords = new HashMap<>();

    @Override
    public long create(Word entity) {
        long id = idGenerator.getAndIncrement();
        entity.setId(id);
        mapWords.put(id, entity);
        return id;
    }

    @Override
    public boolean update(Word entity) {
        long id = entity.getId();
        if (mapWords.containsKey(id)) {
            mapWords.put(id, entity);
            return true;
        }
        return false;
    }

    @Override
    public Word read(long id) {
        return mapWords.get(id);
    }

    @Override
    public boolean delete(long id) {
        Word word = mapWords.get(id);
        return mapWords.remove(id, word);
    }

    @Override
    public List<Word> find(Specification<Word> specification) {
        List<Word> words = new ArrayList<>(mapWords.values());
        List<Word> foundWords = new ArrayList<>();
        for (Word word : words) {
            if (specification.match(word)) {
                foundWords.add(word);
            }
        }
        return foundWords;
    }

    @Override
    public List<Word> getAll() {
        return new ArrayList<>(mapWords.values());
    }
}
