package by.training.port.model;

import java.util.concurrent.atomic.AtomicInteger;

public class Warehouse {
    private AtomicInteger store;
    private final int maxCapacity;

    public Warehouse(int currentCapacity, int maxCapacity) {
        this.store = new AtomicInteger(currentCapacity);
        this.maxCapacity = maxCapacity;
    }

    public AtomicInteger getStore() {
        return store;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }
}
