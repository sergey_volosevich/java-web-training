package by.training.tariff.repository;

public interface Specification<T> {
    boolean match(T entity);
}
