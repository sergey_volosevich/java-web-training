package by.training.sharing.role;

import by.training.sharing.dao.DAOException;

public class RoleNotFoundException extends DAOException {

    private static final long serialVersionUID = 3556485069076316941L;

    public RoleNotFoundException(String message) {
        super(message);
    }
}
