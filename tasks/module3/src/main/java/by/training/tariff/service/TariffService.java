package by.training.tariff.service;

import by.training.tariff.entity.LightTariff;
import by.training.tariff.repository.TariffRepository;

public class TariffService {

    private TariffRepository repository;

    public TariffService(TariffRepository repository) {
        this.repository = repository;
    }

    public void save(LightTariff tariff) {
        repository.create(tariff);
    }
}
