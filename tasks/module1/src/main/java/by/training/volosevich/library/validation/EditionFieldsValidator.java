package by.training.volosevich.library.validation;

import by.training.volosevich.library.entity.GenreType;
import by.training.volosevich.library.validation.util.ValueChecker;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.*;

abstract class EditionFieldsValidator implements Validator {

    private static final Logger LOGGER = LogManager.getLogger();

    private static final int YEAR_OF_FIRST_BOOK = 868;
    private static final int MIN_SIZE_OF_NAME_EDITION = 4;
    private static final int MIN_SIZE_OF_PUBLISHING_HOUSE_EDITION = 10;
    private static final int MIN_SIZE_NUMBER_OF_PAGES = 10;

    private List<String> fields = new ArrayList<>();
    List<String> errorMessages = new ArrayList<>();

    EditionFieldsValidator() {
        fields.add("name");
        fields.add("publishingHouse");
        fields.add("genreType");
        fields.add("numberOfPages");
        fields.add("releaseYear");
    }

    void validateEditionFields(Map<String, String> values) {
        for (String field : fields) {
            if (values.containsKey(field)) {
                String value = values.get(field);
                switch (field) {
                    case "name":
                        validateNameField(field, value);
                        break;
                    case "publishingHouse":
                        validatePublishingHouseField(field, value);
                        break;
                    case "genreType":
                        validateGenreFiled(field, value);
                        break;
                    case "numberOfPages":
                        validateNumberOfPagesField(field, value);
                        break;
                    case "releaseYear":
                        validateReleaseYearField(field, value);
                        break;
                }
            } else {
                errorMessages.add(ValidationMessage.formatMessage(field, ValidationMessage.NOT_ENOUGH_FIELD));
                LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(field,
                        ValidationMessage.NOT_ENOUGH_FIELD));
            }
        }
    }


    private void validateNameField(String nameField, String value) {
        if (ValueChecker.isNotNull(value)) {
            if (value.length() < MIN_SIZE_OF_NAME_EDITION) {
                errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_NAME_EDITION));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_NAME_EDITION));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }

    private void validatePublishingHouseField(String nameField, String value) {
        if (ValueChecker.isNotNull(value)) {
            if (value.length() < MIN_SIZE_OF_PUBLISHING_HOUSE_EDITION) {
                errorMessages.add(ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_PUBLISHING_HOUSE));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_PUBLISHING_HOUSE));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }

    private void validateGenreFiled(String nameField, String value) {
        Optional<GenreType> genreValue = GenreType.fromString(value);
        if (!genreValue.isPresent()) {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.NO_SUCH_GENRE_TYPE));
            LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                    ValidationMessage.NO_SUCH_GENRE_TYPE));
        }
    }

    private void validateNumberOfPagesField(String nameField, String value) {
        if (ValueChecker.isInteger(value)) {
            if (Integer.parseInt(value) < MIN_SIZE_NUMBER_OF_PAGES) {
                errorMessages.add(ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_NUMBER_OF_PAGES));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.INCORRECT_NUMBER_OF_PAGES));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }

    private void validateReleaseYearField(String nameField, String value) {
        if (ValueChecker.isInteger(value)) {
            Calendar calendar = new GregorianCalendar();
            if (Integer.parseInt(value) > calendar.get(Calendar.YEAR)) {
                errorMessages.add(ValidationMessage.formatMessage(nameField,
                        ValidationMessage.NOT_GREATER_THAN_CURRENT_YEAR));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.NOT_GREATER_THAN_CURRENT_YEAR));
            } else if (Integer.parseInt(value) < YEAR_OF_FIRST_BOOK) {
                errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.YEAR_LESS_FIRST_BOOK));
                LOGGER.log(Level.WARN, () -> ValidationMessage.formatMessage(nameField,
                        ValidationMessage.YEAR_LESS_FIRST_BOOK));
            }
        } else {
            errorMessages.add(ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
            LOGGER.log(Level.ERROR, () -> ValidationMessage.formatMessage(nameField, ValidationMessage.INCORRECT_VALUE));
        }
    }
}


