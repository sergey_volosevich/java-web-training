package by.training.sharing.bike;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.EntityNotFoundException;
import by.training.sharing.dao.JDBCTemplate;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.BIKE_DAO;
import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;

@Bean(name = BIKE_DAO)
public class BikeDAOImpl implements BikeDAO {

    public static final Logger LOGGER = LogManager.getLogger(BikeDAOImpl.class);

    private static final String FAILED_TO_FIND_BIKE_BY_ID = "Failed to find bike by id";

    private static final String SELECT_ALL_BIKES = "select b.bike_id, b.serial_number, bl.latitude, bl.longitude" +
            " from bike b, bike_location bl where bl.bike_id = b.bike_id and b.bike_state = ?;";
    private static final String INSERT_INTO_BIKE = "INSERT INTO bike (serial_number, password, rental_price_id) VALUES (?, ?, ?);";
    private static final String INSERT_INTO_BIKE_LOCATION = "insert into bike_location (latitude, longitude, bike_id) values (?, ?, ?);";
    private static final String SELECT_BIKE_BY_ID = "select b.bike_id, b.serial_number, b.password, b.bike_state, bl.latitude," +
            "bl.longitude, b.rental_price_id from bike b, bike_location bl where bl.bike_id = b.bike_id and b.bike_id=?;";

    private JDBCTemplate jdbcTemplate;

    public BikeDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long save(BikeDTO bikeDTO) throws DAOException {
        try {
            Number bikeId = jdbcTemplate.executeAndReturnKey(INSERT_INTO_BIKE, new Object[]{bikeDTO.getSerialNumber(),
                    bikeDTO.getBikePassword(), bikeDTO.getPriceId()});
            jdbcTemplate.executeUpdate(INSERT_INTO_BIKE_LOCATION, new Object[]{bikeDTO.getLatitude(),
                    bikeDTO.getLongitude(), bikeId.longValue()});
            return bikeId.longValue();
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public boolean update(BikeDTO bikeDTO) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public BikeDTO getById(long id) throws DAOException {
        try {
            BikeDTO bikeDTO = jdbcTemplate.queryForObject(SELECT_BIKE_BY_ID, new Object[]{id}, new BikeRowMapper());
            if (bikeDTO != null) {
                return bikeDTO;
            }
            throw new EntityNotFoundException(FAILED_TO_FIND_BIKE_BY_ID);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public List<BikeDTO> findAll() throws DAOException {
        return null;
    }

    @Override
    public List<BikeDTO> findAllWithLocationByState(BikeStatus status) throws DAOException {
        try {
            return jdbcTemplate.queryForList(SELECT_ALL_BIKES, new Object[]{status.ordinal()}, (rs, rowNum) -> {
                BikeDTO bikeDTO = new BikeDTO();
                long id = rs.getLong("bike_id");
                bikeDTO.setId(id);
                String serialNumber = rs.getString("serial_number");
                bikeDTO.setSerialNumber(serialNumber);
                BigDecimal latitude = rs.getBigDecimal("latitude");
                bikeDTO.setLatitude(latitude);
                BigDecimal longitude = rs.getBigDecimal("longitude");
                bikeDTO.setLongitude(longitude);
                return bikeDTO;
            });
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
