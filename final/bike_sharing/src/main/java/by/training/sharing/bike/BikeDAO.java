package by.training.sharing.bike;

import by.training.sharing.dao.CRUDDao;
import by.training.sharing.dao.DAOException;

import java.util.List;

public interface BikeDAO extends CRUDDao<BikeDTO> {
    List<BikeDTO> findAllWithLocationByState(BikeStatus status) throws DAOException;
}
