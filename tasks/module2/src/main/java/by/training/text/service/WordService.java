package by.training.text.service;

import by.training.text.entity.Word;
import by.training.text.repository.Repository;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Optional;

public class WordService {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String WORD_IS_NULL = "Word is null";
    private static final String NOT_VALID_ID = "id less or equals zero";

    private Repository<Word> repository;

    public WordService(Repository<Word> repository) {
        this.repository = repository;
    }

    public long create(Word entity) {
        if (entity != null) {
            return repository.create(entity);
        } else {
            LOGGER.log(Level.ERROR, WORD_IS_NULL);
            return -1;
        }
    }

    public boolean update(Word word) {
        if (word != null) {
            return repository.update(word);
        }
        LOGGER.log(Level.ERROR, WORD_IS_NULL);
        return false;
    }

    public Optional<Word> read(long id) {
        if (id > 0) {
            return Optional.ofNullable(repository.read(id));
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return Optional.empty();
    }

    public boolean delete(long id) {
        if (id > 0) {
            return repository.delete(id);
        }
        LOGGER.log(Level.ERROR, NOT_VALID_ID);
        return false;
    }

    public List<Word> getAll() {
        return repository.getAll();
    }
}
