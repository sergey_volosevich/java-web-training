package by.training.sharing.validation;

import static by.training.sharing.user.UserFieldNameConstants.*;

@Validation
public class TestUserDTO {
    private Long id;

    @ValidationPattern(regexp = "^[a-zA-Z0-9-_\\.]{5,30}$", fieldName = USER_LOGIN, message = "user.login.validation.msg")
    private String userName;

    @ValidationPattern(regexp = "(?=^.{8,}$)(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\\s)[0-9a-zA-Z]*$",
            fieldName = USER_PASSWORD, message = "user.password.validation.msg")
    private String password;

    @ValidationPattern(regexp = "^[а-яА-ЯёЁa-zA-Z-?]{1,45}$", fieldName = USER_NAME, message = "user.name.validation.msg")
    private String name;

    @ValidationPattern(regexp = "^[а-яА-ЯёЁa-zA-Z-?]{1,45}$", fieldName = USER_LAST_NAME,
            message = "user.lastName.validation.msg")
    private String lastName;

    @ValidationPattern(regexp = "^([a-zA-Z0-9_\\-\\.]+)@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.)|" +
            "(([a-zA-Z0-9\\-]+\\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\\]?)$",
            fieldName = USER_EMAIL, message = "user.email.validation.msg")
    private String email;

    @ValidationPattern(regexp = "[+]\\d{12}", fieldName = USER_PHONE, message = "user.phone.validation.msg")
    private String phone;
}
