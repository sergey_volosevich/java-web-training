package by.training.sharing.dao;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;

@Bean(name = JDBC_TEMPLATE)
public class JDBCTemplateImpl implements JDBCTemplate {

    private ConnectionManager connectionManager;

    public JDBCTemplateImpl(@BeanQualifier(CONNECTION_MANAGER) ConnectionManager connectionManager) {
        this.connectionManager = connectionManager;
    }

    @Override
    public int executeUpdate(String sql, Object[] args) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            int i = 0;
            for (Object arg : args) {
                statement.setObject(++i, arg);
            }
            return statement.executeUpdate();
        }
    }

    @Override
    public <T> T queryForObject(String sql, Object[] args, RowMapper<T> rowMapper) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            int i = 0;
            for (Object arg : args) {
                statement.setObject(++i, arg);
            }
            final ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                return rowMapper.mapRow(resultSet, 0);
            }
        }
        return null;
    }

    @Override
    public <T> List<T> query(String sql, RowMapper<T> rowMapper) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            final ResultSet resultSet = statement.executeQuery();
            List<T> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(rowMapper.mapRow(resultSet, 0));
            }
            return result;
        }
    }

    @Override
    public <T> List<T> queryForList(String sql, Object[] args, RowMapper<T> rowMapper) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql)) {
            int i = 0;
            for (Object arg : args) {
                statement.setObject(++i, arg);
            }
            final ResultSet resultSet = statement.executeQuery();
            List<T> result = new ArrayList<>();
            while (resultSet.next()) {
                result.add(rowMapper.mapRow(resultSet, 0));
            }
            return result;
        }
    }

    @Override
    public Number executeAndReturnKey(String sql, Object[] args) throws SQLException {
        try (Connection connection = connectionManager.getConnection();
             PreparedStatement statement = connection.prepareStatement(sql, Statement.RETURN_GENERATED_KEYS)) {
            int i = 0;
            for (Object arg : args) {
                statement.setObject(++i, arg);
            }
            statement.executeUpdate();
            ResultSet generatedKeys = statement.getGeneratedKeys();
            if (generatedKeys.next()) {
                Object generatedKey = generatedKeys.getObject(1);
                return (Number) generatedKey;
            }
        }
        return null;
    }
}
