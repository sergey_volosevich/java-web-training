<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<main class="mdl-layout__content">
    <div class="page-content">
        <div class="mdl-grid ">
            <div class="mdl-cell mdl-cell--2-col ">
            </div>
            <div class="mdl-cell mdl-cell--8-col">
                <div class="mdl-grid">
                    <div class="mdl-cell mdl-cell--12-col">
                        <div class="container align-items-center bg-dark text-light">
                            <p class="h1 text-center"><fmt:message key="401.message"/></p>
                            <hr style="border: none;background-color: darkgrey; color: darkgrey; height: 1px">
                            <a href="<c:out value="${pageContext.request.contextPath}"/>/"><p class="text-center">
                                <fmt:message key="links.back.main.page"/>
                            </p></a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="mdl-cell mdl-cell--2-col ">
            </div>
        </div>
    </div>
</main>