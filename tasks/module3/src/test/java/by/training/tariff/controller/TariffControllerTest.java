package by.training.tariff.controller;

import by.training.tariff.command.*;
import by.training.tariff.entity.LightTariff;
import by.training.tariff.parser.DOMTariffParser;
import by.training.tariff.parser.SAXTariffParser;
import by.training.tariff.parser.StAXTariffParser;
import by.training.tariff.service.TariffService;
import by.training.tariff.validator.FileValidator;
import by.training.tariff.validator.XMLValidator;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(MockitoJUnitRunner.class)
public class TariffControllerTest {
    @Mock
    private TariffService service;
    private TariffController controller;
    private static final String VALID_XML = "valid.xml";
    private static final String NO_VALID_DATA = "invalidData.xml";
    private static final String EMPTY_FILE = "empty.xml";
    private String pathValidXML;
    private String pathNoValidDataXML;
    private String pathEmptyFile;

    @Before
    public void setUp() throws Exception {
        FileValidator fileValidator = new FileValidator();
        XMLValidator xmlValidator = new XMLValidator("tariff.xsd");
        CommandProvider<LightTariff> commandProvider = new CommandProviderImpl();
        DOMTariffParser domTariffParser = new DOMTariffParser();
        DOMParserCommand domParserCommand = new DOMParserCommand(domTariffParser);
        commandProvider.addCommand(CommandType.DOM, domParserCommand);
        SAXTariffParser saxTariffParser = new SAXTariffParser();
        SAXParserCommand saxParserCommand = new SAXParserCommand(saxTariffParser);
        commandProvider.addCommand(CommandType.SAX, saxParserCommand);
        StAXTariffParser stAXTariffParser = new StAXTariffParser();
        StAXParserCommand stAXParserCommand = new StAXParserCommand(stAXTariffParser);
        commandProvider.addCommand(CommandType.STAX, stAXParserCommand);
        controller = new TariffController(service, fileValidator, xmlValidator, commandProvider);
        pathValidXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_XML))
                .toURI()).toString();
        pathNoValidDataXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NO_VALID_DATA)).toURI()).toString();
        pathEmptyFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(EMPTY_FILE)).toURI()).toString();
    }

    @Test
    public void uploadFileValidUsingDOMCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "dom"));
    }

    @Test
    public void uploadFileValidUsingSAXCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "sax"));
    }

    @Test
    public void uploadFileValidUsingStAXCommand() {
        assertTrue(controller.uploadFile(pathValidXML, "stax"));
    }

    @Test
    public void uploadFileEmptyUsingStAXCommand() {
        assertFalse(controller.uploadFile(pathEmptyFile, "stax"));
    }

    @Test
    public void uploadFileInvalidUsingSAXCommand() {
        assertFalse(controller.uploadFile(pathNoValidDataXML, "sax"));
    }
}