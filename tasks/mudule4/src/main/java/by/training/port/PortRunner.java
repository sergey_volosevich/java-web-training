package by.training.port;

import by.training.port.controller.*;
import by.training.port.model.Ship;
import by.training.port.validation.FileValidator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.net.URISyntaxException;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.FutureTask;

public class PortRunner {
    private static final String SHIP_FILE = "valid.txt";
    private static final Logger LOGGER = LogManager.getLogger();
    private static final String CAN_NOT_FIND_PATH_TO_FILE = "Can not find path to file";

    public static void main(String[] args) {

        DataFileReader dataFileReader = new DataFileReader();
        LineParser parser = new LineParser();
        FileValidator validator = new FileValidator();
        Builder<Ship> builder = new ShipBuilder();
        ClassLoader classLoader = PortRunner.class.getClassLoader();
        String shipPath = null;
        try {
            shipPath = Paths.get(classLoader.getResource(SHIP_FILE).toURI()).toString();
        } catch (URISyntaxException e) {
            LOGGER.error(CAN_NOT_FIND_PATH_TO_FILE);
        }
        ShipController controller = new ShipController(dataFileReader, parser, validator, builder);
        List<Ship> shipList = controller.loadFromFile(shipPath);
        ExecutorService executorService = Executors.newCachedThreadPool();
        for (Ship ship : shipList) {
            executorService.submit(ship);
        }
        executorService.shutdown();
    }

}
