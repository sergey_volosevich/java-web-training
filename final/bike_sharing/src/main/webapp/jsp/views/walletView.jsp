<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@page import="by.training.sharing.ApplicationConstants" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>


<c:set var="fillBalance" value="${ApplicationConstants.GET_FILL_BALANCE_VIEW}"/>
<c:set var="addCard" value="${ApplicationConstants.GET_ADD_CARD_VIEW}"/>
<c:set var="deleteCard" value="${ApplicationConstants.GET_DELETE_CARD_VIEW}"/>
<div class="container text-dark">
    <div class="row mt-1 mb-3 align-items-center">
        <div class="col-md-10 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><fmt:message key="wallet.view.header"/></h3>
                </div>
                <div class="panel-body">
                    <c:choose>
                        <c:when test="${not empty sessionScope.get('wallet')}">
                            <hr style="border: none;background-color: darkgrey; color: darkgrey; height: 1px">
                            <div class="text-center">
                                <p>
                                <p><fmt:message key="wallet.view.balance"/></p>
                                <h4 class="font-italic font-weight-bold">
                                    <c:out value="${sessionScope.get('wallet').currentAmount}"/> BYN
                                </h4>
                                <c:choose>
                                    <c:when test="${not empty wallet.creditCards}">
                                        <div class="text-center">
                                            <hr style="border: none;background-color: darkgrey; color: darkgrey; height: 1px">
                                            <p><fmt:message key="wallet.view.card.header"/></p>
                                        </div>
                                        <c:forEach items="${wallet.creditCards}" var="card">
                                            <p><c:out value="${card.cardNumber}"/></p>
                                        </c:forEach>
                                        <hr style="border: none;background-color: darkgrey; color: darkgrey; height: 1px">
                                        <bsh:backButton value="welcomeView"/>
                                        <div class="mdl-card__menu" style="height: 512px">
                                            <button id="demo-menu-lower-right"
                                                    class="mdl-button mdl-js-button mdl-button--icon">
                                                <em class="material-icons">more_vert</em>
                                            </button>
                                            <ul class="mdl-menu mdl-menu--bottom-right mdl-js-menu mdl-js-ripple-effect"
                                                for="demo-menu-lower-right">
                                                <li class="mdl-menu__item ">
                                                    <div class="mdl-chip mdl-chip--contact mdl-chip--deletable">
                                                        <a href="<bsh:command value="${fillBalance}"/>"
                                                           class="mdl-chip__action">
                                                            <em class="material-icons">account_balance_wallet</em></a>
                                                        <span class="mdl-chip__text">
                                                    <fmt:message key="wallet.view.action.fill.balance"/>
                                                </span>
                                                    </div>
                                                </li>
                                                <li class="mdl-menu__item">
                                                    <div class="mdl-chip mdl-chip--contact mdl-chip--deletable">
                                                        <a href="<bsh:command value="${addCard}"/>"
                                                           class="mdl-chip__action">
                                                            <em class="material-icons">credit_card</em></a>
                                                        <span class="mdl-chip__text">
                                                    <fmt:message key="wallet.view.action.add.card"/>
                                                </span>
                                                    </div>
                                                </li>
                                                <li class="mdl-menu__item">
                                                    <div class="mdl-chip mdl-chip--contact mdl-chip--deletable">
                                                        <a href="<bsh:command value="${deleteCard}"/>"
                                                           class="mdl-chip__action">
                                                            <em class="material-icons">delete_forever</em></a>
                                                        <span class="mdl-chip__text">
                                                            <fmt:message key="wallet.view.action.delete.card"/>
                                                        </span>
                                                    </div>
                                                </li>
                                            </ul>
                                        </div>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="text-center">
                                            <p><fmt:message key="wallet.view.card.helper"/>
                                                <a href="<bsh:command value="addCardView"/>"
                                                   class="mdl-badge mdl-badge--overlap">
                                                    <fmt:message key="links.click.here"/>
                                                </a>
                                            </p>
                                            <bsh:backButton value="welcomeView"/>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                                </p>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="text-center">
                                <p class="text-dark">
                                        <fmt:message key="wallet.view.wallet.helper"/>
                                <form action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
                                    <input type="hidden" name="csrf"
                                           value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                                    <input type="hidden" name="commandName" value="createWallet"/>
                                    <button type="submit" style="width: 100%" class="btn btn-outline-dark"
                                            id="add-card">
                                        <fmt:message key="links.click.here"/>
                                    </button>
                                </form>
                                </p>
                                <bsh:backButton value="welcomeView"/>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>
