package by.training.sharing;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

public class ApplicationConstants {

    private ApplicationConstants() {
    }

    //get commands
    public static final String GET_USER_LOGIN = "loginUserView";
    public static final String GET_CHOOSE_BIKE = "chooseBikeView";
    public static final String GET_WELCOME_VIEW = "welcomeView";
    public static final String GET_USER_LOGOUT = "logoutUser";
    public static final String GET_USER_REGISTRATION = "registerUserView";
    public static final String GET_BIKES = "getBikes";
    public static final String GET_FILL_BALANCE_VIEW = "fillBalanceView";
    public static final String GET_WALLET_VIEW = "walletView";
    public static final String GET_ADD_CARD_VIEW = "addCardView";
    public static final String GET_DELETE_CARD_VIEW = "deleteCardView";
    public static final String GET_ORDER_BIKE = "orderBikeView";
    public static final String GET_BIKE_ADDING_FORM = "addBikeView";
    public static final String GET_ORDER_BIKE_VIEW = "toOrderBikeView";
    public static final String GET_USERS = "allUsersView";

    //post commands
    public static final String POST_ADD_CARD = "addCard";
    public static final String POST_FILL_BALANCE = "fillBalance";
    public static final String POST_ADD_BIKE = "addBike";
    public static final String POST_USER_LOGIN = "loginUser";
    public static final String POST_USER_REGISTRATION = "registerUser";
    public static final String POST_DELETE_CARD = "deleteCard";
    public static final String POST_CREATE_WALLET = "createWallet";

    //application constants
    public static final String MAIN_LAYOUT = "/jsp/layout.jsp";
    public static final String VALIDATION_MESSAGES = "vrMessages";
    public static final String FORM_ERROR_CODE = "error";
    public static final String FORM_ERROR_MESSAGE = "form.error";
    public static final String CMD_REQ_PARAMETER = "commandName";
    public static final String VIEW_NAME_REQ_ATTRIBUTE = "viewName";
    public static final String SUCCESS_ADDING = "success";

    //need for security
    public static final Set<String> USER_COMMANDS;
    public static final Set<String> ADMIN_COMMANDS;
    public static final Set<String> POST_COMMANDS;

    static {
        Set<String> post = new HashSet<>();
        Set<String> userCommands = new HashSet<>();
        Set<String> adminCommands = new HashSet<>();
        //add user commands for nav-links
        userCommands.add(GET_CHOOSE_BIKE);
        userCommands.add(GET_WALLET_VIEW);
        //add admin commands for nav-links
        adminCommands.add(GET_USERS);
        adminCommands.add(GET_BIKE_ADDING_FORM);
        //add post commands
        post.add(POST_ADD_CARD);
        post.add(POST_FILL_BALANCE);
        post.add(POST_ADD_BIKE);
        post.add(POST_USER_LOGIN);
        post.add(POST_USER_REGISTRATION);
        post.add(POST_DELETE_CARD);
        post.add(POST_CREATE_WALLET);

        POST_COMMANDS = Collections.unmodifiableSet(post);
        USER_COMMANDS = Collections.unmodifiableSet(userCommands);
        ADMIN_COMMANDS = Collections.unmodifiableSet(adminCommands);
    }
}
