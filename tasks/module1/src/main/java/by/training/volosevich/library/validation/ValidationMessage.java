package by.training.volosevich.library.validation;

public class ValidationMessage {

    private static final String FORMATTED_MESSAGE_FILED = "%s field - %s";

    public static final String FILE_DOES_NOT_EXIST = "File does not exist";
    public static final String FILE_IS_EMPTY = "File is empty";
    public static final String FILE_UNREACHABLE = "File unreachable";
    public static final String FILE_PATH_NULL = "File path must not be null";
    public static final String NO_FIELD_TYPE = "No field \"type\"";
    public static final String NO_DATA = "There is no data in this line";
    public static final String NOT_ENOUGH_FIELD = "Not enough field to fill";
    public static final String NO_SUCH_GENRE_TYPE = "There is no such genre type";
    public static final String NOT_GREATER_THAN_CURRENT_YEAR = "Must not be more than the current year";
    public static final String INCORRECT_MONTH = "Month is not correct";
    public static final String INCORRECT_DAY = "Day of week is not correct";
    public static final String NO_SUCH_TYPE = "There is no such value type";
    public static final String YEAR_LESS_FIRST_BOOK = "Must be greater than year of first printed book";
    public static final String INCORRECT_VALUE = "Value is incorrect";
    public static final String INCORRECT_NAME_EDITION = "Must be more than 4 characters";
    public static final String INCORRECT_PUBLISHING_HOUSE = "Must be more than 10 characters";
    public static final String INCORRECT_NUMBER_OF_PAGES = "Must be more than 5";
    public static final String INCORRECT_AUTHOR = "Must be more than 10 characters";
    public static final String NO_SUCH_PERIODICITY_TYPE = "There is no such periodicity type";
    public static final String INCORRECT_RELEASE_NUMBER = "Must be more than 1";

    private ValidationMessage() {
    }

    public static String formatMessage(String field, String message) {
        return String.format(FORMATTED_MESSAGE_FILED, field, message);
    }
}
