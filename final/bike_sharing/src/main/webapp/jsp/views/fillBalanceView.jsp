<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="vr" uri="bikeSharing" %>
<%@ page import="by.training.sharing.wallet.CreditCardFieldNameConstants" %>


<c:set var="cardIdField" value="${CreditCardFieldNameConstants.CARD_ID}"/>

<style>
    .btn-group {
        width: 100%;
        margin-top: 25px;
    }
</style>
<div class="container text-dark">
    <div class="row mt-1 mb-3">
        <div class="col-md-6 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><fmt:message key="wallet.form.header"/></h3>
                </div>
                <div class="panel-body">
                    <c:choose>
                        <c:when test="${not empty wallet.creditCards}">
                            <form action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
                                <input type="hidden" name="commandName" value="fillBalance"/>
                                <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                                <div class="form-group">
                                    <div id="amountGroup" class="btn-group btn-group-toggle btn-group-vertical"
                                         data-toggle="buttons">
                                        <label for="amountGroup"><fmt:message key="wallet.amount.form.label"/></label>
                                        <label class="btn btn-outline-dark" for="amount1">
                                            <input type="radio" id="amount1" name="amount" value="5.00">5.00 BYN</label>
                                        <label class="btn btn-outline-dark" for="amount2">
                                            <input type="radio" id="amount2" name="amount" value="10.00">10.00
                                            BYN</label>
                                        <label class="btn btn-outline-dark" for="amount3">
                                            <input type="radio" name="amount" id="amount3" value="15.00">15.00
                                            BYN</label>
                                        <label class="btn btn-outline-dark" for="amount4">
                                            <input type="radio" name="amount" id="amount4" value="25.00">25.00
                                            BYN</label>
                                        <label class="btn btn-outline-dark" for="amount5">
                                            <input type="radio" name="amount" id="amount5" value="50.00">50.00
                                            BYN</label>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="card"><fmt:message key="wallet.card.form.label"/></label>
                                    <select id="card" class="form-control" name="<c:out value="${cardIdField}"/>">
                                        <c:forEach items="${wallet.creditCards}" var="card">
                                            <option value="<c:out value="${card.cardId}"/>">
                                                <c:out value="${card.cardNumber}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <p class="form-text alert-danger text-center">
                                    <vr:validationMessage fieldName="${error}" validationMessages="${vrMessages}"/>
                                </p>
                                <c:if test="${not empty requestScope.get('success')}">
                                    <div class="alert alert-success text-center" role="alert">
                                        <fmt:message key="wallet.fill.form.success"/>
                                        <c:out value="${requestScope.get('success')}"/> BYN.
                                    </div>
                                </c:if>
                                <div class="pt-2 d-flex justify-content-between">
                                    <a class="btn btn-outline-dark" href="<bsh:command value="walletView"/>"
                                       role="button"><fmt:message key="form.back.button"/></a>
                                    <button class="btn btn-outline-dark"><fmt:message key="form.fill.button"/></button>
                                </div>
                            </form>
                        </c:when>
                        <c:otherwise>
                            <div class="text-center">
                                No credit cards. To add credit card
                                <a href="<bsh:command value="addCardView"/>" class="mdl-badge badge-dark">click here.
                                </a>
                                <div class="pt-2 d-flex justify-content-between">
                                    <a class="btn btn-outline-dark" style="width: 100%" role="button"
                                       href="<bsh:command value="walletView"/>">
                                        <fmt:message key="form.back.button"/></a>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>