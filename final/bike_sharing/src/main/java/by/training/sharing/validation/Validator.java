package by.training.sharing.validation;

import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public interface Validator {

    ValidationResult validate(Map<String, String> map, Class<?> clazz);

    default boolean isNumber(String strValue) {
        if (strValue == null) {
            return false;
        }
        Pattern pattern = Pattern.compile("^-?\\d+$");
        Matcher matcher = pattern.matcher(strValue);
        return matcher.matches();
    }
}
