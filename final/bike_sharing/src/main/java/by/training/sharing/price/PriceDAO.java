package by.training.sharing.price;

import by.training.sharing.dao.CRUDDao;

public interface PriceDAO extends CRUDDao<PriceDTO> {
}
