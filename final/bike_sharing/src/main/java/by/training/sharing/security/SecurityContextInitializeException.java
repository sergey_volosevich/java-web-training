package by.training.sharing.security;

public class SecurityContextInitializeException extends RuntimeException {

    private static final long serialVersionUID = 5575700081734584202L;

    public SecurityContextInitializeException(String message) {
        super(message);
    }

    public SecurityContextInitializeException(Throwable cause) {
        super(cause);
    }
}
