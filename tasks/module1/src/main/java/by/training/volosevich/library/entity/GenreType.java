package by.training.volosevich.library.entity;

import java.util.Optional;
import java.util.stream.Stream;

public enum GenreType {
    ADUCATIONAL,
    BUILDING,
    MEDICINE,
    CARS;

    public static Optional<GenreType> fromString(String type) {

        return Stream.of(GenreType.values())
                .filter(t -> t.name().equalsIgnoreCase(type))
                .findFirst();
    }
}
