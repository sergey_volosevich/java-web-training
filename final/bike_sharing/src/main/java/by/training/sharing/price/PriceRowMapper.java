package by.training.sharing.price;

import by.training.sharing.dao.RowMapper;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;

public class PriceRowMapper implements RowMapper<PriceDTO> {
    @Override
    public PriceDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        PriceDTO priceDTO = new PriceDTO();
        long id = rs.getLong("rental_price_id");
        priceDTO.setId(id);
        Date date = rs.getDate("date");
        LocalDate localDate = date.toLocalDate();
        priceDTO.setDate(localDate);
        BigDecimal price = rs.getBigDecimal("current_price");
        priceDTO.setPriceValue(price);
        return priceDTO;
    }
}
