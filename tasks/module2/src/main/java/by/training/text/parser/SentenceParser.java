package by.training.text.parser;

import by.training.text.entity.Sentence;
import by.training.text.model.SentenceComposite;
import by.training.text.model.TextLeaf;
import by.training.text.service.SentenceService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class SentenceParser extends AbstractTextParser {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String LEXEME_REGEX = "(?:\\S*\\w+|[-])(?:\\s*\\p{P}*\\s*)";
    private static final String NO_LEXEMES_IN_TEXT = "No lexemes in text";

    private SentenceService sentenceService;

    public SentenceParser(SentenceService service) {
        this.sentenceService = service;
    }

    @Override
    public TextLeaf parse(String line) {
        Sentence sentence = new Sentence();
        SentenceComposite sentenceComposite = new SentenceComposite(sentence, sentenceService);
        Pattern pattern = Pattern.compile(LEXEME_REGEX);
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            TextLeaf wordLeaf = nextParse(matcher.group());
            sentenceComposite.addText(wordLeaf);
        }
        if (sentenceComposite.getComponents().isEmpty()) {
            LOGGER.log(Level.WARN, NO_LEXEMES_IN_TEXT);
            return nextParse(line);
        }
        return sentenceComposite;
    }
}
