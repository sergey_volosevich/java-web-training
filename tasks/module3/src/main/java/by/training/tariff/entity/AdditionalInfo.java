package by.training.tariff.entity;


import java.util.Objects;

public class AdditionalInfo {

    private BillingType type;
    private int favoriteNumbers;
    private double connectionPrice;

    public AdditionalInfo() {
    }

    public AdditionalInfo(BillingType type, int favoriteNumbers, double connectionPrice) {
        this.type = type;
        this.favoriteNumbers = favoriteNumbers;
        this.connectionPrice = connectionPrice;
    }

    public BillingType getType() {
        return type;
    }

    public void setType(BillingType type) {
        this.type = type;
    }

    public int getFavoriteNumbers() {
        return favoriteNumbers;
    }

    public void setFavoriteNumbers(int favoriteNumbers) {
        this.favoriteNumbers = favoriteNumbers;
    }

    public double getConnectionPrice() {
        return connectionPrice;
    }

    public void setConnectionPrice(double connectionPrice) {
        this.connectionPrice = connectionPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof AdditionalInfo)) return false;
        AdditionalInfo that = (AdditionalInfo) o;
        return getFavoriteNumbers() == that.getFavoriteNumbers() &&
                Double.compare(that.getConnectionPrice(), getConnectionPrice()) == 0 &&
                getType() == that.getType();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getType(), getFavoriteNumbers(), getConnectionPrice());
    }

    @Override
    public String toString() {
        return "AdditionalInfo{" +
                "type=" + type +
                ", favoriteNumbers=" + favoriteNumbers +
                ", connectionPrice=" + connectionPrice +
                '}';
    }
}
