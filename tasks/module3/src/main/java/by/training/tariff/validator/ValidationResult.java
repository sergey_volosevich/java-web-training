package by.training.tariff.validator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidationResult {

    private Map<String, List<String>> validationInfo = new HashMap<>();

    public Map<String, List<String>> getValidationInfo() {
        return new HashMap<>(validationInfo);
    }

    public void setValidationInfo(String code, String message) {
        if (validationInfo.containsKey(code)) {
            List<String> list = validationInfo.get(code);
            list.add(message);
            validationInfo.put(code, list);
        } else {
            List<String> list = new ArrayList<>();
            list.add(message);
            validationInfo.put(code, list);
        }
    }

    public boolean isValid() {
        return validationInfo.isEmpty();
    }

    @Override
    public String toString() {
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append("\n");
        for (String s : validationInfo.keySet()) {
            List<String> list = validationInfo.get(s);
            for (String message : list) {
                stringBuilder.append(s).append(" - ").append(message).append("\n");
            }
        }
        return stringBuilder.toString();
    }
}
