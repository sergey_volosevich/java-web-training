package by.training.text.model;

import by.training.text.entity.Paragraph;
import by.training.text.service.ParagraphService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedList;
import java.util.List;

public class ParagraphComposite implements TextComposite {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String MESSAGE = "method save() not supported";
    private static final String ENDING_SENTENCE = "\n";

    private List<TextLeaf> sentences = new LinkedList<>();
    private Paragraph paragraph;
    private ParagraphService service;

    public ParagraphComposite(Paragraph paragraph, ParagraphService service) {
        this.paragraph = paragraph;
        this.service = service;
    }

    @Override
    public void addText(TextLeaf textLeaf) {
        sentences.add(textLeaf);
    }

    @Override
    public List<TextLeaf> getComponents() {
        return sentences;
    }

    @Override
    public String getString() {
        StringBuilder stringBuilder = new StringBuilder();
        for (TextLeaf sentence : sentences) {
            if (sentence.getString().endsWith(ENDING_SENTENCE)) {
                stringBuilder.append(sentence.getString());
            } else {
                stringBuilder.append(sentence.getString()).append(" ");
            }
        }
        return stringBuilder.toString();
    }

    @Override
    public void save(Long id) {
        paragraph.setTextId(id);
        long paragraphId = service.create(paragraph);
        for (TextLeaf sentence : sentences) {
            sentence.save(paragraphId);
        }
    }

    @Override
    public void save() {
        LOGGER.log(Level.ERROR, MESSAGE);
    }
}
