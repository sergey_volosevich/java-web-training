package by.training.sharing.bike;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.core.Command;
import by.training.sharing.core.CommandName;
import by.training.sharing.price.PriceDTO;
import by.training.sharing.price.PriceService;
import by.training.sharing.service.ServiceException;
import by.training.sharing.util.RequestUtils;
import by.training.sharing.validation.ValidationResult;
import by.training.sharing.validation.Validator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.math.BigDecimal;
import java.util.List;
import java.util.Map;

import static by.training.sharing.ApplicationConstants.*;
import static by.training.sharing.bike.BikeFiledNameConstants.*;
import static by.training.sharing.core.BeanNameConstants.*;

/**
 * This class contains commands for working with {@link by.training.sharing.bike.BikeDTO}.
 * Each method is one command executed per 1 request from the client.
 */

@Bean(name = BIKE_CONTROLLER)
@Command
public class BikeController {

    private static final Logger LOGGER = LogManager.getLogger(BikeController.class);

    private static final String PRICE_REQ_ATTRIBUTE = "prices";
    private static final String FAILED_TO_FIND_PRICES = "Failed to find prices. Cause:";
    private static final String BIKE_REQ_ATTRIBUTE = "bikes";
    private static final String BIKES_JSP = "/jsp/views/getBikes.jsp";
    private static final String FAILED_TO_GET_UNUSED_BIKES = "Failed to get unused bikes. Cause:";
    private static final String SUCCESS_ADDING = "success";
    private static final String OK_MESSAGE = "ok";

    private BikeService bikeService;
    private Validator validator;
    private PriceService priceService;

    public BikeController(@BeanQualifier(BIKE_SERVICE) BikeService bikeService,
                          @BeanQualifier(VALIDATOR) Validator validator,
                          @BeanQualifier(PRICE_SERVICE) PriceService priceService) {
        this.bikeService = bikeService;
        this.validator = validator;
        this.priceService = priceService;
    }

    /**
     * Get all unused bikes from database and then forms xml document, used by Google Map API.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_BIKES)
    public void getAllUnusedBikes(HttpServletRequest request, HttpServletResponse response) {
        try {
            RequestUtils.addCsrfToken(request);
            List<BikeDTO> bikes = bikeService.findAllBikesByState(BikeStatus.UNUSED);
            request.setAttribute(BIKE_REQ_ATTRIBUTE, bikes);
            RequestUtils.forward(request, response, BIKES_JSP);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_GET_UNUSED_BIKES, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method gets chooseBikeView.jsp and displays view, on which displayed map with bikes on it.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_CHOOSE_BIKE)
    public void getWelcomePage(HttpServletRequest request, HttpServletResponse response) {
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_CHOOSE_BIKE);
        RequestUtils.forward(request, response, MAIN_LAYOUT);
    }

    /**
     * This method gets and displays view with form to add {@link by.training.sharing.bike.BikeDTO}.
     * Only user with role ADMIN can invoke this method
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = GET_BIKE_ADDING_FORM)
    public void getBikeAddingFrom(HttpServletRequest request, HttpServletResponse response) {
        RequestUtils.getFlushAttribute(request, SUCCESS_ADDING);
        RequestUtils.addCsrfToken(request);
        try {
            List<PriceDTO> prices = priceService.findAll();
            HttpSession session = request.getSession();
            session.setAttribute(PRICE_REQ_ATTRIBUTE, prices);
            request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_BIKE_ADDING_FORM);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        } catch (ServiceException e) {
            LOGGER.error(FAILED_TO_FIND_PRICES, e);
            RequestUtils.sendError(response, HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * This method adds new {@link by.training.sharing.bike.BikeDTO} to database.
     * Only user with role ADMIN can invoke this method. If adding success redirected to view with
     * {@link by.training.sharing.bike.BikeDTO} adding form with successful message, otherwise forward to view
     * with {@link by.training.sharing.bike.BikeDTO} adding form with validation messages or error messages.
     *
     * @param request  a {@link ServletRequest} object that represents the
     *                 request the client makes of the servlet
     * @param response a {@link ServletResponse} object that represents
     *                 the response the servlet returns to the client
     */

    @CommandName(name = POST_ADD_BIKE)
    public void addBike(HttpServletRequest request, HttpServletResponse response) {
        Map<String, String> fieldValues = RequestUtils.getParametersFromRequest(request, SERIAL_NUMBER, BIKE_PASSWORD,
                LATITUDE, LONGITUDE, PRICE_ID);
        ValidationResult validationResult = validator.validate(fieldValues, BikeDTO.class);
        request.setAttribute(VIEW_NAME_REQ_ATTRIBUTE, GET_BIKE_ADDING_FORM);
        request.setAttribute(VALIDATION_MESSAGES, validationResult);
        if (!validationResult.isValid()) {
            RequestUtils.forward(request, response, MAIN_LAYOUT);
            return;
        }
        BikeDTO bikeDTO = buildBikeDTO(fieldValues);
        boolean isSave = bikeService.save(bikeDTO);
        if (isSave) {
            RequestUtils.setFlushAttribute(request, SUCCESS_ADDING, OK_MESSAGE);
            RequestUtils.removeCsrfToken(request);
            RequestUtils.sendRedirectToCommand(request, response, GET_BIKE_ADDING_FORM);
        } else {
            validationResult.setValidationInfo(FORM_ERROR_CODE, FORM_ERROR_MESSAGE);
            RequestUtils.forward(request, response, MAIN_LAYOUT);
        }
    }

    private BikeDTO buildBikeDTO(Map<String, String> parameterMap) {
        BikeDTO bikeDTO = new BikeDTO();
        String serialNumber = getParameter(SERIAL_NUMBER, parameterMap);
        bikeDTO.setSerialNumber(serialNumber);
        String bikePassword = getParameter(BIKE_PASSWORD, parameterMap);
        bikeDTO.setBikePassword(bikePassword);
        String latitude = getParameter(LATITUDE, parameterMap);
        bikeDTO.setLatitude(new BigDecimal(latitude));
        String longitude = getParameter(LONGITUDE, parameterMap);
        bikeDTO.setLongitude(new BigDecimal(longitude));
        String priceId = getParameter(PRICE_ID, parameterMap);
        bikeDTO.setPriceId(Long.parseLong(priceId));
        return bikeDTO;
    }

    private String getParameter(String parameterName, Map<String, String> map) {
        return map.get(parameterName);
    }
}
