package by.training.sharing.validation;

public enum DecimalConstraintType {
    GREATER_THAN,
    LESS_THAN,
    MORE_THAN_OR_EQUALS,
    LESS_THAN_OR_EQUALS
}
