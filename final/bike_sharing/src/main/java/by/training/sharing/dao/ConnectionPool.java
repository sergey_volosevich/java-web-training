package by.training.sharing.dao;

import by.training.sharing.util.PropertiesReader;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.IOException;
import java.lang.reflect.Proxy;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Enumeration;
import java.util.Properties;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

class ConnectionPool {

    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);

    private static final String CAN_NOT_REGISTER_DRIVER_WITH_NAME = "Can not register driver with name - {}";
    private static final String CAN_NOT_CREATE_CONNECTION_POOL = "Can not create connection pool";
    private static final String CONNECTION_SUCCESSFULLY_CREATED = "Connection successfully created";
    private static final String CAN_NOT_CREATE_CONNECTION = "Can not create connection.";
    private static final String ERROR_DELETING_CONNECTION_POOL = "Error deleting connection from the given" +
            " away connection pool";
    private static final String ERROR_ALLOCATING_CONNECTION_IN_THE_POOL = "Error allocating connection in the pool";
    private static final String DEREGISTER_JDBC_DRIVER = "Deregister JDBC driver: {}";
    private static final String ERROR_DEREGISTER_JDBC_DRIVER_ROOT_CAUSE = "Error deregister JDBC driver: {}." +
            " Root cause: ";
    private static final String CAN_NOT_CLOSE_CONNECTION = "Can not close connection. Root cause: ";
    private static final String CONNECTION_SUCCESSFULLY_CLOSED = "Connection successfully closed.";
    private static final String CONNECTION_POOL_SUCCESSFULLY_CREATED = "Connection pool successfully created";
    private static final String DATABASE_PROPERTIES = "database.properties";
    private static final String CAN_NOT_FIND_PROPERTY_FILE = "Can not find property file";
    private static final String METHOD_CLOSE = "close";
    private static final String METHOD_HASH_CODE = "hashCode";
    private static final String CONNECTION_RELEASED_TO_POOL = "Connection successful released to pool";
    private static final String CONNECTION_IS_GIVEN = "Connection is given";


    private static ConnectionPool instance;
    private static Lock lock = new ReentrantLock();
    private static AtomicBoolean isCreated = new AtomicBoolean();
    private final BlockingQueue<Connection> availableConnections;
    private final BlockingQueue<Connection> usedConnections;

    private String driverName;
    private String url;
    private String user;
    private String password;
    private int poolSize;

    private ConnectionPool() {
        Properties properties = null;
        try {
            properties = PropertiesReader.readProperties(DATABASE_PROPERTIES);
        } catch (IOException e) {
            LOGGER.error(e);
        }
        if (properties == null) {
            throw new ConnectionPoolInitializeException(CAN_NOT_FIND_PROPERTY_FILE);
        }
        this.driverName = properties.getProperty(DBParameter.DB_DRIVER);
        this.url = properties.getProperty(DBParameter.DB_URL);
        this.user = properties.getProperty(DBParameter.DB_USER);
        this.password = properties.getProperty(DBParameter.DB_PASSWORD);
        String poolCapacity = properties.getProperty(DBParameter.DB_POOL_CAPACITY);
        this.poolSize = Integer.parseInt(poolCapacity);
        availableConnections = new LinkedBlockingQueue<>(poolSize);
        usedConnections = new LinkedBlockingQueue<>(poolSize);
        registerDriver();
        init();
        LOGGER.info(CONNECTION_POOL_SUCCESSFULLY_CREATED);
    }

    static ConnectionPool getInstance() {
        if (!isCreated.get()) {
            lock.lock();
            try {
                if (instance == null) {
                    instance = new ConnectionPool();
                    isCreated.set(true);
                }
            } finally {
                lock.unlock();
            }
        }
        return instance;
    }

    private void init() {
        for (int i = 0; i < poolSize; i++) {
            try {
                Connection connection = DriverManager.getConnection(url, user, password);
                LOGGER.info(CONNECTION_SUCCESSFULLY_CREATED);
                availableConnections.add(connection);
            } catch (SQLException e) {
                LOGGER.error(CAN_NOT_CREATE_CONNECTION, e);
            }
        }
        if (availableConnections.isEmpty()) {
            throw new ConnectionPoolInitializeException(CAN_NOT_CREATE_CONNECTION_POOL);
        }
    }

    private void registerDriver() {
        try {
            Class.forName(driverName);
        } catch (ClassNotFoundException e) {
            LOGGER.error(CAN_NOT_REGISTER_DRIVER_WITH_NAME, driverName, e);
        }
    }

    Connection getConnection() {
        Connection connection = null;
        try {
            connection = availableConnections.take();
            usedConnections.put(connection);
            connection = createProxyConnection(connection);
        } catch (InterruptedException e) {
            LOGGER.error(e);
            Thread.currentThread().interrupt();
        }
        LOGGER.debug(CONNECTION_IS_GIVEN);
        return connection;
    }

    private Connection createProxyConnection(Connection connection) {
        return (Connection) Proxy.newProxyInstance(connection.getClass().getClassLoader(),
                new Class[]{Connection.class},
                (proxy, method, args) -> {
                    if (METHOD_CLOSE.equals(method.getName())) {
                        releaseConnection(connection);
                        LOGGER.debug(CONNECTION_RELEASED_TO_POOL);
                        return null;
                    } else if (METHOD_HASH_CODE.equals(method.getName())) {
                        return connection.hashCode();
                    } else {
                        return method.invoke(connection, args);
                    }
                });
    }

    private void releaseConnection(Connection connection) {
        if (!usedConnections.remove(connection)) {
            LOGGER.error(ERROR_DELETING_CONNECTION_POOL);
        }
        if (!availableConnections.offer(connection)) {
            LOGGER.error(ERROR_ALLOCATING_CONNECTION_IN_THE_POOL);
        }
    }

    void destroyPool() {
        closePool();
        deregisterDrivers();
    }

    private void closePool() {
        for (int i = 0; i < poolSize; i++) {
            Connection connection;
            try {
                connection = availableConnections.take();
                connection.close();
                LOGGER.info(CONNECTION_SUCCESSFULLY_CLOSED);
            } catch (InterruptedException e) {
                LOGGER.error(e);
                Thread.currentThread().interrupt();
            } catch (SQLException e) {
                LOGGER.error(CAN_NOT_CLOSE_CONNECTION, e);
            }
        }
    }

    private void deregisterDrivers() {
        Enumeration<Driver> drivers = DriverManager.getDrivers();
        while (drivers.hasMoreElements()) {
            Driver driver = drivers.nextElement();
            try {
                DriverManager.deregisterDriver(driver);
                LOGGER.info(DEREGISTER_JDBC_DRIVER, driver);
            } catch (SQLException e) {
                LOGGER.error(ERROR_DEREGISTER_JDBC_DRIVER_ROOT_CAUSE, driver, e);
            }
        }
    }
}
