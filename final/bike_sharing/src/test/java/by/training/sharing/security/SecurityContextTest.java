package by.training.sharing.security;


import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.mockito.Mock;
import org.mockito.Mockito;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.stream.IntStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(JUnit4.class)
public class SecurityContextTest {

    private static final Logger LOGGER = LogManager.getLogger(SecurityContext.class);
    private static final int N_THREADS = 15;

    @Test
    public void getInstance() throws InterruptedException {
        ExecutorService executorService = Executors.newFixedThreadPool(N_THREADS);
        IntStream.range(0, N_THREADS).forEach(i -> executorService.submit(() -> {
            SecurityContext instance = SecurityContext.getInstance();
        }));
        TimeUnit.SECONDS.sleep(2);
    }

    @Test
    public void getAccessRestriction() {
        SecurityContext securityContext = SecurityContext.getInstance();
        String noSuchCommand = securityContext.getAccessRestriction("NoSuchCommand");
        String loginUser = securityContext.getAccessRestriction("loginUser");
        assertEquals("ALL", loginUser);
        assertNull(noSuchCommand);
    }
}