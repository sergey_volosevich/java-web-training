package by.training.volosevich.library.controller;

import by.training.volosevich.library.service.LibraryService;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URISyntaxException;
import java.nio.file.Paths;

import static org.junit.Assert.assertEquals;

@RunWith(MockitoJUnitRunner.class)
public class LibraryControllerTest {

    private static final String VALID_FILE = "valid.txt";
    private static final String EMPTY_FILE = "empty.txt";
    private static final String ALL_INVALID_FILE = "allInvalid.txt";
    private static final String THREE_LINES_VALID_FILE = "threeLinesValid.txt";
    private static final String NO_FILE = "src\\";

    private LibraryController controller;

    @Mock
    private LibraryService service;

    @Before
    public void setUp() {
        service = Mockito.mock(service.getClass());
        controller = new LibraryController(service);
    }

    @Test
    public void saveEntityFromFileValidFile() throws URISyntaxException {
        String validFilePath = Paths.get(getClass().getClassLoader().getResource(VALID_FILE).toURI()).toString();
        assertEquals(6, controller.saveEntityFromFile(validFilePath));
    }

    @Test
    public void saveEntityFromFileEmptyFile() throws URISyntaxException {
        String validFilePath = Paths.get(getClass().getClassLoader().getResource(EMPTY_FILE).toURI()).toString();
        assertEquals(0, controller.saveEntityFromFile(validFilePath));
    }

    @Test
    public void saveEntityFromFileAllInvalidFile() throws URISyntaxException {
        String validFilePath = Paths.get(getClass().getClassLoader().getResource(ALL_INVALID_FILE).toURI()).toString();
        assertEquals(0, controller.saveEntityFromFile(validFilePath));
    }

    @Test
    public void saveEntityFromFileThreeLinesValidFile() throws URISyntaxException {
        String validFilePath = Paths.get(getClass().getClassLoader().getResource(THREE_LINES_VALID_FILE).toURI())
                .toString();
        assertEquals(3, controller.saveEntityFromFile(validFilePath));
    }

    @Test
    public void saveEntityFromFileNoFile() throws URISyntaxException {
        assertEquals(0, controller.saveEntityFromFile(NO_FILE));
    }
}