package by.training.sharing.role;

import by.training.sharing.dao.CRUDDao;
import by.training.sharing.dao.DAOException;

import java.util.List;

public interface RoleDAO extends CRUDDao<RoleDTO> {

    void assignDefaultRoles(long userId) throws DAOException;

    void assignRole(long roleId, long userId) throws DAOException;

    List<RoleDTO> getUserRoles(long userId) throws DAOException;

}
