package by.training.sharing.price;

import by.training.sharing.core.Bean;
import by.training.sharing.core.BeanQualifier;
import by.training.sharing.dao.DAOException;
import by.training.sharing.dao.JDBCTemplate;

import java.sql.SQLException;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.JDBC_TEMPLATE;
import static by.training.sharing.core.BeanNameConstants.PRICE_DAO;

@Bean(name = PRICE_DAO)
public class PriceDAOImpl implements PriceDAO {

    private static final String SELECT_ALL_PRICES = "SELECT rental_price_id, date, current_price FROM rental_price;";

    private JDBCTemplate jdbcTemplate;

    public PriceDAOImpl(@BeanQualifier(JDBC_TEMPLATE) JDBCTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public long save(PriceDTO priceDTO) throws DAOException {
        return 0;
    }

    @Override
    public boolean update(PriceDTO priceDTO) {
        return false;
    }

    @Override
    public boolean delete(long id) {
        return false;
    }

    @Override
    public PriceDTO getById(long id) throws DAOException {
        return null;
    }

    @Override
    public List<PriceDTO> findAll() throws DAOException {
        try {
            return jdbcTemplate.query(SELECT_ALL_PRICES, new PriceRowMapper());
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }
}
