package by.training.sharing.bike;

import by.training.sharing.validation.DecimalConstraint;
import by.training.sharing.validation.DecimalConstraintType;
import by.training.sharing.validation.Validation;
import by.training.sharing.validation.ValidationPattern;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import static by.training.sharing.bike.BikeFiledNameConstants.*;

@Validation
public class BikeDTO implements Serializable {

    private static final long serialVersionUID = -7284333967518221889L;

    private long id;
    @ValidationPattern(fieldName = SERIAL_NUMBER, message = "bike.serial.number.validation.msg",
            regexp = "^[a-zA-Z0-9-\\.]{5,30}$")
    private String serialNumber;
    @ValidationPattern(fieldName = BIKE_PASSWORD, message = "bike.password.validation.msg",
            regexp = "^[a-zA-Z0-9]{5,30}$")
    private String bikePassword;
    @DecimalConstraint(fieldName = LATITUDE, message = "bike.latitude.max.validation.msg", value = "53.977702",
            constraint = DecimalConstraintType.LESS_THAN_OR_EQUALS)
    @DecimalConstraint(fieldName = LATITUDE, message = "bike.latitude.min.validation.msg", value = "53.822202",
            constraint = DecimalConstraintType.MORE_THAN_OR_EQUALS)
    private BigDecimal latitude;

    @DecimalConstraint(fieldName = LONGITUDE, message = "bike.longitude.max.validation.msg", value = "27.700280",
            constraint = DecimalConstraintType.LESS_THAN_OR_EQUALS)
    @DecimalConstraint(fieldName = LONGITUDE, message = "bike.longitude.min.validation.msg", value = "27.388028",
            constraint = DecimalConstraintType.MORE_THAN_OR_EQUALS)
    private BigDecimal longitude;
    private long priceId;
    private BikeStatus status;

    public BikeDTO() {
    }

    public BikeDTO(long id, String serialNumber, String bikePassword, BigDecimal latitude, BigDecimal longitude,
                   long priceId, BikeStatus status) {
        this.id = id;
        this.serialNumber = serialNumber;
        this.bikePassword = bikePassword;
        this.latitude = latitude;
        this.longitude = longitude;
        this.priceId = priceId;
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

    public String getBikePassword() {
        return bikePassword;
    }

    public void setBikePassword(String bikePassword) {
        this.bikePassword = bikePassword;
    }

    public BigDecimal getLatitude() {
        return latitude;
    }

    public void setLatitude(BigDecimal latitude) {
        this.latitude = latitude;
    }

    public BigDecimal getLongitude() {
        return longitude;
    }

    public void setLongitude(BigDecimal longitude) {
        this.longitude = longitude;
    }

    public long getPriceId() {
        return priceId;
    }

    public void setPriceId(long priceId) {
        this.priceId = priceId;
    }

    public BikeStatus getStatus() {
        return status;
    }

    public void setStatus(BikeStatus status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BikeDTO)) return false;
        BikeDTO bikeDTO = (BikeDTO) o;
        return getId() == bikeDTO.getId() &&
                getPriceId() == bikeDTO.getPriceId() &&
                Objects.equals(getSerialNumber(), bikeDTO.getSerialNumber()) &&
                Objects.equals(getBikePassword(), bikeDTO.getBikePassword()) &&
                Objects.equals(getLatitude(), bikeDTO.getLatitude()) &&
                Objects.equals(getLongitude(), bikeDTO.getLongitude()) &&
                getStatus() == bikeDTO.getStatus();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getSerialNumber(), getBikePassword(), getLatitude(), getLongitude(), getPriceId(),
                getStatus());
    }

    @Override
    public String toString() {
        return "BikeDTO{" +
                "id=" + id +
                ", serialNumber='" + serialNumber + '\'' +
                ", bikePassword='" + bikePassword + '\'' +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", priceId=" + priceId +
                ", status=" + status +
                '}';
    }
}
