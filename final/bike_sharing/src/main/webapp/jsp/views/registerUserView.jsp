<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="by.training.sharing.user.UserFieldNameConstants" %>
<%@taglib prefix="bsh" uri="bikeSharing" %>

<fmt:message key="user.login.form.label" var="login"/>
<fmt:message key="user.password.form.label" var="password"/>
<fmt:message key="user.name.form.label" var="name"/>
<fmt:message key="user.lastName.form.label" var="lastName"/>
<fmt:message key="user.email.form.label" var="email"/>
<fmt:message key="user.phone.form.label" var="phone"/>
<fmt:message key="form.back.button" var="back"/>
<fmt:message key="form.submit.button" var="submit"/>

<div class="container text-dark">
    <div class="row mt-1 mb-3">
        <div class="col-md-6 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 style="color: #212529" class="panel-title text-center">
                        <fmt:message key="user.registration.form"/>
                    </h3>
                </div>
                <div class="panel-body">
                    <form class="registration-form" action="<c:out value="${pageContext.request.contextPath}"/>/"
                          method="post"
                          accept-charset="UTF-8">
                        <input type="hidden" name="commandName" value="registerUser"/>
                        <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                        <div class="mdc-text-field username">
                            <input type="text" class="mdc-text-field__input" id="username-input"
                                   name="<c:out value="${UserFieldNameConstants.USER_LOGIN}"/>"
                                   pattern="^[a-zA-Z0-9-_\.]{5,30}$"
                                   required
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_LOGIN)}"/>">
                            <label class="mdc-floating-label" for="username-input">
                                <c:out value="${login}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.login.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_LOGIN}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="mdc-text-field password">
                            <input type="password" class="mdc-text-field__input" id="password"
                                   name="<c:out value="${UserFieldNameConstants.USER_PASSWORD}"/>"
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_PASSWORD)}"/>"
                                   required pattern="(?=^.{8,}$)(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?!.*\s)[0-9a-zA-Z]*$">
                            <label class="mdc-floating-label" for="password">
                                <c:out value="${password}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.password.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_PASSWORD}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="mdc-text-field name">
                            <input type="text" class="mdc-text-field__input" id="name-input"
                                   name="<c:out value="${UserFieldNameConstants.USER_NAME}"/>"
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_NAME)}"/>"
                                   required pattern="^[а-яА-ЯёЁa-zA-Z-?]{1,45}$">
                            <label class="mdc-floating-label" for="name-input">
                                <c:out value="${name}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.name.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_NAME}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="mdc-text-field lastName">
                            <input type="text" class="mdc-text-field__input" id="lastName-input"
                                   name="<c:out value="${UserFieldNameConstants.USER_LAST_NAME}"/>"
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_LAST_NAME)}"/>"
                                   required pattern="^[а-яА-ЯёЁa-zA-Z-?]{1,45}$">
                            <label class="mdc-floating-label" for="lastName-input">
                                <c:out value="${lastName}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.lastName.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_LAST_NAME}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="mdc-text-field email">
                            <input type="email" class="mdc-text-field__input" id="email-input"
                                   name="<c:out value="${UserFieldNameConstants.USER_EMAIL}"/>"
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_EMAIL)}"/>"
                                   required
                                   pattern="^([a-zA-Z0-9_\-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([a-zA-Z0-9\-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$">
                            <label class="mdc-floating-label" for="email-input">
                                <c:out value="${email}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.email.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_EMAIL}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="mdc-text-field phone">
                            <input type="tel" class="mdc-text-field__input" id="phone-input"
                                   name="<c:out value="${UserFieldNameConstants.USER_PHONE}"/>"
                                   value="<c:out value="${param.get(UserFieldNameConstants.USER_PHONE)}"/>"
                                   required pattern="^[+]\d{3}((?:29)|(?:33)|(?:44)|(?:25))\d{7}$">
                            <label class="mdc-floating-label" for="phone-input">
                                <c:out value="${phone}"/>
                            </label>
                            <div class="mdc-line-ripple"></div>
                        </div>
                        <div class="mdc-text-field-helper-line helper">
                            <div class="mdc-text-field-helper-text mdl-color-text--black">
                                <fmt:message key="user.phone.form.helper"/>
                            </div>
                            <p class="text-danger">
                                <bsh:validationMessage fieldName="${UserFieldNameConstants.USER_PHONE}"
                                                       validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <p class="text-danger">
                            <bsh:validationMessage fieldName="error" validationMessages="vrMessages"/>
                        </p>
                        <div class="button-container">
                            <a role="button" class="mdc-button mdc-button--raised cancel"
                               href="<c:out value="${pageContext.request.contextPath}"/>">
                                <c:out value="${back}"/>
                            </a>
                            <button class="mdc-button mdc-button--raised submit">
                    <span class="mdc-button__label">
                        <c:out value="${submit}"/>
                    </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

