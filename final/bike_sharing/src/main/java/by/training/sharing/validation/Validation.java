package by.training.sharing.validation;


import java.lang.annotation.*;

/**
 * This annotation is used to to mark class, whose fields must be validated.
 */

@Inherited
@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface Validation {
}
