package by.training.tariff.entity;

import java.util.Objects;

public class LightTariff {

    private long id;
    private String tariffName;
    private String dateCreation;
    private OperatorName operatorName;
    private double payroll;
    private int freeMinutesInNetwork;
    private int internetTraffic = 50;
    private CallPrice price;
    private AdditionalInfo additionalInfo;


    public LightTariff() {
    }

    public LightTariff(long id, String tariffName, String dateCreation, OperatorName operatorName, double payroll,
                       AdditionalInfo additionalInfo, CallPrice price, int freeMinutesInNetwork, int internetTraffic) {
        this.id = id;
        this.tariffName = tariffName;
        this.dateCreation = dateCreation;
        this.operatorName = operatorName;
        this.payroll = payroll;
        this.additionalInfo = additionalInfo;
        this.price = price;
        this.freeMinutesInNetwork = freeMinutesInNetwork;
        this.internetTraffic = internetTraffic;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTariffName() {
        return tariffName;
    }

    public void setTariffName(String tariffName) {
        this.tariffName = tariffName;
    }

    public String getDateCreation() {
        return dateCreation;
    }

    public void setDateCreation(String dateCreation) {
        this.dateCreation = dateCreation;
    }

    public OperatorName getOperatorName() {
        return operatorName;
    }

    public void setOperatorName(OperatorName operatorName) {
        this.operatorName = operatorName;
    }

    public double getPayroll() {
        return payroll;
    }

    public void setPayroll(double payroll) {
        this.payroll = payroll;
    }

    public AdditionalInfo getAdditionalInfo() {
        return additionalInfo;
    }

    public void setAdditionalInfo(AdditionalInfo additionalInfo) {
        this.additionalInfo = additionalInfo;
    }

    public CallPrice getPrice() {
        return price;
    }

    public void setPrice(CallPrice price) {
        this.price = price;
    }

    public int getFreeMinutesInNetwork() {
        return freeMinutesInNetwork;
    }

    public void setFreeMinutesInNetwork(int freeMinutesInNetwork) {
        this.freeMinutesInNetwork = freeMinutesInNetwork;
    }

    public int getInternetTraffic() {
        return internetTraffic;
    }

    public void setInternetTraffic(int internetTraffic) {
        this.internetTraffic = internetTraffic;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof LightTariff)) return false;
        LightTariff that = (LightTariff) o;
        return getId() == that.getId() &&
                Double.compare(that.getPayroll(), getPayroll()) == 0 &&
                getFreeMinutesInNetwork() == that.getFreeMinutesInNetwork() &&
                getInternetTraffic() == that.getInternetTraffic() &&
                Objects.equals(getTariffName(), that.getTariffName()) &&
                Objects.equals(getDateCreation(), that.getDateCreation()) &&
                getOperatorName() == that.getOperatorName() &&
                Objects.equals(getPrice(), that.getPrice()) &&
                Objects.equals(getAdditionalInfo(), that.getAdditionalInfo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getTariffName(), getDateCreation(), getOperatorName(), getPayroll(),
                getFreeMinutesInNetwork(), getInternetTraffic(), getPrice(), getAdditionalInfo());
    }

    @Override
    public String toString() {
        return "LightTariff{" +
                "id=" + id +
                ", tariffName='" + tariffName + '\'' +
                ", dateCreation='" + dateCreation + '\'' +
                ", operatorName=" + operatorName +
                ", payroll=" + payroll +
                ", freeMinutesInNetwork=" + freeMinutesInNetwork +
                ", internetTraffic=" + internetTraffic +
                ", price=" + price +
                ", info=" + additionalInfo +
                '}';
    }
}
