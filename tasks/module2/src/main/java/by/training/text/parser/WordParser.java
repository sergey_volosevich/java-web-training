package by.training.text.parser;

import by.training.text.entity.Word;
import by.training.text.model.TextLeaf;
import by.training.text.model.WordLeaf;
import by.training.text.service.WordService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WordParser extends AbstractTextParser {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String WORD_REGEX = "(\\S*\\w+|[-])(\\s*\\p{Punct}*\\s*)";
    private static final String NO_WORDS_OR_CHARACTERS_IN_TEXT = "No words or characters in text";

    private WordService wordService;

    public WordParser(WordService wordService) {
        this.wordService = wordService;
    }

    @Override
    public TextLeaf parse(String line) {
        Pattern pattern = Pattern.compile(WORD_REGEX);
        Matcher matcher = pattern.matcher(line);
        if (matcher.matches()) {
            Word word = new Word(matcher.group(1), matcher.group(2));
            return new WordLeaf(word, wordService);
        } else {
            LOGGER.log(Level.ERROR, NO_WORDS_OR_CHARACTERS_IN_TEXT);
            return new WordLeaf(new Word("", ""), wordService);
        }
    }
}
