package by.training.tariff.entity;

import java.util.Objects;

public class BusinessTariff extends ComfortTariff {

    private int freeInternationalCall;

    public BusinessTariff() {
    }

    public BusinessTariff(long id, String tariffName, String dateCreation, OperatorName name, double payroll,
                          AdditionalInfo parameters, CallPrice price, int freeMinutesInNetwork, int internetTraffic,
                          int freeMinutesOutNetwork, int freeInternationalCall) {
        super(id, tariffName, dateCreation, name, payroll, parameters, price, freeMinutesInNetwork, internetTraffic,
                freeMinutesOutNetwork);
        this.freeInternationalCall = freeInternationalCall;
    }

    public int getFreeInternationalCall() {
        return freeInternationalCall;
    }

    public void setFreeInternationalCall(int freeInternationalCall) {
        this.freeInternationalCall = freeInternationalCall;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof BusinessTariff)) return false;
        if (!super.equals(o)) return false;
        BusinessTariff tariff = (BusinessTariff) o;
        return getFreeInternationalCall() == tariff.getFreeInternationalCall();
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getFreeInternationalCall());
    }

    @Override
    public String toString() {
        return "BusinessTariff{" +
                "freeInternationalCall=" + freeInternationalCall +
                "} " + super.toString();
    }
}
