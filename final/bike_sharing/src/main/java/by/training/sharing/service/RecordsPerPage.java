package by.training.sharing.service;

import java.util.Optional;
import java.util.stream.Stream;

public enum RecordsPerPage {

    FIVE_ROWS(5), TEN_ROWS(10), TWENTY_ROWS(20);

    private int numberOfRecords;

    RecordsPerPage(int recordsPerPage) {
        this.numberOfRecords = recordsPerPage;
    }

    public static Optional<RecordsPerPage> getNumberOfRows(int numberOfRecords) {
        return Stream.of(RecordsPerPage.values())
                .filter(recordsPerPage -> recordsPerPage.numberOfRecords == numberOfRecords).findFirst();
    }

    public int getNumberOfRecords() {
        return numberOfRecords;
    }
}
