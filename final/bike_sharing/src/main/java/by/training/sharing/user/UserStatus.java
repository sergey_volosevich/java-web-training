package by.training.sharing.user;

import java.util.Optional;
import java.util.stream.Stream;

public enum UserStatus {
    BANNED, ACTIVE;

    public static Optional<UserStatus> getStatus(int ordinal) {
        return Stream.of(UserStatus.values()).filter(user -> user.ordinal() == ordinal).findFirst();
    }
}
