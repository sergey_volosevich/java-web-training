package by.training.tariff.validator;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.nio.file.Paths;
import java.util.Objects;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

@RunWith(JUnit4.class)
public class XMLValidatorTest {

    private static final String VALID_XML = "valid.xml";
    private static final String XSD_FILE = "tariff.xsd";
    private static final String NOT_WELL_FORMED = "notWellFormed.xml";
    private static final String NO_VALID_DATA = "invalidData.xml";
    private static final String NO_VALID_VALUES = "invalidValues.xml";
    private static final String NOT_XML = "notXML.txt";
    private static final String UNREACHABLE_XSD = "unreachable.xsd";
    private String pathValidXML;
    private String pathNotWellFormedXML;
    private String pathNoValidDataXML;
    private String pathNoValidValuesXML;
    private String pathNotXMLFile;
    private XMLValidator validator;


    @Before
    public void setUp() throws Exception {
        pathValidXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader().getResource(VALID_XML))
                .toURI()).toString();
        pathNotWellFormedXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_WELL_FORMED)).toURI()).toString();
        pathNoValidDataXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NO_VALID_DATA)).toURI()).toString();
        pathNoValidValuesXML = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NO_VALID_VALUES)).toURI()).toString();
        pathNotXMLFile = Paths.get(Objects.requireNonNull(getClass().getClassLoader()
                .getResource(NOT_XML)).toURI()).toString();
        validator = new XMLValidator(XSD_FILE);
    }

    @Test
    public void validateValidFile() {
        ValidationResult validationResult = validator.validate(pathValidXML);
        assertTrue(validationResult.isValid());
    }

    @Test
    public void validateNotWellFormedXMLFile() {
        ValidationResult validationResult = validator.validate(pathNotWellFormedXML);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void validateInValidDataFile() {
        ValidationResult validationResult = validator.validate(pathNoValidDataXML);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void validateInvalidValuesFile() {
        ValidationResult validationResult = validator.validate(pathNoValidValuesXML);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void validateNotXMLFile() {
        ValidationResult validationResult = validator.validate(pathNotXMLFile);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void validateWithUnreachableXSD() {
        XMLValidator xmlValidator = new XMLValidator(UNREACHABLE_XSD);
        ValidationResult validationResult = xmlValidator.validate(pathValidXML);
        assertFalse(validationResult.isValid());
    }

    @Test
    public void validateWithNullXSD() {
        XMLValidator xmlValidator = new XMLValidator(null);
        ValidationResult validationResult = xmlValidator.validate(pathValidXML);
        assertFalse(validationResult.isValid());
    }
}