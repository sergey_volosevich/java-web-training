package by.training.sharing.core;

/**
 * This exception indicates, that {@link BeanRegistryImpl} can not instantiate bean.
 */

public class BeanInstantiationException extends BeanRegistrationException {

    private static final long serialVersionUID = 6751054901213270320L;

    public BeanInstantiationException(String message) {
        super(message);
    }

    public BeanInstantiationException(String message, Exception e) {
        super(message, e);
    }
}
