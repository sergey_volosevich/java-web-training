package by.training.sharing.user;

import by.training.sharing.service.ServiceException;

import java.util.List;
import java.util.Map;

public interface UserService {

    List<UserDTO> getPartOfUsers(int currentPage, int recordsPerPAge) throws ServiceException;

    boolean registerUser(UserDTO userDTO);

    List<UserDTO> getAllUsers() throws ServiceException;

    Map<String, String> findDuplicateFields(String login, String email);

    int countOfRows() throws ServiceException;
}
