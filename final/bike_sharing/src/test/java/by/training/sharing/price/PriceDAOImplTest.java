package by.training.sharing.price;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.dao.*;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import static by.training.sharing.core.BeanNameConstants.CONNECTION_MANAGER;
import static org.junit.Assert.assertEquals;

@RunWith(JUnit4.class)
public class PriceDAOImplTest {

    private static final Logger LOGGER = LogManager.getLogger(PriceDAOImplTest.class);

    private PriceDAOImpl priceDAO;

    @BeforeClass
    public static void createSchema() {
        final ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        URL resource = connectionManager.getClass().getClassLoader().getResource("BikeSharingSQL.sql");
        StringBuilder builder = new StringBuilder();
        try (BufferedReader reader = new BufferedReader(new FileReader(new File(resource.toURI())))) {
            String line;
            while ((line = reader.readLine()) != null) {
                builder.append(line);
            }
        } catch (IOException | URISyntaxException e) {
            LOGGER.error(e);
        }
        String sqlString = builder.toString();
        executeSQLQuery(connectionManager, sqlString);
    }

    private static void executeSQLQuery(ConnectionManager connectionManager, String sqlString) {
        String[] sqlQueries = sqlString.split(";");
        for (String sqlQuery : sqlQueries) {
            String replace = sqlQuery.replace("\n", " ");
            try (Connection connection = connectionManager.getConnection();
                 PreparedStatement statement = connection.prepareStatement(replace);) {
                statement.executeUpdate();
            } catch (SQLException e) {
                LOGGER.error("Failed to create schema. Cause:", e);
            }
        }
    }

    @AfterClass
    public static void clearResources() throws SQLException {
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        ConnectionManager connectionManager = applicationContext.getBean(CONNECTION_MANAGER);
        String sql = "DROP SCHEMA IF EXISTS bike_rental CASCADE;";
        PreparedStatement statement;
        try (Connection connection = connectionManager.getConnection()) {
            statement = connection.prepareStatement(sql);
            statement.executeUpdate();
        }
        LOGGER.info("All clear");
    }

    @Before
    public void setUp() throws Exception {
        TransactionManager transactionManager = new TransactionManagerImpl();
        ConnectionManager connectionManager = new ConnectionManagerImpl(transactionManager);
        JDBCTemplateImpl jdbcTemplate = new JDBCTemplateImpl(connectionManager);
        priceDAO = new PriceDAOImpl(jdbcTemplate);
    }

    @Test
    public void findAll() throws DAOException {
        List<PriceDTO> prices = priceDAO.findAll();
        assertEquals(2, prices.size());
        assertEquals(4.20, prices.get(0).getPriceValue().doubleValue(), 0.00);
    }
}