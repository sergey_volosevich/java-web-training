<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="vr" uri="bikeSharing" %>
<%@ page import="by.training.sharing.bike.BikeFiledNameConstants" %>

<c:set var="serialNumber" value="${BikeFiledNameConstants.SERIAL_NUMBER}"/>
<c:set var="bikePassword" value="${BikeFiledNameConstants.BIKE_PASSWORD}"/>
<c:set var="latitude" value="${BikeFiledNameConstants.LATITUDE}"/>
<c:set var="longitude" value="${BikeFiledNameConstants.LONGITUDE}"/>
<c:set var="priceId" value="${BikeFiledNameConstants.PRICE_ID}"/>

<div class="container text-dark">
    <div class="row mt-1 mb-3">
        <div class="col-md-6 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 class="panel-title text-center"><fmt:message key="bike.form.header"/></h3>
                </div>
                <div class="panel-body">
                    <form action="<c:out value="${pageContext.request.contextPath}"/>/" method="post">
                        <input type="hidden" name="commandName" value="addBike"/>
                        <input type="hidden" name="csrf" value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                        <div class="form-group">
                            <label for="bikeNumber"><fmt:message key="bike.serial.number.form.label"/></label>
                            <input type="text" id="bikeNumber" class="form-control"
                                   name="<c:out value="${serialNumber}"/>"
                                   value="<c:out value="${param.get(serialNumber)}"/>">
                            <small class="form-text text-muted">
                                <fmt:message key="bike.serial.number.form.helper"/>
                            </small>
                            <p class="alert-danger">
                                <vr:validationMessage fieldName="${serialNumber}" validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="form-group">
                            <label for="password"><fmt:message key="bike.password.form.label"/></label>
                            <input type="text" id="password" class="form-control"
                                   name="<c:out value="${bikePassword}"/>"
                                   value="<c:out value="${param.get(bikePassword)}"/>">
                            <small class="form-text text-muted">
                                <fmt:message key="bike.password.form.helper"/>
                            </small>
                            <p class="alert-danger">
                                <vr:validationMessage fieldName="${bikePassword}" validationMessages="vrMessages"/>
                            </p>
                        </div>
                        <div class="form-group">
                            <h6 class="form-text"><fmt:message key="bike.form.coordinates"/></h6>
                            <div class="form-row">
                                <div class="col-md-6">
                                    <label for="latitude"><fmt:message key="bike.latitude.form.label"/></label>
                                    <input type="text" id="latitude" class="form-control"
                                           name="<c:out value="${latitude}"/>"
                                           value="<c:out value="${param.get(latitude)}"/>">
                                    <small class="form-text text-muted">
                                        <fmt:message key="bike.latitude.form.helper"/>
                                    </small>
                                    <p class="alert-danger">
                                        <vr:validationMessage fieldName="${latitude}" validationMessages="vrMessages"/>
                                    </p>
                                </div>
                                <div class="col-md-6">
                                    <label for="longitude"><fmt:message key="bike.longitude.form.label"/></label>
                                    <input type="text" id="longitude" class="form-control"
                                           name="<c:out value="${longitude}"/>"
                                           value="<c:out value="${param.get(longitude)}"/>">
                                    <small class="form-text text-muted">
                                        <fmt:message key="bike.longitude.form.helper"/>
                                    </small>
                                    <p class="alert-danger">
                                        <vr:validationMessage fieldName="${longitude}" validationMessages="vrMessages"/>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <c:choose>
                            <c:when test="${!sessionScope.prices.isEmpty()}">
                                <div class="form-group">
                                    <label for="rental"><fmt:message key="bike.form.rental.price"/></label>
                                    <select id="rental" class="form-control" name="<c:out value="${priceId}"/>">
                                        <c:forEach items="${sessionScope.prices}" var="price">
                                            <c:set var="priceValue" value="${price}"/>
                                            <option value="<c:out value="${price.id}"/>">
                                                <c:out value="${priceValue.priceValue}"/>
                                            </option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <p class="alert-danger">
                                    <vr:validationMessage fieldName="error" validationMessages="vrMessages"/>
                                </p>
                                <c:if test="${not empty requestScope.get('success')}">
                                    <div class="alert alert-success text-center" role="alert">
                                        <fmt:message key="bike.add.form.success"/>
                                    </div>
                                </c:if>
                                <div class="pt-2 d-flex justify-content-between">
                                    <a class="btn btn-outline-dark" role="button"
                                       href="<bsh:command value="welcomeView"/>">
                                        <fmt:message key="links.back.admin.page"/></a>
                                    <button class="btn btn-outline-dark">
                                        <fmt:message key="links.add.bike"/></button>
                                </div>
                            </c:when>
                            <c:otherwise>
                                <div class="text-center">
                                    <fmt:message key="bike.price.form.helper"/>
                                    <a href="<bsh:command value="addCardView"/>" class="mdl-badge mdl-badge--overlap">
                                        <fmt:message key="links.click.here"/>
                                    </a>
                                </div>
                                <bsh:backButton value="welcomeView"/>
                            </c:otherwise>
                        </c:choose>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
