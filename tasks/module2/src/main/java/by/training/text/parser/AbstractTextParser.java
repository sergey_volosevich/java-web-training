package by.training.text.parser;

import by.training.text.model.TextLeaf;

public abstract class AbstractTextParser implements ParserChain<TextLeaf> {

    private ParserChain<TextLeaf> next;

    @Override
    public ParserChain<TextLeaf> linkWith(ParserChain<TextLeaf> next) {
        this.next = next;
        return next;
    }

    TextLeaf nextParse(String text) {
        if (next != null) {
            return next.parse(text);
        } else {
            return null;
        }
    }
}
