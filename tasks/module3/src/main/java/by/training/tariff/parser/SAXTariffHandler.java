package by.training.tariff.parser;

import by.training.tariff.entity.*;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class SAXTariffHandler extends DefaultHandler {

    private static final String NO_SUCH_FILED_IN_DOCUMENT = "No such filed in document - {1}";

    private List<LightTariff> tariffs;
    private String content;
    private CallPrice price;
    private AdditionalInfo info;
    private LightTariff tariff;

    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        switch (qName) {
            case TariffFields.LIGHT_TARIFF:
                tariff = new LightTariff();
                setAttributes(attributes);
                break;
            case TariffFields.COMFORT_TARIFF:
                tariff = new ComfortTariff();
                setAttributes(attributes);
                break;
            case TariffFields.BUSINESS_TARIFF:
                tariff = new BusinessTariff();
                setAttributes(attributes);
                break;
            case TariffFields.CALL_SMS_PRICES:
                price = new CallPrice();
                break;
            case TariffFields.ADDITIONAL_INFO:
                info = new AdditionalInfo();
                break;
            case TariffFields.TARIFFS:
                tariffs = new ArrayList<>();
                break;
            default:
        }
    }

    private void setAttributes(Attributes attributes) {
        String idValue = attributes.getValue(TariffFields.ID);
        long id = Long.parseLong(idValue.substring(1));
        tariff.setId(id);
        if (attributes.getLength() == 2) {
            String value = attributes.getValue(TariffFields.FREE_INTERNET_TRAFFIC);
            int freeInternetTraffic = Integer.parseInt(value);
            tariff.setInternetTraffic(freeInternetTraffic);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        switch (qName) {
            case TariffFields.TARIFF_NAME:
                tariff.setTariffName(content);
                break;
            case TariffFields.DATE_CREATION:
                tariff.setDateCreation(content);
                break;
            case TariffFields.OPERATOR_NAME:
                OperatorName operatorName = OperatorName.valueOf(content.toUpperCase());
                tariff.setOperatorName(operatorName);
                break;
            case TariffFields.PAYROLL:
                tariff.setPayroll(Double.parseDouble(content));
                break;
            case TariffFields.FREE_MINUTES_IN_NETWORK:
                tariff.setFreeMinutesInNetwork(Integer.parseInt(content));
                break;
            case TariffFields.IN_NETWORK_CALL:
                price.setInNetworkCall(Double.parseDouble(content));
                break;
            case TariffFields.OUT_NETWORK_CALL:
                price.setOutNetworkCall(Double.parseDouble(content));
                break;
            case TariffFields.INTERNATIONAL_CALL:
                price.setInternationalCall(Double.parseDouble(content));
                break;
            case TariffFields.SMS_PRICE:
                price.setSmsPrice(Double.parseDouble(content));
                break;
            case TariffFields.CALL_SMS_PRICES:
                tariff.setPrice(price);
                break;
            case TariffFields.BILLING_TYPE:
                BillingType billingType = BillingType.valueOf(content.toUpperCase());
                info.setType(billingType);
                break;
            case TariffFields.FAVORITE_NUMBER:
                info.setFavoriteNumbers(Integer.parseInt(content));
                break;
            case TariffFields.CONNECTION_PRICE:
                info.setConnectionPrice(Double.parseDouble(content));
                break;
            case TariffFields.ADDITIONAL_INFO:
                tariff.setAdditionalInfo(info);
                break;
            case TariffFields.FREE_MINUTES_OUT_NETWORK:
                ((ComfortTariff) tariff).setFreeMinutesOutNetwork(Integer.parseInt(content));
                break;
            case TariffFields.FREE_MINUTES_INTERNATIONAL_CALL:
                ((BusinessTariff) tariff).setFreeInternationalCall(Integer.parseInt(content));
                break;
            case TariffFields.TARIFFS:
                break;
            case TariffFields.LIGHT_TARIFF:
            case TariffFields.COMFORT_TARIFF:
            case TariffFields.BUSINESS_TARIFF:
                tariffs.add(tariff);
                break;
            default:
                throw new SAXException(MessageFormat.format(NO_SUCH_FILED_IN_DOCUMENT, qName));
        }
    }

    @Override
    public void characters(char[] ch, int start, int length) throws SAXException {
        content = new String(ch, start, length);
    }

    public List<LightTariff> getTariffs() {
        return tariffs;
    }
}
