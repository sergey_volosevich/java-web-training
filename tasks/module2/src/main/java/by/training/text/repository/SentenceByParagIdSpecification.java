package by.training.text.repository;

import by.training.text.entity.Sentence;

public class SentenceByParagIdSpecification implements Specification<Sentence> {

    private long paragraphId;

    public SentenceByParagIdSpecification(long paragraphId) {
        this.paragraphId = paragraphId;
    }

    @Override
    public boolean match(Sentence entity) {
        return entity.getParagraphId() == paragraphId;
    }
}
