package by.training.text.parser;

import by.training.text.entity.Paragraph;
import by.training.text.model.ParagraphComposite;
import by.training.text.model.TextLeaf;
import by.training.text.service.ParagraphService;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ParagraphParser extends AbstractTextParser {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String SENTENCE_REGEX = "(?:[A-Z])(?:.\\n*)+?(?:[.?!]+\\n?)";
    private static final String NO_SENTENCES_IN_TEXT = "No sentences in text";

    private ParagraphService service;

    public ParagraphParser(ParagraphService service) {
        this.service = service;
    }

    @Override
    public TextLeaf parse(String line) {
        Paragraph paragraph = new Paragraph();
        ParagraphComposite paragraphComposite = new ParagraphComposite(paragraph, service);
        Pattern pattern = Pattern.compile(SENTENCE_REGEX);
        Matcher matcher = pattern.matcher(line);
        while (matcher.find()) {
            TextLeaf sentence = nextParse(matcher.group());
            paragraphComposite.addText(sentence);
        }
        if (paragraphComposite.getComponents().isEmpty()) {
            LOGGER.log(Level.WARN, NO_SENTENCES_IN_TEXT);
            return nextParse(line);
        }
        return paragraphComposite;
    }
}
