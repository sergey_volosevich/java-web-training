package by.training.sharing.dao;

import java.sql.SQLException;
import java.util.List;

public interface JDBCTemplate {

    int executeUpdate(String sql, Object[] args) throws SQLException;

    <T> T queryForObject(String sql, Object[] args, RowMapper<T> rowMapper) throws SQLException;

    <T> List<T> query(String sql, RowMapper<T> rowMapper) throws SQLException;

    <T> List<T> queryForList(String sql, Object[] args, RowMapper<T> rowMapper) throws SQLException;

    Number executeAndReturnKey(String sql, Object[] args) throws SQLException;
}
