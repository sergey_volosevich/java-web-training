package by.training.sharing.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.annotation.WebInitParam;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * This web filter sets request and response encodings.
 */

@WebFilter(filterName = "CharacterEncodingFilter", servletNames = "sharing", initParams = {
        @WebInitParam(name = "encoding", value = "UTF-8")})
public class CharacterEncodingFilter implements Filter {

    private static final Logger LOGGER = LogManager.getLogger(CharacterEncodingFilter.class);
    private static final String FILTER_INIT = "CharacterEncodingFilter successful initialize. Encoding is \'{}\'";
    private static final String FILTER_DESTROYED = "CharacterEncodingFilter successful destroyed.";
    private static final String CONTENT_TYPE = "text/html; charset=UTF-8";
    private static final String DEFAULT_ENCODING = "UTF-8";

    private String encoding;

    public void destroy() {
        LOGGER.info(FILTER_DESTROYED);
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest httpServletRequest = (HttpServletRequest) req;
        String requestEncoding = httpServletRequest.getCharacterEncoding();
        if (requestEncoding == null) {
            httpServletRequest.setCharacterEncoding(encoding);
        }
        resp.setContentType(CONTENT_TYPE);
        resp.setCharacterEncoding(encoding);
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {
        encoding = config.getInitParameter("encoding");
        if (encoding == null) {
            encoding = DEFAULT_ENCODING;
        }
        LOGGER.info(FILTER_INIT, encoding);
    }
}
