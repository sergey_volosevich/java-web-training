<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="bsh" tagdir="/WEB-INF/tags" %>
<%@taglib prefix="vr" uri="bikeSharing" %>
<%@ page import="by.training.sharing.bike.BikeFiledNameConstants" %>

<c:set var="bikePassword" value="${BikeFiledNameConstants.BIKE_PASSWORD}"/>

<div class="container text-dark">
    <div class="row mt-1 mb-3 align-items-center">
        <div class="col-md-10 mx-auto bg-light">
            <div class="panel panel-default" style="margin:20px">
                <div class="panel-heading">
                    <h3 class="panel-title text-center">Unlock Bike</h3>
                </div>
                <div class="panel-body">
                    <c:choose>
                        <c:when test="${not empty sessionScope.get('wallet')}">
                            <hr style="border: none; background-color: darkgrey; color: darkgrey; height: 1px">
                            <div class="text-center">
                                <p><fmt:message key="wallet.view.balance"/></p>
                                <h4 class="font-italic font-weight-bold">
                                    <c:out value="${sessionScope.get('wallet').currentAmount}"/> BYN
                                </h4>
                            </div>
                                <c:choose>
                                    <c:when test="${wallet.currentAmount gt 0.00}">
                                        <form action="<c:out value="${pageContext.request.contextPath}"/>"
                                              method="post">
                                            <input type="hidden" name="commandName" value="#"/>
                                            <input type="hidden" name="csrf"
                                                   value="<c:out value="${sessionScope.get('csrf')}"/>"/>
                                            <div class="form-group">
                                                <label for="bike-password" class="text-left">Input bike password</label>
                                                <input id="bike-password" type="password"
                                                       name="<c:out value="${bikePassword}"/>"
                                                       value="<c:out value="${param.get(bikePassword)}"/>"
                                                       class="form-control"/>
                                                <small class="form-text text-muted text-left">
                                                    password is under the bike seat
                                                </small>
                                                <p class="alert-danger">
                                                    invalid password
                                                </p>
                                            </div>
                                            <bsh:backButton value="chooseBikeView"/>
                                            <button class="btn btn-outline-dark btn-block">Unlock bike</button>
                                        </form>
                                    </c:when>
                                    <c:otherwise>
                                        <div class="text-center">
                                            To ride a bike you must fill the balance
                                                <a href="<bsh:command value="walletView"/>"
                                                   class="mdl-badge mdl-badge--overlap">
                                                    <fmt:message key="links.click.here"/>
                                                </a>
                                            <bsh:backButton value="chooseBikeView"/>
                                        </div>
                                    </c:otherwise>
                                </c:choose>
                        </c:when>
                        <c:otherwise>
                            <div class="text-center text-dark">
                                <fmt:message key="wallet.view.wallet.helper"/>
                                <a type="button" class="btn btn-outline-dark btn-block"
                                   href="<bsh:command value="walletView"/>">
                                    <fmt:message key="links.click.here"/>
                                </a>
                                <bsh:backButton value="welcomeView"/>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
            </div>
        </div>
    </div>
</div>