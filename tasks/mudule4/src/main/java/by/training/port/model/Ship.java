package by.training.port.model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.concurrent.TimeUnit;

public class Ship implements Runnable {

    private static final Logger LOGGER = LogManager.getLogger();
    private static final String SHIP_ID = "Ship-{} with task {}  arrives to the port.";
    private static final String SHIP_GET_DOCK = "Ship-{} get dock {}.";
    private static final String SHIP_READY = "Ship-{} with number of containers - {} is ready.";
    private static final String SHIP_RELEASE_DOCK = "Ship-{} release dock {}.";
    private static final String SHIP_OUT = "Ship-{} go out from port.";

    private long id;
    private TaskType taskType;
    private int maxCapacity;
    private int currentNumContainer;

    public Ship(long id, TaskType taskType, int maxCapacity, int currentNumContainer) {
        this.id = id;
        this.taskType = taskType;
        this.maxCapacity = maxCapacity;
        this.currentNumContainer = currentNumContainer;
    }

    @Override
    public void run() {
        LOGGER.info(SHIP_ID, id, taskType);
        try {
            TimeUnit.SECONDS.sleep(2);
        } catch (InterruptedException e) {
            LOGGER.error("Interrupted!", e);
            Thread.currentThread().interrupt();
        }
        Port port = Port.getInstance();
        Dock dock = port.getDock();
        LOGGER.info(SHIP_GET_DOCK, id, dock.getId());
        dock.doTask(this);
        LOGGER.info(SHIP_READY, id, currentNumContainer);
        LOGGER.info(SHIP_RELEASE_DOCK, id, dock.getId());
        port.releaseDock(dock);
        LOGGER.info(SHIP_OUT, id);
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public TaskType getTaskType() {
        return taskType;
    }

    public int getMaxCapacity() {
        return maxCapacity;
    }

    public void setMaxCapacity(int maxCapacity) {
        this.maxCapacity = maxCapacity;
    }

    public int getCurrentNumContainer() {
        return currentNumContainer;
    }

    public void setCurrentNumContainer(int currentNumContainer) {
        this.currentNumContainer = currentNumContainer;
    }
}
