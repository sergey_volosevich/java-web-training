package by.training.text.repository;

import by.training.text.entity.Paragraph;

public class ParagraphByTextIdSpecification implements Specification<Paragraph> {

    private long textId;

    public ParagraphByTextIdSpecification(long textId) {
        this.textId = textId;
    }

    @Override
    public boolean match(Paragraph entity) {
        return entity.getTextId() == textId;
    }
}
