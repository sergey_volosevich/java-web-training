package by.training.sharing.security;

import by.training.sharing.dao.DAOException;
import by.training.sharing.role.RoleDAOImpl;
import by.training.sharing.user.UserDAOImpl;
import by.training.sharing.user.UserDTO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpSession;
import java.util.Optional;

public class SecurityServiceImplTest {

    private SecurityServiceImpl securityService;

    @Before
    public void setUp() throws Exception {


    }

    @Test
    public void loginUser() throws DAOException, SecurityException {
        UserDAOImpl userDAO = Mockito.mock(UserDAOImpl.class);
        PasswordEncoder passwordEncoder = Mockito.mock(PasswordEncoder.class);
        RoleDAOImpl roleDAO = Mockito.mock(RoleDAOImpl.class);
        SecurityServiceImpl securityService = new SecurityServiceImpl(userDAO, passwordEncoder, roleDAO);
        UserDTO userDTO = new UserDTO();
        userDTO.setUserName("login");
        userDTO.setPassword("3894233qQ");
        Mockito.when(userDAO.findByLogin("login")).thenReturn(Optional.of(userDTO));
        Mockito.when(passwordEncoder.authenticate("3894233qQ".toCharArray(), "$93$16$JhiYMaWCYwX-Yd5w_RcmnhVsEFbNjbv04B72JICnTd0")).thenReturn(false);
        HttpSession httpSession = Mockito.mock(HttpSession.class);
        boolean isLoginUser = securityService.loginUser(httpSession, userDTO);
        Assert.assertFalse(isLoginUser);
    }

    @Test
    public void logoutUser() {
    }

    @Test
    public void getCurrentUser() {
    }

    @Test
    public void canExecute() {
    }
}