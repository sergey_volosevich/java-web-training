package by.training.tariff.parser;

import by.training.tariff.entity.*;
import by.training.tariff.validator.XMLErrorHandler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;

public class DOMTariffParser implements Parser<LightTariff> {

    private static final String NO_SUCH_FILED_IN_DOCUMENT = "No such filed in document - {1}";

    @Override
    public List<LightTariff> parse(String pathXML) throws ParserException {
        DocumentBuilderFactory builderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder;
        try {
            builderFactory.setFeature(XMLConstants.FEATURE_SECURE_PROCESSING, true);
            builder = builderFactory.newDocumentBuilder();
            builder.setErrorHandler(new XMLErrorHandler());
        } catch (ParserConfigurationException e) {
            throw new ParserException(e);
        }
        Document document;
        try {
            document = builder.parse(pathXML);
        } catch (SAXException | IOException e) {
            throw new ParserException(e);
        }
        List<LightTariff> tariffs = new ArrayList<>();
        Element root = document.getDocumentElement();
        NodeList nodeTariffs = root.getChildNodes();
        for (int i = 0; i < nodeTariffs.getLength(); i++) {
            Node node = nodeTariffs.item(i);
            if (node.getNodeName().equals(TariffFields.LIGHT_TARIFF)) {
                LightTariff tariff = new LightTariff();
                buildTariff(node, tariff);
                tariffs.add(tariff);
            } else if (node.getNodeName().equals(TariffFields.COMFORT_TARIFF)) {
                ComfortTariff tariff = new ComfortTariff();
                buildTariff(node, tariff);
                tariffs.add(tariff);
            } else if ((node.getNodeName().equals(TariffFields.BUSINESS_TARIFF))) {
                BusinessTariff tariff = new BusinessTariff();
                buildTariff(node, tariff);
                tariffs.add(tariff);
            }
        }
        return tariffs;
    }

    private void buildTariff(Node node, LightTariff tariff) throws ParserException {
        Element element = (Element) node;
        String idValue = element.getAttribute(TariffFields.ID);
        long id = Long.parseLong(idValue.substring(1));
        tariff.setId(id);
        if (element.hasAttribute(TariffFields.FREE_INTERNET_TRAFFIC)) {
            String internetTraffic = element.getAttribute(TariffFields.FREE_INTERNET_TRAFFIC);
            int freeInternetTraffic = Integer.parseInt(internetTraffic);
            tariff.setInternetTraffic(freeInternetTraffic);
        }
        NodeList list = node.getChildNodes();
        for (int i = 0; i < list.getLength(); i++) {
            Node field = list.item(i);
            if (field instanceof Element) {
                String value = field.getLastChild().getTextContent();
                String nodeName = field.getNodeName();
                switch (nodeName) {
                    case TariffFields.TARIFF_NAME:
                        tariff.setTariffName(value);
                        break;
                    case TariffFields.DATE_CREATION:
                        tariff.setDateCreation(value);
                        break;
                    case TariffFields.OPERATOR_NAME:
                        OperatorName operatorName = OperatorName.valueOf(value.toUpperCase());
                        tariff.setOperatorName(operatorName);
                        break;
                    case TariffFields.PAYROLL:
                        double payroll = Double.parseDouble(value);
                        tariff.setPayroll(payroll);
                        break;
                    case TariffFields.FREE_MINUTES_IN_NETWORK:
                        tariff.setFreeMinutesInNetwork(Integer.parseInt(value));
                        break;
                    case TariffFields.CALL_SMS_PRICES:
                        CallPrice callPrice = buildCallPrice(field);
                        tariff.setPrice(callPrice);
                        break;
                    case TariffFields.ADDITIONAL_INFO:
                        AdditionalInfo info = buildAdditionalInfo(field);
                        tariff.setAdditionalInfo(info);
                        break;
                    case TariffFields.FREE_MINUTES_OUT_NETWORK:
                        ((ComfortTariff) tariff).setFreeMinutesOutNetwork(Integer.parseInt(value));
                        break;
                    case TariffFields.FREE_MINUTES_INTERNATIONAL_CALL:
                        ((BusinessTariff) tariff).setFreeInternationalCall(Integer.parseInt(value));
                        break;
                    default:
                        throw new ParserException(MessageFormat.format(NO_SUCH_FILED_IN_DOCUMENT, nodeName));
                }
            }
        }
    }

    private AdditionalInfo buildAdditionalInfo(Node node) throws ParserException {
        AdditionalInfo info = new AdditionalInfo();
        NodeList nodes = node.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node field = nodes.item(i);
            if (field instanceof Element) {
                String value = field.getLastChild().getTextContent();
                String nodeName = field.getNodeName();
                switch (nodeName) {
                    case TariffFields.BILLING_TYPE:
                        info.setType(BillingType.valueOf(value.toUpperCase()));
                        break;
                    case TariffFields.FAVORITE_NUMBER:
                        info.setFavoriteNumbers(Integer.parseInt(value));
                        break;
                    case TariffFields.CONNECTION_PRICE:
                        info.setConnectionPrice(Double.parseDouble(value));
                        break;
                    default:
                        throw new ParserException(MessageFormat.format(NO_SUCH_FILED_IN_DOCUMENT, nodeName));
                }
            }
        }
        return info;
    }

    private CallPrice buildCallPrice(Node node) throws ParserException {
        CallPrice price = new CallPrice();
        NodeList nodes = node.getChildNodes();
        for (int i = 0; i < nodes.getLength(); i++) {
            Node field = nodes.item(i);
            if (field instanceof Element) {
                String value = field.getLastChild().getTextContent();
                String nodeName = field.getNodeName();
                switch (nodeName) {
                    case TariffFields.IN_NETWORK_CALL:
                        price.setInNetworkCall(Double.parseDouble(value));
                        break;
                    case TariffFields.OUT_NETWORK_CALL:
                        price.setOutNetworkCall(Double.parseDouble(value));
                        break;
                    case TariffFields.INTERNATIONAL_CALL:
                        price.setInternationalCall(Double.parseDouble(value));
                        break;
                    case TariffFields.SMS_PRICE:
                        price.setSmsPrice(Double.parseDouble(value));
                        break;
                    default:
                        throw new ParserException(MessageFormat.format(NO_SUCH_FILED_IN_DOCUMENT, nodeName));
                }
            }
        }
        return price;
    }
}
