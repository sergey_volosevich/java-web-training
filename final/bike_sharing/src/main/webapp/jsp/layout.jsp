<%@ page contentType="text/html;charset=UTF-8" language="java" pageEncoding="UTF-8" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="bsh" tagdir="/WEB-INF/tags" %>

    <main class="mdl-layout__content">
        <div class="page-content">
            <div class="mdl-grid ">
                <div class="mdl-cell mdl-cell--2-col ">
                </div>
                <div class="mdl-cell mdl-cell--8-col">
                    <c:choose>
                        <c:when test="${not empty viewName}">
                            <jsp:include page="views/${viewName}.jsp"/>
                        </c:when>
                        <c:when test="${not empty param.logout}">
                            <div class="container text-center text-light">
                                <h1><fmt:message key="logout.success"/></h1>
                                <a href="<c:out value="${pageContext.request.contextPath}"/>">
                                    <fmt:message key="links.back.main.page"/>
                                </a>
                            </div>
                        </c:when>
                        <c:otherwise>
                            <div class="mdl-grid">
                                <div class="mdl-cell mdl-cell--12-col">
                                    <div class="text-light bg-dark">
                                        <h1 class="text-center"><fmt:message key="welcome.message"/></h1>
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book.
                                        It has survived not only five centuries, but also the leap into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                        with
                                        the release of Letraset sheets containing Lorem Ipsum passages, and more
                                        recently
                                        with desktop publishing software like Aldus PageMaker including versions of
                                        Lorem
                                        Ipsum.
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book.
                                        It has survived not only five centuries, but also the leap into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                        with
                                        the release of Letraset sheets containing Lorem Ipsum passages, and more
                                        recently
                                        with desktop publishing software like Aldus PageMaker including versions of
                                        Lorem
                                        Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book.
                                        It has survived not only five centuries, but also the leap into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                        with
                                        the release of Letraset sheets containing Lorem Ipsum passages, and more
                                        recently
                                        with desktop publishing software like Aldus PageMaker including versions of
                                        Lorem
                                        Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book.
                                        It has survived not only five centuries, but also the leap into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                        with
                                        the release of Letraset sheets containing Lorem Ipsum passages, and more
                                        recently
                                        with desktop publishing software like Aldus PageMaker including versions of
                                        Lorem
                                        Ipsum.Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                        Lorem
                                        Ipsum has been the industry's standard dummy text ever since the 1500s, when an
                                        unknown printer took a galley of type and scrambled it to make a type specimen
                                        book.
                                        It has survived not only five centuries, but also the leap into electronic
                                        typesetting, remaining essentially unchanged. It was popularised in the 1960s
                                        with
                                        the release of Letraset sheets containing Lorem Ipsum passages, and more
                                        recently
                                        with desktop publishing software like Aldus PageMaker including versions of
                                        Lorem
                                        Ipsum.
                                    </div>
                                </div>
                            </div>
                        </c:otherwise>
                    </c:choose>
                </div>
                <div class="mdl-cell mdl-cell--2-col">
                </div>
            </div>
        </div>

    </main>
