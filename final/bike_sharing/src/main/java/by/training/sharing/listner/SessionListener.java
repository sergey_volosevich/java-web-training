package by.training.sharing.listner;

import by.training.sharing.core.ApplicationContext;
import by.training.sharing.core.BeanNameConstants;
import by.training.sharing.security.SecurityService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

@WebListener()
public class SessionListener implements HttpSessionListener {

    private static final Logger LOGGER = LogManager.getLogger(SessionListener.class);
    private static final String SESSION_WITH_ID_CREATED = "Session with id \'{}\' created";
    private static final String SESSION_WITH_ID_DESTROYED = "Session with id \'{}\' destroyed";

    public void sessionCreated(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        LOGGER.debug(SESSION_WITH_ID_CREATED, session.getId());
    }

    /**
     * When session destroyed, the user associated with it is logged out from security context.
     *
     * @param se the HttpSessionEvent containing the session
     */
    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        ApplicationContext applicationContext = ApplicationContext.getInstance();
        SecurityService securityService = applicationContext.getBean(BeanNameConstants.SECURITY_SERVICE);
        securityService.logoutUser(session);
        LOGGER.debug(SESSION_WITH_ID_DESTROYED, session.getId());
    }
}
