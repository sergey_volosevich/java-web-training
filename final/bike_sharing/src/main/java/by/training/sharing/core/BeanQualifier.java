package by.training.sharing.core;

import java.lang.annotation.*;

/**
 * This annotation used in constructor with parameter to tell {@link BeanRegistry} which bean needed to create and
 * substitute as a constructor parameter.
 */

@Documented
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.PARAMETER)
public @interface BeanQualifier {
    /**
     * Name of Bean, which used in annotation{@link Bean#name()}.
     *
     * @return name of bean, which need to inject in object.
     */
    String value();
}
