package by.training.sharing.price;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

public class PriceDTO implements Serializable {
    private static final long serialVersionUID = -4347127458723942636L;
    private long id;
    private LocalDate date;
    private BigDecimal priceValue;

    public PriceDTO() {
    }

    public PriceDTO(long id, LocalDate date, BigDecimal priceValue) {
        this.id = id;
        this.date = date;
        this.priceValue = priceValue;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public BigDecimal getPriceValue() {
        return priceValue;
    }

    public void setPriceValue(BigDecimal priceValue) {
        this.priceValue = priceValue;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof PriceDTO)) return false;
        PriceDTO priceDTO = (PriceDTO) o;
        return getId() == priceDTO.getId() &&
                Objects.equals(getDate(), priceDTO.getDate()) &&
                Objects.equals(getPriceValue(), priceDTO.getPriceValue());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getId(), getDate(), getPriceValue());
    }

    @Override
    public String toString() {
        return "PriceDTO{" +
                "id=" + id +
                ", date=" + date +
                ", priceValue=" + priceValue +
                '}';
    }
}
